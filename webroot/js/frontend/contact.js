(function () {
   
  // validate signup form on keyup and submit
  var validateSignUp = function(element) {

      $.validator.addMethod("passwordRegex", function (value, element) {
          return this.optional(element) || /^((?=.*\d)|(?=.*["!#$%&'()*+,\-:;.<=>?@{|}~^_`\[\]\\]))(?=.*[a-zA-Z]).+$/i.test(value);
      }, "Use at least 1 letter & number or symbol.");
    
      $(element).validate({
        rules: {         
             'name': {
              required: true,
            },

               'email': {
              required: true,
            },

               'phone': {
              required: true,
            },
           
        },

        messages: {
            'data[User][email]': {
              required: "Please enter a valid email address"
            },

            'data[User][lastname]': {
              required: "Please provide a password",
              minlength: "Your password must be at least 9 characters long"
            },  
        },

     errorPlacement: function(error, element) {
                if (element.attr("name") == "name" )
                    error.insertAfter($(element).closest('.form-group'));    
                else if (element.attr("name") == "email" )
                    error.insertAfter($(element).closest('.form-group')); 
                      else if (element.attr("name") == "phone" )
                    error.insertAfter($(element).closest('.form-group'));                  
                else
                    error.insertAfter(element);
            },

      });
    };

  $(document).ready(function() {
      // console.log('sdhas');
    validateSignUp("#signupform");
  });

})();


