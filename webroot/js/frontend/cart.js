$(document).ready(function(){
	$(".travellers").on("change",function() {
		$("input[name='travellers']").val($(this).val());
	});

    $('form[name="book_tour"]').on("submit", function(e){
        e.preventDefault();
		var tour_booking_date = $("#tour_booking_date").val();

		var err = false;
		$(".error-highlight").remove();
		if(tour_booking_date == '' || tour_booking_date == null || tour_booking_date == 0) {
			$("#tour_booking_date").closest(".input-group").after("<span class='error-highlight'>Required</span>");
			err = true;
		}

		var pickup_location = $("#pickup_location").val();		
		if(pickup_location == '' || pickup_location == null || pickup_location == 0) {
			$("#pickup_location").closest(".input-group").after("<span class='error-highlight'>Required</span>");
			err = true;
		}

		if(err == true) {
			return false;
		}

        var $this = $(this);
        $this.find("[type='submit']").prop('disabled', true);
        var $this_serialize = $this.serializeArray();
        $this_serialize.push({ name: "pick_up_location", value: pickup_location });
        
        $.post($this.attr('action'),$this_serialize,function(data){
        	var res = $.parseJSON(data);
            $('.cart-quantity').html(res.cart_count);
           
           	window.location.href = "http://bestshuttlecancun.com/basket";
        });
    });
	
	$(".removeItemFromCart").on("click",function(e) {
		e.preventDefault();
        var $this = $(this);
        $.post($this.attr('href'),{item_id:$(this).attr("data-item-id"), item_name:$(this).attr("data-item-name")},function(data){
        	var res = $.parseJSON(data);
			$('.cart-quantity').html(res.cart_count);
            $this.closest(".cart-list").remove();
			var itemName= $this.attr("data-item-name");
            var itemId= $this.attr("data-item-id");
			$(".checkout-cart-table tr[data-item-type='"+itemName+"'][data-item-id='"+itemId+"']").remove();
			var totalCartPrice = 0;
			$(".item-price-cart").each(function() {
				totalCartPrice+=parseFloat($(this).html());
			});
			if(totalCartPrice == 0) {
				$(".cart-total").remove();
			}
			$(".show-totalcart-price").html(totalCartPrice);
			
        });
	});

    $('form[name="book_shuttle"]').on("submit", function(e){
        e.preventDefault();
        var $this = $(this);
		
		var err = false;

		$(".error-highlight").remove();
        var travellers = $this.find("select[name='travellers']").val();

		if(travellers == '' || travellers == null || travellers == 0) {
			$this.find("select[name='travellers']").closest(".input-group").after("<span class='error-highlight'>Required</span>");
			err = true;
		}

		if(travellers == 10) {
			$("#shuttle-10-contact").modal("show");
			err = true;	
		}

		var drop_of_locations = $this.find("select[name='drop_of_locations']").val();
		if(drop_of_locations == '' || drop_of_locations == null || drop_of_locations == 0) {
			$this.find("select[name='drop_of_locations']").closest(".input-group").after("<span class='error-highlight'>Required</span>");
			err = true;
		}

		var shuttle_arrival_date = $this.find("input[name='shuttle_arrival_date']").val();
		if(shuttle_arrival_date == '' || shuttle_arrival_date == null || shuttle_arrival_date == 0) {
			$this.find("input[name='shuttle_arrival_date']").closest(".input-group").after("<span class='error-highlight'>Required</span>");
			err = true;
		}

		if($this.find("input[name='is_round_trip']").is(":checked")) {
			var shuttle_departure_date = $this.find("input[name='shuttle_departure_date']").val();
			if(shuttle_departure_date == '' || shuttle_departure_date == null || shuttle_departure_date == 0) {
				$this.find("input[name='shuttle_departure_date']").closest(".input-group").after("<span class='error-highlight'>Required</span>");
				err = true;
			}
		}

		if($this.find("input[name='is_airport_pickup']").is(":checked")) {
			//no pickup location validation
		}else{
			var pickup_location = $this.find("input[name='pickup_location']").val();
			if(pickup_location == '' || pickup_location == null || pickup_location == 0 || pickup_location.length < 3) {
				$this.find("input[name='pickup_location']").closest(".input-group").after("<span class='error-highlight'>Required</span>");
				err = true;
			}
		}

		if(err == true) {
			return false;
		}

        $this.find("[type='submit']").prop('disabled', true);
        $.post($this.attr('action'),$this.serialize(),function(data){
        	
            var res = $.parseJSON(data);
			$this.find(".error-highlight").remove();
			if(res.code == 0) {
				$.each(res.error, function(i, obj) {
					if(obj.field == 'travellers' && obj.message == 'show_modal') {
						$("#shuttle-10-contact").modal("show");						
					}else{
						$this.find(":input[name='"+obj.field+"']").closest(".input-group").after("<span class='error-highlight'>Required</span>");
					}
				});
				$this.find("[type='submit']").prop('disabled', false);
			}else{				
				$('.cart-quantity').html(res.cart_count);
				window.location.href = "http://bestshuttlecancun.com/basket";
			}
        });
    });
});
