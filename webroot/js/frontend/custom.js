    new WOW().init();

    $(function() {
        var $popover = $('.popover-markup>.trigger').popover({
            html: true,
            placement: 'bottom',
            // trigger:'focus',
            // title: '<?= lang("Select passengers");?><a class="close demise");">×</a>',
            content: function() {
                return $(this).parent().find('.content').html();
            }
        });

        // open popover & inital value in form
        var passengers = [1, 0, 0];
        $('.popover-markup>.trigger').click(function(e) {
            e.stopPropagation();
            $(".popover-content input").each(function(i) {
                $(this).val(passengers[i]);
            });
        });

        // close popover
        $(document).click(function(e) {
            if ($(e.target).is('.demise')) {
                $('.popover-markup>.trigger').popover('hide');
            }
        });

        // store form value when popover closed
        $popover.on('hide.bs.popover', function() {
            $(".popover-content input").each(function(i) {
                passengers[i] = $(this).val();
            });
        });

        // spinner(+-btn to change value) & total to parent input 
        $(document).on('click', '.number-spinner a', function() {
            var btn = $(this),
                input = btn.closest('.number-spinner').find('input'),

                total = $('.passengers').val(),
                oldValue = input.val().trim();

            if (btn.attr('data-dir') == 'up') {
                if (oldValue < input.attr('max')) {
                    oldValue++;
                    total++;
                }
            } else {
                if (oldValue > input.attr('min')) {
                    oldValue--;
                    total--;
                }
            }
            
            input.val(oldValue);

            adult = $(this).closest('.popover-markup').find('#adult').val();
            child = $(this).closest('.popover-markup').find('#child').val();
   
            $("input[name='adults']").val(adult);
            $("input[name='childs']").val(child);
            
            //Count actual price of Adult
            $("#num-adults").html(adult);
            $("#total-adults-price").html(parseFloat(adult)*parseFloat($("#adults-price").html()));
            
            //Count actual price of Child
            $("#num-childs").html(child);
            $("#total-childs-price").html(parseFloat(child)*parseFloat($("#childs-price").html()));
            
            //Count final total price of Adult and Child
            $(".final-total-price").html(parseFloat($("#total-adults-price").html())+parseFloat($("#total-childs-price").html()));
            
            var totalPromoPrice = parseFloat($("#adults-price").attr("promo-price"))*adult + parseFloat($("#childs-price").attr("promo-price"))*child;

            $(".final-total-promo-price").html(totalPromoPrice);
            
            $("form[name='book_tour'] input[name='adults']").val(adult);
            $("form[name='book_tour'] input[name='childs']").val(child);
            
            $('.passengers').val(adult + ' Adults ' + child + ' child');

        });

        $(document).on('click','#is_round_trip',function () {
            if ($(this).is(':checked')) {
                 $('.shuttle_departure_date_block').removeClass('hide');
            }else {
                $('.shuttle_departure_date_block').addClass('hide');
            }
        });


        $(document).on('click', function(e) {
          $('[data-toggle="popover"],[data-original-title]').each(function() {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
              $(this).popover('hide').data('bs.popover').inState.click = false // fix for BS 3.3.6
            }

          });
        });

    });

    (function() {

        // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
        if (!String.prototype.trim) {
            (function() {
                // Make sure we trim BOM and NBSP
                var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                String.prototype.trim = function() {
                    return this.replace(rtrim, '');
                };
            })();
        }

        [].slice.call(document.querySelectorAll('input.input__field')).forEach(function(inputEl) {
            // in case the input is already filled..
            if (inputEl.value.trim() !== '') {
                classie.add(inputEl.parentNode, 'input--filled');
            }

            // events:
            inputEl.addEventListener('focus', onInputFocus);
            inputEl.addEventListener('blur', onInputBlur);
        });

        function onInputFocus(ev) {
            classie.add(ev.target.parentNode, 'input--filled');
        }

        function onInputBlur(ev) {
            if (ev.target.value.trim() === '') {
                classie.remove(ev.target.parentNode, 'input--filled');
            }

        }
    })();

    