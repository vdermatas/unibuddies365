

<section class="about_us d-flex justify-content-center align-items-center">

    <div class="row">
        <div class="col-12">
            <h1 class="text-white"><?php echo $pages->title_en; ?></h1>
        </div>
    </div>

</section>
<div class="container">
    
        <div class="" id="about_txt">
             <?php echo $pages->content_en; ?>
        </div>
    
   
</div>

<style type="text/css">
    /*--===================about_us========-------*/
.about_us{

    background-size: cover; background:url(<?php echo $this->Url->build('/uploads/images/'.$pages->bg_image); ?>)bottom center no-repeat;
    position: relative;
    height: 60vh;
    padding-top: 100px;
    padding-bottom: 100px;}

    .about_social a{background: #1c3e71;
        border-radius: 3px; padding: 10px; margin: 5px;
    }

    
/*.Services_bg{background-size: cover; background-image:url(../images/login.png);
    background-position: 50% 70%;
    position: relative;
    height: 100vh;
    padding-top: 100px;
    padding-bottom: 100px;}*/
        .Services-wraper {padding: 20px; color: #fff}
        .Services_tag a{display: block; font-size: 24px; text-decoration: none; color: #1c3e70; padding-bottom: 10px;}
         .Services_tag a:hover{color: #ffc107;}
        .Services_tag .btn{border:solid #ffc107;}
        .Services_tag .btn:hover{background: #fff; border:solid #f9a63a;}
        #about_txt{ line-height: 24px;}
         #about_txt strong{padding-right: 10px;}
        #about_txt .text-center{text-align: justify!important;}
</style>