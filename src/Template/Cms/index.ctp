<?php  
use Cake\Http\Session;
 $session = New Session(); ?>
<section class="home-bg d-flex text-center justify-content-center align-items-center">
    <div class="top-banner-text ">
        <div class="main-heading">
            <?php echo $bannerSection->title; ?>
        </div>
        <!-- <ul class="list-unstyled">
            <li><img src="images/tick.png"> 100% Success</li>
            <li><img src="images/tick.png"> 100% Money back guarantee</li>
            <li><img src="images/tick.png"> 100% Confidentiality</li>
            <li><img src="images/tick.png"> 0% Plagiarism</li>
        </ul> -->

        <?php echo $bannerSection['content_' . $session->read('Config.language')]?>
        
        <div class="row text-center justify-content-center button-margin">
            <div class="col-sm-4 pl-0"><button class="btn btn-block btn-orange"><?php echo $bannerSection->writer_button; ?></button></div>
            <div class="col-sm-3 pr-0"><button class="btn btn-orange-border btn-block"><?php echo $bannerSection->student_button; ?></button></div>
        </div>
    </div>
</section>

<section>
    <div class="row ml-0 mr-0">
        <div class="col-sm-12 col-md-12 col-lg-6 why-choose-us-left-section">
            <div class="row mb-4">
                <div class="col-sm-6">
                    <img src="<?php echo $this->Url->build('/'.$whychooseUs->section_1_image); ?>">
                    <div class="sub-title"><?php echo $whychooseUs->section_1_title; ?></div>
                    <p><?php echo $whychooseUs->section_1_content; ?></p>
                </div>
                <div class="col-sm-6">
                    <img src="<?php echo $this->Url->build('/'.$whychooseUs->section_2_image); ?>">
                    <div class="sub-title"><?php echo $whychooseUs->section_2_title; ?></div>
                    <p><?php echo $whychooseUs->section_2_content; ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <img src="<?php echo $this->Url->build('/'.$whychooseUs->section_3_image); ?>">
                    <div class="sub-title"><?php echo $whychooseUs->section_3_title; ?></div>
                    <p><?php echo $whychooseUs->section_3_content; ?></p>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-6 pr-0 position-relative ">
            <div class="choose-us-bg">
                <div class="text-choose-us">
                   <?php echo $whychooseUs->title; ?>
                </div>
            </div>
            <img class="w-100 h-100" src="<?php echo $this->Url->build('/'.$whychooseUs->background_image); ?>">
        </div>
    </div>
</section>

<section class="who-we-are">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <img src="<?php echo $this->Url->build('/'.$about->background_image); ?>">
            </div>
            <div class="col-sm-6 who-we-are-right">
                <?php echo $about->title; ?>
                <p class="pl-2 mt-2"><?php echo $about->description; ?></p>
                <a class="btn btn-blue mt-3 ml-2 " href="<?php echo $this->Url->build('/users/login'); ?>">Know More</a>
            </div>
        </div>
    </div>
</section>

<section class="service-we-provide">
    <div class="container-fluid">
        <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-5 col-lg-5 col-md-12 service-we-provide-left">
                <?php echo $service->title; ?>
                <p> <?php echo $service->description; ?> </p>
                <button class="btn btn-dark-blue mt-3  ">All Services</button>
            </div>
            <div class="col-sm-7 col-lg-7 col-md-12 service-we-provide-right">
                <div class="row mb-4">
                    <div class="col-sm-6">
                        <img src="<?php echo $this->Url->build('/'.$service->section_1_image); ?>">
                        <div class="sub-title"><?php echo $service->section_1_title; ?> </div>
                        <p><?php echo $service->section_1_content; ?> 
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <img src="<?php echo $this->Url->build('/'.$service->section_2_image); ?>">
                        <div class="sub-title"><?php echo $service->section_2_title; ?> </div>
                        <p><?php echo $service->section_2_content; ?> 
                        </p>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-sm-6">
                        <img src="<?php echo $this->Url->build('/'.$service->section_3_image); ?>">
                        <div class="sub-title"><?php echo $service->section_3_title; ?> </div>
                        <p><?php echo $service->section_3_content; ?> 
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <img src="<?php echo $this->Url->build('/'.$service->section_4_image); ?>">
                        <div class="sub-title"><?php echo $service->section_4_title; ?> </div>
                        <p><?php echo $service->section_4_content; ?> 
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>

<section class="blue-bg-bottom">
    <div class="container">
        <div class="row text-center">
            <div class="col-sm-4 col-4">
                <img class="mb-2" src="images/student.png">
                <div class="big-font-white"><?php echo count($student); ?></div>
                <div class="white-text"><?php echo __('STUDENTS'); ?></div>
            </div>
            <div class="col-sm-4 col-4">
                <img class="mb-2" src="images/writers.png">
                <div class="big-font-white"><?php echo count($writer); ?></div>
                <div class="white-text"><?php echo __('Writers'); ?></div>
            </div>
            <div class="col-sm-4 col-4">
                <img class="mb-2" src="images/completed-project.png">
                <div class="big-font-white"><?php echo count($assignments); ?></div>
                <div class="white-text">Completed projects</div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container expertise text-center">
        <h1>            
            <?php echo __('Find a writer and get your work done with'); ?>
        </h1>
        <div class="expertise-title"><?php echo __('expertise'); ?></div>
        <div class="col-sm-2 m-auto">
            <div class="input-group input-group-lg mb-3">
                <!-- <select class="custom-select form-control" id="inputGroupSelect02">
                    <option selected>Select type</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </select> -->
                <div class="input-group-append">
                    <a href="<?php echo $this->Url->build('/users/login'); ?>" class="input-group-text" for="inputGroupSelect02"><?php echo __('Find Now'); ?></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="words-from-client">
    <div class="row ml-0 mr-0">
        <div class="col-sm-6 pl-0">
            <div class="words-from-client-bg">
                <div class="client-text text-right">
                    <div class="us">Words From</div>
                    <div class="bold-text">Client</div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 d-flex text-center justify-content-center align-items-center">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active px-4">
                        <div>
                            <img class="rounded-circle" src="images/testimonial-img.png">
                        </div>
                        <div class="text-testimonial">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco</p>
                            <div class="name">Andrew Watt</div>
                            <div class="position"><?php echo __('Student'); ?></div>
                        </div>
                    </div>
                    <div class="carousel-item  px-4">
                        <div>
                            <img class="rounded-circle" src="images/testimonial-img.png">
                        </div>
                        <div class="text-testimonial">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco</p>
                            <div class="name">Andrew Watt</div>
                            <div class="position"><?php echo __('Student'); ?></div>
                        </div>
                    </div>
                    <div class="carousel-item  px-4">
                        <div>
                            <img class="rounded-circle" src="images/testimonial-img.png">
                        </div>
                        <div class="text-testimonial">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco</p>
                            <div class="name">Andrew Watt</div>
                            <div class="position"><?php echo __('Student'); ?></div>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>

<style type="text/css">
.home-bg{
    background: url('<?php echo $this->Url->build('/'.$bannerSection->background_image); ?>')bottom center no-repeat;
}
</style>