<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Cancun</title>
        
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,700i,800" rel="stylesheet">
		
		<?php echo $this->Html->css('frontend/bootstrap'); ?>
        <?php echo $this->Html->css('frontend/home'); ?>
        <?php echo $this->Html->css('frontend/tour-detail'); ?>
        <?php echo $this->Html->css('frontend/animate'); ?>
        <?php echo $this->Html->css('frontend/owl.transitions.min'); ?>
        <?php echo $this->Html->css('frontend/owl.theme.min'); ?>
        <?php echo $this->Html->css('frontend/owl.carousel.min'); ?>
        <?php echo $this->Html->css('frontend/owl.carousel.min'); ?>
        <?php echo $this->Html->css('frontend/font-awesome'); ?>
        <?php echo $this->Html->css('frontend/set2'); ?>
		
    </head>
    <body>
        <div class="container-fluid">
            <nav class="navbar navbar-default navbar-fixed-top  nav-top  clearfix ">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">
                         <?php echo $this->Html->image('frontend/logo.png', ['alt' => 'logo', 'class' => 'img-responsive']); ?>
                        </a>
                    </div>
                    <div id="navbar" class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right ">
                            <li class="">
                                <a href="#">Home</a>
                            </li>
                            <li class="">
                                <a href="#">About Us</a>
                            </li>
                            <li class="">
                                <a href="#">Tours</a>
                            </li>
                            <li class="">
                                <a href="#">Transportation</a>
                            </li>
                            <li class="">
                                <a href="#">Contact Us</a>
                            </li>
                            <li class="svg-width">
                                <?php
                                    echo $this->Html->image("frontend/united-kingdom.svg", [
                                        "alt" => "uk",
                                        'url' => ['action' => '#']
                                    ]);
                                ?>
                                <!--<a href="#"><img alt="uk" src="img/united-kingdom.svg"></a>-->
                            </li>
                            <li class="svg-width">
                                 <?php
                                    echo $this->Html->image("frontend/mexico.svg", [
                                        "alt" => "mexixo",
                                        'url' => ['action' => '#']
                                    ]);
                                ?>
                                 <!--<a href="#"><img alt="mexixo" src="img/mexico.svg"></a>-->
                            </li>
                            <li class="">
                                <a href="#"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="badge">4</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        <div class="container tour-details">
            <div class="col-sm-12 tour-header">
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <span class="tour-name">Cozumel Plus</span> <span class="tour-rating">4.8/5</span><span class="star-rating"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></span>
                    </div>
                    <div class="col-sm-6 col-xs-12 text-right">
                        <div class="tour-price">$66.00 – $625.00</div>
                    </div>
                </div>
            </div>
        </div>
        <div id="owl-demo" class="owl-carousel owl-theme">
            <?php
				foreach($tour['gallery']['images'] as $k => $v) {
					?>
						<div class="item"><?php echo $this->Html->image($v['url'], ["alt" => "img"]); ?></div>
					<?php
				}
			?>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#description" aria-controls="description" role="tab" data-toggle="tab">Description</a></li>
                        <li role="presentation"><a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab">Rating and Reviews</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active " id="description">
                            <div class="description">
                                <h3>Description</h3>
                                <?php
									echo $tour['description'];
								?>
                            </div>
                            <div class="amazing-tour clearfix">
                                <h3>Do not miss this amazing tour that includes</h3>
                                <div class="col-sm-7">
                                    <?php
										echo $tour['facilities'];
									?>
                                </div>
                                
                            </div>
                            <div class="amazing-tour clearfix">
                                <h3>Recommendations</h3>
                                <div class="col-sm-7">
                                    <?php
										echo $tour['recommendations'];
									?>
                                </div>
                                
                            </div>
                            <div class="amazing-tour clearfix">
                                <h3>Not Include</h3>
                                <div class="col-sm-12">
                                     <?php
										echo $tour['no_of_children'];
									?>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="reviews">
                            <div class="reviews col-lg-12 ">
                                <div class="row review-content">
                                    <div class="col-lg-1">
                                       
										<?php echo $this->Html->image('frontend/small-img.png', ['alt' => 'logo', 'class' => 'img-responsive']); ?>
                                    </div>
                                    <div class="col-lg-8 padding-zero">
                                        <div class="reviewer-name"> Ron Viseli </div>
                                        <div class="date">july 05 2017</div>
                                    </div>
                                    <div class="col-lg-3 text-right">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="review-border-bottom clearfix">
                                            <p> This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</p>
                                            <div class=" text-right">
                                                <span class="btn-thumb">
                                                <a herf="#">
                                                <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                                <span>6</span>
                                                </a>
                                                </span>
                                                <span class="btn-thumb-down">
                                                <a href="#">
                                                <i class="fa fa-thumbs-down" aria-hidden="true"></i>
                                                <span>1</span>
                                                </a>
                                                </span>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row review-content">
                                    <div class="col-lg-1">
                                        
										<?php echo $this->Html->image('frontend/small-img.png', ['alt' => 'logo', 'class' => 'img-responsive']); ?>
										
                                    </div>
                                    <div class="col-lg-8 padding-zero">
                                        <div class="reviewer-name"> Ron Viseli </div>
                                        <div class="date">july 05 2017</div>
                                    </div>
                                    <div class="col-lg-3 text-right">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="review-border-bottom clearfix">
                                            <p> This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</p>
                                            <div class=" text-right">
                                                <span class="btn-thumb">
                                                <a herf="#">
                                                <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                                <span>26</span>
                                                </a>
                                                </span>
                                                <span class="btn-thumb-down">
                                                <a href="#">
                                                <i class="fa fa-thumbs-down" aria-hidden="true"></i>
                                                <span>2</span>
                                                </a>
                                                </span>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 book-now">
                    <div class=" form-group">
                        <div class="popover-markup">
                            <div class="trigger form-group form-group-lg form-group-icon-left ">
                                <!-- <i class="fa fa-users input-icon input-icon-highlight"></i> -->
                                <!--   <label>Passenger</label> -->
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-users" aria-hidden="true"></i></span>
                                    <input type="text" name="passengers" value="1"  id="passengers" class="form-control input-lg guest-input" placeholder="Guest" aria-describedby="basic-addon1">
                                </div>
                                <!--  <input type="number" name="passengers" placeholder="Guest" id="passengers" class="form-control" value="1"> -->
                            </div>
                            <div class="content hide">
                                <div class="form-group">
                                    <label class="control-label col-md-5"><strong>Adult</strong></label>
                                    <div class="input-group number-spinner col-md-7">
                                        <span class="input-group-btn">
                                        <a class="btn btn-danger" data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></a>
                                        </span>
                                        <input type="text" disabled name="adult" id="adult" class="form-control text-center" value="1" max=9 min=1>
                                        <span class="input-group-btn">
                                        <a class="btn btn-info" data-dir="up"><span class="glyphicon glyphicon-plus"></span></a>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-5"><strong>Child</strong></label>
                                    <div class="input-group number-spinner col-md-7">
                                        <span class="input-group-btn">
                                        <a class="btn btn-danger" data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></a>
                                        </span>
                                        <input type="text" disabled name="child" id="child" class="form-control text-center" value="0" max=9 min=0>
                                        <span class="input-group-btn">
                                        <a class="btn btn-info" data-dir="up"><span class="glyphicon glyphicon-plus"></span></a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></span>
                            <span class="input input--nao">
                            <input class="input__field input__field--nao"  type="text">
                            <label class="input__label input__label--nao" for="input-1">
                            <span class="input__label-content input__label-content--nao">Date</span>
                            </label>
                            </span>
                        </div>
                    </div>
                    <div class="row price-text-tour">
                        <div class="col-sm-8">Adults   2x $66  </div>
                        <div class="col-sm-4 text-right">$172:00</div>
                    </div>
                    <div class="row price-text-tour">
                        <div class="col-sm-8">Children   2x $35  </div>
                        <div class="col-sm-4 text-right">$70:00</div>
                    </div>
                    <div class="form-group text-center">
                        <span class="strike-through">$242</span><span class="total-price">$200</span>
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-see-green">Add to cart</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="container you-may-like">
            <h2 class="text-center">You May Also like</h2>
            <div class="col-sm-3 text-center">
                <div class="box-white-like">
					
					<?php echo $this->Html->image('frontend/tours-img.jpeg', ['alt' => 'tours', 'class' => 'img-responsive']); ?>
					
                    <div class="tour-name">Cozumel Plus</div>
                    <p>A fun experience that will take you to know Playa del Carmen and Tulum.</p>
                    <div class="price-tour">$92.00 - $690.00</div>
                    <div class="row">
                        <div class="col-sm-6 star-icon"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></div>
                        <div class="col-sm-6"><button class="btn btn-see-green">Add to cart</button></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 text-center">
                <div class="box-white-like">
                   
					<?php echo $this->Html->image('frontend/tours-img.jpeg', ['alt' => 'tours', 'class' => 'img-responsive']); ?>
					
					
                    <div class="tour-name">Cozumel Plus</div>
                    <p>A fun experience that will take you to know Playa del Carmen and Tulum.</p>
                    <div class="price-tour">$92.00 - $690.00</div>
                    <div class="row">
                        <div class="col-sm-6 star-icon"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></div>
                        <div class="col-sm-6"><button class="btn btn-see-green">Add to cart</button></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 text-center">
                <div class="box-white-like">
                   		<?php echo $this->Html->image('frontend/tours-img.jpeg', ['alt' => 'tours', 'class' => 'img-responsive']); ?>
					
                    <div class="tour-name">Cozumel Plus</div>
                    <p>A fun experience that will take you to know Playa del Carmen and Tulum.</p>
                    <div class="price-tour">$92.00 - $690.00</div>
                    <div class="row">
                        <div class="col-sm-6 star-icon"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></div>
                        <div class="col-sm-6"><button class="btn btn-see-green">Add to cart</button></div>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <div class="container">
                <div class="col-sm-4">
                    <h3>Site map</h3>
                    <ul class="list-unstyled">
                        <li><a href="#">About us</a></li>
                        <li><a href="#">Tours</a></li>
                        <li><a href="#">Transporation</a></li>
                        <li><a href="#">Contact us</a></li>
                        <li><a href="#">Term</a></li>
                        <li><a href="#">Child seat policy</a></li>
                        <li><a href="#">Animal seat policy</a></li>
                    </ul>
                </div>
                <div class="col-sm-4">
                    <h3>Quick Links</h3>
                    <ul class="list-unstyled">
                        <li><a href="#">Term</a></li>
                        <li><a href="#">Child seat policy</a></li>
                        <li><a href="#">Animal seat policy</a></li>
                        <li><a href="#">Cancellation policy</a></li>
                    </ul>
                </div>
                <div class="col-sm-4">
                    <h3>Follow us on</h3>
                    <ul class="list-inline">
                        <li><a href="#">		<?php echo $this->Html->image('frontend/fb.png', ['alt' => 'logo', 'class' => 'img-responsive']); ?>
					</a></li>
                        <li><a href="#"><?php echo $this->Html->image('frontend/instagram.png', ['alt' => 'logo', 'class' => 'img-responsive']); ?>
					</a></li>
                    </ul>
                </div>
            </div>
        </footer>
        <div class="copyright-text text-center">
            <p>Copyright © 2017- 2018 Diseñada por Plannifycancun - <a href="#">Privacy Policy</a></p>
            <p>Asistace in Cancun tel: <strong>+5219987349473</strong> - <a href="#">Cancellation Policy</a></p>
        </div>
        <!-- Bootstrap core JavaScript
            ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <?php echo $this->Html->script('frontend/jQuery-2.1.4.min'); ?>
        <?php echo $this->Html->script('frontend/wow'); ?>
        <?php echo $this->Html->script('frontend/moment'); ?>
        <?php echo $this->Html->script('frontend/bootstrap'); ?>
        <?php echo $this->Html->script('frontend/classie'); ?>
        <?php echo $this->Html->script('frontend/owl.carousel.min'); ?>
        <?php echo $this->Html->script('frontend/custom'); ?>
		
        <script>
            $(document).ready(function () {
                $("#owl-demo").owlCarousel({
                    navigation: true,
                    navigationText: ['&lsaquo;','&rsaquo;'],
                    autoPlay:3000,
                    // autoplayTimeout:1000,
                    // autoplayHoverPause:true,
                    singleItem: true,
                    afterInit: makePages,
                    afterUpdate: makePages
                });
                function makePages() {
                    $.each(this.owl.userItems, function(i) {
                    $('.owl-controls .owl-page').eq(i)
                    .css({
                    'background': 'url(' + $(this).find('img').attr('src') + ')',
                    'background-size': 'cover'
                    })
                    });
                }
            
            
            });
        </script>
    </body>
</html>