<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Article[]|\Cake\Collection\CollectionInterface $articles
  */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= __('Home Page Sections') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Home Page Sections') ?></a>
        </li>
        <li class="active"> <?= __('All Home Page Sections') ?></li>
    </ol>
</section>
<!-- Main content -->

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?= __('Home Page Sections') ?></h3>

                    <?php echo $this->element("search_form"); ?>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover table-striped">
                        <thead>
            <tr>
                <th scope="col">Sr.</th>
                <th scope="col"><?= $this->Paginator->sort('title') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                
                <td>Banner Section</td>
                <td class="actions">
                    <a class="btn btn-primary"  href="<?php echo $this->Url->build('/admin/homepage-banner-sections'); ?>">View</a>
                </td>
            </tr>
            <tr>
                <td>2</td>
                
                <td>Why Choose Us Section</td>
                <td class="actions">
                    <a class="btn btn-primary"  href="<?php echo $this->Url->build('/admin/homepage-sections/why-choose-us'); ?>">View</a>
                </td>
            </tr>
            <tr>
                <td>3</td>
                
                <td>About Section</td>
                <td class="actions">
                    <a class="btn btn-primary"  href="<?php echo $this->Url->build('/admin/homepage-sections/about'); ?>">View</a>
                </td>
            </tr>
            <tr>
                <td>4</td>
                
                <td>Service Section</td>
                <td class="actions">
                    <a class="btn btn-primary"  href="<?php echo $this->Url->build('/admin/homepage-sections/services'); ?>">View</a>
                </td>
            </tr>
        </tbody>
    </table>
     </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
