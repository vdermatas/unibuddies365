<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Article[]|\Cake\Collection\CollectionInterface $galleries
  */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= __('Galleries') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Galleries') ?></a>
        </li>
        <li class="active"> <?= __('All Galleries') ?></li>
    </ol>
</section>
<!-- Main content -->

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?= __('Galleries') ?></h3>

                    <?php echo $this->element("search_form"); ?>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover table-striped">
                        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col">Images</th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php  $i = 1 ; foreach ($galleries as $gallery): ?>
            <tr>
                <td><?= $i ?></td>
                <td><?= h($gallery->name) ?></td>
                <td><a href="#" class='view_images' data-gid='<?= $gallery->id ?>'>View</a></td>
                <td><?= h($gallery->created) ?></td>
                <td><?= h($gallery->modified) ?></td>
                <td class="actions">
                    
                   <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-pencil-square-o')).'', array('action' => 'edit', $gallery->id), array('escape' => false)) ?> | 
                    <?= $this->Form->postLink(__('<i class="fa fa-trash-o"></i>'),['action' => 'delete',$gallery->id    ],['escape' => false,   'confirm' => __('Are you sure you want to delete {0}?', $gallery->name)])
                    ?>
                </td>
            </tr>
             <?php $i++; endforeach; ?>
        </tbody>
    </table>
     </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">

                        <li><?=$this->Paginator->first('<< ' . __('first ')) ?></li>
                        <li><?=$this->Paginator->prev('< ' . __('previous ')) ?>
                        <li><?=$this->Paginator->numbers() ?></li>
                        <li><?=$this->Paginator->next(__('next ') . '>') ?></li>
                        <li><?=$this->Paginator->last(__('last') . ' >>') ?></li>
                    </ul>
                    <p><?=$this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->

<?php echo $this->element('image_upload'); ?>

