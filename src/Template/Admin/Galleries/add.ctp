<?php
/**
  * @var \App\View\AppView $this
  */
?>
<section class="content-header">
    <h1>
        <?= __('Galleries') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Galleries') ?></a>
        </li>
        <li class="active"> <?= __('Add Gallery') ?></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                         <h3 class="box-title">Add Gallery</h3>
                </div>
                <div class="galleries form large-9 medium-8 columns content">
                <?= $this->Form->create($gallery, ['id'=> 'addgallery']) ?>
                <fieldset>
                    <?php
                        echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('name' , ['id'=> 'name', 'class'=> 'NoWhiteSpaceAtBeginn', 'maxlength'=>"70"])."</div></div>";
                    ?>
                </fieldset>
                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</section>


<script>
$(document).ready(function() {
     $.validator.addMethod("spaceSpecialCharAllowed", function(value, element) {
        return this.optional(element) || /^[^-\s][a-zA-Z0-9&-\s-]+[\S]$/gm.test(value);
    }, "Space at beginning/end and special characters are not allowed.");

    $('#addgallery').validate({
        rules:{
            name:{
                required: true,
                minlength: 3,
                maxlength: 70,
                spaceSpecialCharAllowed: true,
            }
        },
        messages:{
            name:{
                required:"Please enter name",
                maxlength:"Maximum 70 characters are allowed"
            } 
        }
    });
});
</script>