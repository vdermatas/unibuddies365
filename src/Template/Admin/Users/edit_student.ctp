<?php
/**
  * @var \App\View\AppView $this
  */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= __('Student') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Student') ?></a>
        </li>
        <li class="active"> <?= __('Edit Student') ?></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                         <h3 class="box-title">Edit Student</h3>
                </div>
                    <!-- Main content -->
               
                <div class="editUser form large-9 medium-8 columns content">
                    <?= $this->Form->create($user, ['id' => 'editUser']) ?>
                    <fieldset>
                        <?php

                            echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('first_name', ['id' => 'first_name', 'required' => true, 'label' => 'First Name'])."</div><div class='col-sm-6'>";

                            echo $this->Form->control('last_name', ['id' => 'last_name', 'required' => true, 'label' => 'Last Name'])."</div></div>";

                            echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('mobile', ['id' => 'mobile', 'required' => true, 'label' => 'Mobile Number'])."</div><div class='col-sm-6'>";

                            echo $this->Form->control('address', ['id' => 'address', 'required' => true, 'label' => 'Address'])."</div></div>";

                            echo "<div class='row'><div class='col-sm-4'>".$this->Form->control('city', ['id' => 'state', 'required' => true, 'label' => 'City'])."</div><div class='col-sm-4'>";

                            echo $this->Form->control('state', ['id' => 'state', 'required' => true, 'label' => 'State'])."</div><div class='col-sm-4'>"; 

                             echo $this->Form->control('zipcode', ['id' => 'zipcode', 'required' => true, 'label' => 'Zipcode'])."</div></div>";

                            echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('username', ['id' => 'username', 'required' => true, 'label' => 'Username '])."</div><div class='col-sm-6'>";

                            //echo $this->Form->control('password', ['id' => 'password', 'required' => true, 'label' => 'Password'])."</div></div>";

                            echo $this->Form->control('email', ['id' => 'email', 'required' => true, 'label' => 'Email'])."</div><div class='col-sm-6'></div></div>";
                            
                            //echo $this->Form->control('user_type_id', ['options' => ['1' => 'Admin', '2' => 'Student', '3' => 'Writer'], 'id' => 'user_type_id', 'required' => true, 'label' => 'User Type'])."</div></div>";



                            


                        ?>
                        
                    </fieldset>
                    <?= $this->Form->button(__('Submit')) ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                         <h3 class="box-title">Assignment Details</h3>
                           <?php if($user->status == 1){ ?> 
                            <a class="btn btn-danger pull-right" href="<?php echo $this->Url->build('/admin/users/deactivate/'.$user->id); ?>">Deactivate</a>
                         <?php  }
                         if($user->status == 0){  ?> 
                            <a class="btn btn-success pull-right" href="<?php echo $this->Url->build('/admin/users/activate/'.$user->id); ?>">Activate</a>
                         <?php } ?>
                </div>
                    <!-- Main content -->
               
                <div class="editUser form large-9 medium-8 columns content">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Sr. Num</th>
                                    <th scope="col">Title</th>
                                    <th scope="col">Delivery Date</th>
                                    <th scope="col">Created Date</th>
                                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if(!empty($user['assignments'])){
                                $i=1 ; foreach ($user['assignments'] as $users): ?>
                                <tr>
                                    <td><?= $i ?></td>

                                    <td><?= h($users['title']) ?></td>
                                    <td><?= h($users['delivery_date']) ?></td>
                                    <td><?= h($users['created']) ?></td>
                                  
                            
                                    <td class="actions">
                                        
                                        <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-pencil-square-o')).'', array( 'controller' => 'assignments' , 'action' => 'view', $users['id']), array('escape' => false)) ?> 
                                    </td>
                                </tr>
                                <?php endforeach; 
                                }else{
                                    echo "<tr><td colspan='5'><center>No assignments posted yet.</center></td></tr>";
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function(){
    $('#editUser').validate({
        rules:{
            first_name:"required",
            last_name:"required",
            email:"required",
            username:"required",
            password:"required"
        },
        messages:{
            first_name:"Please enter first name",
            last_name:"Please enter first name",
            email: "Please enter correct email id",
            username: "Please enter username",
            password: "Please enter password"
        }
    });
});
</script>

