<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <span>Dashboard</span>
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><span id=''>10</span></h3>
              <p>Total Students</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><span id=''>15</span></h3>
              <p>Total Writers</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><span id=''>0</span></h3>

              <p>Total Assignments</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
      <!-- /.row -->
      
      <!-- /.row (main row) -->
    </section>
    <input type="hidden" id="year" value="<?php echo date('Y'); ?>">
    <!-- /.content -->
<?php

$this->Html->script([
  //'https://code.jquery.com/ui/1.11.4/jquery-ui.min.js',
  'https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',
  'AdminLTE./plugins/morris/morris.min',
 // 'AdminLTE./plugins/sparkline/jquery.sparkline.min',
 // 'AdminLTE./plugins/jvectormap/jquery-jvectormap-1.2.2.min',
 // 'AdminLTE./plugins/jvectormap/jquery-jvectormap-world-mill-en',
 // 'AdminLTE./plugins/knob/jquery.knob',
 // 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js',
 // 'AdminLTE./plugins/datepicker/bootstrap-datepicker',
 // 'AdminLTE./plugins/daterangepicker/daterangepicker',
 // 'AdminLTE./plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min',
],
['block' => 'script']);
?>

<?php $this->start('scriptBotton'); ?>
<script type="text/javascript">


  $(document).ready(function() {
    var url = window.location;
    if(window.location.pathname.slice(-1) == '/'){
      var adminUrl = 'users/dashboard';
    }else{
      var adminUrl = '/users/dashboard';
    }
    // if()
    $.get( url+adminUrl, function( datas ) {
            var res = JSON.parse(datas);
            // alert(datas.chartData);
            console.log(res.chartData);
            $("#total_products_count").html(res.Products);
            $("#total_orders_count").html(res.TotalOrders);
            $("#db_user_enquiries").html(res.Enquiries);
            $("#db_total_earning").html(res.TotalPrice);
            var bar_data = {
              data : [['January', res.jan], ['February', res.feb], ['March', res.mar], ['April', res.apr], ['May', res.may], ['June', res.jun] ,['July' , res.jul] , ['August' , res.aug] , ['September' , res.sep ] , ['October' , res.oct ] , ['November' , res.nov ] , ['December', res.dec ] ],
              color: '#3c8dbc'
            }
            
            $.plot('#bar-chart', [bar_data], {
              grid  : {
                borderWidth: 1,
                borderColor: '#f3f3f3',
                tickColor  : '#f3f3f3'
              },
              series: {
                bars: {
                  show    : true,
                  barWidth: 0.5,
                  align   : 'center'
                }
              },
              xaxis : {
                mode      : 'categories',
                tickLength: 0
              }
            })

     });


    });
   
 

</script>
<?php  $this->end(); ?>
