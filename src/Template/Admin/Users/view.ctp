<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\User $user
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Types'), ['controller' => 'UserTypes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Type'), ['controller' => 'UserTypes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Cms Pages'), ['controller' => 'CmsPages', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cms Page'), ['controller' => 'CmsPages', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Images'), ['controller' => 'Images', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Image'), ['controller' => 'Images', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Manage Home Page'), ['controller' => 'ManageHomePage', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Manage Home Page'), ['controller' => 'ManageHomePage', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Menus'), ['controller' => 'Menus', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Menu'), ['controller' => 'Menus', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Username') ?></th>
            <td><?= h($user->username) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($user->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Type') ?></th>
            <td><?= $user->has('user_type') ? $this->Html->link($user->user_type->id, ['controller' => 'UserTypes', 'action' => 'view', $user->user_type->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('First Name') ?></th>
            <td><?= h($user->first_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Name') ?></th>
            <td><?= h($user->last_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Profile Image') ?></th>
            <td><?= h($user->profile_image) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($user->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($user->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Articles') ?></h4>
        <?php if (!empty($user->articles)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Category Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Slug') ?></th>
                <th scope="col"><?= __('Excerpt') ?></th>
                <th scope="col"><?= __('Content') ?></th>
                <th scope="col"><?= __('Created By') ?></th>
                <th scope="col"><?= __('Modified By') ?></th>
                <th scope="col"><?= __('Publish Date') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Trashed') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->articles as $articles): ?>
            <tr>
                <td><?= h($articles->id) ?></td>
                <td><?= h($articles->category_id) ?></td>
                <td><?= h($articles->title) ?></td>
                <td><?= h($articles->slug) ?></td>
                <td><?= h($articles->excerpt) ?></td>
                <td><?= h($articles->content) ?></td>
                <td><?= h($articles->created_by) ?></td>
                <td><?= h($articles->modified_by) ?></td>
                <td><?= h($articles->publish_date) ?></td>
                <td><?= h($articles->created) ?></td>
                <td><?= h($articles->modified) ?></td>
                <td><?= h($articles->trashed) ?></td>
                <td><?= h($articles->user_id) ?></td>
                <td><?= h($articles->type) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Articles', 'action' => 'view', $articles->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Articles', 'action' => 'edit', $articles->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Articles', 'action' => 'delete', $articles->id], ['confirm' => __('Are you sure you want to delete # {0}?', $articles->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Categories') ?></h4>
        <?php if (!empty($user->categories)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Slug') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Parent Id') ?></th>
                <th scope="col"><?= __('Lft') ?></th>
                <th scope="col"><?= __('Rght') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Trashed') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->categories as $categories): ?>
            <tr>
                <td><?= h($categories->id) ?></td>
                <td><?= h($categories->user_id) ?></td>
                <td><?= h($categories->slug) ?></td>
                <td><?= h($categories->name) ?></td>
                <td><?= h($categories->parent_id) ?></td>
                <td><?= h($categories->lft) ?></td>
                <td><?= h($categories->rght) ?></td>
                <td><?= h($categories->created) ?></td>
                <td><?= h($categories->modified) ?></td>
                <td><?= h($categories->trashed) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Categories', 'action' => 'view', $categories->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Categories', 'action' => 'edit', $categories->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Categories', 'action' => 'delete', $categories->id], ['confirm' => __('Are you sure you want to delete # {0}?', $categories->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Cms Pages') ?></h4>
        <?php if (!empty($user->cms_pages)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Title En') ?></th>
                <th scope="col"><?= __('Title Spa') ?></th>
                <th scope="col"><?= __('Meta Description En') ?></th>
                <th scope="col"><?= __('Meta Description Spa') ?></th>
                <th scope="col"><?= __('Meta Content En') ?></th>
                <th scope="col"><?= __('Meta Content Spa') ?></th>
                <th scope="col"><?= __('Meta Keyword En') ?></th>
                <th scope="col"><?= __('Meta Keyword Spa') ?></th>
                <th scope="col"><?= __('Slug') ?></th>
                <th scope="col"><?= __('Content En') ?></th>
                <th scope="col"><?= __('Content Spa') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->cms_pages as $cmsPages): ?>
            <tr>
                <td><?= h($cmsPages->id) ?></td>
                <td><?= h($cmsPages->user_id) ?></td>
                <td><?= h($cmsPages->title_en) ?></td>
                <td><?= h($cmsPages->title_spa) ?></td>
                <td><?= h($cmsPages->meta_description_en) ?></td>
                <td><?= h($cmsPages->meta_description_spa) ?></td>
                <td><?= h($cmsPages->meta_content_en) ?></td>
                <td><?= h($cmsPages->meta_content_spa) ?></td>
                <td><?= h($cmsPages->meta_keyword_en) ?></td>
                <td><?= h($cmsPages->meta_keyword_spa) ?></td>
                <td><?= h($cmsPages->slug) ?></td>
                <td><?= h($cmsPages->content_en) ?></td>
                <td><?= h($cmsPages->content_spa) ?></td>
                <td><?= h($cmsPages->created) ?></td>
                <td><?= h($cmsPages->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CmsPages', 'action' => 'view', $cmsPages->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CmsPages', 'action' => 'edit', $cmsPages->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CmsPages', 'action' => 'delete', $cmsPages->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cmsPages->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Images') ?></h4>
        <?php if (!empty($user->images)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Url') ?></th>
                <th scope="col"><?= __('Thumbnail Url') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Gallery Id') ?></th>
                <th scope="col"><?= __('Is Deleted') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->images as $images): ?>
            <tr>
                <td><?= h($images->id) ?></td>
                <td><?= h($images->name) ?></td>
                <td><?= h($images->url) ?></td>
                <td><?= h($images->thumbnail_url) ?></td>
                <td><?= h($images->user_id) ?></td>
                <td><?= h($images->gallery_id) ?></td>
                <td><?= h($images->is_deleted) ?></td>
                <td><?= h($images->created) ?></td>
                <td><?= h($images->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Images', 'action' => 'view', $images->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Images', 'action' => 'edit', $images->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Images', 'action' => 'delete', $images->id], ['confirm' => __('Are you sure you want to delete # {0}?', $images->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Manage Home Page') ?></h4>
        <?php if (!empty($user->manage_home_page)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Best Prices Best Holidays En') ?></th>
                <th scope="col"><?= __('Best Prices Best Holidays Spa') ?></th>
                <th scope="col"><?= __('Shared Shuttle En') ?></th>
                <th scope="col"><?= __('Shared Shuttle Spa') ?></th>
                <th scope="col"><?= __('Private Shuttle En') ?></th>
                <th scope="col"><?= __('Private Shuttle Spa') ?></th>
                <th scope="col"><?= __('Best And Cheapest Shuttles In Cancun En') ?></th>
                <th scope="col"><?= __('Best And Cheapest Shuttles In Cancun Spa') ?></th>
                <th scope="col"><?= __('Whats App En') ?></th>
                <th scope="col"><?= __('Whats App Spa') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->manage_home_page as $manageHomePage): ?>
            <tr>
                <td><?= h($manageHomePage->id) ?></td>
                <td><?= h($manageHomePage->user_id) ?></td>
                <td><?= h($manageHomePage->best_prices_best_holidays_en) ?></td>
                <td><?= h($manageHomePage->best_prices_best_holidays_spa) ?></td>
                <td><?= h($manageHomePage->shared_shuttle_en) ?></td>
                <td><?= h($manageHomePage->shared_shuttle_spa) ?></td>
                <td><?= h($manageHomePage->private_shuttle_en) ?></td>
                <td><?= h($manageHomePage->private_shuttle_spa) ?></td>
                <td><?= h($manageHomePage->best_and_cheapest_shuttles_in_cancun_en) ?></td>
                <td><?= h($manageHomePage->best_and_cheapest_shuttles_in_cancun_spa) ?></td>
                <td><?= h($manageHomePage->whats_app_en) ?></td>
                <td><?= h($manageHomePage->whats_app_spa) ?></td>
                <td><?= h($manageHomePage->created) ?></td>
                <td><?= h($manageHomePage->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ManageHomePage', 'action' => 'view', $manageHomePage->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ManageHomePage', 'action' => 'edit', $manageHomePage->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ManageHomePage', 'action' => 'delete', $manageHomePage->id], ['confirm' => __('Are you sure you want to delete # {0}?', $manageHomePage->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Menus') ?></h4>
        <?php if (!empty($user->menus)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Page Id') ?></th>
                <th scope="col"><?= __('Slug') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Parent Id') ?></th>
                <th scope="col"><?= __('Lft') ?></th>
                <th scope="col"><?= __('Rght') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Trashed') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->menus as $menus): ?>
            <tr>
                <td><?= h($menus->id) ?></td>
                <td><?= h($menus->user_id) ?></td>
                <td><?= h($menus->page_id) ?></td>
                <td><?= h($menus->slug) ?></td>
                <td><?= h($menus->name) ?></td>
                <td><?= h($menus->parent_id) ?></td>
                <td><?= h($menus->lft) ?></td>
                <td><?= h($menus->rght) ?></td>
                <td><?= h($menus->created) ?></td>
                <td><?= h($menus->modified) ?></td>
                <td><?= h($menus->trashed) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Menus', 'action' => 'view', $menus->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Menus', 'action' => 'edit', $menus->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Menus', 'action' => 'delete', $menus->id], ['confirm' => __('Are you sure you want to delete # {0}?', $menus->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
