<?php
/**
  * @var \App\View\AppView $this
  */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= __('Writer') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Writer') ?></a>
        </li>
        <li class="active"> <?= __('Edit Writer') ?></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                         <h3 class="box-title">Edit Writer</h3>
                </div>
                    <!-- Main content -->
               
                <div class="editUser form large-9 medium-8 columns content">
                    <?= $this->Form->create($user, ['id' => 'editUser']) ?>
                    <fieldset>
                        <?php

                            echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('first_name', ['id' => 'first_name', 'required' => true, 'label' => 'First Name'])."</div><div class='col-sm-6'>";

                            echo $this->Form->control('last_name', ['id' => 'last_name', 'required' => true, 'label' => 'Last Name'])."</div></div>";

                            echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('mobile', ['id' => 'mobile', 'required' => true, 'label' => 'Mobile Number'])."</div><div class='col-sm-6'>";

                            echo $this->Form->control('address', ['id' => 'address', 'required' => true, 'label' => 'Address'])."</div></div>";

                            echo "<div class='row'><div class='col-sm-4'>".$this->Form->control('city', ['id' => 'state', 'required' => true, 'label' => 'City'])."</div><div class='col-sm-4'>";

                            echo $this->Form->control('state', ['id' => 'state', 'required' => true, 'label' => 'State'])."</div><div class='col-sm-4'>"; 

                             echo $this->Form->control('zipcode', ['id' => 'zipcode', 'required' => true, 'label' => 'Zipcode'])."</div></div>";

                            echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('username', ['id' => 'username', 'required' => true, 'label' => 'Username '])."</div><div class='col-sm-6'>";

                            //echo $this->Form->control('password', ['id' => 'password', 'required' => true, 'label' => 'Password'])."</div></div>";

                            echo $this->Form->control('email', ['id' => 'email', 'required' => true, 'label' => 'Email'])."</div><div class='col-sm-6'></div></div>";
                            
                            //echo $this->Form->control('user_type_id', ['options' => ['1' => 'Admin', '2' => 'Student', '3' => 'Writer'], 'id' => 'user_type_id', 'required' => true, 'label' => 'User Type'])."</div></div>";



                            


                        ?>
                        
                    </fieldset>
                    <?= $this->Form->button(__('Submit')) ?>
                    <?= $this->Form->end() ?>
                </div>
               
            </div>

        </div>
    </div>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                         <h3 class="box-title">Writer Profile Details</h3>
                         <?php if($user->status == 1){ ?> 
                            <a class="btn btn-danger pull-right" href="<?php echo $this->Url->build('/admin/users/deactivate/'.$user->id); ?>">Deactivate</a>
                         <?php  }
                         if($user->status == 0){  ?> 
                            <a class="btn btn-success pull-right" href="<?php echo $this->Url->build('/admin/users/activate/'.$user->id); ?>">Activate</a>
                         <?php } ?>
                </div>
                    <!-- Main content -->
               
                <div class="editUser form large-12 medium-8 columns content">
                    <div class="box-body table-responsive no-padding">
                         <div class="right-profle-section">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4 class="d-inline mr-3 name"><?php echo $this->request->getSession()->read('Auth.Writer.first_name')." ".$this->request->getSession()->read('Auth.Writer.last_name'); 
                                            ?></h4> <span><img
                                            src="<?php echo $this->Url->build('/images/star-img.svg'); ?>"> ( 0 )</span>
                                </div>

                            </div>
                            <?php if(!empty($this->request->getSession()->read('Auth.Writer.state'))  && !empty($this->request->getSession()->read('Auth.Writer.city'))){ ?>
                            <div class="row">
                                <div class="col-sm-12 location">
                                    <img src="<?php echo $this->Url->build('/images/location.svg'); ?>"> <?php echo $this->request->getSession()->read('Auth.Writer.city').", "; 
                                            ?><?php echo $this->request->getSession()->read('Auth.Writer.state'); 
                                            ?>
                                </div>
                            </div>
                            <?php } ?>

                            <div class="row">
                                <div class="col-sm-12">
                                    <?php if(!empty($user['writer_profile']['writer_documents'])){ 
                                        $documentArray = [];
                                    ?>
                                    <span><strong>Type of Document:</strong> </span> <span><?php foreach ($user['writer_profile']['writer_documents'] as $key => $document) {
                                        $documentArray[] = '<span class="add-cat-bg ml-3">'.$document->document_type."</span>";
                                    } ?></span>
                                    <?php echo implode(",",$documentArray);  } ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                     <?php if(!empty($user['writer_profile']['writer_thematics'])){ 
                                        $thematicArray = [];
                                    ?>
                                    <span><strong>Thematic Unity:</strong> </span> <span><?php foreach ($user['writer_profile']['writer_thematics'] as $key => $thematic) {
                                        $thematicArray[] = '<span class="add-cat-bg ml-3">'.$thematic->thematic_type."</span> ";
                                    } ?></span>
                                    <?php  echo implode(",",$thematicArray); }?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <span><strong>Total Experience:</strong> </span> <span><?php echo $user['writer_profile']['experience']; ?> years</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php if(!empty($user['writer_profile']['writer_skills'])){ 
                                        $skillArray = [];
                                    ?>
                                    <span><strong>Skills:</strong> </span> <span><?php foreach ($user['writer_profile']['writer_skills'] as $key => $skill) {
                                        $skillArray[] = '<span class="add-cat-bg ml-3">'.$skill->languages."</span> ";
                                    } ?></span>
                                    <?php echo implode(",",$skillArray); } ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <span><strong>Total Projects Completed: </strong> </span> <span>0
                                        Projects</span>
                                </div>
                            </div>
                            <?php if(!empty($user['writer_profile']['iban'])){ ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <span><strong>IBAN:</strong> </span> <span><?php echo $user['writer_profile']['iban']; ?> </span>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if(!empty($user['writer_profile']['bank_code'])){ ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <span><strong>Bank Code:</strong> </span> <span><?php echo $user['writer_profile']['bank_code']; ?> </span>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function(){
    $('#editUser').validate({
        rules:{
            first_name:"required",
            last_name:"required",
            email:"required",
            username:"required",
            password:"required"
        },
        messages:{
            first_name:"Please enter first name",
            last_name:"Please enter first name",
            email: "Please enter correct email id",
            username: "Please enter username",
            password: "Please enter password"
        }
    });
});
</script>

