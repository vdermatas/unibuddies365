<?php
/**
  * @var \App\View\AppView $this
  */
?>
<section class="content-header">
    <h1>
        <?= __('Users') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Users') ?></a>
        </li>
        <li class="active"> <?= __('Change Password') ?></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                         <h3 class="box-title">Change Password</h3>
                </div>
                <!-- Main content -->
                <div class="users form large-9 medium-8 columns content">
                    <?= $this->Form->create('', ['id' => 'changePwd']) ?>
                    <fieldset>
                            <?= "<div class='row'><div class='col-sm-6'>".$this->Form->input('old_password',['type' => 'password' , 'label'=>'Old password', 'id' => 'old_password', 'required' => true])."</div><div class='col-sm-6'></div></div>"?>
                            <?= "<div class='row'><div class='col-sm-6'>".$this->Form->input('password1',['type'=>'password' ,'label'=>'Password', 'id' => 'password1' , 'required' => true]) ?>
                            <?= $this->Form->input('password2',['type' => 'password' , 'label'=>'Repeat password', 'id' => 'password' , 'required' => true])."</div></div>"?>
                    </fieldset>&nbsp;&nbsp;
                    <?= $this->Form->button(__('Submit')) ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function(){
    $('#changePwd').validate({
        rules:{
            old_password:"required",
            password1: "required",
            password2:{
                
                equalTo: "#password1"      
            }
        },
        messages:{
            old_password:"Please enter old password",
            password1: "Please enter new password"
        }
    });
});
</script>
