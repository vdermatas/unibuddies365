<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Article[]|\Cake\Collection\CollectionInterface $articles
  */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= __('Writers') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i>  <?= __('Writers') ?></a>
        </li>
        <li class="active"> <?= __('All Writers') ?></li>
    </ol>
</section>
<!-- Main content -->

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?= __('Writers') ?></h3>

                     <?php echo $this->element("search_form"); ?>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover table-striped">
                        <thead>
            <tr>
                <th scope="col">Sr. Num</th>
                <th scope="col"><?= $this->Paginator->sort('first_name', 'Name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email', 'Email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status', 'Status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified', 'Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php $i=1; foreach ($users as $user): ?>
            <tr>
                <td><?= $i ?></td>                
                <td><?= h($user->first_name." ".$user->last_name) ?></td>
                <td><?= h($user->email) ?></td>
                <td>
                    <?php 
                        if($user->status == 1){ 
                            echo "<span class='btn btn-success'>Activated</span>"; 
                        }else{ 
                            echo "<span class='btn btn-danger'>Not Activated</span>"; 
                        } 
                    ?>
                </td>
                <td><?= h($user->created) ?></td>
                <td class="actions">                    
                    <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-edit')).'', array('action' => 'editWriter', $user->id), array('escape' => false)) ?> 
                    
                </td>
            </tr>
            <?php $i++; endforeach; ?>
        </tbody>
    </table>
     </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">

                        <li><?=$this->Paginator->first('<< ' . __('First ')) ?></li>
                        <li><?=$this->Paginator->prev('< ' . __('Previous ')) ?>
                        <li><?=$this->Paginator->numbers() ?></li>
                        <li><?=$this->Paginator->next(__('Next ') . '>') ?></li>
                        <li><?=$this->Paginator->last(__('Last') . ' >>') ?></li>
                    </ul>
                    <p><?=$this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->

