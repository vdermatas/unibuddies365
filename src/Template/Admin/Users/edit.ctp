<?php
/**
  * @var \App\View\AppView $this
  */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= __('Users') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Users') ?></a>
        </li>
        <li class="active"> <?= __('Add Users') ?></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                         <h3 class="box-title">Add user</h3>
                </div>
                    <!-- Main content -->
               
                <div class="editUser form large-9 medium-8 columns content">
                    <?= $this->Form->create($user, ['id' => 'editUser']) ?>
                    <fieldset>
                        <?php

                            echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('first_name', ['id' => 'first_name', 'required' => true, 'label' => 'First Name'])."</div><div class='col-sm-6'>";

                            echo $this->Form->control('last_name', ['id' => 'last_name', 'required' => true, 'label' => 'Last Name'])."</div></div>";

                            echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('username', ['id' => 'username', 'required' => true, 'label' => 'Username '])."</div><div class='col-sm-6'>";

                            //echo $this->Form->control('password', ['id' => 'password', 'required' => true, 'label' => 'Password'])."</div></div>";

                            echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('email', ['id' => 'email', 'required' => true, 'label' => 'Email'])."</div><div class='col-sm-6'>";
                            
                            echo $this->Form->control('user_type_id', ['options' => ['1' => 'Admin', '2' => 'Student', '3' => 'Writer'], 'id' => 'user_type_id', 'required' => true, 'label' => 'User Type'])."</div></div>";
                        ?>
                        
                    </fieldset>
                    <?= $this->Form->button(__('Submit')) ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function(){
    $('#editUser').validate({
        rules:{
            first_name:"required",
            last_name:"required",
            email:"required",
            username:"required",
            password:"required"
        },
        messages:{
            first_name:"Please enter first name",
            last_name:"Please enter first name",
            email: "Please enter correct email id",
            username: "Please enter username",
            password: "Please enter password"
        }
    });
});
</script>

