<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Profile
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-user"></i> User</a></li>
    <li class="active">Profile</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">

  <div class="row">
    <div class="col-md-3">

      <!-- Profile Image -->
      <div class="box box-primary">
        <div class="box-body box-profile">
			<?php echo $this->Html->image('/files/uploads/'.$this->request->getSession()->read('Auth.Admin.profile_image'), array('class' => 'profile-user-img img-responsive img-circle', 'alt' => 'User profile picture')); ?>
			
			<h3 class="profile-username text-center"><?php echo $this->request->getSession()->read('Auth.Admin.first_name'). ' '.$this->request->getSession()->read('Auth.Admin.last_name'); ?></h3>
        
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </div>
    <!-- /.col -->
    <div class="col-md-9">
      <div class="nav-tabs-custom">        
        <div class="tab-content">        
          <div class="active tab-pane" id="settings">
				<?= $this->Form->create($user,array('enctype'=>'multipart/form-data')); ?>
				<?php
					echo $this->Form->control('first_name');
					echo $this->Form->control('last_name');
					echo $this->Form->control('email');
					echo $this->Form->control('upload_image', [
											'type' => 'file'
										]);
				?>
				
				<?= $this->Form->button(__('Submit')) ?>
				<?= $this->Form->end() ?>
		  
		  </div>
          <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
      </div>
      <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

</section>
<!-- /.content -->
