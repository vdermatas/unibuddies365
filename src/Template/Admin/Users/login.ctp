<!-- File: src/Template/Users/login.ctp -->

<div class="users form text-center custom-login">
<?= $this->Flash->render() ?>
<?php echo $this->Flash->render('auth'); ?> 
<?= $this->Form->create() ?>
    <fieldset class="text-left">
        <?= $this->Form->control('username') ?>
        <?= $this->Form->control('password') ?>
    </fieldset>
<?= $this->Form->button(__('Login')); ?>
<?= $this->Form->end() ?>

</div>

<style type="text/css">
.login-page {
    background: url(../img/sectionimages/<?php echo $bg_images[2]['image']; ?>);
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center top;
}
.logo{
	background-color: black;
	position: relative;
	top: 16px;
}
</style>
