<div class="container">
            <div class="row">
                <div class="col-md-12"><h2>Create Menu</h2></div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading clearfix"><h5 class="pull-left">Menu</h5>
                        </div>
                        <div class="panel-body" id="cont">
                            <ul id="myEditor" class="sortableLists list-group">
                            </ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <!-- <button id="btnOut" type="button" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Output</button>
                    </div>
                    <div class="form-group"><textarea id="out" class="form-control" cols="50" rows="10"></textarea> -->
                        <button type="button" class="btn btn-success save"><i class="glyphicon glyphicon-ok"></i> Save</button>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Add Menu</div>
                        <div class="panel-body">
                            <form id="frmEdit" class="form-horizontal">
                                <div class="form-group">
                                    <label for="text" class="col-sm-2 control-label">Title</label>
                                    <div class="col-sm-10">
                                            <input type="text" class="form-control item-menu" name="text" id="text" placeholder="Title">
 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="custom_url" class="col-sm-2 control-label"> Url</label>
                                    <div class="col-sm-10">
                                            <input type="text" class="form-control item-menu" name="custom_url" id="custom_url" placeholder="Custom Url">
 
                                    </div>
                                </div> 
                                <center>( or )</center><br>
                                <div class="form-group">
                                    <label for="page" class="col-sm-2 control-label">Page</label>
                                    <div class="col-sm-10">
                                        <select name="page" id="page" class="form-control item-menu">
                                            <option value=""></option>
                                            <?php foreach ($CmsPages as $key => $CmsPage) { ?>
                                                <option value="<?php echo $CmsPage->slug; ?>"><?php echo $CmsPage->title_en; ?></option>
                                             <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="panel-footer">
                            <button type="button" id="btnUpdate" class="btn btn-primary" disabled><i class="fa fa-refresh"></i> Update</button>
                            <button type="button" id="btnAdd" class="btn btn-success"><i class="fa fa-plus"></i> Add</button>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
        </div>
        <script>
                jQuery(document).ready(function () {
                    clickFunction();
                });
                // menu items
                var strjson = <?php echo $menus->data; ?>;
                //icon picker options
                var iconPickerOptions = {searchText: 'Buscar...', labelHeader: '{0} de {1} Pags.'};
                //sortable list options
                var sortableListOptions = {
                    placeholderCss: {'background-color': 'cyan'}
                };

                var editor = new MenuEditor('myEditor', {listOptions: sortableListOptions, iconPicker: iconPickerOptions, labelEdit: 'Edit'});
                editor.setForm($('#frmEdit'));
                editor.setUpdateButton($('#btnUpdate'));
                
                // $('#btnReload').on('click', function () {
                //     editor.setData(strjson);
                // });


                $('#btnOut').on('click', function () {
                    var str = editor.getString();
                    $("#out").text(str);
                });

                $("#btnUpdate").click(function(){
                    editor.update();
                });

                $('#btnAdd').click(function(){
                    editor.add();
                });

            function clickFunction()
            {
                editor.setData(strjson);
            }
            $(document).on('click','.save', function(){
                var data = editor.getString();
                $.ajax({
                    type: "POST",
                    url: 'createMenu',
                    data:{ data: data},
                    dataType:'json',
                    success: function(data) {
                        if(data.status == 1){
                            alert('Menu saved Successfully');
                            location.reload();
                        }else{
                            alert('Something went wrong');
                        }
                    }
                });
            });
        </script>