<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="menus form large-9 medium-8 columns content">
    <?= $this->Form->create($menu) ?>
    <fieldset>
        <legend><?= __('Add Menu') ?></legend>
        <?php
           
            echo $this->Form->control('name');
            echo $this->Form->control('parent_id', ['options' => $parentMenus, 'empty' => true]);
			
			echo $this->Form->control('page_id', ['options' => $CmsPages]);

           
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
