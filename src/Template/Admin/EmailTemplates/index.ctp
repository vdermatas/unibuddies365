<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Article[]|\Cake\Collection\CollectionInterface $articles
  */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= __('Email Templates') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Email Templates') ?></a>
        </li>
        <li class="active"> <?= __('All Email Templates') ?></li>
    </ol>
</section>
<!-- Main content -->

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?= __('Email Templates') ?></h3>

                    <?php echo $this->element("search_form"); ?>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover table-striped">
                        <thead>
            <tr>
                <th scope="col">Sr.</th>
                <th scope="col"><?= $this->Paginator->sort('template_for') ?></th>
                <th scope="col"><?= $this->Paginator->sort('subject') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 1; foreach($emailTemplates as $emailTemplate): ?>
            <tr>
                <td><?php echo $i; ?></td>
                
                <td><?= h($emailTemplate->template_for) ?></td>
                <td><?= h($emailTemplate->subject) ?></td>
                <td><?= h($emailTemplate->created) ?></td>
                <td><?= h($emailTemplate->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-pencil-square-o')).'', array('action' => 'edit', $emailTemplate->id), array('escape' => false)) ?>
                </td>
            </tr>
            <?php $i++; endforeach; ?>
        </tbody>
    </table>
     </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">

                        <li><?=$this->Paginator->first('<< ' . __('first ')) ?></li>
                        <li><?=$this->Paginator->prev('< ' . __('previous ')) ?>
                        <li><?=$this->Paginator->numbers() ?></li>
                        <li><?=$this->Paginator->next(__('next ') . '>') ?></li>
                        <li><?=$this->Paginator->last(__('last') . ' >>') ?></li>
                    </ul>
                    <p><?=$this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
