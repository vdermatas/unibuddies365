<?php
/**
  * @var \App\View\AppView $this
  */
?>
<section class="content-header">
    <h1>
        <?= __('Email Templates') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Email Templates') ?></a>
        </li>
        <li class="active"> <?= __('Edit Email Templates') ?></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                         <h3 class="box-title">Edit Email Template</h3>
                </div>
                <!-- Main content -->
                <?php echo $this->Html->script('tinymce/tinymce.min.js'); ?>
                <?php $this->TinymceElfinder->defineElfinderBrowser()?>
                <div class="emailTemplates form large-9 medium-8 columns content">
                    <?= $this->Form->create($emailTemplate, ['id' => 'email_template']) ?>
                    <fieldset>
                        <?php            
                            echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('template_for')."</div><div class='col-sm-6' >";
                            echo $this->Form->control('subject')."</div></div>";
                            echo "<div class='row'><div class='col-xs-12'>".$this->Form->control('body',['required' => false])."</div></div>";
                        ?>
                    </fieldset>
                    <?= $this->Form->button(__('Submit')) ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function(){
    $('#email_template').validate({
        rules:{
            template_for: "required",
            subject: "required"
        },
        messages:{
            template_for:"Please enter template for",
            subject: "Please enter subject"
        }
    });
});
</script>
