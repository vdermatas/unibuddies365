<?php
/**
  * @var \App\View\AppView $this
  */
?><?php use Cake\Core\Configure; ?>
<section class="content-header">
    <h1>
        <?= __('Page Banner Section') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Page Banner Section') ?></a>
        </li>
        <li class="active"> <?= __('Change Banner Image') ?></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Edit Sections</h3>
                    </div>
                    <!-- Main content -->
                <div class="cmsPages form large-9 medium-8 columns content">
                <?= $this->Form->create($homepageSection, ['type' => 'file' , 'id' => 'editSection']) ?>
                <fieldset>
                    
                    
                     Current Image : <img src="<?php echo Configure::read("APP_BASE_URL")."/".$homepageSection->image; ?>" width="200px" height="100px"> 
                        <?php
                        echo "&nbsp;&nbsp;".$this->Form->control('image', ['type' => 'file','required' => false, 'label' => 'Change Banner Image ']); 
                 

                    ?>
                </fieldset>
                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</section>
