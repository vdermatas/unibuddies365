<?php
/**
  * @var \App\View\AppView $this
  */
?><?php use Cake\Core\Configure; ?>
<section class="content-header">
    <h1>
        <?= __('HomePage Sections') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('HomePage Sections') ?></a>
        </li>
        <li class="active"> <?= __('Edit HomePage Section') ?></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                    <!-- Main content -->
               <?php echo $this->Html->script('tinymce/tinymce.min.js'); ?>
                <?php $this->TinymceElfinder->defineElfinderBrowser()?>
                <div class="cmsPages form large-9 medium-8 columns content">
                <?= $this->Form->create($homepageslider, ['type' => 'file' , 'id' => 'editSection']) ?>
                <fieldset>
                    <legend><?= __('Edit Sections') ?></legend>
                    <?php
                        echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('title', ['id' => 'title', 'required' => true, 'label' => 'Title '])."</div>";

                      
                        echo "<div class='col-sm-12'>".$this->Form->control('text', ['type' => 'textarea','label' => 'Content'])."</div></div>"; 
                        echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('image', ['label' => 'Change Image' , 'type' => 'file'])."</div>"; 
                    ?>
                    <div class="col-sm-6">
                    <img src="<?php echo Configure::read("APP_BASE_URL")."/".$homepageslider->url; ?>" width="220px" height="100px">
                </div>

                </fieldset>
                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function(){
    $('#editSection').validate({
        rules:{
            title:{
                required: true
            },
            text:{
                required : true
            }
        },
        messages:{
            title:{
                required:"Please enter title"
            },
            text:{
                required:"Content is required"
            }
        }
    });
});
</script>