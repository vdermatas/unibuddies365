<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Article[]|\Cake\Collection\CollectionInterface $articles
  */
?>
<?php use Cake\Core\Configure; ?>



<?php echo $this->Html->script('AdminLTE./plugins/imagePreview/imagePreview'); ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= __('Sections') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Sections') ?></a>
        </li>
        <li class="active"> <?= __('All Sections') ?></li>
    </ol>
</section>
<!-- Main content -->

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?= __('Sections') ?></h3>

                   <?php //echo $this->element("search_form"); ?>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding table-striped">
                    <table class="table table-hover">
                        <thead>
            <tr>
                <th scope="col">Sr. Num</th>  
                <th scope="col">Preview Section</th>
			    <th scope="col"><?= $this->Paginator->sort('section_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('title') ?></th>
                <th scope="col">Show / Hide Section</th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php $i=1; foreach ($homepageSections as $homepageSection): ?>
            <tr>
                <td><?= $i ?></td> 
                <td><a href="<?php echo Configure::read("APP_BASE_URL")."/img/".$homepageSection->thumbnail; ?>" rel="<?php echo Configure::read("APP_BASE_URL")."/img/".$homepageSection->thumbnail; ?>" class="screenshot"><img src="<?php echo Configure::read("APP_BASE_URL")."/img/".$homepageSection->thumbnail; ?>" width="100px" height="100px" class=""></a></td>              
                <td><?= h($homepageSection->section_name) ?></td>               
                <td><?= h($homepageSection->title) ?></td>                
                
                <td><div class="checkbox">
                      <label>
                        <?php
                            $checkClass = '';
                            if( $homepageSection['status'] == 1) {
                                $checkClass ='checked';
                            }
                        ?>
                        <input type="checkbox" <?php echo $checkClass; ?> value="" class='checkbox_section_settings' data-item-id='<?php echo $homepageSection->id; ?>' >
                      </label>
                    </div></td> 
                <td class="action">
                   <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-pencil-square-o')).'', array('action' => 'edit', $homepageSection->id), array('escape' => false)) ?> 
                </td>
            </tr>
            <?php $i++; endforeach; ?>
         </tbody>
    </table>
     </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">

                        <li><?=$this->Paginator->first('<< ' . __('First ')) ?></li>
                        <li><?=$this->Paginator->prev('< ' . __('Previous ')) ?>
                        <li><?=$this->Paginator->numbers() ?></li>
                        <li><?=$this->Paginator->next(__('Next ') . '>') ?></li>
                        <li><?=$this->Paginator->last(__('Last') . ' >>') ?></li>
                    </ul>
                    <p><?=$this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>

<!-- /.content -->
<!-- <script src="http://code.jquery.com/jquery-1.12.4.min.js"></script> -->
<script>
    $(document).ready(function(){
        $(".checkbox_section_settings").on('click',function(){
            var r2 = null;
            var url = "homepages/changeStatus";
            var status = 0;

            if($(this).is(":checked")) { //add in record as digital menu item
              status = 1;
            }

            var id = $(this).attr("data-item-id");

            $.ajax({
                  type: "POST",
                  url : url,
                  dataType: "json",
                  data: {'id': id, 'status':status},
                  beforeSend: function() {

                  },
                  success: function(data) {

                    if(data.code == 1) {
                          
                    }else{
                      alert('Something wrong, please contact to developer!');
                    }

                  }
            });
        });
    });

</script>
<style type="text/css">
#screenshot{
    position:absolute;
    border:1px solid #ccc;
    background:#333;
    padding:5px;
    display:none;
    color:#fff;
    }
    #screenshot img{
        width: 500px;
    }
</style>

