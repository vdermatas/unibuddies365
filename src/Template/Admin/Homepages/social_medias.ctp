<?php
/**
  * @var \App\View\AppView $this
  */
?><?php use Cake\Core\Configure; ?>
<section class="content-header">
    <h1>
        <?= __('Home Page Sections') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Home Page Sections') ?></a>
        </li>
        <li class="active"> <?= __('Social Media Sections') ?></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Edit Sections (we can make some fields empty , empty fields will not reflect frontend) & links will open on new tab</h3>
                </div>
                    <!-- Main content -->
                <?php echo $this->Html->script('tinymce/tinymce.min.js'); ?>
                <?php $this->TinymceElfinder->defineElfinderBrowser()?>
                <div class="cmsPages form large-9 medium-8 columns content">
                <?= $this->Form->create($socialMedia, ['id' => 'editSection']) ?>
                <fieldset>
                    
                    <?php
                        echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('facebook', ['id' => 'title', 'label' => 'Facebook '])."</div></div>";
                        echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('twitter', ['id' => 'title', 'label' => 'Twitter '])."</div></div>";
                        echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('pinterest', ['id' => 'title', 'label' => 'Google '])."</div></div>";
                        echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('google', ['id' => 'title', 'label' => 'Pineterest '])."</div></div>";
                        echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('linkedin', ['id' => 'title', 'label' => 'Linked In '])."</div></div>";

                    ?>
                </fieldset>
                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</section>
