<?php
/**
  * @var \App\View\AppView $this
  */
?><?php use Cake\Core\Configure; ?>
<section class="content-header">
    <h1>
        <?= __('Home Page Sections') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Home Page Sections') ?></a>
        </li>
        <li class="active"> <?= __('Edit Home Page Section') ?></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                    <!-- Main content -->
                <?php echo $this->Html->script('tinymce/tinymce.min.js'); ?>
                <?php $this->TinymceElfinder->defineElfinderBrowser()?>
                <div class="cmsPages form large-9 medium-8 columns content">
                <?= $this->Form->create($homepageSection, ['type' => 'file' , 'id' => 'editSection']) ?>
                <fieldset>
                    <legend><?= __('Edit Sections') ?></legend>
                    <?php
                        echo "<div class='row'><div class='col-sm-12'>".$this->Form->control('title', ['type' =>'textarea' ,'id' => 'title', 'required' => true, 'label' => 'Title ' ])."</div></div>";

                      
                        echo "<div class='row'><div class='col-xs-12'>".$this->Form->control('content', ['type' => 'textarea', 'label' => 'Content'])."</div></div>"; if($homepageSection->id != 3 && $homepageSection->id != 5) {?>&nbsp;&nbsp;
                                 <img src="<?php echo Configure::read("APP_BASE_URL")."/".$homepageSection->image; ?>" width="200px" height="100px"> 
                                    <?php
                                    echo "&nbsp;&nbsp;".$this->Form->control('image', ['type' => 'file','required' => false, 'label' => '   Change background Image or Section Image']); 
                                }elseif($homepageSection->id == 5) {
                                    echo "<div class='row'><div class='col-xs-6'>".$this->Form->control('map', [ 'label' => 'Enter Map Iframe (copy from google map iframe source)' , 'type' => 'textarea'])."</div></div>";
                                }

                    ?>
                </fieldset>
                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function(){
    $('#editSection').validate({
        rules:{
            title:{
                required: true
            },
            content:{
                required : true
            }
        },
        messages:{
            title:{
                required:"Please enter title"
            },
            content:{
                required:"Content is required"
            }
        }
    });
});
</script>