<?php use Cake\Core\Configure; ?>
<section class="content-header">
    <h1>
        <?= __('Slider Images') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Slider Images') ?></a>
        </li>
        <li class="active"> <?= __('Add Slider Images') ?></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <h1>&nbsp;&nbsp;Upload File</h1>
				<div class="content">
				    <?= $this->Flash->render() ?>
				    <div class="upload-frm">
                        <?php echo $this->Html->script('tinymce/tinymce.min.js'); ?>
                <?php $this->TinymceElfinder->defineElfinderBrowser()?>
				        <?php echo $this->Form->create($uploadData, ['type' => 'file' , 'id' => 'uploadSlider']); ?>
                           <?php echo "<div class='row'><div class='col-sm-6'>".$this->Form->input('title', ['class' => 'form-control'])."</div><div class='col-sm-6'></div></div><div class='row'><div class='col-sm-12'>"; ?>
				           <?php echo $this->Form->input('text', ['type' => 'textarea','class' => 'form-control','label' => 'Slider Text ( optional )'])."</div></div>"; ?>
				             <?php echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('file', ['type' => 'file','label' => 'Upload Image'])."</div><div class='col-sm-6'></div></div>"; ?>
				            <?php echo "<br><br><div classs='row'><div class='col-sm-6'>".$this->Form->button(__('Upload File'), ['type'=>'submit', 'class' => 'btn btn-default submit-btn'])."</div></div>"; ?>
				        <?php echo $this->Form->end(); ?>
				    </div>
				</div><br><br>
				<center><h3><span class="err-message" style="color:red;"></span></h3></center>
				<center><h3><span class="succ-message" style="color:green;"></span></h3></center>
				<h1>&nbsp;&nbsp;Uploaded Files</h1>
				<div class="content overflow-x">
				    <!-- Table -->
				    <table class="table">
				        <tr>
                            <th width="20%">Slider Title</th>
				            <th width="10%">Slider Text</th>
				            <th width="20%">File</th>
				            <th width="22%">Action</th>
				        </tr>
				        <?php if($filesRowNum > 0):$count = 0; foreach($files as $file): $count++; ?>
				        <tr id="<?php echo $file->id; ?>">
                            <td><?php echo $file->title; ?></td>
				            <td><?php echo $file->text; ?></td>
				            <td><img src="<?php echo Configure::read("APP_BASE_URL")."/".$file->url; ?>" width="220px" height="150px"></td>
				            <td>
                                 <?= $this->Html->link(__('Edit Image'), ['action' => 'editSlider', $file->id],[ 'class' => 'btn btn-default']) ?>
                            <input type="button" data-id="<?php echo $file->id; ?>" class="btn btn-default button-click" value="Delete Image"></td>
				        </tr>
				        <?php endforeach; else:?>
				        <tr><td colspan="3">No file(s) found......</td>
				        <?php endif; ?>
				    </table>
				</div>
            </div>
        </div>
    </div>
</section>
<script>
$(document).ready(function(){
    $('#uploadSlider').validate({
        rules:{
            // text:"required",
            title:"required",
            file:"required"
        },
        messages:{
            // text:"Please enter Slider Text",
            title:"Please enter Slider Title",
            file:"Please Add File  "
        }
    });

    $(document).on('click','.button-click',function(e){
    	var id = $(this).attr('data-id');
    	$('.err-message').css('display','none');
    	$('.succ-message').css('display','none');
    	if (confirm('Are You Sure to Remove this slider image ?? ')) {
           
        } else {
            return false;
        }
    	$.ajax({
                  type: "POST",
                  url: "delete-slider",
                  dataType: "json",
                  data: {id:id},
                  success:function(data)
                  {                   
                    if(data.status == 0){
                    	$('.err-message').css('display','block');
                    	$('.err-message').html(data.message);
                    }else{
    					$('.succ-message').css('display','block');
                    	$('.succ-message').html(data.message);
                    	$('#'+id).remove();
                    }
                  },
                  error: function (){ }
            }); 
    });
});
</script>
<style type="text/css">.submit-btn{color:white;}</style>

