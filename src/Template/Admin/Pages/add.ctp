<?php
/**
  * @var \App\View\AppView $this
  */
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="pages form large-9 medium-8 columns content">
                    <?= $this->Form->create($page) ?>
                    <fieldset>
                        <legend><?= __('Add Page') ?></legend>
                        <?php
                            echo $this->Form->control('title');
                            echo $this->Form->control('slug');
                            echo $this->Form->control('excerpt');
                            echo $this->Form->control('content');
                            echo $this->Form->control('created_by');
                            echo $this->Form->control('modified_by');
                            echo $this->Form->control('publish_date');
                            echo $this->Form->control('category_id', ['options' => $categories]);
                            echo $this->Form->control('trashed', ['empty' => true]);
                            echo $this->Form->control('site_id', ['options' => $sites]);
                            echo $this->Form->control('type');
                        ?>
                    </fieldset>
                    <?= $this->Form->button(__('Submit')) ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</section>

