<?php
/**
  * @var \App\View\AppView $this
  */
?>
<?php 
    use Cake\Core\Configure;
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Testimonials') ?></a>
        </li>
        <li class="active"> <?= __('Add Testimonial') ?></li>
    </ol>
</section>
<!-- Main content -->
<?php echo $this->Html->script('tinymce/tinymce.min.js'); ?>
<?php $this->TinymceElfinder->defineElfinderBrowser()?>
<div class="tours form large-9 medium-8 columns content">
    <?= $this->Form->create($testimonial, array('id'=>'edittestimonial','type' => 'file')) ?>
    <fieldset>
        <legend><?= __('Add Testimonial') ?></legend>
        <?php
            echo $this->Form->control('author_name_en',['required' => true, 'label' => '
                Author Name (English)']);
            echo $this->Form->control('author_name_spa',['required' => true, 'label' => '
                Author Name (Spanish)']);
            echo $this->Form->control('company_name_en',['required' => true, 'label' => '
                Company Name (English)']);
            echo $this->Form->control('company_name_spa',['required' => true, 'label' => '
                Company Name (Spanish)']);
            echo $this->Form->control('position_en',['required' => true, 'label' => '
                Position (English)']);
            echo $this->Form->control('position_spa',['required' => true,'label' => '
                Position (Spanish)']);
            echo $this->Form->control('image',['type' => 'file']);
        ?>
            <div class="form-group "><img src='<?php echo Configure::read("APP_BASE_URL").'/' .  $testimonial->image; ?>' style="width: 201px;" /> </div>
        <?php
            echo $this->Form->control('feeback_en',['required' => false,'label' => '
                Feeback (English)']);
            echo $this->Form->control('feeback_spa',['required' => false,'label' => '
               Feeback (Spanish)']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

<script>
    $(document).ready(function() {
        $('#edittestimonial').validate();
    });
</script>