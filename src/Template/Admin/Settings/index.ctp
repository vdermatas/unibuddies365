<?php
/**
  * @var \App\View\AppView $this
  */
?>
<section class="content-header">
    <h1>
        <?= __('Settings') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Settings') ?></a>
        </li>
        <li class="active"> <?= __('Settings') ?></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                         <h3 class="box-title">Settings</h3>
                </div>
                <!-- Main content -->
                <div class="users form large-9 medium-8 columns content">
                    <?= $this->Form->create($setting, ['id' => 'seetings']) ?>
                    <fieldset>
                            <?= "<div class='row'><div class='col-sm-6'>".$this->Form->input('commission',['type' => 'text' , 'label'=>'commission', 'id' => 'commission', 'required' => true])."</div><div class='col-sm-6'>".$this->Form->input('notification_email',['type' => 'text' , 'label'=>'Notification Email', 'id' => 'notification_email', 'required' => true])."</div></div>"?>
                    </fieldset>&nbsp;&nbsp;
                    <?= $this->Form->button(__('Submit')) ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</section><br><Br><br><br><Br><br><br><Br><br><Br>

<script>
$(document).ready(function(){
    $('#seetings').validate({
        rules:{
            commission:{
                     required: true,
                    number : true
            }
        },
        messages:{
            commission:"Please enter old password"
        }
    });
});
</script>
