<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\ManageHomePage $manageHomePage
  */
?>

<div class="manageHomePage view large-9 medium-8 columns content">
    <h3><?= h($manageHomePage->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($manageHomePage->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($manageHomePage->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($manageHomePage->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Best Prices Best Holidays En') ?></h4>
        <?= $this->Text->autoParagraph(h($manageHomePage->best_prices_best_holidays_en)); ?>
    </div>
    <div class="row">
        <h4><?= __('Best Prices Best Holidays Spa') ?></h4>
        <?= $this->Text->autoParagraph(h($manageHomePage->best_prices_best_holidays_spa)); ?>
    </div>
    <div class="row">
        <h4><?= __('Shared Shuttle En') ?></h4>
        <?= $this->Text->autoParagraph(h($manageHomePage->shared_shuttle_en)); ?>
    </div>
    <div class="row">
        <h4><?= __('Shared Shuttle Spa') ?></h4>
        <?= $this->Text->autoParagraph(h($manageHomePage->shared_shuttle_spa)); ?>
    </div>
    <div class="row">
        <h4><?= __('Private Shuttle En') ?></h4>
        <?= $this->Text->autoParagraph(h($manageHomePage->private_shuttle_en)); ?>
    </div>
    <div class="row">
        <h4><?= __('Private Shuttle Spa') ?></h4>
        <?= $this->Text->autoParagraph(h($manageHomePage->private_shuttle_spa)); ?>
    </div>
    <div class="row">
        <h4><?= __('Best And Cheapest Shuttles In Cancun En') ?></h4>
        <?= $this->Text->autoParagraph(h($manageHomePage->best_and_cheapest_shuttles_in_cancun_en)); ?>
    </div>
    <div class="row">
        <h4><?= __('Best And Cheapest Shuttles In Cancun Spa') ?></h4>
        <?= $this->Text->autoParagraph(h($manageHomePage->best_and_cheapest_shuttles_in_cancun_spa)); ?>
    </div>
    <div class="row">
        <h4><?= __('Whats App En') ?></h4>
        <?= $this->Text->autoParagraph(h($manageHomePage->whats_app_en)); ?>
    </div>
    <div class="row">
        <h4><?= __('Whats App Spa') ?></h4>
        <?= $this->Text->autoParagraph(h($manageHomePage->whats_app_spa)); ?>
    </div>
</div>
