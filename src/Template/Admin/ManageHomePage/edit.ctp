<?php
/**
  * @var \App\View\AppView $this
  */
?>
<?php echo $this->Html->script('tinymce/tinymce.min.js'); ?>
<?php $this->TinymceElfinder->defineElfinderBrowser()?>
<div class="manageHomePage form large-9 medium-8 columns content">
    <?= $this->Form->create($manageHomePage) ?>
    <fieldset>
        <legend><?= __('Edit Manage Home Page') ?></legend>
        <?php
            echo $this->Form->control('best_prices_best_holidays_en');
            echo $this->Form->control('best_prices_best_holidays_spa');
            echo $this->Form->control('shared_shuttle_en');
            echo $this->Form->control('shared_shuttle_spa');
            echo $this->Form->control('private_shuttle_en');
            echo $this->Form->control('private_shuttle_spa');
            echo $this->Form->control('best_and_cheapest_shuttles_in_cancun_en');
            echo $this->Form->control('best_and_cheapest_shuttles_in_cancun_spa');
            echo $this->Form->control('whats_app_en');
            echo $this->Form->control('whats_app_spa');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
