<?php
/**
  * @var \App\View\AppView $this
  */
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Manage Home Page') ?></a>
        </li>
        <li class="active"> <?= __('Add') ?></li>
    </ol>
</section>
<!-- Main content -->

<?php echo $this->Html->script('tinymce/tinymce.min.js'); ?>
<?php $this->TinymceElfinder->defineElfinderBrowser()?>
<div class="manageHomePage form large-9 medium-8 columns content">
    <?= $this->Form->create($manageHomePage) ?>
    <fieldset>
       <legend><?= __('Manage Home Page') ?></legend>

        <?php

            echo $this->Form->control('best_prices_best_holidays_en', ['id' => 'best_prices_best_holidays_en', 'required' => true, 'label' => 'Best prices, best holidays English']);
            echo $this->Form->control('best_prices_best_holidays_spa', ['id' => 'best_prices_best_holidays_spa', 'required' => true, 'label' => 'Best prices, best holidays Spanish']);
            echo $this->Form->control('shared_shuttle_en', ['id' => 'shared_shuttle_en', 'required' => true, 'label' => 'Shared shuttle English']);
            echo $this->Form->control('shared_shuttle_spa', ['id' => 'shared_shuttle_spa', 'required' => true, 'label' => 'Shared shuttle Spanish']);
            echo $this->Form->control('private_shuttle_en', ['id' => 'private_shuttle_en', 'required' => true, 'label' => ' Private shuttle English']);
            echo $this->Form->control('private_shuttle_spa', ['id' => 'private_shuttle_spa', 'required' => true, 'label' => ' Private shuttle Spanish']);
            echo $this->Form->control('best_and_cheapest_shuttles_in_cancun_en', ['id' => 'best_and_cheapest_shuttles_in_cancun_en', 'required' => true, 'label' => 'Best and cheapest shuttles in Cancun English']);
            echo $this->Form->control('best_and_cheapest_shuttles_in_cancun_spa', ['id' => 'best_and_cheapest_shuttles_in_cancun_spa', 'required' => true, 'label' => 'Best and cheapest shuttles in Cancun Spanish']);
            echo $this->Form->control('whats_app_en', ['id' => 'whats_app_en', 'required' => true, 'label' => 'Whatsapp Content English']);
            echo $this->Form->control('whats_app_spa', ['id' => 'whats_app_spa', 'required' => true, 'label' => 'Whatsapp Content Spanish']);

        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
