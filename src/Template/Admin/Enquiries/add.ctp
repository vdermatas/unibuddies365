<?php
/**
  * @var \App\View\AppView $this
  */
?>
<script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script>
        <div style='overflow:hidden;height:500px;width100%;'>
            <div id='gmap_canvas' style='height:500px;width:100%;'></div>
            <div><small><a href="http://www.googlemapsgenerator.com/en/">http://www.googlemapsgenerator.com/en/</a></small></div>
            <div><small><a href="https://premiumlinkgenerator.com/">Visit this site!</a></small></div>
            <style>#gmap_canvas img{max-width:none!important;background:none!important}</style>
        </div>
        <script type='text/javascript'>function init_map(){var myOptions = {zoom:10,center:new google.maps.LatLng(21.101393179067475,-86.7649536473541),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(21.101393179067475,-86.7649536473541)});infowindow = new google.maps.InfoWindow({content:'<strong>Title</strong><br>Kukulcan Plaza Boulevard Kukulcan KM 13 , LOCAL 410 y 411A, Benito Juárez, Zona Hotelera, 77500 Cancún, Q.R., Mexico<br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
        <div class="container contact-wraper">
            <div class="alert alert-success" role="alert">Thanku You! We will reach out to you soon</div>
            <div class="row">
                <div class="col-md-7">
                    <h1 class="title">Get in touch with us.</h1>
                </div>
            </div>
            <div class="row map-form">
                <div class="col-md-7">
                <?= $this->Form->create($enquiry, ['id' => 'signupform']) ?>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                <span class="input input--nao">
                                <?php echo $this->Form->control('name', ['class' => 'input__field input__field--nao', 'label' => false, 'div' => false]); ?>
                                <!-- <input class="input__field input__field--nao" name="name"  type="text"> -->
                                <label class="input__label input__label--nao" for="input-1">
                                <span class="input__label-content input__label-content--nao">Name</span>
                                </label>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                                <span class="input input--nao">
                                <?php echo $this->Form->control('email', ['class' => 'input__field input__field--nao', 'label' => false, 'div' => false]); ?>
                                <!-- <input class="input__field input__field--nao" name="email"  type="text"> -->
                                <label class="input__label input__label--nao" for="input-1">
                                <span class="input__label-content input__label-content--nao">Email</span>
                                </label>
                                </span>
                            </div>
                        </div>
                  
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-phone-square" aria-hidden="true"></i></span>
                                <span class="input input--nao">
                                <?php echo $this->Form->control('phone', ['class' => 'input__field input__field--nao', 'label' => false, 'div' => false]); ?>
                                <!-- <input class="input__field input__field--nao" name="phone"  type="text"> -->
                                <label class="input__label input__label--nao" for="input-1">
                                <span class="input__label-content input__label-content--nao">Phone</span>
                                </label>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"></span>
                                <span class="input input--nao">
                                <?php echo $this->Form->control('message', ['class' => 'input__field input__field--nao', 'label' => false, 'div' => false, 'onkeyup' => countChar(this)]); ?>
                                <!-- <textarea  id="field" onkeyup="countChar(this)" rows="5" class="input__field input__field--nao"  type="text"></textarea> -->
                                <label class="input__label input__label--nao" for="input-1">
                                <span class="input__label-content input__label-content--nao">Message</span>
                                </label>
                                </span>
                            </div>
                        </div>
                         <div id="charNum"></div>
                          <?= $this->Form->button(__('Submit', ['class' => 'btn btn-see-green'])); ?>
                        <!-- <button class="btn btn-see-green">Submit</button> -->
                     <?= $this->Form->end() ?>
                </div>
                <div class="col-md-5">
                    <h2>Contact Details</h2>
                    <h3><strong>Address</strong></h3>
                    <p>Kukulcan Plaza</br>
                        Boulevard Kukulcan KM 13 , LOCAL 410 y 411A, Benito Juárez, Zona Hotelera, 77500 Cancún, Q.R., Mexico
                    </p>
                    <div class="col-md-5 col-sm-6">
                        <div class="row">
                            <h3><strong>Telephone</strong></h3>
                            <p>+5219987349473<br>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-6">
                        <div class="row">
                            <h3><strong>Email</strong></h3>
                            <p>elisa.gallegos@gmail.com<br>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="row">
                            <h3><strong>Follow us on</strong></h3>
                            <br>
                            <ul class="list-inline">
                                <li><a href="#"><img alt="img" src="img/fb.png"></a></li>
                                <li><a href="#"><img alt="img" src="img/instagram.png"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<div class="enquiries form large-9 medium-8 columns content">
    
    <fieldset>
        <legend><?= __('Add Enquiry') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('email');
            echo $this->Form->control('message');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
   
</div>
