<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Article[]|\Cake\Collection\CollectionInterface $articles
  */
?>


<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?= __('Enquiry') ?></h3>

                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <div class="enquiries view large-9 medium-8 columns content">
                        <h3><?= h($enquiry->name) ?></h3>
                        <table class="vertical-table table table-bordered">
                            <tr>
                                <th scope="row"><?= __('Name') ?></th>
                                <td><?= h($enquiry->name) ?></td>
                            </tr>
                            <tr>
                                <th scope="row"><?= __('Email') ?></th>
                                <td><?= h($enquiry->email) ?></td>
                            </tr>
                             <tr>
                                <th scope="row"><?= __('Phone') ?></th>
                                <td><?= h($enquiry->phone) ?></td>
                            </tr>
                           
                             <tr>
                                <th scope="row"><?= __('Message') ?></th>
                                <td><?= $this->Text->autoParagraph(h($enquiry->message)); ?></td>
                            </tr>
                        </table>
                        
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
