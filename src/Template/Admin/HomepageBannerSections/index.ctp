<?php
/**
  * @var \App\View\AppView $this
  */
?>
<section class="content-header">
    <h1>
        <?= __('Home Page Banner Section') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Home Page Banner Section') ?></a>
        </li>
        <li class="active"> <?= __('Edit Home Page Banner Section') ?></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                         <h3 class="box-title">Edit Home Page Banner Section</h3>
                </div>
                    <!-- Main content -->
                <?php echo $this->Html->script('tinymce/tinymce.min.js'); ?>
                <?php $this->TinymceElfinder->defineElfinderBrowser()?>
                <div class="cmsPages form large-9 medium-8 columns content">
                <?= $this->Form->create($homepageBannerSections, ['id' => 'editCms','type' => 'file']) ?>
                <fieldset>
                    <?php
                        echo "<div class='row'><div class='col-sm-12'>".$this->Form->control('title', ['id' => 'title' , 'type' => 'textarea' , 'required' => true, 'label' => 'Title'])."</div></div>";
                        echo "<div class='row'><div class='col-sm-12'>".$this->Form->control('content_en', ['id' => 'content', 'required' => true, 'label' => 'Content'])."</div>";

                        echo "<div class='row'><div class='col-sm-12'>".$this->Form->control('content_gre', ['id' => 'content', 'required' => true, 'label' => 'Content Greek' , 'type' => 'textarea'])."</div>";
                       

                        echo "<div class='row'><div class='col-xs-6'>".$this->Form->control('writer_button', ['required' => false, 'label' => 'Writer Button Title'])."</div><div class='col-xs-6'>".$this->Form->control('student_button', ['required' => false, 'label' => 'Student Button Title'])."</div></div>";
                    ?>
                    <div class="row">
                        <div class="col-sm-6 col-md-6">
                          <div class="form-group m-form-group">
                            <label for="image">Background Image ( optional ) </label>
                            <?= $this->Form->control('background_image',['id'=>'image','placeholder' => 'Background Image','label'=>false , 'type' => 'file']) ?>
                          </div><!--end form group-->
                         </div> 
                        <div class="col-sm-6 col-md-6">
                          <div class="form-group m-form-group">
                              <?php if(!empty($homepageBannerSections->background_image)) { ?>            
                                <img src="<?php echo $this->Url->build('/'.$homepageBannerSections->background_image); ?>" width="50%" height="50%">
                              <?php } ?> 
                          </div><!--end form group-->
                        </div>
                      </div>
                </fieldset>
                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function(){
    $('#editCms').validate({
        rules:{
            title:{
                required: true
            },
            content:{
               required: true
            },
            writer_button:{
               required: true
            },
            student_button:{
               required: true
            }
        },
        messages:{
            title:{
                required:"Please enter english title"
            },
            content:{
                required: "Letters, numbers, and underscores only please"
            },
            writer_button:{
                required: "Letters, numbers, and underscores only please"
            },
            student_button:{
                required: "Letters, numbers, and underscores only please"
            }
        }
    });
});
</script>