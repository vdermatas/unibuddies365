<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Transection[]|\Cake\Collection\CollectionInterface $transections
  */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= __('Bookings') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('All Bookings') ?></a>
        </li>
        <li class="active"> <?= __('All Bookings') ?></li>
    </ol>
</section>
<!-- Main content -->

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?= __('All Bookings') ?></h3>
                    <br/><br/>
                    <div class="row">
                    <?= $this->Form->create('', ['type' => 'get']) ?>
						
						 <div class="col-sm-2">
                            <div class="form-group">
                                <div class='input-group date' id='BookingDate'>
                                    <?php echo $this->Form->control('booking_date',['class' => 'form-control','div' => false,'label' => false, 'placeholder' => 'Booking date',  'value' => (!empty($this->request->getQuery()['booking_date']))?$this->request->getQuery()['booking_date']:'' ]); ?>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div><!--/col-->    
						<div class="col-sm-2">
                            <div class="form-group">
                                <div class='input-group date' id='StartDatePicker'>
                                    <?php echo $this->Form->control('start_date',['class' => 'form-control','div' => false,'label' => false, 'placeholder' => 'From date' ,'value' => (!empty($this->request->getQuery()['start_date']))?$this->request->getQuery()['start_date']:'' ]); ?>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div><!--/col-->                       
                        <div class="col-sm-2">
                            <div class="form-group">
                                <div class='input-group date' id='EndDatePicker'>
                                    <?php  echo $this->Form->control('end_date',['class' => 'form-control','div' => false,'label' => false, 'placeholder' => 'To date', 'value' => (!empty($this->request->getQuery()['end_date']))?$this->request->getQuery()['end_date']:'' ]); ?>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div><!--/col-->

                        <div class="col-sm-2">
                            <div class="form-group">
                                <div class='input-group'>
                                    <?php
                                        echo $this->Form->select('booking_type',['Shuttles' => 'Shuttle', 'Tours' => 'Tour'],['empty' => 'Booking Type', 'class' => 'form-control','div' => false,'label' => false, 'value' => (!empty($this->request->getQuery()['booking_type']))?$this->request->getQuery()['booking_type']:'' ]); 
                                    ?>
                                </div>
                            </div>
                        </div><!--/col-->

                        <div class="col-sm-2">
                            <div class="form-group">
                                <div class='input-group'>
                                    <?php  echo $this->Form->control('booked_by',['type' => 'text', 'class' => 'form-control','div' => false,'label' => false, 'placeholder' => 'Booked by',  'value' => (!empty($this->request->getQuery()['booked_by']))?$this->request->getQuery()['booked_by']:'' ]); ?>
                                </div>
                            </div>
                        </div><!--/col-->

                        <div class="col-sm-2">
                            <div class="form-group">
                                <div class='input-group'>
                                    <?php  echo $this->Form->submit('Search',['class' => 'form-control','div' => false,'label' => false]); ?>
                                </div>
                            </div>
                        </div><!--/col-->
                        <?= $this->Form->end(); ?>                        
                    </div>
                </div>
                <!-- /.box-header -->

                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Sr.</th>
                                <th scope="col"><?= $this->Paginator->sort('booking_type') ?></th>
                                <th scope="col">Name</th>
                                <th scope="col"><?= $this->Paginator->sort('total_price',['Amount']) ?></th>
                                <th scope="col"><?= $this->Paginator->sort('created','Date') ?></th>
                                <th scope="col" class="actions"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; $priceTotal = 0; foreach ($bookings as $booking): ?>
                            <tr>
                                <td><?= $i ?></td>      
                                <td><?= $booking->booking_type ?></td>
                               
                                <td><?= $booking->billing_detail->first_name.' '.$booking->billing_detail->last_name ?></td>
                                <td><?= $this->Number->format($booking->total_price) ?></td>
                                    <?php $priceTotal+=$this->Number->format($booking->total_price) ?>
                                <td><?= h($booking->created) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-eye')).'', array('action' => 'view-bookings', $booking->booking_type, $booking->id), array('escape' => false, 'class' => 'view', 'title' => 'view')) ?>
                                </td>
                            </tr>
                            <?php   $i++;  endforeach;  ?>

                            <tr>
                                <td colspan="3" style="text-align: right;"><strong>Total Price</strong></td>
                                <td style="text-align: left;"><strong><?= $priceTotal ?></strong></td>
                                <td></td>
                                <td></td>
                            </tr>

                       </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">

                        <li><?=$this->Paginator->first('<< ' . __('First ')) ?></li>
                        <li><?=$this->Paginator->prev('< ' . __('Previous ')) ?>
                        <li><?=$this->Paginator->numbers() ?></li>
                        <li><?=$this->Paginator->next(__('Next ') . '>') ?></li>
                        <li><?=$this->Paginator->last(__('Last') . ' >>') ?></li>
                    </ul>
                    <p><?=$this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->

<?php echo $this->element('report_booking_details');?>


<script type="text/javascript">
    $(function() {
        $('#StartDatePicker, #EndDatePicker', '#BookingDate').datetimepicker({
            format: 'MM/DD/YYYY',
            sideBySide: true,
            //defaultDate: 'now',
            keepOpen: false,
            useCurrent: true
        });
    });
</script>

