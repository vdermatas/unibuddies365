<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Transection[]|\Cake\Collection\CollectionInterface $transections
  */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= __('Reports') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('All Transactions') ?></a>
        </li>
        <li class="active"> <?= __('All Transactions') ?></li>
    </ol>
</section>
<!-- Main content -->

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?= __('All Transactions') ?></h3>
                    
                                          
                    </div>
                <div class="row">
                <div class="col-xs-12">
                     <?= $this->Form->create('', ['type' => 'get']) ?>
                     <br/>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <div class='input-group date' id='StartDatePicker'>
                                    <?php echo $this->Form->control('start_date',['class' => 'form-control','div' => false,'label' => false, 'placeholder' => 'From date' ,'value' => (!empty($this->request->getQuery()['start_date']))?$this->request->getQuery()['start_date']:'' ]); ?>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div><!--/col-->                       
                        <div class="col-sm-3">
                            <div class="form-group">
                                <div class='input-group date' id='EndDatePicker'>
                                    <?php  echo $this->Form->control('end_date',['class' => 'form-control','div' => false,'label' => false, 'placeholder' => 'To date', 'value' => (!empty($this->request->getQuery()['end_date']))?$this->request->getQuery()['end_date']:'' ]); ?>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div><!--/col-->

                        <div class="col-sm-3">
                            <div class="form-group">
                                <div class='input-group'>
                                    <?php  echo $this->Form->control('transaction_id',['type' => 'text', 'class' => 'form-control','div' => false, 'placeholder' => 'Transaction id', 'label' => false, 'value' => (!empty($this->request->getQuery()['transaction_id']))?$this->request->getQuery()['transaction_id']:'' ]); ?>
                                </div>
                            </div>
                        </div><!--/col-->

                        <div class="col-sm-2">
                            <div class="form-group">
                                <div class='input-group'>
                                    <?php  echo $this->Form->submit('Search',['class' => 'form-control','div' => false,'label' => false]); ?>
                                </div>
                            </div>
                        </div><!--/col-->
                        <?= $this->Form->end(); ?>
                </div>
                    
                    
                
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Sr.</th>
                                <th scope="col"><?= $this->Paginator->sort('transection_id','Transaction Id') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('paypal_transection_id', 'Paypal Transaction Id') ?></th>
                                 <th scope="col">Name</th>
                                <th scope="col"><?= $this->Paginator->sort('amount') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('created','Date') ?></th>
                                <th scope="col" class="actions"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; $priceTotal = 0; foreach ($transections as $transection): //pr($transection); ?>
                            <tr>
                                <td><?= $i ?></td>      
                                <td><?= $transection->transection_id ?></td>
                                <td><?= $transection->paypal_transection_id ?></td>
                                <td><?= $transection->billing_detail->first_name.' '.$transection->billing_detail->last_name ?></td>
                                <td>$ <?= $transection->amount ?></td>
                                    <?php $priceTotal+=$transection->amount ?>
                                <td><?= h($transection->created) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-eye')).'',  ['controller' => 'orders' ,'action' => 'orderview', $transection->billing_detail->frontend_order_id], array('escape' => false, 'class' => 'view-tour', 'target' => '_blank')) ?>
                                </td>
                            </tr>
                            <?php   $i++;  endforeach;  ?>

                            <tr>
                                <td colspan="4" style="text-align: right;"><strong>Total Price</strong></td>
                                <td style="text-align: left;"><strong>$ <?= $priceTotal ?></strong></td>
                                <td></td>
                                <td></td>
                            </tr>

                       </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">

                        <li><?=$this->Paginator->first('<< ' . __('First ')) ?></li>
                        <li><?=$this->Paginator->prev('< ' . __('Previous ')) ?>
                        <li><?=$this->Paginator->numbers() ?></li>
                        <li><?=$this->Paginator->next(__('Next ') . '>') ?></li>
                        <li><?=$this->Paginator->last(__('Last') . ' >>') ?></li>
                    </ul>
                    <p><?=$this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->

<?php echo $this->element('report_transaction_details');?>

<script type="text/javascript">
    $(function() {
        $('#StartDatePicker, #EndDatePicker').datetimepicker({
            format: 'MM/DD/YYYY',
            sideBySide: true,
            //defaultDate: 'now',
            keepOpen: false,
            useCurrent: true
        });
    });
</script>
