<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Transection $transection
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Transection'), ['action' => 'edit', $transection->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Transection'), ['action' => 'delete', $transection->id], ['confirm' => __('Are you sure you want to delete # {0}?', $transection->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Transections'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Transection'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Billing Details'), ['controller' => 'BillingDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Billing Detail'), ['controller' => 'BillingDetails', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="transections view large-9 medium-8 columns content">
    <h3><?= h($transection->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Billing Detail') ?></th>
            <td><?= $transection->has('billing_detail') ? $this->Html->link($transection->billing_detail->id, ['controller' => 'BillingDetails', 'action' => 'view', $transection->billing_detail->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($transection->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Transection') ?></th>
            <td><?= $this->Number->format($transection->transection) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Amount') ?></th>
            <td><?= $this->Number->format($transection->amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($transection->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($transection->modified) ?></td>
        </tr>
    </table>
</div>
