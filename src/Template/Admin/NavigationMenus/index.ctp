<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\NavigationMenu[]|\Cake\Collection\CollectionInterface $navigationMenus
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Navigation Menu'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="navigationMenus index large-9 medium-8 columns content">
    <h3><?= __('Navigation Menus') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('parent_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('description') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($navigationMenus as $navigationMenu): ?>
            <tr>
                <td><?= $this->Number->format($navigationMenu->id) ?></td>
                <td><?= $navigationMenu->has('parent_navigation_menu') ? $this->Html->link($navigationMenu->parent_navigation_menu->name, ['controller' => 'NavigationMenus', 'action' => 'view', $navigationMenu->parent_navigation_menu->id]) : '' ?></td>
                <td><?= h($navigationMenu->name) ?></td>
                <td><?= h($navigationMenu->description) ?></td>
                <td><?= h($navigationMenu->created) ?></td>
                <td><?= h($navigationMenu->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $navigationMenu->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $navigationMenu->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $navigationMenu->id], ['confirm' => __('Are you sure you want to delete # {0}?', $navigationMenu->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
