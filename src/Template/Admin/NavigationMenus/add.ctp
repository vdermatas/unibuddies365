<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Navigation Menus'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Parent Navigation Menus'), ['controller' => 'NavigationMenus', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Parent Navigation Menu'), ['controller' => 'NavigationMenus', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="navigationMenus form large-9 medium-8 columns content">
    <?= $this->Form->create($navigationMenu) ?>
    <fieldset>
        <legend><?= __('Add Navigation Menu') ?></legend>
        <?php
            echo $this->Form->control('parent_id', ['options' => $parentNavigationMenus,
									'empty' => 'No parent menu']);
            echo $this->Form->control('name');
            echo $this->Form->control('description');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
