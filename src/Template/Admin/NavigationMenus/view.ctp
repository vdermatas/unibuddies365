<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\NavigationMenu $navigationMenu
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Navigation Menu'), ['action' => 'edit', $navigationMenu->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Navigation Menu'), ['action' => 'delete', $navigationMenu->id], ['confirm' => __('Are you sure you want to delete # {0}?', $navigationMenu->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Navigation Menus'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Navigation Menu'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Parent Navigation Menus'), ['controller' => 'NavigationMenus', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Parent Navigation Menu'), ['controller' => 'NavigationMenus', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="navigationMenus view large-9 medium-8 columns content">
    <h3><?= h($navigationMenu->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Parent Navigation Menu') ?></th>
            <td><?= $navigationMenu->has('parent_navigation_menu') ? $this->Html->link($navigationMenu->parent_navigation_menu->name, ['controller' => 'NavigationMenus', 'action' => 'view', $navigationMenu->parent_navigation_menu->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($navigationMenu->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description') ?></th>
            <td><?= h($navigationMenu->description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($navigationMenu->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lft') ?></th>
            <td><?= $this->Number->format($navigationMenu->lft) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rght') ?></th>
            <td><?= $this->Number->format($navigationMenu->rght) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($navigationMenu->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($navigationMenu->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Navigation Menus') ?></h4>
        <?php if (!empty($navigationMenu->child_navigation_menus)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Parent Id') ?></th>
                <th scope="col"><?= __('Lft') ?></th>
                <th scope="col"><?= __('Rght') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($navigationMenu->child_navigation_menus as $childNavigationMenus): ?>
            <tr>
                <td><?= h($childNavigationMenus->id) ?></td>
                <td><?= h($childNavigationMenus->parent_id) ?></td>
                <td><?= h($childNavigationMenus->lft) ?></td>
                <td><?= h($childNavigationMenus->rght) ?></td>
                <td><?= h($childNavigationMenus->name) ?></td>
                <td><?= h($childNavigationMenus->description) ?></td>
                <td><?= h($childNavigationMenus->created) ?></td>
                <td><?= h($childNavigationMenus->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'NavigationMenus', 'action' => 'view', $childNavigationMenus->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'NavigationMenus', 'action' => 'edit', $childNavigationMenus->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'NavigationMenus', 'action' => 'delete', $childNavigationMenus->id], ['confirm' => __('Are you sure you want to delete # {0}?', $childNavigationMenus->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
