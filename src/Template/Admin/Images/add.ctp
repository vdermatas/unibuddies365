<?php
/**
  * @var \App\View\AppView $this
  */
  
use Cake\Routing\Router;
?>


<div class="images form large-9 medium-8 columns content">
    <?= $this->Form->create($image) ?>
    <fieldset>
        <legend><?= __('Add Image') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('path');
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('gallery_id', ['options' => $galleries]);
            echo $this->Form->control('is_deleted');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
