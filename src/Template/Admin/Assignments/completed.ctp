<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Article[]|\Cake\Collection\CollectionInterface $articles
  */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= __('Completed Assignments') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Completed Assignments') ?></a>
        </li>
        <li class="active"> <?= __('All Completed Assignments') ?></li>
    </ol>
</section>
<!-- Main content -->

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?= __('Completed Assignments') ?></h3>

                     <?php echo $this->element("search_form"); ?>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th scope="col">Sr. Num</th>
                                <th scope="col"><?= $this->Paginator->sort('title_en', 'Title') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('user_id', 'Posted By') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('price', 'Price') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('first_name', 'Assigned to') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('status', 'Payment status') ?></th>
                                <th scope="col" class="actions"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; foreach ($assignments as $assignment): ?>
                            <tr>
                                <td><?= $i ?></td>                
                                <td><?= h($assignment->title) ?></td>
                                <td> <a target="_blank" href="<?php echo $this->Url->build('/admin/users/edit-student/'.$assignment['user']['id']); ?>"><?= h($assignment['user']['first_name']) ?></a>
                                </td>
                                <td><?= h($assignment['assignment_offers'][0]['price']) ?></td>
                                <td>
                                   <a target="_blank" href="<?php echo $this->Url->build('/admin/users/edit-writer/'.$assignment['assignment_offers'][0]['user']['id']); ?>"><?= h($assignment['assignment_offers'][0]['user']['first_name']) ?> </a>                                       
                                </td>
                                <td><?php if($assignment->status == 3){echo '<button class="btn btn-danger">Payment Not Done</button>';}else{echo '<button class="btn btn-success">Payment Done</button>';} ?></td>
                                <td class="actions">					
                					<?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-eye')).'', array('action' => 'view', $assignment->id), array('escape' => false)) ?> 
                					
                                </td>
                            </tr>
                            <?php $i++; endforeach; ?>
                        </tbody>
                    </table>
                     </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">

                        <li><?=$this->Paginator->first('<< ' . __('First ')) ?></li>
                        <li><?=$this->Paginator->prev('< ' . __('Previous ')) ?>
                        <li><?=$this->Paginator->numbers() ?></li>
                        <li><?=$this->Paginator->next(__('Next ') . '>') ?></li>
                        <li><?=$this->Paginator->last(__('Last') . ' >>') ?></li>
                    </ul>
                    <p><?=$this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->

