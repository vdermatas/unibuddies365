<?php
/**
  * @var \App\View\AppView $this
  */
?>
<section class="content-header">
    <h1>
        <?= __('View assignment') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Assignments') ?></a>
        </li>
        <li class="active"> <?= __('View assignment') ?></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                     <h3 class="box-title">Title : <?php echo $assignment->title; ?></h3>
                     <span class="pull-right btn btn-info"> 
                        <?php if($assignment->status == 4 ){ echo " Paid"; }else{ echo "Not Paid"; } ?>
                    </span>
                    <!-- <span class="pull-right btn btn-info">  -->
                        <?php if($assignment->status == 1 || $assignment->status == 2 ){ ?> 
                        <?= $this->Form->postLink(__('Mark as Completed'), ['action' => 'markComplete', $assignment->id], ['confirm' => __('Are you sure you want to mark this assignment completed and send email to student ?', $assignment->id) , 'class' => 'pull-right btn btn-success' ]) ?> <?php  }else{ echo '<span class="pull-right btn btn-info">Completed</span>'; } ?>
                    <!-- </span> -->
                </div>
                <div class="container">
                    <?php
                        $documentType =  [1=>'Thesis',2 => 'Bachelors thesis' , 3 => 'Doctoral dissertation' , 4 => 'Statistical analysis' , 5  => 'Corrections' , 6  => 'Semester assigment' , 7 => 'Postgraduate assignment' , 8 => 'Presentation' , 9 => 'Scientific Article' , 10 => 'Translation' , 11 => 'Citation Resources'];

                        $thematicUnit = [1=>'Finance and Economics',2 => 'Law' , 3 => 'Business and Management' , 4 => 'Computing and ICT' , 5  => 'Contruction and Engineering' , 6  => 'Education' , 7 => 'Health and Nursing' , 8 => 'Marketing' , 9 => 'Mathematics and Statistics' , 10 => 'Medical Science' , 11 => 'Politics and International Relations' , 12 => 'Science' , 13 => 'Accounting' , 14 => 'Social Sciences' , 15 => 'Shipping and Maritime' , 16 => 'Other'] ;

                        $language = [1=>'English',2 => 'Greek' ] ;
                     ?>
                    <div class="row ">
                        <div class="col-sm-6">
                            <b>Posted By:</b>
                             <a href="<?php echo $this->Url->build('/admin/users/edit-student/'.$assignment['user']['id']) ?>" target="_blank"> <?php echo $assignment['user']['first_name']; ?></a>
                        </div>
                        <div class="col-sm-6">
                            
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-sm-6">
                            <b>Type of Document:</b> <?php echo $documentType[$assignment->document_type]; ?>
                        </div>
                        <div class="col-sm-6">
                            <b>Language:</b> <?php echo $language[$assignment->language]; ?>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-sm-6">
                            <b>Thematic Unity:</b> <?php echo $thematicUnit[$assignment->thematic_unity]; ?>
                        </div>

                        <!-- <div class="col-sm-6">
                                            <b>Pages Required</b> : 4
                                        </div> -->

                    </div>
                    <div class="row ">
                        <div class="col-sm-6">
                            <b>Date to be Delivered </b> : <?php echo date('d F, Y' , strtotime($assignment->delivery_date)); ?>
                        </div>

                    </div>
                    <div class="row ">
                        <div class="col-sm-12"><b>Reference Url </b>: <a target="_blank" href="http://<?php echo $assignment->reference_url; ?>"><?php echo $assignment->reference_url; ?></a>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-sm-6">
                            <b>Description</b> : <?php echo $assignment->description; ?>
                        </div>

                    </div>
                    <?php if(!empty($assignment->attachment_file)){ ?>
                    <div class="row  download-file">
                        <div class="col-sm-12">
                            <a target="_blank" href="<?php echo $this->Url->build('/uploads/student-assignment/'.$assignment->attachment_file) ?>">1. <?php echo $assignment->attachment_file; ?> <img src="<?php echo $this->Url->build('/images/file-download.png'); ?>"></a>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                         <h3 class="box-title">Offers </h3>
                </div>
                <div class="container">
                    <table class="table table-stripped">
                        <thead>
                            <th>Writer Name</th>
                            <th>Actual Price from writer</th>
                            <th>Our Commission</th>
                            <th>Status</th>
                        </thead>
                        <tbody>
                            <?php foreach ($assignment['assignment_offers'] as $key => $offers) { ?>
                                <tr>
                                    <td>
                                        <a href="<?php echo $this->Url->build('/admin/users/edit-writer/'.$offers['user']['id']) ?>" target="_blank">
                                            <?php echo $offers['user']['first_name']; ?>
                                        </a>
                                    </td>
                                    <td>€ <?php echo $offers['price']; ?></td>
                                    <td><?php echo $assignment['admin_commission']; ?> % </td>
                                    <td>
                                        <?php if($offers['status'] == 0 ){ ?>
                                            <span class="btn btn-danger">Not Accepted</span>
                                         <?php  }else { ?> 
                                             <span class="btn btn-success">Accepted</span>
                                         <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                         <h3 class="box-title">Delivery Schedules </h3>
                </div>
                <div class="container">
                    <table class="table table-stripped table-responsive">
                        <thead>
                            <th>Description</th>
                            <th>file</th>
                            <th>Delivery date</th>
                        </thead>
                        <tbody>
                            <?php foreach ($assignment['assignment_offers'] as $key => $offers) { 
                                foreach($offers['assignment_delivery_schedules'] as $keys => $delivery){
                            ?>
                                <tr>
                                    <td width="35%">
                                        <?php echo $delivery['description']; ?>
                                    </td>
                                    <td width="35%">
                                       <a target="_blank" href="<?php echo $this->Url->build('/uploads/student-assignment/'.$delivery['file']) ?>"> <?php echo $delivery['file']; ?></a>
                                    </td>
                                    <td width="30%">
                                        <?php echo date( 'D M, Y' ,strtotime($delivery['delivery_date'])); ?>
                                    </td>
                                </tr>
                            <?php 
                                }
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-6">
            <div class="box">
                <div class="box-header">
                         <h3 class="box-title">Student gave Review to Writer </h3>
                </div>
                <div class="container">
                    <?php if(!empty($assignment['rating_by_student'])){ ?> 
                    <table class="table table-stripped table-responsive">
                        <thead>
                            <th>Rating</th>
                            <th>Message</th>
                        </thead>
                        <tbody>
                            <td> 5 / <?php echo $assignment['rating_by_student']['rating']; ?></td>
                            <td><?php echo $assignment['rating_by_student']['message']; ?></td>
                        </tbody>
                    </table>
                    <?php }else{
                        echo "No Records Found";
                    } ?>
                </div>
            </div>
        </div>
        <!-- <div class="col-xs-6">
            <div class="box">
                <div class="box-header">
                         <h3 class="box-title">Writer gave Review to student </h3>
                </div>
                <div class="container">                    
                    <?php if(!empty($assignment['rating_by_writer'])){ ?> 
                    <table class="table table-stripped table-responsive">
                        <thead>
                            <th>Rating</th>
                            <th>Message</th>
                        </thead>
                        <tbody>
                            <td> 5 / <?php echo $assignment['rating_by_writer']['rating']; ?></td>
                            <td><?php echo $assignment['rating_by_writer']['message']; ?></td>
                        </tbody>
                    </table>
                    <?php }else{
                        echo "No Records Found";
                    } ?>
                </div>
            </div>
        </div> -->
    </div>

</section>


<!-- <br><br><br><br><br><br><br><br><br><br><br><br><br><br> -->

<script>
$(document).ready(function(){
    $('#seetings').validate({
        rules:{
            commission:{
                    required,
                    number
            }
        },
        messages:{
            commission:"Please enter old password"
        }
    });
});
</script>
