<?php
/**
  * @var \App\View\AppView $this
  */
?>
<section class="content-header">
    <h1>
        <?= __('View Delivery Schedules') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Delivery Schedules') ?></a>
        </li>
        <li class="active"> <?= __('View Delivery Schedules') ?></li>
    </ol>
</section>
<?php 
$i = 1;
foreach ($schedules as $key => $schedule) { ?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                         <h3 class="box-title">Deliverable <?php echo $i; ?></h3>
                </div><br>
                <div class="container">
                 
                    <div class="row">
                        <div class="col-sm-3">
                            <b>Description :</b>
                        </div>
                        <div class="col-sm-9">
                            <?php echo $schedule['description']; ?>
                        </div>
                    </div>
                </div><br>
                <div class="container">
                 
                    <div class="row">
                        <div class="col-sm-3">
                            <b>Delivery Scheduled Date :</b>
                        </div>
                        <div class="col-sm-9">
                            <?php echo date('m/d/Y',strtotime($schedule['delivery_date'])); ?>
                        </div>
                    </div>
                </div><br>
                <?php if($schedule['file'] != ''){ ?>
                <div class="container">
                 
                    <div class="row">
                        <div class="col-sm-3">
                            <b>File :</b>
                        </div>
                        <div class="col-sm-9">
                            <a href="<?php echo $this->Url->build('/uploads/student-assignment/'.$schedule['file']);?>"><?php echo $schedule['file']; ?></a>
                        </div>
                    </div>
                </div><br>
                <?php } ?>
                <div class="container">
                 
                    <div class="row">
                        <div class="col-sm-3">
                            <b>Delivery Notes :</b>
                        </div>
                        <div class="col-sm-9">
                            <?php echo $schedule['delivery_notes']; ?>
                        </div>
                    </div>
                </div><br>
            </div>
        </div>
    </div>
</section>
<?php $i++; } ?>


<br><br><br><br><br><br><br><br><br><br><br><br><br><br>

<script>
$(document).ready(function(){
    $('#seetings').validate({
        rules:{
            commission:{
                    required,
                    number
            }
        },
        messages:{
            commission:"Please enter old password"
        }
    });
});
</script>
