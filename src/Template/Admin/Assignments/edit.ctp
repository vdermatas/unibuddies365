<?php
/**
  * @var \App\View\AppView $this
  */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= __('Assignements') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Assignements') ?></a>
        </li>
        <li class="active"> <?= __('Edit Commissions') ?></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                         <h3 class="box-title">Edit Commission</h3>
                </div>
                    <!-- Main content -->
                <div class="cmsPages form large-9 medium-8 columns content">
                    <?= $this->Form->create($assignment, ['id' => 'editCommission']) ?>
                    <fieldset>
                        <?php
                            echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('admin_commission', ['id' => 'admin_commission', 'required' => true, 'label' => 'Title '])."</div><div class='col-sm-6'>";
                        ?>
                    </fieldset>
                    <?= $this->Form->button(__('Submit')) ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function(){
    $('#editCommission').validate({
        rules:{
            admin_commission:"required"
        },
        messages:{
            admin_commission:"Please enter admin commission"
        }
    });
});
</script>