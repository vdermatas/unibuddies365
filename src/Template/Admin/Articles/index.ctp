<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Article[]|\Cake\Collection\CollectionInterface $articles
  */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= __('Articles') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Articles') ?></a>
        </li>
        <li class="active"> <?= __('All Articles') ?></li>
    </ol>
</section>
<!-- Main content -->

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?= __('Articles') ?></h3>
                    <?php echo $this->element("search_form"); ?>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th scope="col">Sr. Num</th>
                                <th scope="col"><?= $this->Paginator->sort('category_id') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('title') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('slug') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                                <th scope="col" class="actions"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1 ; foreach ($articles as $article): ?>
                            <tr>
                                <td><?= $i ?></td>
                                <td><?= $article->has('category') ? $this->Html->link($article->category->name, ['controller' => 'Categories', 'action' => 'view', $article->category->id]) : '' ?></td>
                                <td><?= h($article->title) ?></td>
                                <td><?= h($article->slug) ?></td>
                                <td><?= h($article->created) ?></td>
                                <td><?= h($article->modified) ?></td>
                              
                        
                                <td class="actions">
                                    
                                    <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-pencil-square-o')).'', array('action' => 'edit', $article->id), array('escape' => false)) ?> |
                                    
                                    <?= $this->Form->postLink(__('<i class="fa fa-trash-o"></i>'),['action' => 'delete', $article->id],['escape' => false,  'confirm' => __('Are you sure you want to delete {0}?', $article->title)])
                                    ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">

                        <li><?=$this->Paginator->first('<< ' . __('first ')) ?></li>
                        <li><?=$this->Paginator->prev('< ' . __('previous ')) ?>
                        <li><?=$this->Paginator->numbers() ?></li>
                        <li><?=$this->Paginator->next(__('next ') . '>') ?></li>
                        <li><?=$this->Paginator->last(__('last') . ' >>') ?></li>
                    </ul>
                    <p><?=$this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
