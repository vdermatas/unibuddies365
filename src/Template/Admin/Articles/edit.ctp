<?php
/**
  * @var \App\View\AppView $this
  */
?>
<section class="content-header">
    <h1>
        <?= __('Articles') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Articles') ?></a>
        </li>
        <li class="active"> <?= __('Edit Article') ?></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <?php echo $this->Html->script('tinymce/tinymce.min.js'); ?>
                <?php $this->TinymceElfinder->defineElfinderBrowser()?>
                <div class="articles form large-9 medium-8 columns content">
                    <?= $this->Form->create($article) ?>
                    <fieldset>
                        <legend><?= __('Edit Article') ?></legend>
                        <?php
                             echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('category_id', ['options' => $categories])."</div><div class='col-sm-6'>";
                            echo $this->Form->control('title')."</div></div>";
                            echo "<div class='row'><div class='col-xs-12'>".$this->Form->control('content')."</div></div>";
                        ?>
                    </fieldset>
                    <?= $this->Form->button(__('Submit')) ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</section>

