<script>
    jQuery(document).ready(function(){
        jQuery(".view").click(function(event) {
            event.preventDefault();
            var urlLoc = $(this).attr('href');
            $.getJSON(urlLoc, function(data) {
				var template = $("#ViewBookingTemplate").html();
                $("#ViewBookingModal").html(_.template(template, { data: data}));
                $('#ViewBookingModal').modal('show');
            });
        });
    });
</script>
<!-- Modal -->

<div aria-labelledby="ViewBookingLabel" class="modal fade" id="ViewBookingModal" role="dialog" tabindex="-1">
</div>

<script type="text/html" id='ViewBookingTemplate'>
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Booking</h4>
            </div>

            <div class="modal-body">
                <div class="row">
                        <div class="col-lg-8 col-md-8">

                            <div class="row">
                                
                                <div class="col-lg-12 col-md-12">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#Summery" class=""><!-- <i class="fa fa-indent"></i> --> Booking Detail</a></li>
                                        <li><a data-toggle="tab" href="#Contact" class=""><!-- <i class="fa fa-bookmark-o"></i> --> Billing Details</a></li>
                                        <li><a data-toggle="tab" href="#Address" class=""><!-- <i class="fa fa-home"></i> --> Transaction Details</a></li>
                                    </ul>

                                    <div class="tab-content">
                                        <div id="Summery" class="tab-pane fade in active">
                                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
											
												<% if(data.booking.tour != undefined) { %>
                                                  <div class="panel panel-default">
                                                    
                                                    <div class="panel-heading" role="tab" id="headingOne">
                                                      <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                          Tour
                                                        </a>
                                                      </h4>
                                                    </div>

                                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                      <div class="panel-body">
                                                        <% if(data.booking.tour.length == 0) {
                                                             %>
                                                                <center><h4>--No Any Booking--</h4></center>
                                                             <%
                                                        }else{  %>
                                                            <h4><%= data.booking.tour.name_en %></h4>
                                                            <div class="table-responsive panel">
                                                                <table class="table table-striped">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="">Adults</td>
                                                                            <td><%= data.booking.adults %></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="">Childs</td>
                                                                            <td><%= data.booking.childs %></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="">Tour Booking Date</td>
                                                                            <td><%= data.booking.tour_booking_date %></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="">Booking At</td>
                                                                            <td>
                                                                                <% var dateTimeBook = new Date(data.booking.created);%>

                                                                                <%= moment(dateTimeBook).format("MM/DD/YYYY h:mm a") %>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="">Price</td>
                                                                            <td><%= data.booking.total_price %></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>  
                                                            <br/>
                                                        <% } %>
                                                      </div>
                                                    </div>
                                                  </div>
												<% } %>
												
                                                <% if(data.booking.drop_of_location != undefined) { %>
                                                  <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="headingTwo">
                                                      <h4 class="panel-title">
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                          Shuttle
                                                        </a>
                                                      </h4>
                                                    </div>
                                                    <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                                                      <div class="panel-body">
                                                        <% 
                                                            if(data.booking.drop_of_location.length == 0) {
                                                             %>
                                                                <center><h4>--No Any Booking--</h4></center>
                                                             <%
                                                        }else{
                                                            %>
                                                            <h4><%= data.booking.drop_of_location.location_en %></h4>
                                                            <div class="table-responsive panel">
                                                                <table class="table table-striped">
                                                                    <tbody>
                                                                       
                                                                        <tr>
                                                                            <td class=""> Travellers</td>
                                                                            <td><%= data.booking.travellers.replace("_","-") %></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="">Drop off Location</td>
                                                                            <td><%= data.booking.drop_of_location.location_en %></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class=""> Airport Pickup</td>
                                                                            <td><%= data.booking.is_airport_pickup == 1?'Yes':'No' %></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class=""> Pick Up Location</td>
                                                                            <td><%= data.booking.pick_up_location != ''? booking.pick_up_location:'--' %></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="">Round Trip</td>
                                                                            <td><%= data.booking.is_round_trip == 1?'Yes':'No' %></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="">Arrival</td>
                                                                            <td><%= data.booking.arrival %></td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td class="">Booked At</td>
                                                                            <td>
                                                                                <% var dateTimeBook = new Date(data.booking.created);%>

                                                                                <%= moment(dateTimeBook).format("MM/DD/YYYY h:mm a") %>
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td class="">Price</td>
                                                                            <td><%= data.booking.total_price %></td>
                                                                        </tr> 

                                                                    </tbody>
                                                                </table>
                                                            </div>  
                                                            <br/>
                                                        <% } %>

                                                      </div>
                                                    </div>
                                                  </div>
                                                <% } %>
                                            </div>
                                        </div>
										
                                        <div id="Address" class="tab-pane fade">
                                            <div class="table-responsive panel">
                                                <table class="table table-striped">
                                                    <tbody>
                                                        <tr>
                                                            <td class="">Transaction Id</td>
                                                            <td><%= data.booking.billing_detail.transections.transection_id %></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="">Paypal Transaction Id</td>
                                                            <td><%= data.booking.billing_detail.transections.paypal_transection_id %></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="">Total Price</td>
                                                            <td><%= data.booking.billing_detail.transections.amount %></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="">Transaction Date</td>
                                                            <td>
                                                                <% var dateTimeBook = new Date(data.booking.billing_detail.transections.created);%>
                                                                <%= moment(dateTimeBook).format("MM/DD/YYYY h:mm a") %>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
										
                                        <div id="Contact" class="tab-pane fade">
                                            <div class="table-responsive panel">
                                                <table class="table table-striped">
                                                    <tbody>                                            
                                                            <tr>
                                                                <td class="">Name</td>
                                                                <td><%= data.booking.billing_detail.first_name %>&nbsp;<%= data.booking.billing_detail.last_name %></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="">Email</td>
                                                                <td><%= data.booking.billing_detail.email %></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="">Address</td>
                                                                <td><%= data.booking.billing_detail.address %></td>
                                                            </tr>
															
															<tr>
                                                                <td class="">City</td>
                                                                <td><%= data.booking.billing_detail.city %></td>
                                                            </tr>
															
															<tr>
                                                                <td class="">State</td>
                                                                <td><%= data.booking.billing_detail.state.name %></td>
                                                            </tr>
															
															<tr>
                                                                <td class="">Country</td>
                                                                <td><%= data.booking.billing_detail.country.name %></td>
                                                            </tr>
															
                                                            <tr>
                                                                <td class="">Zipcode</td>
                                                                <td><%= data.booking.billing_detail.zip %></td>
                                                            </tr>
															
                                                            <tr>
                                                                <td class="">Phone</td>
                                                                <td><%= data.booking.billing_detail.phone %></td>
                                                            </tr>
															
                                                            <tr>
                                                                <td class="">Info</td>
                                                                <td><%= data.booking.billing_detail.additional_information %></td>
                                                            </tr>                                                     
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
            </div>
        </div>
    </div>
</script>

