<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\CmsPage $cmsPage
  */
?>

<div class="cmsPages view large-9 medium-8 columns content">
    <h3><?= h($cmsPage->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= h($cmsPage->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $cmsPage->has('user') ? $this->Html->link($cmsPage->user->id, ['controller' => 'Users', 'action' => 'view', $cmsPage->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Title En') ?></th>
            <td><?= h($cmsPage->title_en) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Title Spa') ?></th>
            <td><?= h($cmsPage->title_spa) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Slug') ?></th>
            <td><?= h($cmsPage->slug) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($cmsPage->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($cmsPage->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Content En') ?></h4>
        <?= $this->Text->autoParagraph(h($cmsPage->content_en)); ?>
    </div>
    <div class="row">
        <h4><?= __('Content Spa') ?></h4>
        <?= $this->Text->autoParagraph(h($cmsPage->content_spa)); ?>
    </div>
</div>
