<?php
/**
  * @var \App\View\AppView $this
  */
?>
<section class="content-header">
    <h1>
        <?= __('Pages') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Pages') ?></a>
        </li>
        <li class="active"> <?= __('Edit Page') ?></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                         <h3 class="box-title">Edit page</h3>
                </div>
                    <!-- Main content -->
                <?php echo $this->Html->script('tinymce/tinymce.min.js'); ?>
                <?php $this->TinymceElfinder->defineElfinderBrowser()?>
                <div class="cmsPages form large-9 medium-8 columns content">
                <?= $this->Form->create($cmsPage, ['id' => 'editCms','type' => 'file']) ?>
                <fieldset>
                    <?php
                        echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('title_en', ['id' => 'title_en', 'required' => true, 'label' => 'Title'])."</div><div class='col-sm-6'>";
                            echo $this->Form->control('meta_description_en', ['id' => 'meta_description_en', 'required' => true, 'label' => 'Meta Description'])."</div></div>";
                            echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('meta_content_en', ['id' => 'meta_content_en', 'required' => true, 'label' => 'Title Meta Content'])."</div><div class='col-sm-6'>";
                            echo $this->Form->control('meta_keyword_en', ['id' => 'meta_keyword_en', 'required' => true, 'label' => 'Meta Keyword'])."</div></div>";
                           

                            echo "<div class='row'><div class='col-xs-12'>".$this->Form->control('content_en', ['required' => false, 'label' => 'Content'])."</div></div>";
                    ?>
                    <div class="row">
                        <div class="col-sm-6 col-md-6">
                          <div class="form-group m-form-group">
                            <label for="image">Background Image ( optional ) </label>
                            <?= $this->Form->control('bg_image',['id'=>'image','placeholder' => 'Background Image','label'=>false , 'type' => 'file']) ?>
                          </div><!--end form group-->
                         </div> 
                        <div class="col-sm-6 col-md-6">
                          <div class="form-group m-form-group">
                              <?php if(!empty($cmsPage->bg_image)) { ?>            
                                <img src="<?php echo $this->Url->build('/uploads/images/'.$cmsPage->bg_image); ?>" width="50%" height="50%">
                              <?php } ?> 
                          </div><!--end form group-->
                        </div>
                      </div>
                </fieldset>
                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function(){
    $('#editCms').validate({
        rules:{
            title_en:{
                required: true,
                maxlength: 70
            },
            meta_description_en:{
                maxlength: 160
            }
        },
        messages:{
            title_en:{
                required:"Please enter english title",
                maxlength:"Maximum 70 characters are allowed",
                alphanumeric: "Letters, numbers, and underscores only please"
            },
            meta_description_en:{
                maxlength:"Maximum 160 characters are allowed",
                alphanumeric: "Letters, numbers, and underscores only please"
            }
        }
    });
});
</script>