<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Article[]|\Cake\Collection\CollectionInterface $articles
  */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= __('Section Images') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Section Images') ?></a>
        </li>
        <li class="active"> <?= __('All Section Images') ?></li>
    </ol>
</section>
<!-- Main content -->

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover table-striped">
                        <thead>
            <tr>
                <th scope="col">Sr. Num</th>
                <th scope="col"><?= $this->Paginator->sort('title_en', 'Title') ?></th>
                <th scope="col"><?= $this->Paginator->sort('image', 'Images') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified', 'Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php $i=1; foreach ($images as $image): ?>
            <tr>
                <td><?= $i ?></td>                
                <td><?= h($image->title) ?></td>
                <td><img src="../../img/sectionimages/<?php echo $image->image; ?>" width="50px"></td>
                <td><?= h($image->created) ?></td>
                <td class="actions">					
					<?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-edit')).'', array('action' => 'imageEdit', $image->id), array('escape' => false)) ?> 
					
                </td>
            </tr>
            <?php $i++; endforeach; ?>
        </tbody>
    </table>
     </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->

