<?php
/**
  * @var \App\View\AppView $this
  */
?><?php use Cake\Core\Configure; ?>
<section class="content-header">
    <h1>
        <?= __('BackGround Images Sections') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('BackGround Images Sections') ?></a>
        </li>
        <li class="active"> <?= __('Change Images') ?></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                    <!-- Main content -->
                <div class="cmsPages form large-9 medium-8 columns content">
                <?= $this->Form->create($image, ['type' => 'file' , 'id' => 'editSection']) ?>
                <fieldset>
                    <legend><?= __('Edit ') ?> - <?= $image->title; ?></legend>
                    <?php
                        echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('image', ['id' => 'image',  'label' => 'Change Image' , 'type' => 'file'])."</div>";
                    ?>
                    <div class="col-sm-6">
                    <img src="../../../img/sectionimages/<?php echo $image->image; ?>" width="220px" height="100px">
                </div>

                </fieldset>
                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</section>

