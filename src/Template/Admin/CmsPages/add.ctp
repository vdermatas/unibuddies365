<?php
/**
  * @var \App\View\AppView $this
  */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= __('Pages') ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>  <?= __('Pages') ?></a>
        </li>
        <li class="active"> <?= __('Add Pages') ?></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                         <h3 class="box-title">Add page</h3>
                </div>
                    <!-- Main content -->
                <?php echo $this->Html->script('tinymce/tinymce.min.js'); ?>
                <?php $this->TinymceElfinder->defineElfinderBrowser()?>
                <div class="cmsPages form large-9 medium-8 columns content">
                    <?= $this->Form->create($cmsPage, ['id' => 'addCms' , 'type' => 'file' ]) ?>
                    <fieldset>
                        <?php
                            echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('title_en', ['id' => 'title_en', 'required' => true, 'label' => 'Title '])."</div><div class='col-sm-6'>";
                            echo $this->Form->control('meta_description_en', ['id' => 'meta_description_en', 'required' => true, 'label' => 'Meta Description '])."</div></div>";
                            echo "<div class='row'><div class='col-sm-6'>".$this->Form->control('meta_content_en', ['id' => 'meta_content_en', 'required' => true, 'label' => 'Title Meta Content '])."</div><div class='col-sm-6'>";
                            echo $this->Form->control('meta_keyword_en', ['id' => 'meta_keyword_en', 'required' => true, 'label' => 'Meta Keyword '])."</div></div>";
                           

                            echo "<div class='row'><div class='col-xs-12'>".$this->Form->control('content_en', ['required' => false, 'label' => 'Content '])."</div></div>";


                            echo "<div class='row'><div class='col-xs-6'>".$this->Form->control('bg_image', ['required' => false, 'label' => 'Background Image ' , 'type' => 'file' ])."</div></div>";
                        ?>
                    </fieldset>
                    <?= $this->Form->button(__('Submit')) ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function(){
    $('#addCms').validate({
        rules:{
            title_en:"required",
            title_spa:"required"
        },
        messages:{
            title_en:"Please enter english title",
            title_spa:"Please enter spanish title"
        }
    });
});
</script>