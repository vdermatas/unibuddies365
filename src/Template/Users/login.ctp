 <section class="login-bg d-flex justify-content-center align-items-center login-section">
        <div class="container">
            <div class="col-sm-12 col-xl-8 m-auto">

                <div class="row login-page">
                <div class="col-sm-4 login-left-bg  ">
                    <div class="welcome-back">Welcome Back</div>
                    <div class="to-keep-connected"><?php echo __('To keep connected with us please login with personal Information by email address and password.'); ?></div>
                    
                </div>
                <div class="col-sm-8 pl-0">
                    <div class="signup-wraper">
                        <h3>Sign In</h3>
                        <div class="text-center">
                            <ul class="list-unstyled">
                                <li class="list-inline-item">

                                    <?php 
                                        /*echo $this->Html->image('../images/fb.png', [
                                        'alt' => '']);*/
                                    ?>
                                    
                                </li>
                                <li class="list-inline-item">
                                    <?php /*
                                        echo $this->Html->image('../images/google.png', [
                                        'alt' => '']);*/
                                    ?>
                                    <!-- <img src="images/google.png"> -->
                                </li>
                            </ul>
                        </div>
                        <!-- <div class="Or-you-can-use-your">Or</div> -->
                        
                   <?php echo $this->Flash->render(); ?>
                        <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#student" role="tab"
                                    aria-controls="home" aria-selected="true">I am Student</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#writer" role="tab"
                                    aria-controls="profile" aria-selected="false">I am Writer</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="student" role="tabpanel" aria-labelledby="home-tab">
                                 <?= $this->Form->create('studentLogin',['class' => 'form-horizontal','id' => 'studentLogin' , 'novalidate' => true , 'url' => '/student/users/login'  ]) ?>
                                <div class="signin-detail">
                                    <div class="form-group">
                                        <label>Email address</label>
                                        <?php
                                            echo $this->Form->control('username', [ 'class' => 'form-control' , 'placeholder' => 'Email Address' , 'label' => false ,
                                                'templates' => [
                                                            'inputContainer' => '{{content}}',
                                                            'inputDiv' => ''
                                                        ] 
                                                ]); 
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Password </label>
                                        <?php
                                            echo $this->Form->control('password', [ 'class' => 'form-control' , 'placeholder' => '* * * * * * *' , 'label' => false , 'id' => 'password',
                                                'templates' => [
                                                            'inputContainer' => '{{content}}',
                                                            'inputDiv' => ''
                                                        ] 
                                                ]); 
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                         <?= $this->Form->button(__('Submit'),['class' => 'btn btn-dark-blue']) ?>
                                </div>
                                <?= $this->Form->end() ?>
                            </div>
                            <div class="tab-pane fade" id="writer" role="tabpanel" aria-labelledby="profile-tab">
                                <?= $this->Form->create('writerLogin',['class' => 'form-horizontal','id' => 'writerLogin' , 'novalidate' => true , 'url' => '/writer/users/login'  ]) ?>
                                <div class="signin-detail">
                                    <div class="form-group">
                                        <label>Email address</label>
                                        <?php
                                            echo $this->Form->control('username', [ 'class' => 'form-control' , 'placeholder' => 'Email Address' , 'label' => false ,
                                                'templates' => [
                                                            'inputContainer' => '{{content}}',
                                                            'inputDiv' => ''
                                                        ] 
                                                ]); 
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Password </label>
                                        <?php
                                            echo $this->Form->control('password', [ 'class' => 'form-control' , 'placeholder' => '* * * * * * *' , 'label' => false , 'id' => 'password',
                                                'templates' => [
                                                            'inputContainer' => '{{content}}',
                                                            'inputDiv' => ''
                                                        ] 
                                                ]); 
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                         <?= $this->Form->button(__('Submit'),['class' => 'btn btn-dark-blue']) ?>
                                </div>
                                <?= $this->Form->end() ?>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <p class="already-have-an-account">forgot Password ? <a href="<?php echo $this->Url->build(['controller'=> 'users', 'action' => 'forgotPassword']);?>"> Click Here</a></p>
                            <p><center>( or )</center></p>
                            <p class="already-have-an-account">Don’t have an account? <a href="<?php echo $this->Url->build(['controller'=> 'users', 'action' => 'register']);?>"> Sign Up</a></p>
                        </div> 
                     </div>
                    </div>
                </div>
            </div>           
        </div>
    </div>
</section>