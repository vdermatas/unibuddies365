 <section class="login-bg d-flex justify-content-center align-items-center login-section">
        <div class="container">
            <div class="col-sm-12 col-xl-8 m-auto">

                <div class="row login-page">
                <div class="col-sm-2 pl-0"></div>
                <div class="col-sm-8 pl-0">
                    <div class="signup-wraper">
                        <h3>Forgot Password</h3>                        
                        <?php echo $this->Flash->render(); ?>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="student" role="tabpanel" aria-labelledby="home-tab">
                                 <?= $this->Form->create('studentLogin',['class' => 'form-horizontal' , 'id' => 'resetpassword' , 'novalidate' => true  ]) ?>
                                 <input type="hidden" value="<?php echo $id; ?>" name="remember_token">
                                <div class="signin-detail">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <?php
                                            echo $this->Form->control('password', [ 'class' => 'form-control' , 'placeholder' => 'Password' , 'label' => false , 'id' => 'password' ,
                                                'templates' => [
                                                            'inputContainer' => '{{content}}',
                                                            'inputDiv' => ''
                                                        ] 
                                                ]); 
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Confirm Password</label>
                                        <?php
                                            echo $this->Form->control('confirm_password', [ 'class' => 'form-control' , 'placeholder' => 'Confirm Password' , 'label' => false , 'id' => 'confirm_password' , 'type' => 'password' ,
                                                'templates' => [
                                                            'inputContainer' => '{{content}}',
                                                            'inputDiv' => ''
                                                        ] 
                                                ]); 
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                         <?= $this->Form->button(__('Submit'),['class' => 'btn btn-dark-blue']) ?>
                                </div>
                                <?= $this->Form->end() ?>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <p class="already-have-an-account">Don’t have an account? <a href="<?php echo $this->Url->build(['controller'=> 'users', 'action' => 'register']);?>"> Sign Up</a></p>
                        </div> 
                     </div>
                    </div>                    
                    <div class="col-sm-2 pl-0"></div>
                </div>
            </div>           
        </div>
    </div>
</section>
