<section class="login-bg d-flex justify-content-center align-items-center">
    <div class="container">
        <div class="col-sm-12 col-xl-7 m-auto">
            <div class="signup-wraper">
                <h3>Register Now</h3>
                <div class="text-center">
                    <!-- <ul class="list-unstyled">
                        <li class="list-inline-item"><a href="#"><img src="images/fb.png"></a></li>
                        <li class="list-inline-item"><a href="#"><img src="images/google.png"></a></li>
                    </ul> -->
                </div>
                <div class="Or-you-can-use-your">Or you can use your email for Registration</div>

                <?php echo $this->Flash->render(); ?>

                <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#student" role="tab"
                            aria-controls="home" aria-selected="true">I am Student</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#writer" role="tab"
                            aria-controls="profile" aria-selected="false">I am Writer</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="student" role="tabpanel" aria-labelledby="home-tab">
                         <?= $this->Form->create('studentRegistration',['class' => 'form-horizontal','id' => 'studentRegistration' , 'novalidate' => true , 'url' => '/student/users/studentRegister'  ]) ?>
                        <!-- <label class="custom-radio"> I want to sign up 
                                    <input type="radio" checked="checked" id="r2" value="1" name="radio1">
                                    <span class="checkmark"></span>
                        </label>
                        <label class="custom-radio">No, I want to add assignment without sign up
                            <input type="radio"  id="r1" value="0" name="radio1">
                            <span class="checkmark"></span>
                        </label> -->
                        
                        <div class="signup-detail">
                            <div class="form-group">
                                <label>Email address</label>
                                <?php
                                    echo $this->Form->control('email', [ 'class' => 'form-control' , 'placeholder' => 'Email Address' , 'label' => false , 'type' => 'email' ,
                                        'templates' => [
                                                    'inputContainer' => '{{content}}',
                                                    'inputDiv' => ''
                                                ] 
                                        ]); 
                                ?>
                            </div>
                            <div class="form-group">
                                <label>First name</label>
                                <?php
                                    echo $this->Form->control('first_name', [ 'class' => 'form-control' , 'placeholder' => 'First Name' , 'label' => false ,
                                        'templates' => [
                                                    'inputContainer' => '{{content}}',
                                                    'inputDiv' => ''
                                                ] 
                                        ]); 
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Last name</label>
                                <?php
                                    echo $this->Form->control('last_name', [ 'class' => 'form-control' , 'placeholder' => 'Last Name' , 'label' => false ,
                                        'templates' => [
                                                    'inputContainer' => '{{content}}',
                                                    'inputDiv' => ''
                                                ] 
                                        ]); 
                                ?>
                            </div>
                             <div class="form-group">
                                <label>Phone</label>
                                <?php
                                    echo $this->Form->control('mobile', [ 'class' => 'form-control' , 'placeholder' => 'Phone Number' , 'label' => false ,
                                        'templates' => [
                                                    'inputContainer' => '{{content}}',
                                                    'inputDiv' => ''
                                                ] 
                                        ]); 
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Password </label>
                                <?php
                                    echo $this->Form->control('password', [ 'class' => 'form-control' , 'placeholder' => '* * * * * *' , 'label' => false , 'id' => 'password' ,
                                        'templates' => [
                                                    'inputContainer' => '{{content}}',
                                                    'inputDiv' => ''
                                                ] 
                                        ]); 
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Confirm Password </label>
                                <?php
                                    echo $this->Form->control('confirm_password', [ 'class' => 'form-control' , 'placeholder' => '* * * * * *' , 'label' => false , 'type' => 'password' ,
                                        'templates' => [
                                                    'inputContainer' => '{{content}}',
                                                    'inputDiv' => ''
                                                ] 
                                        ]); 
                                ?>
                            </div>

                            <div class="custom-control custom-checkbox form-group mb-4">
                                <input type="checkbox" class="custom-control-input" name="agree" id="customCheckDisabled1">
                                <label class="custom-control-label" for="customCheckDisabled1">By clicking "Sign Up"
                                    button,
                                    you agree with our <br>
                                    <a href="#">Terms & conditions</a> and <a href="#">Privacy Policy.</a></label>
                            </div>
                            <div class="form-group mb-4">
                                <?= $this->Form->button(__('Register Now'),['class' => 'btn btn-dark-blue']) ?>
                            </div>
                        </div>
                       
                        <div class="form-group text-center">
                            <p class="already-have-an-account">Already have an account? <a href="<?php echo $this->Url->build(['controller'=> 'users', 'action' => 'login']);?>"> Sign in</a></p>
                        </div>
                    <?= $this->Form->end() ?>
                    </div>
                    <div class="tab-pane fade" id="writer" role="tabpanel" aria-labelledby="profile-tab">
                        <?= $this->Form->create('writerRegister',['class' => 'form-horizontal','id' => 'writerRegister' , 'novalidate' => true , 'url' => '/writer/users/writerRegister'  , 'type' => 'file' ]) ?>
                        <div class="form-group">
                                <label>Email address</label>
                                <?php
                                    echo $this->Form->control('email', [ 'class' => 'form-control' , 'placeholder' => 'Email Address' , 'label' => false , 'type' => 'email' ,
                                        'templates' => [
                                                    'inputContainer' => '{{content}}',
                                                    'inputDiv' => ''
                                                ] 
                                        ]); 
                                ?>
                            </div>
                            <div class="form-group">
                                <label>First name</label>
                                <?php
                                    echo $this->Form->control('first_name', [ 'class' => 'form-control' , 'placeholder' => 'First Name' , 'label' => false ,
                                        'templates' => [
                                                    'inputContainer' => '{{content}}',
                                                    'inputDiv' => ''
                                                ] 
                                        ]); 
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Last name</label>
                                <?php
                                    echo $this->Form->control('last_name', [ 'class' => 'form-control' , 'placeholder' => 'Last Name' , 'label' => false ,
                                        'templates' => [
                                                    'inputContainer' => '{{content}}',
                                                    'inputDiv' => ''
                                                ] 
                                        ]); 
                                ?>
                            </div>
                             <div class="form-group">
                                <label>Phone</label>
                                <?php
                                    echo $this->Form->control('mobile', [ 'class' => 'form-control' , 'placeholder' => 'Phone Number' , 'label' => false ,
                                        'templates' => [
                                                    'inputContainer' => '{{content}}',
                                                    'inputDiv' => ''
                                                ] 
                                        ]); 
                                ?>
                            </div>
                            
                            <div class="form-group">
                                <label>Upload Resume</label>
                                <?php
                                    echo $this->Form->control('resume', [ 'class' => 'form-control' , 'label' => false , 'type' => 'file' ,
                                        'templates' => [
                                                    'inputContainer' => '{{content}}',
                                                    'inputDiv' => ''
                                                ] 
                                        ]); 
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Year of Experience</label>
                                <?php
                                    echo $this->Form->control('experience', [ 'class' => 'form-control' , 'label' => false , 'options' => ['1' => 1,'2' => 2,'3' => 3,'4' => 4,'5' => 5,'6' => 6 , '7' => 7 , '8' => 8 , '9' => 9 , '10' => 10] ,
                                        'templates' => [
                                                    'inputContainer' => '{{content}}',
                                                    'inputDiv' => ''
                                                ] 
                                        ]); 
                                ?>
                            </div>

                        <div class="form-group">
                            <label>About Me</label>
                            <?php
                                echo $this->Form->control('about_me', [ 'class' => 'form-control' , 'placeholder' => 'About Me' , 'label' => false , 'type' => 'textarea',
                                    'templates' => [
                                                'inputContainer' => '{{content}}',
                                                'inputDiv' => ''
                                            ] 
                                    ]); 
                            ?>
                        </div>
                        <div class="form-group">
                                <label>Password </label>
                                <?php
                                    echo $this->Form->control('password', [ 'class' => 'form-control' , 'placeholder' => '* * * * * *' , 'label' => false , 'id' => 'writerPassword' ,
                                        'templates' => [
                                                    'inputContainer' => '{{content}}',
                                                    'inputDiv' => ''
                                                ] 
                                        ]); 
                                ?>
                        </div>
                        <div class="form-group">
                            <label>Confirm Password </label>
                            <?php
                                echo $this->Form->control('confirm_password', [ 'class' => 'form-control' , 'placeholder' => '* * * * * *' , 'label' => false , 'type' => 'password' ,
                                    'templates' => [
                                                'inputContainer' => '{{content}}',
                                                'inputDiv' => ''
                                            ] 
                                    ]); 
                            ?>
                        </div>


                        <div class="custom-control custom-checkbox form-group mb-4">
                            <input type="checkbox" class="custom-control-input"  name="agree" id="customCheck">
                            <label class="custom-control-label" for="customCheck">By clicking "Sign Up" button,
                                you agree with our <br>
                                <a href="#">Terms & conditions</a> and <a href="#">Privacy Policy.</a></label>
                        </div>

                        <div class="form-group mb-4">
                             <?= $this->Form->button(__('Register Now'),['class' => 'btn btn-dark-blue']) ?>
                        </div>

                        <div class="form-group text-center">
                            <p class="already-have-an-account">Already have an account? <a href="<?php echo $this->Url->build(['controller'=> 'users', 'action' => 'login']);?>"> Sign in</a></p>
                        </div>

                        <?= $this->Form->end() ?>
                    </div>


                </div>
            </div>
        </div>
    </div>
</section>
