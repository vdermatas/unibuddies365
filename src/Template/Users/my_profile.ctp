<section class="bg-gray py-5 add-address" id="custom-profile">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h5>Your Account</h5>

                    <div class="row">
                        <div class="col-12 col-sm-4 mb-4">

                            
                            <div class="nav flex-column nav-pills bg-white" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                              <a class="nav-link active" id="v-pills-home-tab" href="<?php echo $this->Url->build('/my-profile'); ?>"><i class="fa fa-user mr-1"></i> Profile Information</a>
                              <a class="nav-link" id="v-pills-profile-tab"  href="<?php echo $this->Url->build('/change-password'); ?>" ><i class="fa fa-cog mr-1"></i> Password & Security</a>
                               <a class="nav-link" id="v-pills-order-tab" href="<?php echo $this->Url->build('/my-orders'); ?>"><i class="fa fa-cart-arrow-down mr-1"></i> Your Orders</a>
                               <a class="nav-link" id="v-pills-settings-tab" href="<?php echo $this->Url->build('/users/logout'); ?>"><i class="fa fa-power-off mr-1"></i> Logout</a>
                            </div>

                            
                        </div><!--end sidebar-->


                        <div class="col-12 col-sm-8">
                            <?php echo $this->Flash->render(); ?>
                            <div class="tab-content" id="v-pills-tabContent">

                              <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                  <article class="bg-white col-12 py-3">
                                    <h5 class="mb-4">Personal Information</h5>
                                    <?php echo $this->Form->create($user,array('type' => 'file')); ?>
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label>First Name *</label>
                                                    <?php echo $this->Form->control('first_name', ['label' => false, 'class' => 'form-control' ]); ?>
                                                </div>
                                    
                                                <div class="form-group">
                                                    <label>Mobile Number </label>
                                                    <?php echo $this->Form->control('mobile', ['label' => false, 'class' => 'form-control']); ?>
                                                </div>
                                                <div class="form-group">
                                                    <label>Profile Image</label>
                                                    <?php echo $this->Form->control('profile_image', ['label' => false, 'type' => 'file']); ?>
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-warning btn-block text-white">Confirm</button>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label>Last Name *</label>
                                                    <?php echo $this->Form->control('last_name', ['label' => false, 'class' => 'form-control']); ?>
                                                </div>
                                                <div class="form-group">
                                                    <label>Email Address *</label>
                                                    <?php echo $this->Form->control('email', ['label' => false, 'class' => 'form-control']); ?>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                    </form>
                                </article>
                              </div><!--end tab 1-->
                            
                              <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">...</div>
                              


                            </div><!--end tab content parent-->
                        </div><!--end content right-->
                    </div>
                    

                    

                </div>
            </div>
        </div>
    </section>