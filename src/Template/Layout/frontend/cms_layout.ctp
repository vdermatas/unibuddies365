<?php
    use Cake\Core\Configure;

    
?>
<?php echo $this->Html->charset(); ?>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- <title>Home Page</title> -->
        <title><?php echo $title; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php
            echo $this->Html->css(['bootstrap.min', 'style']);
            echo $this->Html->css('https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900&display=swap');
        ?>
    </head>

    <body>
        <?php echo $this->element('frontend/cms_header'); ?>
        <?php echo $this->fetch('content'); ?>
        <?php echo $this->element('frontend/cms_footer'); ?>

        <!-- Bootstrap core JavaScript
            ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <?php
            echo $this->Html->script(['jquery-3.3.1.min', 'popper.min', 'bootstrap.min','jquery.validate.js','additional-methods.min.js']);
        ?>

        <script type="text/javascript">
            $('.navbar-toggler').on('click', function() {
                $(this).toggleClass('active');
                $(this).next().toggleClass('site-header-show site-header-open');
            });
            
            $('.site-header-mobi-panel a').on('click', function(){
                $(this).closest('.site-header-mobi-panel').prev().toggleClass('active');
                $(this).closest('.site-header-mobi-panel').toggleClass('site-header-show site-header-open');
            });
            
            var scroll_pos = 0;
                $(document).scroll(function() {
                scroll_pos = $(this).scrollTop();
                if (scroll_pos > 50) {
                        $(".navbar").addClass('change-scroll');
                } else {
                        $(".navbar").removeClass('change-scroll');
                }
            });
        </script>
    </body>
</html>
