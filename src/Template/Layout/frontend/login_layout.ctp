<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Unibuddies | login | Register Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php
            echo $this->Html->css(['frontend/bootstrap.min', 'frontend/style']);
            echo $this->Html->css('https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900&display=swap');
        ?>
        
    </head>
    <nav class="navbar navbar-expand-lg fixed-top navbar-light custom-nav ">
        <a href="<?php echo $this->Url->build('/', true);?>" class="navbar-brand"><?php 
                    echo $this->Html->image('../images/logo.svg', [
                        'alt' => ''
                    ]);
                ?></a>
        <button class="navbar-toggler " type="button">
            <span></span>
            <span></span>
            <span></span>
        </button>

        <div class="site-header-mobi-panel ">
            <div class="site-header-mobi">
                <nav class="site-nav">
                    <ul class="site-nav-menu list-unstyled">
                        <?php foreach (json_decode($frontendMenus[0]['data']) as $key => $menus) {  ?>
                            <li>
                                <a href="<?php if(!empty($menus->custom_url)){ echo $this->Url->build($menus->custom_url); }else{ echo $this->Url->build("/$menus->page"); } ?>" ><?php echo $menus->text; ?></a> 
                            </li>
                        <?php } ?>
                        <li class="nav-item">
                            <a href="?lang=en_EN" class="nav-link"><img src="<?php echo $this->Url->build('/images/flags/united-kingdom.svg'); ?>" style="width: 30px;height: 20px;"></a>                        
                        </li>
                        <li class="nav-item">
                            <a href="?lang=gre_GRE" class="nav-link"><img src="<?php echo $this->Url->build('/images/flags/greek.svg'); ?>" style="width: 30px;height: 20px;"></a>                        
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <?php foreach (json_decode($frontendMenus[0]['data']) as $key => $menus) {  ?>
                <li class="nav-item">
                    <a href="<?php if(!empty($menus->custom_url)){ echo $this->Url->build($menus->custom_url); }else{ echo $this->Url->build("/$menus->page"); } ?>" class="nav-link"><?php echo $menus->text; ?></a> 
                </li>
            <?php } ?>
            <li class="nav-item">
                <a href="?lang=en_EN" class="nav-link"><img src="<?php echo $this->Url->build('/images/flags/united-kingdom.svg'); ?>" style="width: 30px;height: 20px;"></a>                        
            </li>
            <li class="nav-item">
                <a href="?lang=gre_GRE" class="nav-link"><img src="<?php echo $this->Url->build('/images/flags/greek.svg'); ?>" style="width: 30px;height: 20px;"></a>                        
            </li>
            </ul>
        </div>
    </nav>
<?php echo $this->fetch('content'); ?>
    <footer class="footer">
            <div class="container">
                <div class="row mb-sm-4 mb-0">
                    <?php 
                        echo $this->Html->image('../images/footer-logo.png', [
                        'alt' => '', 'class'=> 'footer-logo']);
                      ?>
                </div>
                <div class="row">
                    <div class="col-sm-7 pl-0">
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <a href="<?php echo $this->Url->build('/', true);?>">Home</a>
                            </li>
                            <li class="list-inline-item">
                                <a href="">About</a>
                            </li>
                            <li class="list-inline-item">
                                <a href="">Services</a>
                            </li>
                            <li class="list-inline-item">
                                <a href="">Resources</a>
                            </li>
                            <li class="list-inline-item">
                                <a href="">FAQs</a>
                            </li>
                            <li class="list-inline-item">
                                <a href="">Privacy Policy</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-5 newsletter">
                        <label>Join our Newsletter</label>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Enter your email"
                                aria-label="Recipient's username" aria-describedby="button-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon2">Subscribe</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-0 mt-sm-0">
                    <div class="col-sm-7 follow-us-section pl-0">
                        <div class="sub-heading">Follow Us:</div>
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <a href="#">
                                    <?php 
                                        echo $this->Html->image('../images/facebook.png', [
                                        'alt' => '']);
                                      ?>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#">
                                    <?php 
                                        echo $this->Html->image('../images/twitter.png', [
                                        'alt' => '']);
                                    ?>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#">
                                    <?php 
                                        echo $this->Html->image('../images/instagram.png', [
                                        'alt' => '']);
                                    ?>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                    <?php 
                                        echo $this->Html->image('../images/linkdin.png', [
                                        'alt' => '']);
                                    ?>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-5 copy-right">
                        <p>&copy;Copyright 2019. All Rights Reserved</p>
                    </div>
                </div>
            </div>
        </footer>
        
    <body>

        <!-- Bootstrap core JavaScript
            ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <?php
            echo $this->Html->script(['frontend/jquery-3.3.1.min', 'frontend/popper.min', 'frontend/bootstrap.min','jquery.validate.js','additional-methods.min.js']);
        ?>
        <script type="text/javascript">
            $('.navbar-toggler').on('click', function() {
                $(this).toggleClass('active');
                $(this).next().toggleClass('site-header-show site-header-open');
            });
            
            $('.site-header-mobi-panel a').on('click', function(){
                $(this).closest('.site-header-mobi-panel').prev().toggleClass('active');
                $(this).closest('.site-header-mobi-panel').toggleClass('site-header-show site-header-open');
            });
            
            var scroll_pos = 0;
                $(document).scroll(function() {
                scroll_pos = $(this).scrollTop();
                if (scroll_pos > 50) {
                        $(".navbar").addClass('change-scroll');
                } else {
                        $(".navbar").removeClass('change-scroll');
                }
            });
        </script>
         <script>
            $(document).ready(function(){


                $("#studentLogin").validate({
                    rules: {
                        username: {
                            required: true,
                            email: true
                        },
                        password: {
                            required: true,
                            minlength: 5
                        } 
                      
                    },
                    messages: {
                        username: "Please enter a your email address",
                        password: {
                            required: "Please provide a password",
                            minlength: "Your password must be at least 5 characters long"
                        }
                    }
                });

                $("#writerLogin").validate({
                    rules: {
                        username: {
                            required: true,
                            email: true
                        },
                        password: {
                            required: true,
                            minlength: 5
                        } 
                      
                    },
                    messages: {
                        username: "Please enter a your email address",
                        password: {
                            required: "Please provide a password",
                            minlength: "Your password must be at least 5 characters long"
                        }
                    }
                });


                $("#studentRegistration").validate({
                    rules: {
                        first_name: {
                            required : true
                        },
                        last_name: {
                            required : true
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        password: {
                            required: true,
                            minlength: 5
                        },
                        confirm_password: {
                            required: true,
                            minlength: 5,
                            equalTo: "#password"
                        },
                        mobile: {
                            required: true,
                            minlength: 10
                        },
                        agree: {
                            required : true
                        }
                    },
                    messages: {
                        first_name: "Please enter your first name",
                        last_name: "Please enter your last name",
                        email: "Please enter a your email address",
                        mobile: "Please Enter Valid Mobile Number",
                        agree: "Check agree button to proceed register",
                        password: {
                            required: "Please provide a password",
                            minlength: "Your password must be at least 5 characters long"
                        },
                        confirm_password: {
                            required: "Please provide a password",
                            minlength: "Your password must be at least 5 characters long",
                            equalTo: "Please enter the same password as above"
                        }
                    }
                });


                $("#writerRegister").validate({
                    rules: {
                        first_name: {
                            required : true
                        },
                        last_name: {
                            required : true
                        },
                         resume:{
                            required:true,
                            extension: "docx|rtf|doc|pdf"
                        },
                        about_me: {
                            required : true
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        password: {
                            required: true,
                            minlength: 5
                        },
                        confirm_password: {
                            required: true,
                            minlength: 5,
                            equalTo: "#writerPassword"
                        },
                        mobile: {
                            required: true,
                            minlength: 10
                        },
                        agree: {
                            required : true
                        }
                    },
                    messages: {
                        first_name: "Please enter your first name",
                        last_name: "Please enter your last name",
                        resume:{
                            required:"Please upload your resume",                  
                            extension:"select valied input file format"
                        },
                        about_me: "Please enter about your previous skills",
                        email: "Please enter a your email address",
                        mobile: "Please Enter Valid Mobile Number",
                        agree: "Check agree button to proceed register",
                        password: {
                            required: "Please provide a password",
                            minlength: "Your password must be at least 5 characters long"
                        },
                        confirm_password: {
                            required: "Please provide a password",
                            minlength: "Your password must be at least 5 characters long",
                            equalTo: "Please enter the same password as above"
                        }
                    }
                });

                $("#resetpassword").validate({
                    rules: {
                        password: {
                            required: true,
                            minlength: 5
                        },
                        confirm_password: {
                            required: true,
                            minlength: 5,
                            equalTo: "#password"
                        },
                    },
                    messages: {
                        password: {
                            required: "Please provide a password",
                            minlength: "Your password must be at least 5 characters long"
                            
                        },
                        confirm_password: {
                            required: "Please provide a password",
                            minlength: "Your password must be at least 5 characters long",
                            equalTo: "Please enter the same password as above"
                        }
                    }
                });

                $("#forgotpassword").validate({
                    rules: {
                        email: {
                            required: true,
                            email: true
                        }
                    },
                    messages: {
                        email: "Please enter a your email address"
                    }
                });



                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
            });
        </script>

        <style type="text/css">label.error{color: red;}</style>
    </body>
</html>
