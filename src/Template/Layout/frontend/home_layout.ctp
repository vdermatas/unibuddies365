<?php
    use Cake\Core\Configure;

    use Cake\Network\Session;
?>
<?php echo $this->Html->docType(); ?>
<html lang="en">
    <head>
        <?php echo $this->Html->charset();?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="<?php echo empty($meta_description) ? '' : $meta_description ;?>">
        <meta name="keywords" content="<?php echo empty($meta_keyword) ? '' : strtolower($meta_keyword) ;?>">
        <meta name="author" content="">
        <title>Cheolis</title>
        <?php echo $this->Html->css('frontend/bootstrap.min'); ?>
        <?php echo $this->Html->css('frontend/style'); ?>
        <?php echo $this->Html->css('frontend/font-awesome.min'); ?>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <?php echo $this->Html->script('frontend/jQuery-2.1.4.min'); ?>
    </head>
    <body>
    <nav class="navbar navbar-expand-lg fixed-top navbar-light custom-nav ">
        <a href="#" class="navbar-brand"><img src="images/logo.svg" alt=""></a>
        <button class="navbar-toggler " type="button">
            <span></span>
            <span></span>
            <span></span>
        </button>

        <div class="site-header-mobi-panel ">
            <div class="site-header-mobi">
                <nav class="site-nav">
                    <ul class="site-nav-menu list-unstyled">
                        <li>
                            <a href="#">Home</a>
                        </li>
                        <li>
                            <a href="#">About</a>
                        </li>
                        <li>
                            <a href="#">Services</a>
                        </li>
                        <li>
                            <a href="#">Resources</a>
                        </li>
                        <li> <a href="#">Sign In</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a href="#" class="nav-link">Home</a> 
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">About</a>                    
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">Services</a>                        
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">Resources</a>                        
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">Sign In</a>                        
                </li>
            </ul>
        </div>
    </nav>
    <?php
        echo $this->Flash->render();
        echo $this->fetch('content'); ?>

<?php echo $this->Html->script('jquery-3.3.1.min.js'); ?>
<?php echo $this->Html->script('popper.min.js'); ?>
<?php echo $this->Html->script('bootstrap.min.js'); ?>

    <script type="text/javascript">
        $('.navbar-toggler').on('click', function() {
            $(this).toggleClass('active');
            $(this).next().toggleClass('site-header-show site-header-open');
        });
        
        $('.site-header-mobi-panel a').on('click', function(){
            $(this).closest('.site-header-mobi-panel').prev().toggleClass('active');
            $(this).closest('.site-header-mobi-panel').toggleClass('site-header-show site-header-open');
        });
        
        var scroll_pos = 0;
            $(document).scroll(function() {
            scroll_pos = $(this).scrollTop();
            if (scroll_pos > 50) {
                    $(".navbar").addClass('change-scroll');
            } else {
                    $(".navbar").removeClass('change-scroll');
            }
        });
    </script>
    </body>
</html>
