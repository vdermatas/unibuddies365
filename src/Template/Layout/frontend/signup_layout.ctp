<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Login Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900&display=swap" rel="stylesheet">
    </head>
    <nav class="navbar navbar-expand-lg fixed-top navbar-light custom-nav ">
        <a href="#" class="navbar-brand"><img src="images/logo.svg" alt=""></a>
        <button class="navbar-toggler " type="button">
            <span></span>
            <span></span>
            <span></span>
        </button>

        <div class="site-header-mobi-panel ">
            <div class="site-header-mobi">
                <nav class="site-nav">
                    <ul class="site-nav-menu list-unstyled">
                        <li>
                            <a href="#">Home</a>
                        </li>
                        <li>
                            <a href="#">About</a>
                        </li>
                        <li>
                            <a href="#">Services</a>
                        </li>
                        <li>
                            <a href="#">Resources</a>
                        </li>
                        <li> <a href="#">Sign In</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a href="#" class="nav-link">Home</a> 
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">About</a>                    
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">Services</a>                        
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">Resources</a>                        
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">Sign In</a>                        
                </li>
            </ul>
        </div>
    </nav>


    <section class="login-bg d-flex justify-content-center align-items-center login-section">
        <div class="container">
            <div class="col-sm-12 col-xl-8 m-auto">

                <div class="row login-page">
                <div class="col-sm-4 login-left-bg  ">
                    <div class="welcome-back">Welcome Back</div>
                    <div class="to-keep-connected">To keep connected with us please login with personal Information by email address and password.</div>
                    
                </div>
                <div class="col-sm-8 pl-0">
                    <div class="signup-wraper">
                    <h3>Sign In</h3>
                    <div class="text-center">
                    <ul class="list-unstyled">
                        <li class="list-inline-item"><a href="#"><img src="images/fb.png"></a></li>
                        <li class="list-inline-item"><a href="#"><img src="images/google.png"></a></li>
                    </ul>
                </div>
                    <div class="Or-you-can-use-your">Or</div>
                    <div class="form-group">
                        <label>Email address</label>
                        <input type="email" class="form-control" placeholder="keithcarlson@example.com ">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="text" class="form-control" placeholder=" ">
                    </div>

                    <div class="form-group text-right forgot-password">
                        <a href="#">Forgot Password?</a>
                    </div>
                   

                    <div class="form-group mb-4">
                        <button class="btn btn-dark-blue">Sign In</button>
                    </div>

                    <div class="form-group text-center">
                        <p class="already-have-an-account">Don’t have an account? <a href="#"> Sign Up</a></p>
                    </div>
                </div>
                </div>
                </div>
           
            </div>
        </div>
    </section>

   

    <footer class="footer">
            <div class="container">
                <div class="row mb-sm-4 mb-0">
                    <img class="footer-logo" src="images/footer-logo.png">
                </div>
                <div class="row">
                    <div class="col-sm-7 pl-0">
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <a href="">Home</a>
                            </li>
                            <li class="list-inline-item">
                                <a href="">About</a>
                            </li>
                            <li class="list-inline-item">
                                <a href="">Services</a>
                            </li>
                            <li class="list-inline-item">
                                <a href="">Resources</a>
                            </li>
                            <li class="list-inline-item">
                                <a href="">FAQs</a>
                            </li>
                            <li class="list-inline-item">
                                <a href="">Privacy Policy</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-5 newsletter">
                        <label>Join our Newsletter</label>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Enter your email"
                                aria-label="Recipient's username" aria-describedby="button-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon2">Subscribe</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-0 mt-sm-0">
                    <div class="col-sm-7 follow-us-section pl-0">
                        <div class="sub-heading">Follow Us:</div>
                        <ul class="list-inline">
                            <li class="list-inline-item"><a href="#"><img src="images/facebook.png"></a></li>
                            <li class="list-inline-item"><a href="#"><img src="images/twitter.png"></a></li>
                            <li class="list-inline-item"><a href="#"><img src="images/instagram.png"></a></li>
                            <li class="list-inline-item"><a href="#"><img src="images/linkdin.png"></a></li>
                        </ul>
                    </div>
                    <div class="col-sm-5 copy-right">
                        <p>&copy;Copyright 2019. All Rights Reserved</p>
                    </div>
                </div>
            </div>
        </footer>
        
    <body>

        <!-- Bootstrap core JavaScript
            ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/popper.min.js "></script>
        <script src="js/bootstrap.min.js "></script>
        <script type="text/javascript">
            $('.navbar-toggler').on('click', function() {
                $(this).toggleClass('active');
                $(this).next().toggleClass('site-header-show site-header-open');
            });
            
            $('.site-header-mobi-panel a').on('click', function(){
                $(this).closest('.site-header-mobi-panel').prev().toggleClass('active');
                $(this).closest('.site-header-mobi-panel').toggleClass('site-header-show site-header-open');
            });
            
            var scroll_pos = 0;
                $(document).scroll(function() {
                scroll_pos = $(this).scrollTop();
                if (scroll_pos > 50) {
                        $(".navbar").addClass('change-scroll');
                } else {
                        $(".navbar").removeClass('change-scroll');
                }
            });
        </script>
    </body>
</html>
