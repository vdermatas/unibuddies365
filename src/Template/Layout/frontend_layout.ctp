
<?php
    use Cake\Core\Configure;

    use Cake\Network\Session;
?>
<?php echo $this->Html->docType(); ?>
<html lang="en">
    <head>
        <?php echo $this->Html->charset();?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="<?php echo empty($meta_description) ? '' : $meta_description ;?>">
        <meta name="keywords" content="<?php echo empty($meta_keyword) ? '' : strtolower($meta_keyword) ;?>">
        <meta name="author" content="">
        <title>UniBuddies365</title>
        <?php echo $this->Html->css('frontend/bootstrap.min'); ?>
        <?php echo $this->Html->css('frontend/style'); ?>
        <?php echo $this->Html->css('frontend/font-awesome.min'); ?>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <?php echo $this->Html->script('frontend/jQuery-2.1.4.min'); ?>
    </head>
    <body>
        <header>
            <div class="coupon text-center py-2">
              <p><?php echo  $couponText->content; ?></p>
            </div>
        </header>
        <section class="banner">
        <div class="container position-relative">
            <?php echo $this->element('frontend/navigation'); ?>
        </div>
        <div class="banner-image py-4 d-flex align-items-center justify-content-center">
            <?php if($this->request->getParam['action'] == 'myBasket'){ ?>
                <h1 class="d-block">My
                    <span>Basket</span>
                </h1>
            <?php }elseif($this->request->getParam['action'] == 'addAddress'){?>
                <h1 class="d-block">Add
                    <span>Address</span>
                </h1>
            <?php }elseif($this->request->getParam['action'] == 'Payment'){?>
                <h1 class="d-block">Payment
                    <span>Method</span>
                </h1>
            <?php }elseif($this->request->getParam['action'] == 'reviewOrder'){?>
                <h1 class="d-block">Review
                    <span>Order</span>
                </h1>
            <?php }elseif($this->request->getParam['action'] == 'shipping'){?>
                <h1 class="d-block">Online <span>Store</span></h1>
            <?php }elseif($this->request->getParam['action'] == 'register'){?>
                <h1 class="d-block">Create <span>an account</span></h1>
            <?php }elseif($this->request->getParam['action'] == 'login'){?>
                <h1 class="d-block">Login</h1>
            <?php } ?>
            
        </div>
    </section>
    
		<?php
            //echo $this->Flash->render();
    		echo $this->fetch('content');
    		echo $this->element('frontend/footer');       
        ?>

        <!-- Bootstrap core JavaScript
            ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
		<?php echo $this->Html->script('frontend/wow'); ?>
        <?php echo $this->Html->script('frontend/moment'); ?>
        <?php echo $this->Html->script('frontend/bootstrap'); ?>
        <?php echo $this->Html->script('frontend/classie'); ?>
        <?php //echo $this->Html->script('frontend/contact'); ?>
        <?php //echo $this->Html->script('frontend/custom'); ?>
        <?php echo $this->Html->script('frontend/jquery.fancybox.min'); ?>
        <?php echo $this->Html->script('frontend/bootstrap-datetimepicker.min'); ?>
        <?php echo $this->Html->script('frontend/cart'); ?>
        <?php echo $this->Html->script('frontend/forms'); ?>

        <!-- 
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>-->

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script> 
        
        <?php echo $this->Html->script('jquery.validate.min'); ?>
        <?php echo $this->Html->script('https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js'); ?>
        
       
        <!-- <div class="modal fade" id='shuttle-10-contact' tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php //echo __('Please contact with administrator'); ?></h4>
              </div>
              <div class="modal-body">
                <p><?php ///echo __('If you want to book a shuttle with more than 10 people then please'); ?> <span style="color: #00ba9e;"><a target="_blank" href='<?php //echo Configure::read("APP_BASE_URL") .'/contact-us';?>'><?php //echo __('contact us.'); ?></a></span></p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default shuttle-10-contact-close" ><?php //echo __('Close'); ?></button>
              </div>
            </div>
          </div> 
        </div> -->
        
    </body>
</html>