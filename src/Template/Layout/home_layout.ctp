<?php
    use Cake\Core\Configure;
?>
<?php echo $this->Html->docType(); ?>
<html lang="en">
    <head>
        <?php echo $this->Html->charset();?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="<?php echo empty($meta_description) ? '' : $meta_description ;?>">
        <meta name="keywords" content="<?php echo empty($meta_keyword) ? '' : strtolower($meta_keyword) ;?>">
        <meta name="author" content="">
        <title>UniBuddies365</title>
        <?php echo $this->Html->css('frontend/bootstrap.min'); ?>
        <?php echo $this->Html->css('frontend/style'); ?>
        <?php echo $this->Html->css('frontend/font-awesome.min'); ?>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <?php echo $this->Html->script('frontend/jQuery-2.1.4.min'); ?>
    </head>
    <body>
        <header>
            <?php if($couponText->status == 1){?>
            <div class="coupon text-center py-2">
                <p><?php echo  $couponText->content; ?></p>
            </div>
            <?php } ?>
        </header>

        <section class="banner sliderBanner home_banner">
        <div class="container position-relative">
            <?php echo $this->element('frontend/navigation'); ?>
        </div>
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <?php $i = 1;  foreach ($sliderImages as $key => $sliderImage) { ?>
                <div class="carousel-item <?php if($i == 1){ echo "active"; } ?>">
                     <?php echo $this->Html->image('/'.$sliderImage->url, ['alt' => 'Slider Image', 'class' => 'd-block w-100', 'escape' =>true]); ?> 
                    <div class="carousel-caption">
                        <h1>
                            <span class="d-none d-md-block"><?php echo $sliderImage->text; ?></span>
                        </h1><br>
                        <!-- <p class="text-uppercase d-none d-md-block"><?php //echo $sliderImage->text; ?></p> -->
                        <a href="products"><?php echo $this->Html->image('sectionimages/'.$bg_images[1]['image'].'', ['alt' => 'cheolis' , 'height' => '45px']); ?></a>
                    </div>
                </div>
                <?php $i++; } ?>

                
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>

            <div class="search-box container mt-5">
                <div class="row">
                <!-- <div class="col-md-4 col-sm-4  d-none d-md-block"></div> -->
                    <div class="col-md-6 col-sm-4 col-6 mx-auto">
                         <form class="form-horizontal" action="search" method="get">
                            <input type="text" name="search" class="form-control" placeholder="Search Products">
                        </form>
                    </div>
                    <!-- <div class="col-md-4 col-sm-4 d-none d-md-block"></div> -->
                 </div>
            </div><!--end search box-->



        </div>
    </section>

        
        <?php
        echo $this->Flash->render();
        echo $this->fetch('content'); ?>
        <!--product listing section-->
    <!-- <section class="selling-list banner my-5">
        <div class="container">

            <div class="row">
                <div class="col-12 text-center">
                    <h1 class="d-block">
                        <span>New</span> Arrivals
                    </h1>
                </div> 
            </div>
            <div class="row mt-4">
                <?php foreach ($products as $key => $product) { ?>
                <div class="col-sm-6 col-md-6 col-lg-3 col-12 pb-4">
                    <div class="card mb-sm-4 mb-4 mb-lg-0 h-100">
                         <span class="badge badge-custom">15% off</span> 
                     <div class="card-top broder bg-gray text-center">                         
                         <?php 
                            //if(!empty($product['gallery']['images'][0]['url'])){ 
                            //echo $this->Html->image($product['gallery']['images'][0]['url'], ['alt' => 'Product', 'class' => 'my-img-responsive selected w-100 img-fluid h-100' , 'width' => '100%', 'height' => '100%']); 
                            }//else{ echo $this->Html->image('frontend/food/product.png', ['alt' => 'Product', 'class' => 'card-img-top p-2 w-100 img-fluid h-100' , 'id'=>'mainImg']); 
                             //}
                          ?>
                     </div>
                      <div class="card-body">
                        <h6><a href="<?php //echo $this->Url->build('/product/view/'.$product->slug.''); ?>" style="color:black;"><?php //echo $product->name_en; ?></a></h6>
                        <span class="amt mb-2"><span class="fa fa-usd"></span> <?php// echo $product->price; ?></span>
                        <a href="<?php //echo $this->Url->build('/product/view/'.$product->slug.''); ?>" class="btn btn-warning btn-sm text-white float-right">View Product Detail</a>
                      </div>
                    </div>
                </div>
                <?php //} ?>             
            </div>
        </div>
    </section>-->
    <!--product listing section-->
    <section class="selling-list banner my-5">
        <div class="container">

            <div class="row">
                <div class="col-12 text-center">
                    <h1 class="d-block">
                        <span>Best</span> Value List
                    </h1>
                </div><!--end col 12-->
            </div><!--row-->

            <div class="row mt-4">
                <?php foreach ($topproducts as $key => $topproduct) { ?>
                <div class="col-sm-6 col-md-6 col-lg-3 col-12 pb-4">
                    <div class="card mb-sm-4 mb-4 mb-lg-0 h-100 all_boder">
                        <!-- <span class="badge badge-custom">15% off</span> -->
                     <a href="<?php echo $this->Url->build('/product/view/'.$topproduct->slug.''); ?>">
                         <div class="card-top broder bg-gray text-center">                         
                             <?php 
                                if(!empty($topproduct['gallery']['images'][0]['url'])){ 
                                echo $this->Html->image($topproduct['gallery']['images'][0]['url'], ['alt' => 'Product', 'class' => 'my-img-responsive selected w-100 img-fluid h-100' , 'width' => '100%', 'height' => '100%']); 
                                }else{ echo $this->Html->image('frontend/food/product.png', ['alt' => 'Product', 'class' => 'card-img-top p-2 w-100 img-fluid h-100' , 'id'=>'mainImg']); 
                                 }
                              ?>
                         </div>
                     </a>
                      <div class="card-body product-section">
                       
                        <h6><a href="<?php echo $this->Url->build('/product/view/'.$topproduct->slug.''); ?>" style="color:black;"><?php echo $topproduct->name_en; ?></a></h6>  <span class="amt mb-2"><span class="fa fa-usd"></span> <?php echo $topproduct->price; ?></span>
                   
                      
                        <a href="<?php echo $this->Url->build('/product/view/'.$topproduct->slug.''); ?>" class="btn btn-warning btn-sm text-white product-btn">View Product Detail</a>
                      </div>
                    </div>
                </div><!--end item-->
                <?php } ?>             
            </div><!--end row-->
        </div><!--end container-->
    </section><!--end selling list-->    
    <?php if($homePageSections[1]['status'] ==1 ) { echo $homePageSections[1]['content']; } ?>
    <!--end banner list-->
    
    <!--bottom banner section-->
    <?php if($homePageSections[0]['status'] == 1 ) { ?>
    <section class="banner my-5 py-4">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h1 class="d-block">
                        <?php echo $homePageSections[0]['title']; ?>
                    </h1>
                </div><!--end col 12-->
            </div><!--row-->
            <div class="row mt-4">              
                <?php echo $homePageSections[0]['content'];  ?>


                <div class="col-12 col-sm-6 col-md-6">
                    <?php echo $this->Html->image("/".$homePageSections[0]['image'], ['alt' => 'Product',  'id'=> 'mainImg' , 'class' => 'img-fluid']);  ?>
                </div><!--end col-->

            </div><!--end row-->
        </div><!--end container-->
    </section><!--end banner list-->
    <div class="row iframe-holder mt-4">  
                <?php if(!empty($videos)){ foreach ($videos as $key => $video) {  ?>
                  <div class="col-sm-6 col-xs-12 col-md-6">
                    <div class="card p-3">
                      <div class="iframe-container w-100">
                        <?php echo $video->video_url; ?>
                      </div>
                    </div>
                  </div>
                <?php } } ?>
            </div>
<?php } ?>
    <style type="text/css">
        .food-banner-list{
            background: url('<?php echo $homePageSections[1]['image']; ?>') center center no-repeat;
            background-size: cover;
             position: relative;
        }
    </style>
        <?php
        echo $this->element('frontend/footer');
       
        ?>
        <!-- Bootstrap core JavaScript
            ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <?php echo $this->Html->script('frontend/wow'); ?>
        <?php echo $this->Html->script('frontend/jquery.validate.min'); ?>
        <?php echo $this->Html->script('frontend/moment'); ?>
        <?php echo $this->Html->script('frontend/bootstrap'); ?>
        <?php echo $this->Html->script('frontend/classie'); ?>
        <?php //echo $this->Html->script('frontend/contact'); ?>
        <?php echo $this->Html->script('frontend/custom'); ?>
        <?php echo $this->Html->script('frontend/jquery.fancybox.min'); ?>
        <?php echo $this->Html->script('frontend/bootstrap-datetimepicker.min'); ?>
        <?php echo $this->Html->script('frontend/cart'); ?>
        <?php echo $this->Html->script('https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js'); ?>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

    <script>
        $('.carousel').carousel({
          interval: 3000
        });
        $('#carouselExampleIndicators').hover(function(){
            $(".carousel").carousel('pause');
        },function(){
            $(".carousel").carousel('cycle');
        });
    </script>
       
        
    </body>

    <style type="text/css">
        /*.searchBox{
            margin-top: 135px;
            z-index: 2;
            position: relative;
        }
        .sliderBanner{
            margin-top: -170px;
        }*/

        .search-box{
            z-index: 0;
            position: absolute;
            top: 30%;
            left: 50%;
            transform: translate(-50%);
        }

        @media only screen and (max-width: 480px){
            .search-box{
                top: 35%;
            }
        }
    </style>
</html>