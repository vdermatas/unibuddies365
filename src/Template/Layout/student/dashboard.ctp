<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Student Profile</title>

     <?php
        echo $this->Html->css(['student/font-face' ,'student/bootstrap-multiselect' ,'student/fancymetags' , 'student/jquery-ui']);
        echo $this->Html->css(['student/vendor/font-awesome-5/css/fontawesome-all.min', 'student/vendor/mdi-font/css/material-design-iconic-font.min' , 'student/vendor/bootstrap-4.1/bootstrap.min' ,'student/vendor/animsition/animsition.min' ,'student/vendor/perfect-scrollbar/perfect-scrollbar' , 'student/jquery.rateyo', 'student/theme' , 'student/style' ]);
    ?>
    <?php
        echo $this->Html->script(['student/vendor/jquery-3.2.1.min']);
    ?>
</head>

<body>

    <div class="page-container2">
        
       <?php echo $this->element('student/header'); ?>
       <?php echo $this->element('student/aside'); ?>
        <!-- <section class="au-breadcrumb m-t-75">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 p-2 p-sm-0 mb-0 mb-sm-4 mt-0 mt-sm-4">
                            <div class="au-breadcrumb-content">
                                <div class="au-breadcrumb-left">
                                    <h4>Profile</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
        <?php echo $this->fetch('content'); ?>
    </div>
    <!-- Add Profile Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Add Profile</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Upload image</label>
                        <input type="file" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

     <?php
        echo $this->Html->script([ 'student/vendor/bootstrap-4.1/popper.min', 'student/vendor/bootstrap-4.1/bootstrap.min','student/vendor/animsition/animsition.min','student/vendor/perfect-scrollbar/perfect-scrollbar','student/js/main','student/js/bootstrap-multiselect','student/js/fancymetags.jQuery','student/js/jquery-ui','jquery.validate.js','additional-methods.min.js','student/js/jquery.rateyo' ]);
    ?>


    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $("#datepicker,#datepicker1").datepicker({
                    changeMonth: true,
                    changeYear: true
                });
            });

            /** add category **/
            $(function () {
                $("#tagfield").fancymetags({
                    theme: "black"
                });
            });

            /** multiselect **/
            $('#example-multiple-selected').multiselect({
                includeSelectAllOption: true,
            });

            /** Profile Nav **/
            $('.profile-nav-link').click(function () {
                let index = $(".profile-nav-link").index(this);
                $('.prfile-section').addClass('d-none');
                $('.profile-nav-link').removeClass('active');
                $(this).addClass('active');
                $(".prfile-section:eq(" + index + " )").removeClass('d-none');
            });

            // For each table within the content area...
            $('table').each(function (t) {
                // Add a unique id if one doesn't exist.
                if (!this.id) {
                    this.id = 'table_' + t;
                }
                // Prepare empty variables.
                var headertext = [],
                    theads = document.querySelectorAll('#' + this.id + ' thead'),
                    headers = document.querySelectorAll('#' + this.id + ' th'),
                    tablerows = document.querySelectorAll('#' + this.id + ' th'),
                    tablebody = document.querySelector('#' + this.id + ' tbody');
                // For tables with theads...
                for (var i = 0; i < theads.length; i++) {
                    // If they have more than 2 columns...
                    if (headers.length > 2) {
                        // Add a responsive class.
                        this.classList.add('responsive');
                        // Get the content of the appropriate th.
                        for (var i = 0; i < headers.length; i++) {
                            var current = headers[i];
                            headertext.push(current.textContent.replace(/\r?\n|\r/, ''));
                        }
                        // Apply that as a data-th attribute on the corresponding cells.
                        for (var i = 0, row; row = tablebody.rows[i]; i++) {
                            for (var j = 0, col; col = row.cells[j]; j++) {
                                col.setAttribute('data-th', headertext[j]);
                            }
                        }
                    }
                }
            });


             $("#changePassword").validate({
                    rules: {
                        old_password: {
                            required : true
                        },
                        password: {
                            required: true,
                            minlength: 5
                        },
                        confirm_password: {
                            required: true,
                            minlength: 5,
                            equalTo: "#password"
                        }
                    },
                    messages: {
                        old_password: "Please enter your old password",
                        password: {
                            required: "Please provide a password",
                            minlength: "Your password must be at least 5 characters long"
                        },
                        confirm_password: {
                            required: "Please provide a password",
                            minlength: "Your password must be at least 5 characters long",
                            equalTo: "Please enter the same password as above"
                        }
                    }
                });

             $("#studentProfile").validate({
                    rules: {
                        first_name: {
                            required : true
                        },
                        last_name: {
                            required : true
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        mobile: {
                            required: true,
                            minlength: 10
                        },
                        address: {
                            required : true
                        },
                        zipcode: {
                            required : true
                        }
                    },
                    messages: {
                        first_name: "Please enter your first name",
                        last_name: "Please enter your last name",
                        email: "Please enter a your email address",
                        address: "Please enter a your  address",
                        zipcode: "Please enter a your zipcode",
                        mobile: "Please Enter Valid Mobile Number"
                    }
                });

             $("#addAssignement").validate({
                    rules: {
                        title: {
                            required : true
                        },
                        description: {
                            required : true
                        },
                        assignment_type_value: {
                            required: true
                        },
                        reference_url: {
                            required : true
                        },
                        attachment_file: {
                            required : true,
                            extension: "docx|rtf|doc|pdf"
                        },
                        delivery_date: {
                            required : true
                        }
                    },
                    messages: {
                        title: "Please enter your assignment title",
                        description: "Please enter your assignment description",
                        reference_url: "Please enter a your reference url",
                        attachment_file: {
                            required:"Please upload your attachment file",                  
                            extension:"Please upload file formats properly eg: pdf , docx , rtf"
                        },
                        assignment_type_value: "Please enter a your value",
                        delivery_date: "Please select delivery date"
                    }
                });

             $("#editAssignement").validate({
                    rules: {
                        title: {
                            required : true
                        },
                        description: {
                            required : true
                        },
                        assignment_type_value: {
                            required: true
                        },
                        reference_url: {
                            required : true
                        },
                        delivery_date: {
                            required : true
                        }
                    },
                    messages: {
                        title: "Please enter your assignment title",
                        description: "Please enter your assignment description",
                        reference_url: "Please enter a your reference url",
                        assignment_type_value: "Please enter a your value",
                        delivery_date: "Please select delivery date"
                    }
                });

             $('.datepicker').datepicker({ minDate: 0 , dateFormat: 'yy-mm-dd'});
             
        });

        $(".rateyo").rateYo({
            onChange: function (rating, rateYoInstance) { 
              // console.log("RateYo initialized! with " + rating);
              $('#ratingVal').val(rating);
            }
        });
       
    </script>
    <style type="text/css">
        .error{
            color: red !important;
        }
        .ui-widget-header .ui-icon {
            background-image: url(<?php echo $this->Url->build('/images/ui-icons_444444_256x240.png'); ?>) !important;
        }
    </style>
</body>

</html>