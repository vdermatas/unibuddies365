 <section class="bg-gray py-5 add-address">
        <div class="container">
            <div class="row ">
                <div class="col-12 py-3">

                </div>
              </div>

            <div class="row">
              <div class="col-2"></div>
                <div class="col-8">

                      <article class="bg-white row">
                        <div class="col-12">

                          <?php echo $this->Flash->render(); ?><br>
                          <h3 class="mb-4">Contact us </h3>
                          <?php echo $this->Form->create("contact", ['id' => 'contactForm']); ?>
                              <div class="row">
                                  <div class="col-12">

                                      <div class="form-group">
                                          <label>Name</label>
                                          <?php echo $this->Form->control('name', ['label' => false, 'class' => 'form-control']); ?>
                                      </div>
                                      <div class="form-group">
                                          <label>Email</label>
                                          <?php echo $this->Form->control('email', ['label' => false, 'class' => 'form-control']); ?>
                                      </div>

                                      <div class="form-group">
                                          <label>Mobile Number</label>
                                          <?php echo $this->Form->control('phone', ['label' => false, 'class' => 'form-control']); ?>
                                      </div>
                                      <div class="form-group">
                                          <label>Message</label>
                                          <textarea name="message" id="msg" cols="30" rows="10" class="form-control"></textarea>
                                      </div>

                                      <div class="form-group">
                                          <?php echo $this->Form->button('Submit', ['type'=> 'submit', 'class'=> 'btn btn-warning btn-lg btn-block text-white']); ?>
                                      </div>
                                  </div>

                              </div>
                          </form>
                        </div>


                      </article>

                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->
    </section>
    <script type="text/javascript">
    $(document).ready(function(){
        $("#contactForm").validate({
            rules: {
                name: {
                    required : true
                },
                email: {
                    required: true,
                    email: true 
                },
                phone: {
                    required: true,
                    minlength: 10
                },
                message : {
                    required: true,
                    minlength: 10
                }
            },
            messages: {
                name: "Please enter your Name",
                email: "Please enter a valid email address",
                phone: "Please Enter Valid Mobile Number",
                message : "Please Enter Your Query here"
            }
        });
    });
    </script>