<!--  <script src='https://www.google.com/recaptcha/api.js'></script> -->
<script src='https://www.google.com/recaptcha/api.js?render=6Lco5owUAAAAAObl358-AtrXVdaxNdtJb3bb6aHG'></script>
 <section class="bg-gray py-5 add-address">
        <div class="container">
            <div class="row ">
                <div class="col-12 py-3">

                </div>
              </div>

            <div class="row">
                <div class="col-12">
                      <article class="bg-white row" id="getTouch">                        
                    <?php if($homePageSections[4]['status'] == 1 ){ ?>
                        <div class="col-md-6 col-12 col-sm-12">
                        <?php echo $homePageSections[4]['content']; ?>
                        <p><?php echo $homePageSections[4]['map']; ?></p>

                      </div>
                        <div class="col-md-6 py-4 col-12 col-sm-12">
                      <?php }else { ?>
                        <div class="col-md-12 py-4 col-12 col-sm-12">
                      <?php } ?>
                          <?php echo $this->Flash->render(); ?>
                          <h3 class="mb-4"><?php echo $homePageSections[4]['title']; ?></h3>
                          <?php echo $this->Form->create("contact", ['id' => 'contactForm']); ?>
                              <div class="row">
                                  <div class="col-12">

                                      <div class="form-group">
                                          <label>Name</label>
                                          <?php echo $this->Form->control('name', ['label' => false, 'class' => 'form-control']); ?>
                                      </div>
                                      <div class="form-group">
                                          <label>Email</label>
                                          <?php echo $this->Form->control('email', ['label' => false, 'class' => 'form-control']); ?>
                                      </div>

                                      <div class="form-group">
                                          <label>Mobile Number</label>
                                          <?php echo $this->Form->control('phone', ['label' => false, 'class' => 'form-control']); ?>
                                      </div>
                                      <div class="form-group">
                                          <label>Message</label>
                                          <textarea name="message" id="msg" cols="30" rows="10" class="form-control"></textarea>
                                      </div>
                                      <input type="hidden" id="g-recaptcha" value="" name="g-recaptcha-response">

                                      <div class="form-group">
                                          <?php echo $this->Form->button('Submit', ['type'=> 'submit', 'class'=> 'btn btn-warning btn-lg btn-block text-white']); ?>
                                      </div>
                                  </div>

                              </div>
                          </form>
                        </div>


                      </article>

                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->
    </section>
    <script type="text/javascript">
    $(document).ready(function(){
        $("#contactForm").validate({
            rules: {
                name: {
                    required : true
                },
                email: {
                    required: true,
                    email: true 
                },
                phone: {
                    required: true,
                    minlength: 10
                },
                message : {
                    required: true,
                    minlength: 10
                }
            },
            messages: {
                name: "Please enter your Name",
                email: "Please enter a valid email address",
                phone: "Please Enter Valid Mobile Number",
                message : "Please Enter Your Query here"
            }
        });
    });
    </script>
    <script>
grecaptcha.ready(function() {
grecaptcha.execute('6Lco5owUAAAAAObl358-AtrXVdaxNdtJb3bb6aHG', {action: 'contactUs'})
.then(function(token) {
  console.log(token);
  $('#g-recaptcha').val(token);
});
});
</script>