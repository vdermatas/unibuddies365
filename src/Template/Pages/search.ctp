<section class="bg-gray py-5">
        <div class="container">
            <div class="row category">
                <div class="col-12 col-sm-4 col-md-3">
                    <article>
                        <h5>Categories</h5>
                        <!-- <div class="list-group">
                            <a href="#" class="list-group-item list-group-item-action active">Home Needs (151)</a>
                            <a href="#" class="list-group-item list-group-item-action">Fruits and Vegetables (200)</a>
                            <a href="#" class="list-group-item list-group-item-action">Beverages (100)</a>
                            <a href="#" class="list-group-item list-group-item-action">Bread Dairy and Eggs (80)</a>
                            <a href="#" class="list-group-item list-group-item-action disabled">Personal Care</a>
                            <a href="#" class="list-group-item list-group-item-action disabled">Kids Utilities</a>
                        </div> -->
                        <?php 
                            if(!empty($productCategories)){
                                foreach ($productCategories as  $categories) {
                        ?>
                        <div class="list-group">
                            <a href='category/<?php echo $categories->slug; ?>' class="list-group-item list-group-item-action">
                                <?php echo $categories->name; echo " (".count($categories['products']).")";?> 
                            </a>
                        </div>
                        <?php }} ?>
                    </article>
                    <!--<article>
                        <h5>Brand</h5>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button">
                                    <span class="fa fa-search"></span>
                                </button>
                            </div>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                            <label class="form-check-label" for="defaultCheck1">The Solas</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                            <label class="form-check-label" for="defaultCheck1">18 Herbs</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck3">
                            <label class="form-check-label" for="defaultCheck1">24 Mantra</label>
                        </div>
                    </article>
                    <article>
                        <h5>Veg &amp; Non-veg</h5>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="">
                            <label class="form-check-label" for="defaultCheck1">Vegetarian</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="">
                            <label class="form-check-label" for="defaultCheck1">Non Vegetarian</label>
                        </div>
                    </article>-->
                </div>
                <div class="col-12 col-md-9 col-sm-8 mt-sm-0">
                    <?php echo $this->Flash->render(); ?>
                    <h5>All Search Products </h5>
                    <article class="row py-3 justify-content-center justify-content-sm-start">
                        <?php 
                            if(!empty($product)){
                                foreach ($product as $key => $value) {
                        ?>
                        <div class="col-auto mb-4">
                            <div class="card h-100">
                                <a href='product/view/<?php echo $value->slug; ?>'>
                                <?php 
                                if(!empty($value['gallery']['images'][0]['url'])){
                                echo $this->Html->image($value['gallery']['images'][0]['url'], ['alt' => 'Product', 'class' => 'card-img-top p-2']); 
                                 }else{ echo $this->Html->image('frontend/food/product.png', ['alt' => 'Product', 'class' => 'card-img-top p-2" id="mainImg']); 
                                }?>
                                </a>
                                <div class="card-body p-2">
                                    <a href="product/view/<?php echo $value->slug; ?>">
                                    <h5 class="card-title"><?php echo $value->name_en; ?></h5>
                                    </a>
                                    <input type="hidden" class="product_id" value="<?php echo $value->id;?>">
                                    <?php 
                                    if(!empty($cartDetail['Products'][$value->id])){
                                    if($cartDetail['Products'][$value->id]['product_id'] == $value->id){ 
                                    ?>
                                    <div class="row m-0">
                                        <div class="col-6 pl-0">
                                            <div class="qty-btn pt-2">
                                                <br>
                                            </div>
                                        </div>
                                        <div class="col-10 text-right pr-0">
                                            <a href="<?php echo $this->Url->build('/my-basket'); ?>" class="btn btn-warning btn-sm text-white">Product In Cart</a>
                                        </div>
                                    </div>
                                    <?php }else{ ?>
                                    <div class="row m-0">
                                        <div class="col-6 pl-0">
                                            <span>Qty</span>
                                            <div class="qty-btn pt-2">
                                                <a class="btn btn-sm btn-outline-dark minus_qty" data-product-id="<?php echo $value->id;?>" id="id<?php echo $value->id;?>">
                                                    <span class="fa fa-minus "></span>
                                                </a>
                                                <!-- <span class="d-inline px-2 total-count">1</span> -->
                                                <input type="text" style="width:27px;" class="charecters_val d-inline pl-1 quantity" id="quantity" value="1" data-pd-id = "<?php echo $value->id;?>" readonly>
                                                <a class="btn btn-sm btn-outline-dark add_qty" data-product-id="<?php echo $value->id;?>" id="class<?php echo $value->id;?>">
                                                    <span class="fa fa-plus"></span>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-6 text-right pr-0">
                                            <span class=" d-block mb-2"><span class="fa fa-usd"></span><span class="amt"> <?php echo number_format((float)$value->price, 2, '.', ''); ?></span></span>
                                            <button data-product-id="<?php echo $value->id;?>" data-product-qty='1'  class="btn btn-warning btn-sm text-white add-cart <?php echo $value->id;?>">Add to Cart</button>
                                        </div>
                                    </div>
                                    <?php } } else{ ?>
                                    <div class="row m-0">
                                        <div class="col-6 pl-0">
                                            <span>Qty</span>
                                            <div class="qty-btn pt-2">
                                                <a class="btn btn-sm btn-outline-dark minus_qty"data-product-id="<?php echo $value->id;?>" id="id<?php echo $value->id;?>">
                                                    <span class="fa fa-minus "></span>
                                                </a>
                                                <!-- <span class="d-inline px-2 total-count">1</span> -->
                                                <input type="text" style="width:27px;" class="charecters_val d-inline pl-1 quantity" id="quantity" value="1" data-pd-id = "<?php echo $value->id;?>" readonly>
                                                <a class="btn btn-sm btn-outline-dark add_qty" data-product-id="<?php echo $value->id;?>" id="class<?php echo $value->id;?>">
                                                    <span class="fa fa-plus"></span>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-6 text-right pr-0">
                                            <span class=" d-block mb-2"><span class="fa fa-usd"></span> <span class="amt<?php echo $value->id;?>"><?php  echo number_format((float)$value->price, 2, '.', ''); ?></span></span>
                                            <button data-product-id="<?php echo $value->id;?>" data-product-qty='1'  class="btn btn-warning btn-sm text-white add-cart <?php echo $value->id;?>">Add to Cart</button>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                       <?php } } ?>
                    </article>
                    <div class="box-footer clearfix">
                     <nav aria-label="Page navigation">
                        <ul class="pagination">
                            <?php
                            $this->Paginator->templates([
                                'prevActive' => '<li class="page-item"><a class="page-link" href="{{url}}"><<</a></li>',
                                'prevDisabled' => '<li class="page-item disabled"><a class="page-link" href="{{url}}"><<</a></li>',
                                'number' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
                                'current' => '<li class="page-item active"><a class="page-link" href="{{url}}">{{text}}</a></li>',
                                'nextActive' => '<li class="page-item"><a class="page-link" href="{{url}}">>></a></li>',
                                'nextDisabled' => '<li class="page-item disabled"><a class="page-link" href="{{url}}">>></a></li>'
                            ]); ?>
                            <?= $this->Paginator->prev() ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next() ?>
                        </ul>
                    </nav>
                    <p><?=$this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                </div>
                    <!-- <div class="clearfix">
                        <div class="pull-left">
                            <nav aria-label="Page navigation">
                                <ul class="pagination">
                                    <li class="page-item">
                                        <a class="page-link" href="#" aria-label="Previous">
                                            <span aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                    </li>
                                    <li class="page-item active">
                                        <a class="page-link" href="#">1</a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link" href="#">2</a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link" href="#">3</a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link" href="#" aria-label="Next">
                                            <span aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <a href="" class="btn btn-warning pull-right text-white">Continue</a>
                    </div> -->
                </div>
            </div>
        </div>
    </section>
    <!-- add-product total-count subtract-product -->
    <script type="text/javascript">
    $(document).on('blur','.quantity',function(){
        var pid = $(this).attr('data-pd-id');
        var added = $(this).val();
        $('button.add-cart[data-product-id="'+pid+'"]').attr('data-product-qty',added);
    });
   $(document).on('click','.add_qty',function(){
        var quantity = parseFloat($(this).closest('div').find('input').val());
        if(isNaN(quantity)) { var quantity = 0; }
        var added = quantity + 1;
        $(this).closest('div').find('input').val(added);
        var pid = $(this).attr('data-product-id');
        var amount = parseFloat($('.amt'+pid).html());
        var total = amount*added/quantity;
        $('.amt'+pid).html(total.toFixed(2));
        $('button.add-cart[data-product-id="'+pid+'"]').attr('data-product-qty',added);
    });
   $(document).on('click','.minus_qty',function(){
        var quantity = parseFloat($(this).closest('div').find('input').val());
        var added = quantity - 1;
         if(added <= 0){
            return false;
        }
        $(this).closest('div').find('input').val(added);
        var pid = $(this).attr('data-product-id');
        var amount = parseFloat($('.amt'+pid).html());
        var total = amount*added/quantity;
        $('.amt'+pid).html(total.toFixed(2));
        $('button.add-cart[data-product-id="'+pid+'"]').attr('data-product-qty',added);

    });
   $(document).on('click','.add-cart',function(){
        var quantity = $(this).attr('data-product-qty');
        var product_id = $(this).attr('data-product-id');
        // alert(quantity);return false;
        if(quantity <= 0 || quantity == ''  || quantity == null){
            alert('Quantity Must be Greater than zero');
            return false;
        }
        $.ajax({
                  type: "POST",
                  url: "add-cart",
                  dataType: "json",
                  data: {quantity:quantity,product_id:product_id},
                  success:function(data)
                  {            
                    $('.top-cart-count').html(" ( "+data.cart_count+" )");
                    $('.'+product_id).removeClass("add-cart");
                    $('.'+product_id).html('Product Added to Cart');
                    $('#id'+product_id).css("display","none");
                    $('#class'+product_id).css("display","none");
                    $('.'+product_id).css('margin-left','-60px');
                  },
                  error: function (){ }
            });
    });
   $('.charecters_val').keyup(function () { 
    this.value = this.value.replace(/[^1-9\:]/g,'');
});
    </script>