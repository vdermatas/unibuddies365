<?php
    use Cake\Core\Configure;
    $session = $this->request->session();
?>
<section class="py-5 add-address about-us">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    
                   <h2 class="mb-4"><?php echo $content['title_en']?></h2>
                   <p><?php echo $content['content_en']?></p>


                    </div><!--end col-->              
            </div><!--end row-->
             <div class="row iframe-holder mt-4">  
                <?php if(!empty($videos)){ foreach ($videos as $key => $video) {  ?>
                  <div class="col-sm-6 col-xs-12 col-md-6">
                    <div class="card p-3">
                      <div class="iframe-container w-100">
                        <?php echo $video->video_url; ?>
                      </div>
                    </div>
                  </div>
                <?php } } ?>
            </div>
        </div><!--end container-->
    </section>
    <script type="text/javascript">
        $(document).ready(function(){
            $('iframe').removeAttr("width");
            $('iframe').attr("width",'100%');
        });
    </script>