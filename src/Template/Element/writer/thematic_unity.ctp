
<select class="form-control" id="thematic-unity" multiple="multiple" name="thematic_type[]">
    <option value="Finance and Economics" <?php if(array_search('Finance and Economics', array_column($user['writer_profile']['writer_thematics'], 'thematic_type')) !== False) {  echo "selected"; } ?>>Finance and Economics</option>
    <option value="Law"  <?php if(array_search('Law', array_column($user['writer_profile']['writer_thematics'], 'thematic_type')) !== False) {  echo "selected"; } ?>>Law</option>
    <option value="Business and Management" <?php if(array_search('Business and Management', array_column($user['writer_profile']['writer_thematics'], 'thematic_type')) !== False) {  echo "selected"; } ?>>Business and Management</option>
    <option value="Computing and ICT"  <?php if(array_search('Computing and ICT', array_column($user['writer_profile']['writer_thematics'], 'thematic_type')) !== False) {  echo "selected"; } ?>>Computing and ICT</option>
    <option value="Contruction and Engineering" <?php if(array_search('Contruction and Engineering', array_column($user['writer_profile']['writer_thematics'], 'thematic_type')) !== False) {  echo "selected"; } ?>>Contruction and Engineering</option>
    <option value="Education" <?php if(array_search('Education', array_column($user['writer_profile']['writer_thematics'], 'thematic_type')) !== False) {  echo "selected"; } ?>>Education</option>
    <option value="Health and Nursing"  <?php if(array_search('Health and Nursing', array_column($user['writer_profile']['writer_thematics'], 'thematic_type')) !== False) {  echo "selected"; } ?>>Health and Nursing</option>
    <option value="Marketing"  <?php if(array_search('Marketing', array_column($user['writer_profile']['writer_thematics'], 'thematic_type')) !== False) {  echo "selected"; } ?>>Marketing</option>
    <option value="Mathematics and Statistics" <?php if(array_search('Mathematics and Statistics', array_column($user['writer_profile']['writer_thematics'], 'thematic_type')) !== False) {  echo "selected"; } ?>>Mathematics and Statistics</option>
    <option value="Medical Science" <?php if(array_search('Medical Science', array_column($user['writer_profile']['writer_thematics'], 'thematic_type')) !== False) {  echo "selected"; } ?>>Medical Science</option>
    <option value="Politics and International Relations" <?php if(array_search('Politics and International Relations', array_column($user['writer_profile']['writer_thematics'], 'thematic_type')) !== False) {  echo "selected"; } ?>>Politics and International Relations</option>
    <option value="Science" <?php if(array_search('Science', array_column($user['writer_profile']['writer_thematics'], 'thematic_type')) !== False) {  echo "selected"; } ?>>Science</option>
    <option value="Accounting" <?php if(array_search('Accounting', array_column($user['writer_profile']['writer_thematics'], 'thematic_type')) !== False) {  echo "selected"; } ?>>Accounting</option>
    <option value="Social Sciences" <?php if(array_search('Social Sciences', array_column($user['writer_profile']['writer_thematics'], 'thematic_type')) !== False) {  echo "selected"; } ?>>Social Sciences</option>
    <option value="Shipping and Maritime"  <?php if(array_search('Shipping and Maritime', array_column($user['writer_profile']['writer_thematics'], 'thematic_type')) !== False) {  echo "selected"; } ?>>Shipping and Maritime</option>
    <option value="Other" <?php if(array_search('Other', array_column($user['writer_profile']['writer_thematics'], 'thematic_type')) !== False) {  echo "selected"; } ?>>Other</option>

</select>