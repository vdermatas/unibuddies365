<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 p-2 p-sm-0 mb-4 mt-4">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <h4>Ongoing Assignments</h4>
                        </div>
                        <a href="#" class="btn browse-assignment">
                            Browse Assignment</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="section-content mt-1 mr-4 ml-4 mb-4">
    <div class="col-sm-12 px-4 sort-by">
        <form class="form-inline">
            <input type="text" class="form-control mr-2" placeholder="Search">
            <button class="btn btn-primary mt-3 mt-sm-0">Search</button>
        </form>
    </div>
    <div class="table-responsive mobile-res-tb ongoing-assignment-page">
        <table class="table  header-text-left">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Lesson title</th>
                    <th scope="col">Price</th>
                    <th scope="col">Delivery Schedule</th>
                    <th scope="col">Next Delivery</th>
                    <th scope="col">Last Delivered</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td scope="row">242344</td>
                    <td><a href="#">Annotated bibliograph..</a>
                        <div class="client"><b>Client:</b> Seth Steve</div>
                    </td>
                    <td class="price-box">€90 <div class="custom-label text-info text-white px-1"> (1 delivery
                            date)</div>
                    </td>
                    <td class="delivery-schedule-td">
                        <span class="mr-3"> <a data-toggle="modal" class="add-delivery-date"
                                data-target="#setDeliveryModal" href="#"> <img src="<?php echo $this->Url->build('/images/plus.png'); ?>"> Add</a>
                        </span>
                        <span> <a data-toggle="modal" class="add-delivery-date" data-target="#viewDelivery"
                                href="#"><img src="<?php echo $this->Url->build('/images/information.png'); ?>"> View</a>
                        </span>
                    </td>
                    <td>24 May, 2019</td>
                    <td>24 May, 2019</td>

                    <td>
                        <div class="dropdown">
                            <button class=" more-btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="<?php echo $this->Url->build('/images/more-horiz.svg'); ?>">
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" data-toggle="modal" data-target="#ViewFeedback"
                                    href=" #">Mark as Completed
                                </a>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>