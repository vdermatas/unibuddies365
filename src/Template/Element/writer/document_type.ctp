
<select class="form-control" id="type-of-document" multiple="multiple" name="document_type[]">
    <option value="Thesis" <?php if(array_search('Thesis', array_column($user['writer_profile']['writer_documents'], 'document_type')) !== False) {  echo "selected"; } ?>>Thesis</option>
    <option value="Bachelor's thesis" <?php if(array_search('Bachelors thesis', array_column($user['writer_profile']['writer_documents'], 'document_type')) !== False) {  echo "selected"; } ?>>Bachelor's thesis</option>
    <option value="doctoral dissertation" <?php if(array_search('doctoral dissertation', array_column($user['writer_profile']['writer_documents'], 'document_type')) !== False) {  echo "selected"; } ?>>doctoral dissertation</option>
    <option value="statistical analysis" <?php if(array_search('statistical analysis', array_column($user['writer_profile']['writer_documents'], 'document_type')) !== False) {  echo "selected"; } ?>>statistical analysis</option>
    <option value="corrections" <?php if(array_search('corrections', array_column($user['writer_profile']['writer_documents'], 'document_type')) !== False) {  echo "selected"; } ?>>corrections</option>
    <option value="Semester assigment" <?php if(array_search('Semester assigment', array_column($user['writer_profile']['writer_documents'], 'document_type')) !== False) {  echo "selected"; } ?>>Semester assigment</option>
    <option value="Postgraduate assignment" <?php if(array_search('Postgraduate assignment', array_column($user['writer_profile']['writer_documents'], 'document_type')) !== False) {  echo "selected"; } ?>>Postgraduate assignment</option>
    <option value="Presentation" <?php if(array_search('Presentation', array_column($user['writer_profile']['writer_documents'], 'document_type')) !== False) {  echo "selected"; } ?>>Presentation</option>
    <option value="Scientific Article" <?php if(array_search('Scientific Article', array_column($user['writer_profile']['writer_documents'], 'document_type')) !== False) {  echo "selected"; } ?>>Scientific Article</option>
    <option value="Translation" <?php if(array_search('Translation', array_column($user['writer_profile']['writer_documents'], 'document_type')) !== False) {  echo "selected"; } ?>>Translation</option>
    <option value="Citation Resources" <?php if(array_search('Citation Resources', array_column($user['writer_profile']['writer_documents'], 'document_type')) !== False) {  echo "selected"; } ?>>Citation Resources</option>

</select>