<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<center><div class="message error alert alert-danger" onclick="this.classList.add('hidden');"><?= $message ?></div></center>
