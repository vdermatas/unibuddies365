<nav class="navbar navbar-expand-lg fixed-top navbar-light custom-nav ">
    <a href="<?php echo $this->Url->build('/', true);?>" class="navbar-brand"><img src="images/logo.svg" alt=""></a>
    <button class="navbar-toggler " type="button">
        <span></span>
        <span></span>
        <span></span>
    </button>

    <div class="site-header-mobi-panel ">
        <div class="site-header-mobi">
            <nav class="site-nav">
                <ul class="site-nav-menu list-unstyled">
                    <?php foreach (json_decode($frontendMenus[0]['data']) as $key => $menus) {  ?>
                        <li>
                            <a href="<?php if(!empty($menus->custom_url)){ echo $this->Url->build($menus->custom_url); }else{ echo $this->Url->build("/$menus->page"); } ?>" ><?php echo $menus->text; ?></a> 
                        </li>
                    <?php } ?>

                    <li class="nav-item">
                        <a href="?lang=en_EN" class="nav-link"><img src="<?php echo $this->Url->build('/images/flags/united-kingdom.svg'); ?>" style="width: 30px;height: 20px;"></a>                        
                    </li>
                    <li class="nav-item">
                        <a href="?lang=gre_GRE" class="nav-link"><img src="<?php echo $this->Url->build('/images/flags/greek.svg'); ?>" style="width: 30px;height: 20px;"></a>                        
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
             <?php foreach (json_decode($frontendMenus[0]['data']) as $key => $menus) {  
                 if(!empty($menus->children)) { ?> 
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="Preview" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                         <?php echo $menus->text; ?>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="Preview">
                           <?php foreach ($menus->children as $key => $value) { ?>
                                <a class="dropdown-item" href="#">Umrah Visa</a>
                          <?php } ?>
                        </div>
                    </li>

                 <?php }else{ ?>
                    <li class="nav-item">
                        <a href="<?php if(!empty($menus->custom_url)){ echo $this->Url->build($menus->custom_url); }else{ echo $this->Url->build("/$menus->page"); } ?>" class="nav-link"><?php echo $menus->text; ?></a> 
                    </li>
                <?php } ?>
            <?php } ?>
            <li class="nav-item">
                <a href="?lang=en_EN" class="nav-link"><img src="<?php echo $this->Url->build('/images/flags/united-kingdom.svg'); ?>" style="width: 30px;height: 20px;"></a>                        
            </li>
            <li class="nav-item">
                <a href="?lang=gre_GRE" class="nav-link"><img src="<?php echo $this->Url->build('/images/flags/greek.svg'); ?>" style="width: 30px;height: 20px;"></a>                        
            </li>
            <!-- <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" id="Preview" href="#" role="button" aria-haspopup="true" aria-expanded="false">Service</a>
                <div class="dropdown-menu" aria-labelledby="Preview">
                    <a class="dropdown-item" href="#">Umrah Visa</a>
                    <a class="dropdown-item" href="#">Umrah Kit</a>
                    <a class="dropdown-item" href="#">Hotel</a>
                    <a class="dropdown-item" href="#">Food</a>
                    <a class="dropdown-item" href="#">Transport</a>
                </div>
            </li> -->
        </ul>
    </div>
</nav>

