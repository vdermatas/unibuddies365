<?php
    use Cake\Core\Configure;


?>
<nav class="navbar navbar-expand-lg navbar-dark bg-transparent position-absolute py-0">
                <a class="navbar-brand pt-0" href="<?php echo $this->Url->build('/'); ?>">
                     <?php echo $this->Html->image('frontend/'.$bg_images[0]['image'].'', ['alt' => 'unibuddies365']); ?>
                   
                </a>
                <button class="navbar-toggler" type="button" onclick="openNav()" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto sidenav" id="mySidenav">
                        <a href="javascript:void(0)" class="closebtn d-lg-none d-block" onclick="closeNav()">&times;</a>
                        
                        <?php 

                        foreach (json_decode($frontendMenus[0]['data']) as $key => $menus) { 
    
                            if(!empty($menus->children)) { ?>                                
                            <li class="nav-item dropdown custom-dropdown">
                                <a class="nav-link dropdown-toggle" href="<?php if(!empty($menus->custom_url)){ echo $this->Url->build("/$menus->custom_url"); }else{ echo $this->Url->build("/$menus->page"); } ?>" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 <?php echo $menus->text; ?>
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <li><a class="dropdown-item" href="<?php echo $this->Url->build("/$menus->page"); ?>"><?php echo $menus->text; ?></a></li>
                               <?php foreach ($menus->children as $key => $value) {
                                    # code...       
                                    if(!empty($value->children)){ ?>
                                       
                                        <li class="dropdown-submenu">
                                          <a class="dropdown-item dropdown-toggle " href="<?php if(!empty($value->custom_url)){ echo $this->Url->build("/$value->custom_url"); }else{echo $this->Url->build("/$value->page"); } ?>"><?php  echo $value->text; ?></a>
                                             <ul class="dropdown-menu">
                                     <?php foreach ($value->children as $key => $values) {
                                            if(!empty($values->children)){ ?>
                                               
                                                <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="<?php if(!empty($values->custom_url)){ echo $this->Url->build("/$values->custom_url"); }else{ echo $this->Url->build("/$values->page"); } ?>"><?php  echo $values->text; ?></a>
                                                         <ul class="dropdown-menu">
                                             <?php   foreach ($values->children as $key => $node) {
                                                    # code... 
                                                     if(!empty($node->children)) {  ?>
                                                        <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="<?php if(!empty($node->custom_url)){ echo $this->Url->build("/$node->custom_url"); }else{echo $this->Url->build("/$node->page"); } ?>"><?php  echo $node->text; ?></a>
                                                         <ul class="dropdown-menu">
                                                        <?php    foreach ($node->children as $keys => $secondnode) { ?>
                                                               <li><a class="dropdown-item" href="<?php if(!empty($secondnode->custom_url)){ echo $this->Url->build("/$secondnode->custom_url"); }else{ echo $this->Url->build("/$secondnode->page"); } ?>"><?php echo $secondnode->text; ?></a></li>
                                                       <?php     } ?> </ul> <?php }else{
                                                    ?>
                                                    
                                                     <li><a class="dropdown-item" href="<?php if(!empty($node->custom_url)){ echo $this->Url->build("/$node->custom_url"); }else{echo $this->Url->build("/$node->page"); } ?>"><?php echo $node->text; ?></a></li>
                                              <?php }  } ?> </ul>
                                         <?php   }else{ ?>
                                                
                                                <li><a class="dropdown-item" href="<?php if(!empty($values->custom_url)){ echo $this->Url->build("/$values->custom_url"); }else{echo $this->Url->build("/$values->page"); }?>"><?php echo $values->text; ?></a></li>
                                          <?php  }
                                            
                                        } ?> </ul></li> <?php 
                                    }else{ ?>
                                        <li><a class="dropdown-item" href="<?php if(!empty($value->custom_url)){ echo $this->Url->build("/$value->custom_url"); }else{echo $this->Url->build("/$value->page"); }?>"><?php echo $value->text; ?></a></li>
                            <?php  }
                                } ?> </ul></li>
                           <?php }else{ ?>
                             <li class="nav-item">
                                <a class="nav-link" href="<?php if(!empty($menus->custom_url)){ echo $this->Url->build("/$menus->custom_url"); }else{ echo $this->Url->build("/$menus->page"); } ?>"><?php echo $menus->text; ?></a>
                            </li> 
                         <?php   }
                        }
                        ?>
                        
                        <li class="nav-item d-lg-block d-none">
                            <a class="nav-link line_sec" href="#">|</a>
                        </li>
                        
                        <?php if(!empty($this->request->getSession()->read('Auth.Customer'))){ ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                            Hi <?php echo $this->request->getSession()->read('Auth.Customer.first_name'); ?> 
                          </a>
                          <ul class="dropdown-menu custom-dropdown">
                            <!-- User image -->
                                <li class="user-header">
                                    <?php  if($this->request->getSession()->read('Auth.Customer.profile_image') != "" || $this->request->getSession()->read('Auth.Customer.profile_image') != NULL){ 
                                         echo $this->Html->image('/files/uploads/'.$this->request->getSession()->read('Auth.Customer.profile_image'), array('class' => 'rounded-circle user-img', 'alt' => 'User profile picture')); 
                                    }else{ ?>
                                    <img src="https://wrappixel.com/demos/admin-templates/material-pro/assets/images/users/1.jpg" class="rounded-circle user-img" alt="User Image">
                                    <?php } ?>                        <p>
                                        <?php echo $this->request->getSession()->read('Auth.Customer.first_name')." ".$this->request->getSession()->read('Auth.Customer.last_name'); 
                                        ?>  <br>                          
                                        <small> 
                                            <?php echo $this->request->getSession()->read('Auth.Customer.email'); ?>                          </small>
                                    </p>
                                </li>                            
                                
                                <li class="user-footer d-flex justify-content-between">
                                    <div>                                    
                                        <a href="<?php echo $this->Url->build('/my-profile'); ?>" class="btn btn-warning btn-block text-white">Profile</a>                            
                                    </div>
                                    <div>
                                       <a href="<?php echo $this->Url->build('/users/logout'); ?>" class="btn custom-btn">Log Out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>

                        <?php } else { ?>
                        <li class="nav-item">
                        <a class="nav-link" href="<?php echo $this->Url->build('/login'); ?>">Login</a>
                        </li>   
                        <?php } ?>
                        <li class="nav-item d-lg-block d-none">
                            <a class="nav-link line_sec" href="#">|</a>
                        </li>
                        <li class="nav-item">
                            <!-- <a class="nav-link" href="my-basket"> -->
                            <a class="nav-link" href="<?php echo $this->Url->build('/my-basket'); ?>">
                                <span class="fa fa-shopping-cart top-cart-count"> ( <?php echo $cart_count; ?> ) </span>
                            </a>
                        </li>
                        
                    </ul>
                </div>
            </nav>
            