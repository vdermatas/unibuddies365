<?php
	use Cake\Core\Configure;
?>
<nav class="navbar navbar-default navbar-fixed-top  nav-top  clearfix ">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?php echo Configure::read("APP_BASE_URL"); ?>">
				<?php echo $this->Html->image('frontend/logo.png', ['alt' => 'logo', 'class' => 'img-responsive']); ?>
			</a>
		</div>
		<div id="navbar" class="collapse navbar-collapse">
			<ul class="nav navbar-nav navbar-right ">
				<li class="">
					<a href="<?php echo Configure::read("APP_BASE_URL"); ?>"><?php echo __('Home'); ?></a>
				</li>
				<li class="">
					<a href="<?php echo Configure::read("APP_BASE_URL"); ?>/about-us"><?php echo __('About Us'); ?></a>
				</li>
				<li class="">
					<a href="<?php echo Configure::read("APP_BASE_URL"); ?>/tours"><?php echo __('Tours'); ?></a>
				</li>
				<li class="">
					<a href="<?php echo Configure::read("APP_BASE_URL");?>/transportations"><?php echo __('Transporation'); ?></a>
				</li>
				<li class="">
					<a href="<?php echo Configure::read("APP_BASE_URL"); ?>/contact-us"><?php echo __('Contact Us'); ?></a>
				</li>
				<li class="svg-width">
					<?php
						echo $this->Html->image("frontend/united-kingdom.svg", [
						    "alt" => "uk",
						    'url' => $this->Url->build() . '?lang=en_US'
						]);
					?>
				</li>
				
				<li class="svg-width">
					<?php
						echo $this->Html->image("frontend/mexico.svg", [
														"alt" => "mexixo",
														'url' => $this->Url->build() . '?lang=es_SPA'
													]);
					?>
				</li>
				<li class="">
					<a href="<?php echo Configure::read("APP_BASE_URL").'/basket'; ?>"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="badge cart-quantity" id='cart-quantity'><?php if(isset($cart_count)){ echo $cart_count; }else{ echo "0"; };?></span></a>
				</li>
			</ul>
		</div>
	</div>
</nav>