<?php
	use Cake\Core\Configure;
	$session = $this->request->session();
?>
<div class="best-price">
	<div class="container">
		<div class="col-sm-5 wow slideInLeft">
			<div class="white-wraper bottom-white-wraper">
				<h2><?php echo __('Best prices, best holidays'); ?></h2>
				<!-- <strong><?php echo __('Sit back and relax. Book any of our services and excursions with the Best Prices in the Riviera Maya Area.'); ?></strong>
				<p>
				<p><?php echo __('We offer you the Best Cancun Airport transportation with experienced drivers to provide safe tranfers to your hotel. Find with us the best deals to get to any destination and to discover unforgettable places such as mayan ruins, cenotes and natural parks!'); ?>
				</p>
				</p>
				<p>
					<?php echo __('With over 10 years experience as transportation company offering the best pick ups between the International Airport and the Hotels in Cancun and the Riviera Maya.'); ?>
				</p> -->
				<?php if(!empty($homeData['best_prices_best_holidays_'.$session->read('Config.language')])){
					echo $homeData['best_prices_best_holidays_'.$session->read('Config.language')];
				}
				?>
			</div>
		</div>
		<div class="col-sm-7 wow slideInRight">
			<div class="white-wraper">
				<h2><i class="fa fa-share-alt" aria-hidden="true"></i> <?php echo __('Shared shuttle'); ?>:</h2>
				<!-- <p><?php echo __('Share transportation to your hotel, with people arriving a the same time as you in order to keep costs down. Shuttle Vans are always available at the airport for you not to wait longer than 20 minutes.'); ?></p> -->
				<?php if(!empty($homeData['shared_shuttle_'.$session->read('Config.language')])){
					echo $homeData['shared_shuttle_'.$session->read('Config.language')];
				}
				?>
			</div>
			<div class="white-wraper">
				<h2><i class="fa fa-key" aria-hidden="true"></i> <?php echo __('Private shuttle'); ?>:</h2>
				<!-- <p>
					<?php echo __('Have a personal driver take you to your hotel for your dream vacation in Cancun in one of our Shuttle Vans. If you have a group of people coming with you this is the best option to have someone waiting for you just in time for your arrival. No waiting time.'); ?>
				</p> -->
				<?php if(!empty($homeData['private_shuttle_'.$session->read('Config.language')])){
					echo $homeData['private_shuttle_'.$session->read('Config.language')];
				}
				?>
			</div>
		</div>
	</div>
	<div class="bottom-strip">
		<div class="col-sm-8 text-strip  ">
			<!-- <h3><?php echo __('NEED SOME HELP PLANNING YOUR TRIP?'); ?></h3>
			<h3><?php echo __('Whatsapp Us On'); ?></h3>
			<div class="whatsapp-number">+5219987349473</div> -->
			<?php if(!empty($homeData['whats_app_'.$session->read('Config.language')])){
					echo $homeData['whats_app_'.$session->read('Config.language')];
				}
				?>
		</div>
	</div>
</div>