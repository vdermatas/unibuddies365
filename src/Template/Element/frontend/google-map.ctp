<style>
  #map{
    height: 300px;
    width: 100%;  
  }
</style>

<div aria-labelledby="myModalLabel" class="modal fade" id="mapModal" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo __('Select Pickup Location'); ?></h4>
      </div>
      <div class="modal-body">
        <div id="map"></div>
      </div>
    </div>
  </div>
</div>

<script>
  $('.booking-map').one('click', function() {
     initMap();
  });

  var map;
  var markers = [];

  function initMap() {
     map = new google.maps.Map(document.getElementById('map'), {
         center: {
             lat: 21.0403459,
             lng: -86.875758
         },
         zoom: 12,
     });

     $('#mapModal').on('shown.bs.modal', function() {
         var currentCenter = map.getCenter(); // Get current center before resizing
         google.maps.event.trigger(map, "resize");
         map.setCenter(currentCenter); // Re-set previous center
     });

     var geocoder = new google.maps.Geocoder;
     var infowindow = new google.maps.InfoWindow;
     var marker = new google.maps.Marker;

     google.maps.event.addListener(map, 'click', function(event) {
         if (infowindow) {
             infowindow.close();
         }
         if (marker) {
             for (var i = 0; i < markers.length; i++) {
                 markers[i].setMap(null);
             }
             markers.length = 0;
         }
         var latlngu = event.latLng;
         var address = getAddress(geocoder, latlngu, infowindow);
     });
  }

  function getAddress(geocoder, latlngu, infowindow) {
    
     var latlngStr = String(latlngu).trim().replace('(', '').replace(')', '').split(',', 2);
     var latlng = {
         lat: parseFloat(latlngStr[Object.keys(latlngStr)[0]]),
         lng: parseFloat(latlngStr[Object.keys(latlngStr)[1]])
     };
     geocoder.geocode({
         'location': latlng
     }, function(results, status) {
         if (status === 'OK') {
             if (results[0]) {
                 var marker = new google.maps.Marker({
                     position: latlng,
                     map: map
                 });
                 markers.push(marker);
                 infowindow.setContent(results[0].formatted_address);
                 infowindow.open(map, marker);
                 $("input[name='pickup_location']").val(results[0].formatted_address).closest(".input--nao").addClass("input--filled");

                 $("#mapModal").modal("hide");

             } else {
                 window.alert('No results found');
             }
         } else {
             window.alert('Geocoder failed due to: ' + status);
         }
     });
  }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmUmIiFFyHv6Jv5_X3a6C0ogQgQbUgwMw"
    async defer></script>