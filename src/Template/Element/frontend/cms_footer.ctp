
<footer class="footer">
    <div class="container">
        <div class="row mb-sm-4 mb-0">
            <img class="footer-logo" src="images/footer-logo.png">
        </div>
        <div class="row">
            <div class="col-sm-7 pl-0">
                <ul class="list-inline">
                    <li class="list-inline-item">
                        <a href="<?php echo $this->Url->build('/', true);?>">Home</a>
                    </li>
                    <li class="list-inline-item">
                        <a href="">About</a>
                    </li>
                    <li class="list-inline-item">
                        <a href="">Services</a>
                    </li>
                    <li class="list-inline-item">
                        <a href="">Resources</a>
                    </li>
                    <li class="list-inline-item">
                        <a href="">FAQs</a>
                    </li>
                    <li class="list-inline-item">
                        <a href="">Privacy Policy</a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-5 newsletter">
                <label>Join our Newsletter</label>
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Enter your email"
                        aria-label="Recipient's username" aria-describedby="button-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button" id="button-addon2">Subscribe</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-0 mt-sm-0">
            <div class="col-sm-7 follow-us-section pl-0">
                <div class="sub-heading">Follow Us:</div>
                <ul class="list-inline">
                    <li class="list-inline-item"><a href="#"><img src="images/facebook.png"></a></li>
                    <li class="list-inline-item"><a href="#"><img src="images/twitter.png"></a></li>
                    <li class="list-inline-item"><a href="#"><img src="images/instagram.png"></a></li>
                    <li class="list-inline-item"><a href="#"><img src="images/linkdin.png"></a></li>
                </ul>
            </div>
            <div class="col-sm-5 copy-right">
                <p>&copy;Copyright 2019. All Rights Reserved</p>
            </div>
        </div>
    </div>
</footer>
