<div class="cheapest-shuttle">
	<div class="container ">
		<h1 class="text-center wow zoomIn"><?php echo __('Best and cheapest shuttles in'); ?> <span class="see-text">Cancun</span></h1>
		<p class="text-center"><?php echo __('Sit back and relax. Book any of our services and excursions with the Best Prices in the Riviera Maya Area.'); ?></p>
		
		<div class="gallery-cancun clearfix col-sm-12">
			<div class="row">
				<div class="col-sm-3"><img class="img-responsive" alt="img" src="img/frontend/img-2.jpg"></div>
				<div class="col-sm-6"><img class="img-responsive" alt="img" src="img/frontend/img-3.jpg"></div>
				<div class="col-sm-3"><img class="img-responsive" alt="img" src="img/frontend/img-1.jpg"></div>
			</div>
			<div class="row">
				<div class="col-sm-3"><img class="img-responsive" alt="img" src="img/frontend/img-4.jpg"></div>
				<div class="col-sm-3"><img class="img-responsive" alt="img" src="img/frontend/img-5.jpg"></div>
				<div class="col-sm-3"><img class="img-responsive" alt="img" src="img/frontend/img-6.jpg"></div>
				<div class="col-sm-3"><img class="img-responsive" alt="img" src="img/frontend/img-7.jpg"></div>
			</div>
		</div>
		
	</div>
</div>