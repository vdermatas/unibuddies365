<?php
    use Cake\Core\Configure;
    $session = $this->request->getSession();
?>
    <!--footer section-->
    <footer class="pt-5 text-center">
        <?php if($homePageSections[3]['status'] == 1 ) { ?>
        <div class="col-12">
            <div class="row py-4">
                <div class="col-md-4 col-sm-12">
                    
                    <div class="col-12">
                        <ul class="list-inline">
                            <?php if(!empty($socialmedialinks['facebook'])){ ?>
                            <li class="list-inline-item">
                                <a href="<?php echo $socialmedialinks['facebook']; ?>" target="_blank">
                                    <span class="fa fa-2x fa-facebook-square"></span>
                                </a>
                            </li>
                            <?php } ?>
                            <?php if(!empty($socialmedialinks['twitter'])){ ?>
                            <li class="list-inline-item">
                                <a href="<?php echo $socialmedialinks['twitter']; ?>" target="_blank">
                                    <span class="fa fa-2x fa-twitter-square"></span>
                                </a>
                            </li>
                            <?php } ?>
                            <?php if(!empty($socialmedialinks['pinterest'])){ ?>
                            <li class="list-inline-item">
                                <a href="<?php echo $socialmedialinks['pinterest']; ?>" target="_blank">
                                    <span class="fa fa-2x fa-google-plus-square"></span>
                                </a>
                            </li>
                            <?php } ?>
                            <?php if(!empty($socialmedialinks['google'])){ ?>
                            <li class="list-inline-item">
                                <a href="<?php echo $socialmedialinks['google']; ?>" target="_blank">
                                    <span class="fa fa-2x fa-pinterest-square"></span>
                                </a>
                            </li>
                            <?php } ?>
                            <?php if(!empty($socialmedialinks['linkedin'])){ ?>
                            <li class="list-inline-item">
                                <a href="<?php echo $socialmedialinks['linkedin']; ?>" target="_blank">
                                    <span class="fa fa-2x fa-linkedin-square"></span>
                                </a>
                            </li>
                            <?php } ?>                            
                        </ul>
                    </div>
                    <?php echo $this->Html->image("/".$homePageSections[3]['image'], ['alt' => 'Cheolis']); ?>
                </div>

              
                <?php echo $homePageSections[3]['content']; ?>
            </div>
        </div>
    <?php } ?>

        <div class="copyright text-center py-4">
            <p>Copyright &copy; 2018 Cheolis.com. All Rights Reserved.</p>
        </div>
    </footer>
     <script>
        function openNav() {
            // document.getElementById("mySidenav").style.width = "250px";
            $('#mySidenav').css('transform','translateX(0%)');
        }

        function closeNav() {
            // document.getElementById("mySidenav").style.width = "0";
            $('#mySidenav').css('transform','translateX(100%)');
            $('.navbar-collapse').removeClass('show');
            $(this).attr('aria-expanded','false');
        }



         $(document).ready(function () {
        var url = window.location;
        $('ul.navbar-nav a[href="'+ url +'"]').parent().addClass('active');
        $('ul.navbar-nav a').filter(function() {
             return this.href == url;
        }).parent().addClass('active');
    });

            $('.dropdown-menu a.dropdown-toggle').on('mouseover', function(e) {
              if (!$(this).next().hasClass('show')) {
                $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
              }
              var $subMenu = $(this).next(".dropdown-menu");
              $subMenu.toggleClass('show');


              $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
                $('.dropdown-submenu .show').removeClass("show");
              });


              return false;
            });
        </script>
 <style type="text/css">
.banner-image {
    background: url('<?php echo $this->Url->build('/'.$homePageSections[5]['image']); ?>') no-repeat center center/cover;
    background-color: #0c0c0c;
}
 </style>