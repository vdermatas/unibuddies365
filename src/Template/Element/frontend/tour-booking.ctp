<div class="col-lg-12 clearfix">				  
	<div class="form-group">
		<div class="col-sm-12">
			<h3><?php echo __('Cozumel Plus'); ?></h3>
		</div>
	</div>
 	<div class="col-sm-4 form-group">
		<div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></span>
                <span class="input input--nao">
	                <input class="input__field input__field--nao tour_booking_date" id='tour_booking_date'  type="text"  value=''>
	                <label class="input__label input__label--nao" for="input-1">
	               		<span class="input__label-content input__label-content--nao"><?php echo __('Date'); ?></span>
	                </label>
                </span>
            </div>
        </div>
    </div>

	<div class="col-sm-4 form-group">
		<div class="popover-markup">
			<div class="trigger form-group form-group-lg form-group-icon-left " data-original-title="" title="">
				<!-- <i class="fa fa-users input-icon input-icon-highlight"></i> -->
				<!--   <label>Passenger</label> -->
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-users" aria-hidden="true"></i></span>
					<input type="text" value="<?php echo '1 ' . __('Adult') . ' 0 ' . __('Child'); ?>" class="passengers form-control input-lg guest-input" placeholder="Guest" aria-describedby="basic-addon1">
				</div>
				<!--  <input type="number" name="passengers" placeholder="Guest" id="passengers" class="form-control" value="1"> -->
			</div>
			<div class="content hide">
				<div class="form-group">
                    <label class="control-label col-md-5">
                        <strong><?php echo __('Adult'); ?></strong>
                    </label>
                    <div class="input-group number-spinner col-md-12">
                        <span class="input-group-btn">
                        <a class="btn btn-danger" data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></a>
                        </span>
                        <input type="text" disabled name="adult" id="adult" class="form-control text-center" value="1" max=9 min=1>
                        <span class="input-group-btn">
                        <a class="btn btn-info" data-dir="up"><span class="glyphicon glyphicon-plus"></span></a>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-5">
                        <strong><?php echo __('Child'); ?></strong>
                    </label>
                    <div class="input-group number-spinner col-md-12">
                        <span class="input-group-btn">
                        <a class="btn btn-danger" data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></a>
                        </span>
                        <input type="text" disabled name="child" id="child" class="form-control text-center" value="0" max=9 min=0>
                        <span class="input-group-btn">
                        <a class="btn btn-info" data-dir="up"><span class="glyphicon glyphicon-plus"></span></a>
                        </span>
                    </div>
                </div>
			</div>

		</div>
	</div>

    <div class="col-sm-4 form-group">
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                <span class="input input--nao">
                <?php echo $this->Form->text('pickup_location', ['class' => 'input__field input__field--nao', 'id' => 'pickup_location']); ?>
                <label class="input__label input__label--nao" for="input-1">
                <span class="input__label-content input__label-content--nao"><?php echo __('Pick Up Location'); ?></span>
                </label>
                </span>
            </div>
        </div>
    </div>

    <div class="col-sm-4 form-group">
        <div class="map-marker"><a href="#" data-toggle="modal" class= "booking-map" data-target="#mapModal"><i class="fa fa-map-marker" aria-hidden="true"></i></a></div>
    </div>
	

    <div class="form-group clearfix">
		<div class="col-sm-12">
			<?php
				echo $this->Form->create('Basket',array('name'=>'book_tour','url'=>array('controller'=>'basket','action'=>'book_tour'),'novalidate' => true));
				echo $this->Form->hidden('tour_id');
				echo $this->Form->hidden('adults');
				echo $this->Form->hidden('childs');
				echo $this->Form->hidden('booking_date', array('value'=>date('m/d/Y h:i:s A')));
				echo $this->Form->submit(__('Update Now'),array('class'=>'btn btn-see-green'));
				echo $this->Form->end();
			?>
		</div>
	</div>
</div>

<script type="text/javascript"> 
    $(function () {
        $('#tour_booking_date').datetimepicker({ 
            sideBySide: true,
            defaultDate: 'now',
            minDate: 'now',
            keepOpen: false,
            useCurrent: true
        });

        $('#tour_booking_date').on('dp.change', function(e) {
            var formatedValue = $("#tour_booking_date").val();
            $("input[name='booking_date']").val(formatedValue);            
        });

        $("#tour_booking_date").parent("span.input").addClass("input--filled");
        $("input[name='booking_date']").val($("#tour_booking_date").val());
    });
</script>

