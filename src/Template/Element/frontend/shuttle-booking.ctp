<?php
    use Cake\Core\Configure;
    $session = $this->request->session();
?>
<div class="col-lg-12">
    <?php
	    echo $this->Form->create('Basket',array('name'=>'book_shuttle','url'=>array('controller'=>'basket','action'=>'book_shuttle')));
	?>
        <div class="col-sm-12">
            <div class="checkbox is-airport">
                <label>
                    <?php echo $this->Form->checkbox('is_airport_pickup'); ?>
                    <?php echo __('Is Airport your pick up or Drop off Location?'); ?>
                </label>
            </div>
        </div>
        <div class="col-sm-5 col-xs-10 form-group show-pickup">
            <div class="input-group">
                <span class="input-group-addon"><!--<i class="fa fa-map-marker" aria-hidden="true"></i>--></span>
                <span class="input input--nao">
				<?php echo $this->Form->text('pickup_location', ['class' => 'input__field input__field--nao']); ?>
				<label class="input__label input__label--nao" for="input-1">
				<span class="input__label-content input__label-content--nao"><?php echo __('Pick up Place or Hotel'); ?></span>
                </label>
                </span>
            </div>
        </div>
        <div class="col-sm-1 col-xs-1 map-marker show-pickup">
            <a href="#" data-toggle="modal" class="booking-map" data-target="#mapModal">
                <?php
                    echo $this->Html->image('frontend/Google-Maps-icon.png', [
                        'alt' => 'Google Map Icon'
                    ]
                );
                ?>   
             </a>
         </div>
        <div class="col-sm-6 col-xs-12 form-group drop-location">
            <div class="input-group">
               <!-- <span class="input-group-addon" id="basic-addon1"><i class="fa fa-map-marker" aria-hidden="true"></i></span>-->
                <?php
						$DropLocArr = [];
						
						if($session->read('Config.language') == 'en') {
							foreach($drop_of_locations as $k => $v) {
								$DropLocArr[$v['id']] = $v['location_en'];
							}
						}
						if($session->read('Config.language') == 'spa') {
							foreach($drop_of_locations as $k => $v) {
								$DropLocArr[$v['id']] = $v['location_spa'];
							}
						}
						
						echo $this->Form->select('drop_of_locations', $DropLocArr, [
                		'empty' => __('Drop Off Location')
                	]);
        		?>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="checkbox is-airport">
                <label>
                    <?php echo $this->Form->checkbox('is_round_trip',['id' => 'is_round_trip']); ?>
                    <?php echo __('Round Trip'); ?>
                </label>
            </div>
        </div>
        <div class="col-sm-4 form-group">
            <div class="input-group date">
                <span class="input-group-addon"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></span>
                <span class="input input--nao">
					<?php echo $this->Form->text('shuttle_arrival_date', ['class' => 'input__field input__field--nao shuttle_schedule_date']); ?>
					<label class="input__label input__label--nao" for="input-1">
						<span class="input__label-content input__label-content--nao"><?php echo __('Arrival'); ?></span>
	                </label>
                </span>
            </div>
        </div>
        <div class="col-sm-4 form-group departure hide shuttle_departure_date_block">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></span>
                <span class="input input--nao">
					<?php echo $this->Form->text('shuttle_departure_date', ['class' => 'input__field input__field--nao shuttle_schedule_date']); ?>
					<label class="input__label input__label--nao" for="input-1">
						<span class="input__label-content input__label-content--nao"><?php echo __('Departure'); ?></span>
	                </label>
                </span>
            </div>
        </div>
        <div class="col-sm-4 form-group drop-location">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-users" aria-hidden="true"></i></span>
                <?php			
					$arrayOfTraveller =  ['1_7'=>'1-7 of People', '8_10'=>'8-10 of People', '10'=>'10+ of People'];
	                echo $this->Form->select('travellers', $arrayOfTraveller, [
	                		'empty' => __('# of People')
	                	]);
				?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <?php echo $this->Form->submit(__('Book Now'), array('class'=>'btn btn-see-green book_shuttle')); ?>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
</div>
<script type="text/javascript">
$(function() {
    $('[name=shuttle_arrival_date],[name=shuttle_departure_date]').datetimepicker({
        sideBySide: true,
        defaultDate: 'now',
        minDate: 'now',
        keepOpen: false,
        useCurrent: true
    });

    $('[name=shuttle_arrival_date],[name=shuttle_departure_date]').closest(".input--nao").addClass("input--filled");

    $("[name=is_airport_pickup]").on('click', function(e) {
        if (this.checked) {
           // $(".show-pickup").hide();
        } else {
            //$(".show-pickup").show();
        }
    });

    $("img").on("error", function() {
        $(this).unbind("error").attr("src", "../img/no-image-found.jpg");
    });
});

$(document).ready(function() {
    $(".shuttle-10-contact-close").on("click", function() {
        $("#shuttle-10-contact").modal("hide");
    });
});
</script>