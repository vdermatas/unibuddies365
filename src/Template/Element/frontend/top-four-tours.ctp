<?php 
    use Cake\Core\Configure;
    $session = $this->request->session();
?>
<div class="top-tour" id="topTour" >
    <div class="container ">
        <h1 class="text-center wow zoomIn"><?php echo __('Top Tours'); ?></h1>
        <p class="text-center"><?php echo __('Some of our most loved tours by our customers. Have a look and find that suits to your requirement.'); ?></p>
       <?php
            foreach($tours as $key => $value) {
                if($value['is_top_tour'] == 1) { ?>
                    <div class="col-sm-3">
                        <a href='<?php echo Configure::read("APP_BASE_URL"); ?>/tours/<?php echo $value['slug'];?>'>
                            <div class="tour-wraper">
                                <?php                                
                                    foreach($value['gallery']['images'] as $k => $v) {
                                        if($value['gallery']['default_image_id'] == $v['id']) {
                                            echo $this->Html->image($v['url'], ["alt" => "img", 'class' => 'img-responsive']);
                                            break;
                                        }
                                    }
                                ?>                           
                                <div class="bottom-tour-strip wow zoomIn text-center">
                                    <?php echo $value['name_' . $session->read('Config.language')];?>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php 
                }
            } 
        ?>
       <div class="text-right col-sm-12 view-all">
            <a href="<?php echo Configure::read("APP_BASE_URL");?>/tours"> <?php echo __('View All'); ?> </a>
        </div>
    </div>
</div>