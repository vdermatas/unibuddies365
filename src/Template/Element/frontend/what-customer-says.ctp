<?php
    use Cake\Core\Configure;
    $session = $this->request->session();
?>

<div class="customer-says-wraper">
	<div class="container">
		<h1 class="text-center wow zoomIn"><?php echo __('What our customer says'); ?></h1>
		<div class="row">
			<?php

			if(isset($testimonials) && !empty($testimonials)) {

				foreach ($testimonials as $key => $value) {	?>

					<div class="col-sm-4">
						<div class="white-box-customer clearfix">
							<div class="customer-review">
								<?php echo $value['feeback_'.$session->read('Config.language')]; ?>
							</div>
							<div class="customer-img col-sm-3">								
								<img src="<?php echo Configure::read("APP_BASE_URL").'/'.$value['image']; ?>" alt="img" class="img-responsive">
							</div>
							<div class="col-sm-9 customer-img-right">
								<div class="customer-name"><?php echo $value['author_name_'.$session->read('Config.language')]; ?></div>
								<div> <?php echo $value['position_'.$session->read('Config.language')]; ?></div>
							</div>
						</div>
					</div>

				<?php
					}
				}
			?>
	</div>
</div>