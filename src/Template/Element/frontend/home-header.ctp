<?php
	use Cake\Core\Configure;
?>
<div class="container-fluid banner">
            <nav class="navbar navbar-default navbar-fixed-top  nav-top  clearfix ">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?php echo Configure::read("APP_BASE_URL"); ?>">
                        <?php echo $this->Html->image('frontend/logo.png', ['alt' => 'logo', 'class' => 'img-responsive']); ?>
                        </a>
                    </div>
                    <div id="navbar" class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right ">
                           
                           <li class="">
                                <a href="<?php echo Configure::read("APP_BASE_URL"); ?>">Home</a>
                            </li>
                            <li class="">
                                <a href="<?php echo Configure::read("APP_BASE_URL"); ?>about-us">About Us</a>
                            </li>
                            <li class="">
                                <a href="<?php echo Configure::read("APP_BASE_URL"); ?>tours">Tours</a>
                            </li>
                            <li class="">
                               <a href="<?php echo Configure::read("APP_BASE_URL");?>transporation">Transporation</a>
                            </li>
                            <li class="">
                                <a href="<?php echo Configure::read("APP_BASE_URL"); ?>contact-us">Contact Us</a>
                            </li>
                            <li class="svg-width">
                                <?php
                                    echo $this->Html->image("frontend/united-kingdom.svg", [
                                        "alt" => "uk",
                                        'url' => ['action' => '#']
                                    ]);
                                ?>
                                <!--<a href="#"><img alt="uk" src="img/united-kingdom.svg"></a>-->
                            </li>
                            <li class="svg-width">
                                 <?php
                                    echo $this->Html->image("frontend/mexico.svg", [
                                        "alt" => "mexixo",
                                        'url' => ['action' => '#']
                                    ]);
                                ?>
                                 <!--<a href="#"><img alt="mexixo" src="img/mexico.svg"></a>-->
                            </li>
                            <li class="">
                                <a href="<?php echo Configure::read("APP_BASE_URL").'basket'; ?>"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="badge cart-quantity" id='cart-quantity'><?php if(isset($cart_count)){ echo $cart_count; }else{ echo "0"; };?></span></a>

                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
          <div class="container clearfix">
                <div class="cancun-book-from col-sm-7 wow slideInLeft">
                    <div class="col-sm-12  top-header-search">
                        <div class="col-sm-8 book-shuttle">Book Your Shuttle</div>
                        <div class="col-sm-4 text-right book-your-tour"><a href="#topTour">Book Your Tour </a></div>
                    </div>
                   <div class="col-lg-12">
                        
                        <?php
                            echo $this->Form->create('Basket',array('name'=>'book_shuttle','url'=>array('controller'=>'basket','action'=>'book_shuttle')));
                           
                        ?>

                        <div class="col-sm-12">
                            <div class="checkbox is-airport">
                                <label>
                                    <?php echo $this->Form->checkbox('is_airport_pickup'); ?>
                                    Is Airport  your pickup location?
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-5 form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                                <span class="input input--nao">
                                <?php echo $this->Form->text('pickup_location', ['class' => 'input__field input__field--nao']); ?>
                                <label class="input__label input__label--nao" for="input-1">
                                <span class="input__label-content input__label-content--nao">Pick Up Location</span>
                                </label>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-1 map-marker"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                        <div class="col-sm-6 form-group drop-location">
                            <div class="input-group">
                              <span class="input-group-addon" id="basic-addon1"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                                <?php
                                    $DropLocArr= ['Drop Off Location']+$drop_of_locations;
                                    echo $this->Form->select('drop_of_locations', $DropLocArr); 
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="checkbox is-airport">
                                <label> <?php echo $this->Form->checkbox('is_round_trip'); ?>Round Trip</label>
                            </div>
                        </div>
                        <div class="col-sm-4 form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></span>
                                <span class="input input--nao">
                                <?php echo $this->Form->text('shuttle_arrival_date', ['class' => 'input__field input__field--nao shuttle_arrival_date', 'value' => date('m/d/Y')]); ?>
                                <label class="input__label input__label--nao" for="input-1">
                                <span class="input__label-content input__label-content--nao">Arrival</span>
                                </label>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-4 form-group departure hide">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></span>
                                <span class="input input--nao">
                                <?php echo $this->Form->text('shuttle_departure_date', ['class' => 'input__field input__field--nao shuttle_departure_date', 'value' => date('m/d/Y')]); ?>
                                <label class="input__label input__label--nao" for="input-1">
                                <span class="input__label-content input__label-content--nao">Departure</span>
                                </label>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-4 form-group">
                            <div class="popover-markup">
                                <div class="trigger form-group form-group-lg form-group-icon-left ">
                                    <!-- <i class="fa fa-users input-icon input-icon-highlight"></i> -->
                                    <!--   <label>Passenger</label> -->
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-users" aria-hidden="true"></i></span>
                                        <input type="text" name="passengers" value="1"  id="passengers" class="form-control input-lg guest-input" placeholder="Guest" aria-describedby="basic-addon1">
                                    </div>
                                    <!--  <input type="number" name="passengers" placeholder="Guest" id="passengers" class="form-control" value="1"> -->
                                </div>
                                <div class="content hide">
                                    <div class="form-group">
                                        <label class="control-label col-md-5"><strong>Adult</strong></label>
                                        <div class="input-group number-spinner col-md-7">
                                            <span class="input-group-btn">
                                            <a class="btn btn-danger" data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></a>
                                            </span>
                                            <input type="text" disabled name="adult" id="adult" class="form-control text-center" value="1" max=9 min=1>
                                            <?php echo $this->Form->hidden('adults',array('value'=>1)); ?>
                                            <span class="input-group-btn">
                                            <a class="btn btn-info" data-dir="up"><span class="glyphicon glyphicon-plus"></span></a>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-5"><strong>Child</strong></label>
                                        <div class="input-group number-spinner col-md-7">
                                            <span class="input-group-btn">
                                            <a class="btn btn-danger" data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></a>
                                            </span>
                                            <input type="text" disabled name="child" id="child" class="form-control text-center" value="0" max=9 min=0>
                                            <?php echo $this->Form->hidden('childs',array('value'=>0)); ?>
                                            <span class="input-group-btn">
                                            <a class="btn btn-info" data-dir="up"><span class="glyphicon glyphicon-plus"></span></a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-12">
                                <?php echo $this->Form->submit('Book Now',array('class'=>'btn btn-see-green book_shuttle')); ?>
                           
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
                <div class="col-sm-3 img-to-do wow slideInLeft"><a href="<?php echo Configure::read("APP_BASE_URL"); ?>things-to-do-in-cancun"><img src="img/frontend/hodings.png"></a></div>
            </div>
        </div>