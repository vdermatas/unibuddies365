 <aside class="menu-sidebar2 js-right-sidebar">
    <div class="logo">
        <a href="#">
            <img src="<?php echo $this->Url->build('/images/icon/logo.svg'); ?>" alt="logo" />
        </a>
    </div>
    <div class="menu-sidebar2__content js-scrollbar1">
        <div class="student-panel bg-primary">Student Panel</div>
        <div class="account2">
            <div class="image img-cir img-120 ">
                <?php if(!empty($this->request->getSession()->read('Auth.Student.profile_image'))){  ?>
                    <img class="rounded-circle" src="<?php echo $this->Url->build('/uploads/studentProfile/'.$this->request->getSession()->read('Auth.Student.profile_image')); ?>">
                <?php }else{ ?>
                    <img class="rounded-circle" src="<?php echo $this->Url->build('/images/user-male.png'); ?>">
                <?php } ?>
            </div>
            <h4 class="name"><?php echo $this->request->getSession()->read('Auth.Student.first_name')." ".$this->request->getSession()->read('Auth.Student.last_name'); 
                                        ?> </h4>
            <div><?php echo $this->request->getSession()->read('Auth.Student.email'); ?> </div>
        </div>
        <nav class="navbar-sidebar2">
            <ul class="list-unstyled navbar__list">
                <li class="active">
                    <a href="<?php echo $this->Url->build('/student/assignments/ongoing-assignments'); ?>">
                        <img src="<?php echo $this->Url->build('/images/ongoing.svg'); ?>"> Ongoing Assignment</a>
                </li>
                <li>
                    <a href="<?php echo $this->Url->build('/student/assignments/past-assignments'); ?>">
                        <img src="<?php echo $this->Url->build('/images/calendar.svg'); ?>"></i> Past Assignment</a>
                </li>
                <li class="has-sub">
                    <a class="js-arrow" href="#">
                        <img src="<?php echo $this->Url->build('/images/settings.svg'); ?>"> Settings
                        <span class="arrow">
                            <i class="fas fa-angle-down"></i>
                        </span>
                    </a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li>
                            <a href="<?php echo $this->Url->build('/student/users/my-profile'); ?>">
                                <img src="<?php echo $this->Url->build('/images/edit-profile.svg'); ?>"> Edit Profile</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->Url->build('/student/users/change-password'); ?>">
                                <img src="<?php echo $this->Url->build('/images/lock.svg'); ?>"> Change Password</a>
                        </li>

                    </ul>
                </li>

                <li class="has-sub">
                    <a class="js-arrow" href="#">
                        <img src="<?php echo $this->Url->build('/images/star.png'); ?>"> Ratings
                        <span class="arrow">
                            <i class="fas fa-angle-down"></i>
                        </span>
                    </a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li>
                            <a href="<?php echo $this->Url->build('/student/assignments/submitted-ratings'); ?>">
                                <!-- <img src="<?php echo $this->Url->build('/images/edit-profile.svg'); ?>">  -->
                                Submitted Ratings</a>
                        </li>
                        <li>
                           <!--  <a href="<?php echo $this->Url->build('/student/assignments/our-ratings'); ?>">
                                <img src="<?php echo $this->Url->build('/images/lock.svg'); ?>"> 
                                Our Ratings</a> -->
                        </li>

                    </ul>
                </li>
                <li>
                    <a href="<?php echo $this->Url->build(['controller'=> 'users', 'action' => 'logout']);?>">
                        <img src="<?php echo $this->Url->build('/images/logout.svg'); ?>"> Logout</a>
                </li>
            </ul>
        </nav>
    </div>
</aside>