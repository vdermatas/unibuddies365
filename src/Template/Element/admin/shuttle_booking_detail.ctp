<script>
    jQuery(document).ready(function(){
        jQuery(".view-booking").click(function(event) {
            event.preventDefault();
            var urlLoc = $(this).attr('href');
            console.log(urlLoc);
            $.getJSON(urlLoc, function(data) {
				
				var dateTimeTrans = new Date(data.transaction.created);
				transactionDate = moment(dateTimeTrans).format("MM/DD/YYYY h:mm a");
				
				var dateTimeBook = new Date(data.booking.created);
				data['booking']['created'] = moment(dateTimeBook).format("MM/DD/YYYY h:mm a");
				
                var template = $("#ViewShuttleBookingsTemplate").html();
                $("#ViewShuttleBookingsModal").html(_.template(template, { bookingData: data.booking, transactionData: data.transaction, billingname: data.booking.billing_detail.first_name+' '+data.booking.billing_detail.last_name, transactionDate: transactionDate}));
                $('#ViewShuttleBookingsModal').modal('show');
            });
        });
    });
</script>
<!-- Modal -->      
<div aria-labelledby="ViewShuttleBookingsLabel" class="modal fade" id="ViewShuttleBookingsModal" role="dialog" tabindex="-1">
</div>

<script type="text/html" id='ViewShuttleBookingsTemplate'>
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Shuttle Bookings</h4>
            </div>
            <div class="modal-body">
              <div class="row">
                        <div class="col-lg-8 col-md-8">

                            <div class="row">
                                
                                <div class="col-lg-12 col-md-12">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#Summery" class=""><!-- <i class="fa fa-indent"></i> --> Booking Detail</a></li>
                                        <li><a data-toggle="tab" href="#Contact" class=""><!-- <i class="fa fa-bookmark-o"></i> --> Billing Details</a></li>
                                        <li><a data-toggle="tab" href="#Address" class=""><!-- <i class="fa fa-home"></i> --> Transaction Details</a></li>
                                    </ul>

                                    <div class="tab-content">
                                        <div id="Summery" class="tab-pane fade in active">

                                            <div class="table-responsive panel">
                                                <table class="table table-striped">
                                                    <tbody>
    
                                                            <tr>
                                                                <td class=""><!-- <i class="fa fa-user"></i> --> Booking Type</td>
                                                                <td><%= bookingData.booking_type %></td>
                                                            </tr>
                                                            <tr>
                                                                <td class=""><!-- <i class="fa fa-list-ol"></i> --> Travellers</td>
                                                                <td><%= bookingData.travellers.replace("_","-") %></td>
                                                            </tr>
                                                            <tr>
                                                                <td class=""><!-- <i class="fa fa-list-ol"></i> --> Drop off Location</td>
                                                                <td><%= bookingData.drop_of_location.location_en %></td>
                                                            </tr>
                                                            <tr>
                                                                <td class=""><!-- <i class="fa fa-list-ol"></i> --> Airport Pickup</td>
                                                                <td><%= bookingData.is_airport_pickup == 1?'Yes':'No' %></td>
                                                            </tr>
                                                            <tr>
                                                                <td class=""><!-- <i class="fa fa-list-ol"></i> --> Pick Up Location</td>
                                                                <td><%= bookingData.pick_up_location != ''? bookingData.pick_up_location:'--' %></td>
                                                            </tr>
                                                            <tr>
                                                                <td class=""><!-- <i class="fa fa-list-ol"></i> --> Round Trip</td>
                                                                <td><%= bookingData.is_round_trip == 1?'Yes':'No' %></td>
                                                            </tr>
                                                            <tr>
                                                                <td class=""><!-- <i class="fa fa-list-ol"></i> --> Arrival</td>
                                                                <td><%= bookingData.arrival %></td>
                                                            </tr>
                                                            <!-- <tr>
                                                                <td class=""> Departure</td>
                                                                <td><%= bookingData.departure %></td>
                                                            </tr> -->
															<tr>
                                                                <td class=""><!-- <i class="fa fa-list-ol"></i> --> Booking At</td>
                                                                <td><%= bookingData.created %></td>
                                                            </tr>
															
                                                             
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div id="Address" class="tab-pane fade">
                                            <div class="table-responsive panel">
                                                <table class="table table-striped">
                                                    <tbody>
                                        
                                                            <tr>
                                                                <td class=""><!-- <i class="fa fa-home"></i> --> Transaction Id</td>
                                                                <td><%= transactionData.transection_id %></td>
                                                            </tr>
                                                            <tr>
                                                                <td class=""><!-- <i class="fa fa-home"></i> --> Total Price</td>
                                                                <td><%= bookingData.total_price %></td>
                                                            </tr>
                                                            <tr>
                                                                <td class=""><!-- <i class="fa fa-home"></i> --> Transaction Date</td>
                                                                <td><%= transactionDate %></td>
                                                            </tr>
                                                            </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div id="Contact" class="tab-pane fade">
                                            <div class="table-responsive panel">
                                                <table class="table table-striped">
                                                    <tbody>
                                            
                                                            <tr>
                                                                <td class=""><!-- <i class="fa fa-envelope-o"></i> --> Name</td>
                                                                <td><%= billingname %></td>
                                                            </tr>
                                                            <tr>
                                                                <td class=""><!-- <i class="glyphicon glyphicon-phone"></i> --> Email</td>
                                                                <td><%= bookingData.billing_detail.email %></td>
                                                            </tr>
                                                            <tr>
                                                                <td class=""><!-- <i class="glyphicon glyphicon-phone"></i> --> Address</td>
                                                                <td><%= bookingData.billing_detail.address %></td>
                                                            </tr>
                                                            <tr>
                                                                <td class=""><!-- <i class="glyphicon glyphicon-phone"></i> --> Zipcode</td>
                                                                <td><%= bookingData.billing_detail.zip %></td>
                                                            </tr>
                                                            <tr>
                                                                <td class=""><!-- <i class="glyphicon glyphicon-phone"></i> --> Phone</td>
                                                                <td><%= bookingData.billing_detail.phone %></td>
                                                            </tr>
                                                            <tr>
                                                                <td class=""><!-- <i class="glyphicon glyphicon-phone"></i> --> Info</td>
                                                                <td><%= bookingData.billing_detail.additional_information %></td>
                                                            </tr>                                                     
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
            </div>
        </div>
    </div>
</script>
