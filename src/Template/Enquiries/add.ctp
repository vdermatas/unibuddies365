<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyDmUmIiFFyHv6Jv5_X3a6C0ogQgQbUgwMw&v=3.exp'></script>
<div style='overflow:hidden;height:500px;width100%;'>
    <div id='gmap_canvas' style='height:500px;width:100%;'></div>
    <div><small><a href="http://www.googlemapsgenerator.com/en/">http://www.googlemapsgenerator.com/en/</a></small></div>
    <div><small><a href="https://premiumlinkgenerator.com/">Visit this site!</a></small></div>
    <style>#gmap_canvas img{max-width:none!important;background:none!important}</style>
</div>
<script type='text/javascript'>function init_map(){var myOptions = {zoom:10,center:new google.maps.LatLng(21.101393179067475,-86.7649536473541),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(21.101393179067475,-86.7649536473541)});infowindow = new google.maps.InfoWindow({content:'<strong>Title</strong><br>Kukulcan Plaza Boulevard Kukulcan KM 13 , LOCAL 410 y 411A, Benito Juárez, Zona Hotelera, 77500 Cancún, Q.R., Mexico<br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
<div class="container contact-wraper">
     <?php echo $this->Flash->render(); ?>
    <div class="row">
        <div class="col-md-7">
            <h1 class="title"><?php echo __('Get in touch with us.'); ?></h1>
        </div>
    </div>
    <div class="row map-form">
        <div class="col-md-7">
             <?= $this->Form->create($enquiry, ['id' => 'enquiryform', 'class' => 'form']) ?>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                        <span class="input input--nao">
                         <?php echo $this->Form->control('name', ['class' => 'input__field input__field--nao',  'label' => false, 'id' => 'name','templates' => ['inputContainer' => '{{content}}'], 'required' => true]); ?>
                        <label class="input__label input__label--nao" for="input-1">
                        <span class="input__label-content input__label-content--nao"><?php echo __('Name'); ?></span>
                        </label>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                        <span class="input input--nao">
                          <?php echo $this->Form->control('email', ['class' => 'input__field input__field--nao', 'label' => false, 'div' => false, 'id' => 'email','templates' => ['inputContainer' => '{{content}}'], 'required' => true]); ?>
                        <label class="input__label input__label--nao" for="input-1">
                        <span class="input__label-content input__label-content--nao"><?php echo __('Email'); ?></span>
                        </label>
                        </span>
                    </div>
                </div>
          
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-phone-square" aria-hidden="true"></i></span>
                        <span class="input input--nao">
                         <?php echo $this->Form->control('phone', ["class" => "input__field input__field--nao", 'label' => false, 'div' => false, 'id' => 'phone','templates' => ['inputContainer' => '{{content}}'], 'required' => true]); ?>
                        <label class="input__label input__label--nao" for="input-1">
                        <span class="input__label-content input__label-content--nao"><?php echo __('Phone'); ?></span>
                        </label>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"></span>
                        <span class="input input--nao">
                         <?php echo $this->Form->control('message', ['class' => 'input__field input__field--nao', 'label' => false, 'div' => false,'templates' => ['inputContainer' => '{{content}}'], 'onkeyup' => 'countChar(this)', 'id' => 'message']); ?>
                        <label class="input__label input__label--nao" for="input-1">
                        <span class="input__label-content input__label-content--nao"><?php echo __('Message'); ?></span>
                        </label>
                        </span>
                    </div>
                </div>
                 <div id="charNum"></div>
                 <?php
                    echo $this->Form->submit(__('Submit'), [
                        'class' => 'btn btn-see-green'
                    ]);
                ?>
                <?= $this->Form->end() ?>
            </form>
        </div>
        <div class="col-md-5">
            <h2><?php echo __('Contact Details'); ?></h2>
            <h3><strong><?php echo __('Address'); ?></strong></h3>
            <?php if(!empty($widgetData['contact_address'])){
                    echo $widgetData['contact_address'];
                }
            ?>
            <div class="col-md-5 col-sm-6">
                <div class="row">
                    <h3><strong><?php echo __('Telephone'); ?></strong></h3>
            <?php if(!empty($widgetData['contact_telephone'])){
                echo $widgetData['contact_telephone'];
                }
            ?>
                </div>
            </div>
            <div class="col-md-5 col-sm-6">
                <div class="row">
                    <h3><strong><?php echo __('Email'); ?></strong></h3>
            <?php if(!empty($widgetData['contact_email'])){
                    echo $widgetData['contact_email'];
                    }
            ?>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="row">
                    <h3><strong><?php echo __('Follow us on'); ?></strong></h3>
                    <br>
                    <ul class="list-inline">
                        <li><a href="https://www.facebook.com/bestshuttlecancun/?ref=bookmarks" target="_blank"><?php echo $this->Html->image('frontend/fb.png', ['alt' => 'fb', 'class' => 'img-responsive']); ?></a></li>
                        <li><a href="#" target="_blank"><?php echo $this->Html->image('frontend/instagram.png', ['alt' => 'instagram', 'class' => 'img-responsive']); ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function countChar(val) {
        var len = val.value.length;
        if (len >= 1000) {
            val.value = val.value.substring(0, 1000);
        } else {
            $('#charNum').text(1000 - len);
        }
    };
</script>



<script>
$(document).ready(function(){  
$('#enquiryform').find('input, textarea').removeClass('form-control'); 
    $('#enquiryform').validate({
        rules:{
            name:"required",
            email: {
                required: true,
                email: true
            },
            phone: {
                required: true
            }
        },
        messages:{
            name:"Please enter your name",
            email: {
                required: "Please enter your email",
                email: "Please enter a valid email"
            },
            phone: {
                required: "Please enter your contact number"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "name" )
                error.insertAfter($(element).closest('.form-group'));
            else if (element.attr("name") == "email" )
                error.insertAfter($(element).closest('.form-group'));
                  else if (element.attr("name") == "phone" )
                    error.insertAfter($(element).closest('.form-group'));
                        else if (element.attr("name") == "message" )
                        error.insertAfter($(element).closest('.form-group'));
            else
                error.insertAfter(element);
        },

    });

});
</script>