<div class="row px-0 px-sm-2 mx-1 edit-profile profile-view">
    <div class="col-sm-12">
    <?php echo $this->Flash->render(); ?>
        <div class="shadow p-4">
            <div class="row">
                <div class="col-sm-3 text-center">                    
                    <?php if(!empty($this->request->getSession()->read('Auth.Writer.profile_image'))){  ?>
                        <img class="profile-img-settings" src="<?php echo $this->Url->build('/uploads/writerProfile/'.$this->request->getSession()->read('Auth.Writer.profile_image')); ?>">
                    <?php }else{ ?>
                        <img class="profile-img-settings" src="<?php echo $this->Url->build('/images/rectangle-12.png'); ?>">
                    <?php } ?>
                </div>
                <div class="col-sm-9">
                    <div class="right-profle-section">
                        <div class="row">
                            <div class="col-sm-9">
                                <h4 class="d-inline mr-3 name"><?php echo $this->request->getSession()->read('Auth.Writer.first_name')." ".$this->request->getSession()->read('Auth.Writer.last_name'); 
                                        ?></h4> <span><img
                                        src="<?php echo $this->Url->build('/images/star-img.svg'); ?>"> ( 0 )</span>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-12 location">
                                <img src="<?php echo $this->Url->build('/images/location.svg'); ?>"> <?php echo $this->request->getSession()->read('Auth.Writer.city').", "; 
                                        ?><?php echo $this->request->getSession()->read('Auth.Writer.state'); 
                                        ?>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-12">
                                <?php if(!empty($user['writer_profile']['writer_documents'])){ 
                                    $documentArray = [];
                                ?>
                                <span><strong>Type of Document:</strong> </span> <span><?php foreach ($user['writer_profile']['writer_documents'] as $key => $document) {
                                    $documentArray[] = '<span class="add-cat-bg ml-3">'.$document->document_type."</span>";
                                } ?></span>
                                <?php echo implode(",",$documentArray);  } ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                 <?php if(!empty($user['writer_profile']['writer_thematics'])){ 
                                    $thematicArray = [];
                                ?>
                                <span><strong>Thematic Unity:</strong> </span> <span><?php foreach ($user['writer_profile']['writer_thematics'] as $key => $thematic) {
                                    $thematicArray[] = '<span class="add-cat-bg ml-3">'.$thematic->thematic_type."</span> ";
                                } ?></span>
                                <?php  echo implode(",",$thematicArray); }?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <span><strong>Total Experience:</strong> </span> <span><?php echo $user['writer_profile']['experience']; ?> years</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <?php if(!empty($user['writer_profile']['writer_skills'])){ 
                                    $skillArray = [];
                                ?>
                                <span><strong>Skills:</strong> </span> <span><?php foreach ($user['writer_profile']['writer_skills'] as $key => $skill) {
                                    $skillArray[] = '<span class="add-cat-bg ml-3">'.$skill->languages."</span> ";
                                } ?></span>
                                <?php echo implode(",",$skillArray); } ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <span><strong>Total Projects Completed: </strong> </span> <span>0
                                    Projects</span>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>


        <div class="shadow p-4 mt-3">
            <div class=" rating-feedback">
                <div class="col-sm-12">
                    <div class="row">
                        <h5 class="mb-4">Past Project Rating and Feedback</h5>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row  p-3 shadow mb-3">
                        <div class="col-sm-9 p-0">
                            <div class="media">
                                <div class="media-body">
                                    <div class="writer-name">Popular Uses Of The Internet</div>
                                    <div><span><img src="<?php echo $this->Url->build('/images/star-img.svg'); ?>"></span><span><small>
                                                4.9/5</small></span></div>
                                    <div class="skills"><b>Feedback:</b> “I was always somebody who felt quite
                                        sorry for myself,.”
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 text-right">
                            $992.00
                        </div>
                    </div>
                    <div class="row  p-3 shadow mb-3">
                        <div class="col-sm-9 p-0">
                            <div class="media">
                                <div class="media-body">
                                    <div class="writer-name">Myspace Layouts The Missing Element</div>
                                    <div><span><img src="<?php echo $this->Url->build('/images/star-img.svg'); ?>"></span><span><small>
                                                4.9/5</small></span></div>
                                    <div class="skills"><b>Feedback:</b> “I was always somebody who felt quite
                                        sorry for myself,.”
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 text-right">
                            $992.00
                        </div>
                    </div>
                    <div class="row  p-3 shadow mb-3">
                        <div class="col-sm-9 p-0">
                            <div class="media">
                                <div class="media-body">
                                    <div class="writer-name">Microsoft Patch Management For Home Users</div>
                                    <div><span><img src="<?php echo $this->Url->build('/images/star-img.svg'); ?>"></span><span><small>
                                                4.9/5</small></span></div>
                                    <div class="skills"><b>Feedback:</b> “I was always somebody who felt quite
                                        sorry for myself,.”
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 text-right">
                            $992.00
                        </div>
                    </div>
                </div>
                <div class="page-naviagtion col-12 text-right mt-5">
                    1-10 of 100 <span><a href="#"><img src="<?php echo $this->Url->build('/images/left-arrow.png'); ?>"></a></span><span><a
                            href="#"><img src="<?php echo $this->Url->build('/images/right-arrow.png'); ?>"></a></span>
                </div>
            </div>
        </div>
    </div>
</div>