<?= $this->Form->create($user,['class' => 'form-horizontal' , 'id' => 'writerProfile' , 'novalidate' => true , 'type' => 'file'  ]) ?>
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 p-2 p-sm-0 mb-0 mb-sm-4 mt-0 mt-sm-4">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <h4>Complete Profile</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="row px-0 px-sm-2 mx-1 edit-profile">
    <div class="col-sm-3 text-center">
        <div class="shadow p-4 edit-pic-profile">
            <div class="img-setting-profile">
                <?php if(!empty($user->profile_image)){  ?>
                    <img src="<?php echo $this->Url->build('/uploads/writerProfile/'.$user->profile_image); ?>">
                <?php }else{ ?>
                    <img src="<?php echo $this->Url->build('/images/user-male.png'); ?>">
                <?php } ?>
                
                <div class="edit-pic" style="display: none;">
                    <input type="file" name="profile_image" style="display: none;" class="fileInput">
                    <a href="#" id="openButton"><img src="<?php echo $this->Url->build('/images/mode-edit.png'); ?>"></a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-9 ">
        <?php echo $this->Flash->render(); ?>
        <div class="shadow px-4 py-4 pb-5">
            <div class="prfile-section " id="contact-information">
                <h5 class="mb-4">Contact Information</h5>

                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>First Name</label>
                            <?php
                                echo $this->Form->control('first_name',['class' => 'form-control'   , 'placeholder' => 'First Name', 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>Last Name</label>
                            <?php
                                echo $this->Form->control('last_name',['class' => 'form-control'   , 'placeholder' => 'Last Name', 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>Phone Number</label>
                            <?php
                                echo $this->Form->control('mobile',['class' => 'form-control'   , 'placeholder' => 'Contact Number', 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
                            ?>
                        </div>
                    </div>
                     <div class="col-sm-5">
                        <div class="form-group">
                            <label>Year of Experience</label>
                            <?php
                                echo $this->Form->control('writer_profile.experience', [ 'class' => 'form-control' , 'label' => false , 'options' => ['1' => 1,'2' => 2,'3' => 3,'4' => 4,'5' => 5,'6' => 6 , '7' => 7 , '8' => 8 , '9' => 9 , '10' => 10] ,
                                    'templates' => [
                                                'inputContainer' => '{{content}}',
                                                'inputDiv' => ''
                                            ] 
                                    ]); 
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>Email Address</label>
                            <?php
                                echo $this->Form->control('email',['class' => 'form-control'   , 'placeholder' => 'Email Address', 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>IBAN</label>
                            <?php
                                echo $this->Form->control('writer_profile.iban',['class' => 'form-control'   , 'placeholder' => 'IBAN', 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Bank Code</label>
                            <?php
                                echo $this->Form->control('writer_profile.bank_code',['class' => 'form-control'   , 'placeholder' => 'Bank Code', 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-9">
                        <div class="form-group">
                            <label>About Me</label>
                            <?php
                                echo $this->Form->control('writer_profile.about_me',['class' => 'form-control' ,  'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ] ,'type' => 'textarea' ]);
                            ?>
                        </div>
                    </div>
                </div>
                <!-- <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-12">
                                <label>Gender</label>
                            </div>
                        </div>
                        <label class="radio-inline mr-3"><input type="radio" name="optradio"> Male</label>
                        <label class="radio-inline"><input type="radio" name="optradio"> Female</label>
                    </div>
                </div> -->
                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>Address</label>
                            <?php
                                echo $this->Form->control('address',['class' => 'form-control'   ,'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ] ,'type' => 'textarea' ]);
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>City</label>
                            <?php
                                echo $this->Form->control('city', [ 'class' => 'form-control' , 'label' => false , 'options' => ['Massachusetts' => 'Massachusetts'] ,
                                    'templates' => [
                                                'inputContainer' => '{{content}}',
                                                'inputDiv' => ''
                                            ] 
                                    ]); 
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>State</label>
                            <?php
                                echo $this->Form->control('state', [ 'class' => 'form-control' , 'label' => false , 'options' => ['Massachusetts' => 'Massachusetts'] ,
                                    'templates' => [
                                                'inputContainer' => '{{content}}',
                                                'inputDiv' => ''
                                            ] 
                                    ]); 
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Zipcode</label>
                            <?php
                                echo $this->Form->control('zipcode',['class' => 'form-control'   , 'placeholder' => 'Zipcode', 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
                            ?>
                        </div>
                    </div>
                </div>

            </div>
            <div class="prfile-section mt-5">
                <h5 class="mb-4">Skills</h5>
                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-12" for="exampleInputEmail1">Language</label>
                        <div class="col-sm-12 multiple-select-custom">
                            <select class="form-control" id="example-multiple-selected" multiple="multiple" name="languages[]">
                                <option value="English" <?php if(array_search('English', array_column($user['writer_profile']['writer_skills'], 'languages')) !== False) {  echo "selected"; } ?>>English</option>
                                <option value="Greek" <?php if(array_search('Greek', array_column($user['writer_profile']['writer_skills'], 'languages')) !== False) {  echo "selected"; } ?>>Greek</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>
            <div class="prfile-section mt-5 ">
                <h5 class="mb-4">Education</h5>
                <div class="row">
                    <div class="col-sm-7">
                        <div class="form-group">
                            <label>Insitute/University/School Name</label>
                            <?php
                                echo $this->Form->control('writer_profile.studied_institute',['class' => 'form-control' ,  'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ] ,'placeholder' => 'Enter Insitute/University/School Name' ]);
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-7">
                        <div class="form-group">
                            <label>Course</label>
                            <?php
                                echo $this->Form->control('writer_profile.course',['class' => 'form-control' ,  'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ] ,'placeholder' => 'Enter Course Name' ]);
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group calander-icon-setting">
                            <img src="<?php echo $this->Url->build('/images/event.svg'); ?>">
                            <label>From</label>
                            <?php
                                echo $this->Form->control('writer_profile.course_from',['class' => 'form-control datepicker' ,  'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ] ,'placeholder' => 'yyyy-mm-dd' , 'type' => 'text'   , 'value' => date('Y-m-d',strtotime($user['writer_profile']['course_from']))  ]);
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group calander-icon-setting">
                            <img src="<?php echo $this->Url->build('/images/event.svg'); ?>">
                            <label>To</label>
                            <?php
                                echo $this->Form->control('writer_profile.course_to',['class' => 'form-control datepicker' ,  'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ] ,'placeholder' => 'yyyy-mm-dd' , 'type' => 'text' , 'value' => date('Y-m-d',strtotime($user['writer_profile']['course_to'])) ]);
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>Upload Diploma</label>
                            <div class="upload-btn-wrapper">
                                <button class="btn btn-gray">Upload Diploma</button>
                                <input type="file" name="certificate">
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-6 col-md-6">
                    <?php if(!empty($user->writer_profile['certificate'])){ ?>
                        <div class="row  download-file">
                            <div class="col-sm-12">
                                <a target="_blank" href="<?php echo $this->Url->build('/uploads/resumes/'.$user->writer_profile['certificate']) ?>">1. <?php echo $user->writer_profile['certificate']; ?> <img src="<?php echo $this->Url->build('/images/file-download.png'); ?>"></a>
                            </div>
                        </div>
                    <?php } ?>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>Upload CV</label>
                            <div class="upload-btn-wrapper">
                                <button class="btn btn-gray">Upload CV</button>
                                <input type="file" name="resume">
                            </div>
                        </div>                        
                    </div>
                    <div class="col-sm-6 col-md-6">
                    <?php if(!empty($user->writer_profile['resume'])){ ?>
                        <div class="row  download-file">
                            <div class="col-sm-12">
                                <a target="_blank" href="<?php echo $this->Url->build('/uploads/resumes/'.$user->writer_profile['resume']) ?>">1. <?php echo $user->writer_profile['resume']; ?> <img src="<?php echo $this->Url->build('/images/file-download.png'); ?>"></a>
                            </div>
                        </div>
                    <?php } ?>
                    </div>
                </div>

            </div>
            <div class="prfile-section mt-5 ">
                <h5 class="mb-4">Categories</h5>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-12" for="exampleInputEmail">Type of Document</label>
                        <div class="col-sm-12 multiple-select-custom">
                            <?php echo $this->element('writer/document_type'); ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-12" for="exampleInputEmail">Thematic Unity</label>
                        <div class="col-sm-12 multiple-select-custom">
                            <?php echo $this->element('writer/thematic_unity'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mt-5">
                <button class="btn px-5 browse-assignment">
                    Save</button>
            </div>
        </div>
    </div>
</div>
 <?= $this->Form->end() ?>
 <script type="text/javascript">
     $(document).ready(function(){
        $("#openButton").click(function(){

          $(".fileInput").trigger('click');
        });
     });
 </script>
