<?= $this->Form->create($user,['class' => 'form-horizontal' , 'novalidate' => true  , 'id' => 'changePassword' ]) ?>

    <section class="au-breadcrumb m-t-75">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 p-2 p-sm-0 mb-4 mt-4">
                        <div class="au-breadcrumb-content">
                            <div class="au-breadcrumb-left">
                                <h4>Settings</h4>
                            </div>
                            <button type="submit" class="btn browse-assignment">
                                Save Changes</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="section-content  mt-1 mr-4 ml-4 mb-4">
        <div class="col-12">
            <h5 class="mb-4">Change Password</h5>
        </div>
        <div class="col-12  col-sm-5 px-4 mr-auto change-password">

        <?php echo $this->Flash->render(); ?>
            <div class="form-group password-group">
                <label>Old Password</label>
                <?php
                    echo $this->Form->control('old_password',['class' => 'form-control' , 'type' => 'password'  , 'placeholder' => 'Old Password', 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
                ?>
            </div>
            <div class="form-group">
                <label>New Password</label>
                <?php
                    echo $this->Form->control('password',['class' => 'form-control' , 'type' => 'password', 'label' => false , 'placeholder' => 'New Password' , 'id' => 'password' , 'templates' => [  'inputContainer' => '{{content}}'  ]  , 'value' => '' ]);
                ?>
            </div>
            <div class="form-group">
                <label>Confirm Password</label>
                <?php
                    echo $this->Form->control('confirm_password',['class' => 'form-control' , 'type' => 'password' , 'label' => false , 'placeholder' => 'Confirm Password' , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
                ?>
            </div>
        </div>
    </div>
 <?= $this->Form->end() ?>
