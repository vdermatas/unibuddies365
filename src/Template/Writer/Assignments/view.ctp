<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 p-2 p-sm-0 mb-0 mb-sm-4 mt-0 mt-sm-4">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <h4>Assignments</h4>
                        </div>
                        <!--   <button class="btn browse-assignment">
                                Save  Assignment</button> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <?php echo $this->Flash->render(); ?>
</div>
<div class="section-content  mt-1 mr-4 ml-4 mb-4 ">
    <div class="assignment-details pb-4">
        <div class="row">
            <div class="col-12">
                <h5><?php echo $assignment->title; ?></h5>
            </div>

        </div>
        <?php
            $documentType =  [1=>'Thesis',2 => 'Bachelors thesis' , 3 => 'Doctoral dissertation' , 4 => 'Statistical analysis' , 5  => 'Corrections' , 6  => 'Semester assigment' , 7 => 'Postgraduate assignment' , 8 => 'Presentation' , 9 => 'Scientific Article' , 10 => 'Translation' , 11 => 'Citation Resources'];

            $thematicUnit = [1=>'Finance and Economics',2 => 'Law' , 3 => 'Business and Management' , 4 => 'Computing and ICT' , 5  => 'Contruction and Engineering' , 6  => 'Education' , 7 => 'Health and Nursing' , 8 => 'Marketing' , 9 => 'Mathematics and Statistics' , 10 => 'Medical Science' , 11 => 'Politics and International Relations' , 12 => 'Science' , 13 => 'Accounting' , 14 => 'Social Sciences' , 15 => 'Shipping and Maritime' , 16 => 'Other'] ;

            $language = [1=>'English',2 => 'Greek' ] ;
         ?>
        <div class="row ">
            <div class="col-sm-6">
                <b>Type of Document:</b> <?php echo $documentType[$assignment->document_type]; ?>
            </div>
            <div class="col-sm-6">
                <b>Language:</b> <?php echo $language[$assignment->language]; ?>
            </div>
        </div>
        <div class="row ">
            <div class="col-sm-6">
                <b>Thematic Unity:</b> <?php echo $thematicUnit[$assignment->thematic_unity]; ?>
            </div>

            <!-- <div class="col-sm-6">
                                <b>Pages Required</b> : 4
                            </div> -->

        </div>
        <div class="row ">
            <div class="col-sm-6">
                <b>Date to be Delivered </b> : <?php echo date('d F, Y' , strtotime($assignment->delivery_date)); ?>
            </div>

        </div>
        <div class="row ">
            <div class="col-sm-12"><b>Reference Url </b>: <a target="_blank" href="http://<?php echo $assignment->reference_url; ?>"><?php echo $assignment->reference_url; ?></a>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-12">
                <h4>Description</h4>
                <p><?php echo $assignment->description; ?></p>
            </div>
        </div>
        <?php if(!empty($assignment->attachment_file)){ ?>
        <div class="row  download-file">
            <div class="col-sm-12">
                <a target="_blank" href="<?php echo $this->Url->build('/uploads/student-assignment/'.$assignment->attachment_file) ?>">1. <?php echo $assignment->attachment_file; ?> <img src="<?php echo $this->Url->build('/images/file-download.png'); ?>"></a>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
<?php if($assignment->status == 3 || $assignment->status == 4){ ?>
<div class="section-content  mt-1 mr-3 ml-3 ml-sm-4 mr-sm-4 mb-4 ">
    <div class="writers-offer pb-4 px-0 px-sm-4">
        <div class="row px-3 mb-3">
            <div class="col-sm-6 col-6">
                <h4>Delivery Schedules</h4>
            </div>
        </div>
        <div class="table-responsive mobile-res-tb">
            <table class="table  header-text-left">
                <thead>
                    <tr>
                        <th scope="col"><b>Description</b></th>
                        <th scope="col"><b>Notes</b></th>
                        <th scope="col"><b>Delivered At</b></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($assignment['assignment_offers'][0]['assignment_delivery_schedules'])){
                        foreach ($assignment['assignment_offers'][0]['assignment_delivery_schedules'] as $key => $deliverySchedules) {
                     ?>
                        <tr>
                            <td scope="row">
                                <?php echo $deliverySchedules['description']; ?>
                                <?php if(!empty($deliverySchedules['file'])){ ?>
                                    <br><br>
                                    <a target="_blank" href="<?php echo $this->Url->build('/uploads/student-assignment/'.$deliverySchedules['file']); ?>">
                                        <img src="<?php echo $this->Url->build('/images/download.png'); ?>">
                                    </a>
                                <?php } ?>
                                    
                            </td>
                            <td><?php echo $deliverySchedules['delivery_notes']; ?></td>
                            <td>
                                <?php echo date('d M,Y', strtotime($deliverySchedules['delivery_date'])); ?>
                                
                            </td>
                        </tr>
                    <?php }
                     } else { ?>
                         <tr>
                             <td colspan="3"><center>No Offers Submitted Yet</center></td>
                         </tr>
                     <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php } ?>
<?php if($assignment->status == 1){ ?>
<div class="section-content  mt-1 mr-4 ml-4 mb-4 ">
    <div class="assignment-details pb-4">
        <h4>Place an offer</h4>
        <?= $this->Form->create($assignmentOfferData,['class' => 'form-horizontal' , 'id' => 'assignmentOfferData' , 'novalidate' => true , 'type' => 'file'  ]) ?>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Price</label>
                        <?php
                            echo $this->Form->control('price',['class' => 'form-control'   , 'placeholder' => '0', 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  , 'type' => 'number' ]);
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group ">
                        <label>Notes</label>
                        <?php
                                echo $this->Form->control('notes',['class' => 'form-control'   , 'placeholder' => 'Assignment Title', 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  , 'type' => 'textarea'  ]);
                            ?>
                    </div>
                </div>
            </div>
            <button class="btn btn-blue">Send</button>
        <?= $this->Form->end() ?>
    </div>
</div>
<?php } ?>
<div class="section-content  mt-1 mr-3 ml-3 ml-sm-4 mr-sm-4 mb-4 ">
    <div class="writers-offer pb-4 px-0 px-sm-4">
        <div class="row px-3 mb-3">
            <div class="col-sm-6 col-6">
                <h4>Last Offers</h4>
            </div>
        </div>
        <div class="table-responsive mobile-res-tb">
            <table class="table  header-text-left">
                <thead>
                    <tr>
                        <th scope="col">From Associate</th>
                        <th scope="col">Special Offer</th>
                        <th scope="col">AT</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($assignment['assignment_offers'])){
                        foreach ($assignment['assignment_offers'] as $key => $assignment_offer) {
                     ?>
                        <tr>
                            <td scope="row"><?php echo $assignment_offer['user']['first_name']; ?></td>
                            <td><span class="green-text"><?php echo $assignment_offer['price']; ?> €</span></td>
                            <td><?php echo date('d/m/Y', strtotime($assignment_offer['created'])); ?></td>
                        </tr>
                    <?php }
                     } else { ?>
                         <tr>
                             <td colspan="3"><center>No Offers Submitted Yet</center></td>
                         </tr>
                     <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>