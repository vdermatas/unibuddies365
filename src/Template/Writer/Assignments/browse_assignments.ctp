   <section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 p-2 p-sm-0 mb-0 mb-sm-4 mt-0 mt-sm-4">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <h4>Assignment Posts </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
<div class="section-content mr-0 mr-sm-4 ml-0 ml-sm-4 mb-0 mb-sm-4">
    <div class="browse-assignment-box px-sm-3">
        <div class="search-filter px-3 mb-4">
            <div class="row">
                <div class="col-sm-7">
                    <?= $this->Form->create('assignment',['class' => 'form-horizontal'  , 'novalidate' => true ]) ?>
                    <div class="input-group">
                        <input type="text" class="form-control" name="title" placeholder="Search Assignment"
                            aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg">
                        <div class="input-group-prepend">
                            <span class="input-group-text blue-bg-input-group" id="inputGroup-sizing-lg">
                                <img src="<?php echo $this->Url->build('/images/search.svg'); ?>">
                            </span>
                        </div>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
                <div class="col-sm-3">
                    <button class="btn btn-blue btn-lg filter-btn">Filters</button>
                </div>
            </div>

            <div class="filter-box mt-5" style="display: none;">
                <?= $this->Form->create('assignment',['class' => 'form-horizontal'  , 'novalidate' => true ]) ?>
                <div class="row">
                    <div class="col-sm-3 ">
                        <div class="form-group">
                            <label>Code</label>
                            <input type="email" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>Title</label>
                            <?php
                                echo $this->Form->control('title',['class' => 'form-control'   , 'placeholder' => 'Assignment Title', 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-4 ">
                        <div class="form-group">
                            <label>Type of Document</label>
                            <?php
                                echo $this->Form->control('document_type',['class' => 'form-control'   , 'options' =>  [1=>'Thesis',2 => 'Bachelors thesis' , 3 => 'Doctoral dissertation' , 4 => 'Statistical analysis' , 5  => 'Corrections' , 6  => 'Semester assigment' , 7 => 'Postgraduate assignment' , 8 => 'Presentation' , 9 => 'Scientific Article' , 10 => 'Translation' , 11 => 'Citation Resources'] , 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
                            ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4 ">
                        <div class="form-group">
                            <label>Thematic Unity</label>
                            <?php
                                echo $this->Form->control('thematic_unity',['class' => 'form-control'  , 'id' => 'thematic'  , 'options' =>  [1=>'Finance and Economics',2 => 'Law' , 3 => 'Business and Management' , 4 => 'Computing and ICT' , 5  => 'Contruction and Engineering' , 6  => 'Education' , 7 => 'Health and Nursing' , 8 => 'Marketing' , 9 => 'Mathematics and Statistics' , 10 => 'Medical Science' , 11 => 'Politics and International Relations' , 12 => 'Science' , 13 => 'Accounting' , 14 => 'Social Sciences' , 15 => 'Shipping and Maritime' , 16 => 'Other'] , 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
                            ?>
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Langauge</label>
                            <?php
                                echo $this->Form->control('language',['class' => 'form-control'   , 'options' =>  [1=>'English',2 => 'Greek' ] , 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>No.Tasks/Pages</label>
                            <select class="form-control" name="assignment_type_value">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-end mt-3">

                    <button class="btn btn-filter mr-3" type="submit">Apply Filters</button>


                    <!-- <button class="btn clear-btn mr-3">Clear Filters</button> -->

                </div>
                 <?= $this->Form->end() ?>
            </div>
        </div>
        <div class="col-12 result-found mb-2"><?= $this->Paginator->counter(['format' => __('{{count}}')]) ?> Results Found</div>


        <?php 
        if(!empty($assignments)){
        foreach ($assignments as $key => $assignment) { ?>
       <div class="col-12 assignment-post-list mt-2">
            <div class="border-bottom pb-3">
                <div class="post-title"><a href="<?php echo $this->Url->build('/writer/assignments/view/'.$assignment->id); ?>"><?php echo $assignment->title; ?> </a></div>
                <div class="row py-1">
                    <?php $document = [1=>'Thesis',2 => 'Bachelors thesis' , 3 => 'Doctoral dissertation' , 4 => 'Statistical analysis' , 5  => 'Corrections' , 6  => 'Semester assigment' , 7 => 'Postgraduate assignment' , 8 => 'Presentation' , 9 => 'Scientific Article' , 10 => 'Translation' , 11 => 'Citation Resources'] ; ?>
                    <div class="col-sm-6 assignment-type"><b>Type of Document:</b> <?php echo $document[$assignment->document_type]; ?> </div>
                    <?php $unity = [1=>'Finance and Economics',2 => 'Law' , 3 => 'Business and Management' , 4 => 'Computing and ICT' , 5  => 'Contruction and Engineering' , 6  => 'Education' , 7 => 'Health and Nursing' , 8 => 'Marketing' , 9 => 'Mathematics and Statistics' , 10 => 'Medical Science' , 11 => 'Politics and International Relations' , 12 => 'Science' , 13 => 'Accounting' , 14 => 'Social Sciences' , 15 => 'Shipping and Maritime' , 16 => 'Other']; ?>
                    <div class="col-sm-6 assignment-type text-right"><b>Thematic Unity:</b> <?php echo $unity[$assignment->thematic_unity]; ?> </div>
                </div>
                <p><?php echo $assignment->description; ?></p>
                <div class="row">
                    <div class="col-sm-6 total-offer-made"><b>Total Offer made:</b> <?php echo count($assignment['assignment_offers']); ?></div>
                    <div class="col-sm-6 delivered-to-be text-right"><b>Delivered to be</b>: <?php echo date('d F Y',strtotime($assignment->delivery_date)); ?></div>
                </div>
            </div>
        </div>
        <?php } ?> <br>        
        <div class="float-right">
            <div class="col-md-6 text-right">
                <ul class="pagination">
                <?php
                    echo $this->Paginator->prev('<i class="fa fa-chevron-left"></i>',
                    array('escape' => false), null, array());
                    echo $this->Paginator->numbers(array('separator' => ''));
                    echo $this->Paginator->next('<i class="fa fa-chevron-right"></i>',
                    array('escape' => false), null, array());
                ?>
                </ul>
            </div>
        </div>
        <div class="float-right">
             <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
         </div><br><br>
        <?php  }else{?>
        <div class="col-12 assignment-post-list mt-2">
            <div class="border-bottom pb-3">
                <center>No records found</center>
            </div>
        </div>
        <?php } ?>
        <!-- <div class="page-naviagtion col-12 text-right mt-5">
            1-10 of 100 <span><a href="#"><img src="<?php echo $this->Url->build('/images/left-arrow.png'); ?>"></a></span><span><a href="#"><img
                        src="<?php echo $this->Url->build('/images/right-arrow.png'); ?>"></a></span>
        </div> -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var flip = 0;
        $(".filter-btn").click(function () {
            $(".filter-box").toggle(flip++ % 2 === 0);
        });
    });
</script>