<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 p-2 p-sm-0 mb-4 mt-4">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <h4>Past Assignments</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <?php echo $this->Flash->render(); ?>
</div>
<div class="section-content  mt-1 mr-4 ml-4 mb-4">
    <div class="table-responsive mobile-res-tb">
        <table class="table  header-text-left">
            <thead>
                <tr>
                    <th scope="col">Lesson title</th>
                    <th scope="col">Student Name</th>
                    <th scope="col">Rating given by student</th>
                    <th scope="col">Price</th>
                    <th scope="col">Delivered Date</th>
                    <th scope="col">Payment status</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($assignments as $key => $assignment) { ?>
                    <tr>
                        <td><?php echo $assignment['assignment']['title']; ?></td>
                        <td><?php echo $assignment['assignment']['user']['first_name']; ?></td>
                        <td>
                            <?php if(!empty($assignment['assignment']['rating_by_student'])){ ?>
                            <img class="one-star" src="<?php echo $this->Url->build('/images/one-star.png'); ?>"> 5/<?php echo $assignment['assignment']['rating_by_student']['rating']; ?>
                            <?php }else{  ?>
                                No ratings submitted
                            <?php } ?>
                            <?php if(empty($assignment['assignment']['rating_by_writer'])){ ?>
                                <!-- <br><button class="btn btn-primary btn-sm submit_review" data-assignment-id="<?php echo $assignment['assignment']['id']; ?>">submit your review</button> -->
                            <?php } ?>
                        </td>
                        <td>€ <?php echo $assignment['price']; ?></td>
                        <td>
                            <?php if(!empty($assignment['assignment_delivery_schedules'])) { echo date('d M, Y',strtotime(end($assignment['assignment_delivery_schedules'])['delivery_date'] )) ; } ?>                                
                        </td>
                        <td scope="row">
                            <?php if($assignment['assignment']['status'] == 3 ){ ?> 
                                <span class="badge badge-warning">Not Paid</span>
                            <?php } ?>
                            <?php if($assignment['assignment']['status'] == 4 ){ ?>
                                <span class="badge badge-info">Paid</span>
                             <?php  } ?>
                            
                        </td>
                        <td>
                            <div class="dropdown">
                                <button class=" more-btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src="<?php echo $this->Url->build('/images/more-horiz.svg'); ?>">
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="<?php echo $this->Url->build('/writer/assignments/view/'.$assignment['assignment']['id']);?>">View</a>
                                    <!-- <a class="dropdown-item" href="#">Rating & Feedback</a> -->
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <br><div class="float-right">
        <div class="col-md-6 text-right">
            <ul class="pagination">
            <?php
                echo $this->Paginator->prev('<i class="fa fa-chevron-left"></i>',
                array('escape' => false), null, array());
                echo $this->Paginator->numbers(array('separator' => ''));
                echo $this->Paginator->next('<i class="fa fa-chevron-right"></i>',
                array('escape' => false), null, array());
            ?>
            </ul>
        </div>
    </div>
    <div class="float-right">
         <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
     </div><br><br>
</div>


   <!--modal for feedback to Student-->
<div class="modal fade" id="submit_reviewModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Feedback to Student</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= $this->Form->create('submitAssignment',['class' => 'form-horizontal' , 'url' => '/writer/assignments/submitRatings'  , 'id' => 'submitAssignment' , 'novalidate' => true ]) ?>
            <div class="modal-body view-delivery">
                <div class="col-12">

                    <div class="form-group">
                        <label for="">Submit Your Review</label>
                        <textarea class="form-control" rows="3" name="message"></textarea>
                        <input type="hidden" name="assignment_id" id="assignment_id">
                        <input type="hidden" name="rating" id="ratingVal" value="">
                    </div>
                    <div class="form-group">
                        <label for="">Rating</label>
                        <div class="rateyo"></div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">

                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            <?= $this->Form->end() ?>

        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on('click','.submit_review',function(){
        $('#assignment_id').val($(this).attr('data-assignment-id'));
        $('#submit_reviewModel').modal('show');
    });
</script>