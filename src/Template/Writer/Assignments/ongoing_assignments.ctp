<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 p-2 p-sm-0 mb-4 mt-4">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <h4>Ongoing Assignments</h4>
                        </div>
                        <a href="<?php echo $this->Url->build('/writer/assignments/browse-assignments'); ?>" class="btn browse-assignment">
                            Browse Assignment</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <?php echo $this->Flash->render(); ?>
</div>
<div class="section-content mt-1 mr-4 ml-4 mb-4">
    <div class="col-sm-12 px-4 sort-by">
        <!-- <form class="form-inline">
            <input type="text" class="form-control mr-2" placeholder="Search">
            <button class="btn btn-primary mt-3 mt-sm-0">Search</button>
        </form> -->
    </div>
    <div class="table-responsive mobile-res-tb ongoing-assignment-page">
        <table class="table  header-text-left">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Lesson title</th>
                    <th scope="col">Price</th>
                    <th scope="col">Delivery Schedule</th>
                    <th scope="col">Next Delivery</th>
                    <th scope="col">Delivery Date</th>
                </tr>
            </thead>
            <tbody>
                 <?php 
                if(!empty($assignments)){
                    $i = 1;
                foreach ($assignments as $key => $assignment) { ?>
                <tr>
                    <td scope="row"><?php echo $i; ?></td>
                    <td><a href="<?php echo $this->Url->build('/writer/assignments/view/'.$assignment['assignment']['id']); ?>"><?php echo $assignment['assignment']['title']; ?></a>
                        <div class="client"><b>Client:</b> <?php echo $assignment['assignment']['user']['first_name']; ?></div>
                    </td>
                    <td class="price-box">€<?php echo $assignment['price']; ?> <div class="custom-label text-info text-white px-1"> ( <?php echo count($assignment['assignment_delivery_schedules']); ?> delivery
                            schedules )</div>
                    </td>
                    <td class="delivery-schedule-td">
                        <span class="mr-3"> 
                            <!-- <a data-toggle="modal" class="add-delivery-date"
                                data-target="#setDeliveryModal" data-offer-id="<?php echo $assignment['id']; ?>" href="#">  -->
                            <?php 


                            $datVariation = (strtotime(date('Y-m-d',strtotime($assignment['assignment']['delivery_date']))) - strtotime(date('Y-m-d'))) / (60 * 60 * 24);

                            ?>
                            <a class="add-delivery-date deliveryFormModal" data-offer-id="<?php echo $assignment['id']; ?>" data-delivery-date-limit="<?php echo $datVariation; ?>" href="#"> 

                                <img src="<?php echo $this->Url->build('/images/plus.png'); ?>"> Add</a>
                        </span>
                        <span> 
                            <!-- <a data-toggle="modal" class="add-delivery-date" data-target="#viewDelivery"
                                href="#"> -->

                            <a class="add-delivery-date deliveryDetailModal" data-offer-id="<?php echo $assignment['id']; ?>"  href="#"> 

                            <img src="<?php echo $this->Url->build('/images/information.png'); ?>"> View</a>
                        </span>
                    </td>
                    <td>
                        <?php 
                            if(!empty($assignment['assignment_delivery_schedules'])){
                                // echo end($assignment['assignment_delivery_schedules'])['delivery_date'];
                                $date = '';
                                $i = 1;
                                foreach ($assignment['assignment_delivery_schedules'] as $key => $nextDay) {
                                   

                                    if( date('m/d/Y',strtotime($nextDay['delivery_date'])) > date('m/d/Y') ){
                                        if(empty($date)){
                                            $date = $nextDay['delivery_date'];
                                        }

                                        if(date('m/d/Y',strtotime($date)) > date('m/d/Y',strtotime($nextDay['delivery_date']))){
                                            $date = date('m/d/Y',strtotime($nextDay['delivery_date']));
                                        }
                                    }
                                 $i++;
                                }

                                 echo date('d M, Y',strtotime($date));
                                
                            } 
                        ?>
                    </td>
                    <td><?php echo date('d M, Y',strtotime($assignment['assignment']['delivery_date'])); ?></td>

                    <td>
                        <div class="dropdown">
                            <button class=" more-btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="<?php echo $this->Url->build('/images/more-horiz.svg'); ?>">
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <?php if($assignment['status'] == 1){ ?>
                                    <a class="dropdown-item submitAssignment" data-offer_id = "<?php echo $assignment['id']; ?>"
                                        href=" #">Mark as Completed
                                    </a>
                                <?php  }else{ ?>
                                    <a class="dropdown-item" style="background-color: orange;"
                                        href=" #">Waiting for student review
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php $i++; } }else{ ?>

                        <tr>
                            <td colspan="7">No records found</td>
                        </tr>

                <?php  } ?>
            </tbody>
        </table>
    </div>
    <br><div class="float-right">
        <div class="col-md-6 text-right">
            <ul class="pagination">
            <?php
                echo $this->Paginator->prev('<i class="fa fa-chevron-left"></i>',
                array('escape' => false), null, array());
                echo $this->Paginator->numbers(array('separator' => ''));
                echo $this->Paginator->next('<i class="fa fa-chevron-right"></i>',
                array('escape' => false), null, array());
            ?>
            </ul>
        </div>
    </div>
    <div class="float-right">
         <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
     </div><br><br>
</div>


<!-- Modal -->
<div class="modal fade" id="setDeliveryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Set Delivery Schedule</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= $this->Form->create('addDeliveryDate',['class' => 'form-horizontal' , 'novalidate' => true , 'type' => 'file' , 'id' => 'setDeliveryDateForm' ]) ?>
            <div class="modal-body">
                <div class="col-sm-12">
                    <h6 class="mb-3 border-bottom pb-2">Deliverable </h6>
                    <div class="form-group">
                        <label>Description</label>
                        <input type="text" name="description" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Date of Delivery</label>
                        <input type="text" class="form-control datepickerCustom" name="delivery_date" placeholder="12/6/2019">
                    </div>
                    <div class="form-group">
                        <label>Upload File</label>
                        <input type="file" name="file" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Notes to be delivered</label>
                        <textarea class="form-control" name="delivery_notes" rows="1"></textarea>
                    </div>
                    <input type="hidden" name="assignment_offer_id" id="assignment_offer_id">
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
             <?= $this->Form->end() ?>
        </div>
    </div>
</div>



<!-- Modal view delivery -->
<div class="modal fade" id="viewDelivery" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">View Deliveries</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body view-delivery">
                <!-- <div class="col-12">
                    <h6 class="mb-4 pb-2 border-bottom">Deliverable 1</h6>

                    <div class="row mb-3">
                        <div class="col-sm-4"><label>Description</label></div>
                        <div class="col-sm-8">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry.</p>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-sm-4"><label>Date of Delivery</label> </div>
                        <div class="col-sm-8">12/7/2019 </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-sm-4"><label>Upload File</label> </div>
                        <div class="col-sm-8"> <a class="download-file" href="#">claudia-woods.pgf <img
                                    src="<?php echo $this->Url->build('/images/download.png'); ?>"> </a>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-sm-4"><label>Notes to be delivered</label> </div>
                        <div class="col-sm-8">
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry.
                            </p>
                        </div>
                    </div>
                </div> -->

                <div class="deliveryScheduleData"></div>

            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on('click','.deliveryFormModal',function(){


        $('#assignment_offer_id').val($(this).attr('data-offer-id'));

        var lastDelDate = $(this).attr('data-delivery-date-limit');

        $('.datepickerCustom').datepicker({ minDate: 0 , maxDate: lastDelDate-1 , dateFormat: 'yy-mm-dd'});

        $('#setDeliveryModal').modal('show');


    });


    $(document).on('click','.deliveryDetailModal',function(){


        var offer_id = $(this).attr('data-offer-id');

        $('.deliveryScheduleData').html('');

        $.ajax({
              type: "POST",
              url: "getDeliverySchedules",
              dataType: "json",
              data: {offer_id:offer_id},
             
              success:function(data)
              { 
                var html = '';

                // console.log(data);
                var i = 1;
                if(data.data.length != 0){

                    $.each(data.data, function(keys,vals) {

                        
                        var someDate = new Date(vals.delivery_date);
                        var numberOfDaysToAdd = 0;
                        var del_date_convertion = new Date(someDate.setDate(someDate.getDate() + numberOfDaysToAdd));  

                        var del_date =  
                              ((del_date_convertion.getMonth() > 8) ? (del_date_convertion.getMonth() + 1) : ('0' + (del_date_convertion.getMonth() + 1))) 
                              + '/' + 
                             ((del_date_convertion.getDate() > 9) ? del_date_convertion.getDate() : ('0' + del_date_convertion.getDate())) 
                             + '/' +  
                              del_date_convertion.getFullYear()
                              ;

                        html+= '<div class="col-12"><h6 class="mb-4 pb-2 border-bottom">Deliverable '+i+'</h6><div class="row mb-3"><div class="col-sm-4"><label>Description</label></div><div class="col-sm-8"><p>'+vals.description+'</p></div></div><div class="row mb-3"><div class="col-sm-4"><label>Date of Delivery</label> </div><div class="col-sm-8">'+del_date+'</div></div><div class="row mb-3"><div class="col-sm-4"><label>Upload File</label> </div><div class="col-sm-8">';

                        if(vals.file != ''){
                            html+= '<a class="download-file" target="_blank" href="<?php echo $this->Url->build('/uploads/student-assignment/');?>'+vals.file+'">'+vals.file+'<img src="<?php echo $this->Url->build('/images/download.png'); ?>"> </a>';
                        }else{
                            html+= '<a class="download-file" href="#">No files attached </a>';
                        }

                        html+='</div></div><div class="row mb-3"><div class="col-sm-4"><label>Notes to be delivered</label> </div><div class="col-sm-8"><p>'+vals.delivery_notes+'</p></div></div></div>'

                        i++;
                    });

                }else{
                   html+= '<div class="col-12"><div class="row mb-12"><div class="col-sm-12"><label><center>No Records Found</center></label></div></div></div>';
                }

                $('.deliveryScheduleData').html(html);

                $('#viewDelivery').modal('show');
              },
              error: function (){ }
        });

        


    });


    $(document).on('click','.submitAssignment',function(){

        var offer_id = $(this).attr('data-offer_id');

        $('.offerId').val(offer_id);

        $('#submitAssignment').modal('show');



    });


</script>

<div class="modal fade" id="submitAssignment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Accept Offer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <div class="col-12">
                    <p class="modal-text-paragraph">Are you want to submit assignment to student for review ?</p>

                </div>
            </div>

            <div class="modal-footer">
                <?= $this->Form->create('submitAssignment',['class' => 'form-horizontal' , 'url' => '/writer/assignments/submitAssignment'  , 'id' => 'submitAssignment' , 'novalidate' => true ]) ?>
                <input type="hidden" class="offerId" name="offer_id">
                <button type="submit" class="btn btn-primary px-3">Yes</button>
                 <?= $this->Form->end() ?>
                <span type="button" class="btn btn-secondary px-3" data-dismiss="modal">NO</span>

            </div>

        </div>
    </div>
</div>