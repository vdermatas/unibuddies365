<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 p-2 p-sm-0 mb-4 mt-4">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <h4>Our Ratings</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <?php echo $this->Flash->render(); ?>
</div>
<div class="section-content mt-1 mr-4 ml-4 mb-4">
    <div class="col-sm-12 px-4 sort-by">
    </div>
    <div class="table-responsive mobile-res-tb">
        <table class="table  header-text-left">
            <thead>
                <tr>
                    <th scope="col">Assignment Title</th>
                    <th scope="col">Student Name</th>
                    <th scope="col">Ratings</th>
                    <th scope="col">Messages</th>
                </tr>
            </thead>
            <tbody>
                <?php if(!empty($ratings)){
                foreach ($ratings as $key => $rating) {  ?>
                <?php if(!empty($rating['assignment']['rating_by_student'])){?>
                <tr>
                    <td><?php echo $rating['assignment']['title']; ?></td>
                    <td><?php echo $rating['assignment']['user']['first_name']; ?></td>
                    <td> <img class="one-star" src="<?php echo $this->Url->build('/images/one-star.png'); ?>"> 5 / <?php echo $rating['assignment']['rating_by_student']['rating']; ?></td>
                    <td><?php echo $rating['assignment']['rating_by_student']['message']; ?></td>
                </tr>
                <?php } ?> 
                <?php } ?> 
                <?php }else{ echo "<tr><td colspan='4'><center>No records found</center></td></tr>"; } ?>
            </tbody>
        </table>

    </div>                    
    <br><div class="float-right">
        <div class="col-md-6 text-right">
            <ul class="pagination">
            <?php
                echo $this->Paginator->prev('<i class="fa fa-chevron-left"></i>',
                array('escape' => false), null, array());
                echo $this->Paginator->numbers(array('separator' => ''));
                echo $this->Paginator->next('<i class="fa fa-chevron-right"></i>',
                array('escape' => false), null, array());
            ?>
            </ul>
        </div>
    </div>
    <div class="float-right">
         <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}')]) ?></p>
     </div><br><br>
</div>

