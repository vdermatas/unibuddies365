<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 p-2 p-sm-0 mb-0 mb-sm-4 mt-0 mt-sm-4">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <h4>Assignments</h4>
                        </div>
                        <!--   <button class="btn browse-assignment">
                                Save  Assignment</button> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <?php echo $this->Flash->render(); ?>
</div>
<div class="section-content  mt-1 mr-4 ml-4 mb-4 ">
    <div class="assignment-details pb-4">
        <div class="row">
            <div class="col-12">
                <h5><?php echo $assignment->title; ?></h5>
            </div>

        </div>
        <?php
            $documentType =  [1=>'Thesis',2 => 'Bachelors thesis' , 3 => 'Doctoral dissertation' , 4 => 'Statistical analysis' , 5  => 'Corrections' , 6  => 'Semester assigment' , 7 => 'Postgraduate assignment' , 8 => 'Presentation' , 9 => 'Scientific Article' , 10 => 'Translation' , 11 => 'Citation Resources'];

            $thematicUnit = [1=>'Finance and Economics',2 => 'Law' , 3 => 'Business and Management' , 4 => 'Computing and ICT' , 5  => 'Contruction and Engineering' , 6  => 'Education' , 7 => 'Health and Nursing' , 8 => 'Marketing' , 9 => 'Mathematics and Statistics' , 10 => 'Medical Science' , 11 => 'Politics and International Relations' , 12 => 'Science' , 13 => 'Accounting' , 14 => 'Social Sciences' , 15 => 'Shipping and Maritime' , 16 => 'Other'] ;

            $language = [1=>'English',2 => 'Greek' ] ;
         ?>
        <div class="row ">
            <div class="col-sm-6">
                <b>Type of Document:</b> <?php echo $documentType[$assignment->document_type]; ?>
            </div>
            <div class="col-sm-6">
                <b>Language:</b> <?php echo $language[$assignment->language]; ?>
            </div>
        </div>
        <div class="row ">
            <div class="col-sm-6">
                <b>Thematic Unity:</b> <?php echo $thematicUnit[$assignment->thematic_unity]; ?>
            </div>

            <!-- <div class="col-sm-6">
                                <b>Pages Required</b> : 4
                            </div> -->

        </div>
        <div class="row ">
            <div class="col-sm-6">
                <b>Date to be Delivered </b> : <?php echo date('d F, Y' , strtotime($assignment->delivery_date)); ?>
            </div>

        </div>
        <div class="row ">
            <div class="col-sm-12"><b>Reference Url </b>: <a target="_blank" href="http://<?php echo $assignment->reference_url; ?>"><?php echo $assignment->reference_url; ?></a>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-12">
                <h4>Description</h4>
                <p><?php echo $assignment->description; ?></p>
            </div>
        </div>
        <?php if(!empty($assignment->attachment_file)){ ?>
        <div class="row  download-file">
            <div class="col-sm-12">
                <a target="_blank" href="<?php echo $this->Url->build('/uploads/student-assignment/'.$assignment->attachment_file) ?>">1. <?php echo $assignment->attachment_file; ?> <img src="<?php echo $this->Url->build('/images/file-download.png'); ?>"></a>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
<?php if($assignment->status == 3 || $assignment->status == 4){ ?>
<div class="section-content  mt-1 mr-3 ml-3 ml-sm-4 mr-sm-4 mb-4 ">
    <div class="writers-offer pb-4 px-0 px-sm-4">
        <div class="row px-3 mb-3">
            <div class="col-sm-6 col-6">
                <h4>Delivery Schedules</h4>
            </div>
        </div>
        <div class="table-responsive mobile-res-tb">
            <table class="table  header-text-left">
                <thead>
                    <tr>
                        <th scope="col"><b>Description</b></th>
                        <th scope="col"><b>Notes</b></th>
                        <th scope="col"><b>Delivered At</b></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($assignment['assignment_offers'][0]['assignment_delivery_schedules'])){
                        foreach ($assignment['assignment_offers'][0]['assignment_delivery_schedules'] as $key => $deliverySchedules) {
                     ?>
                        <tr>
                            <td scope="row">
                                <?php echo $deliverySchedules['description']; ?>
                                <?php if(!empty($deliverySchedules['file'])){ ?>
                                    <br><br>
                                    <a target="_blank" href="<?php echo $this->Url->build('/uploads/student-assignment/'.$deliverySchedules['file']); ?>">
                                        <img src="<?php echo $this->Url->build('/images/download.png'); ?>">
                                    </a>
                                <?php } ?>
                                    
                            </td>
                            <td><?php echo $deliverySchedules['delivery_notes']; ?></td>
                            <td>
                                <?php echo date('d M,Y', strtotime($deliverySchedules['delivery_date'])); ?>
                                
                            </td>
                        </tr>
                    <?php }
                     } else { ?>
                         <tr>
                             <td colspan="3"><center>No Offers Submitted Yet</center></td>
                         </tr>
                     <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php } ?>
<div class="section-content  mt-1 mr-3 ml-3 ml-sm-4 mr-sm-4 mb-4 ">
    <div class="writers-offer pb-4 px-0 px-sm-4">
        <div class="row px-3 mb-3">
            <div class="col-sm-6 col-6">
                <h4>Writer’s Offers</h4>
            </div>
           <!--  <div class="col-sm-6 col-6 d-flex justify-content-end sort-by-section">
                <form class="form-inline">
                    <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Sort by: </label>
                    <select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">
                        <option selected="">Rating</option>
                        <option selected="">Price</option>
                        <option selected="">Date</option>
                    </select>
                </form>
            </div> -->
        </div>
        <?php if(!empty($assignment['assignment_offers'])){ 
            foreach ($assignment['assignment_offers'] as $key => $assignment_offer) {
        ?>
        <div class="row offer-list pb-2 pt-3 px-2 m-0">
            <div class="col-sm-9">
                <div class="media">
                    <!-- <img src="<?php echo $this->Url->build('/images/oval.png'); ?>" class="mr-3" alt="..."> -->
                    <?php if(!empty($assignment_offer['user']['profile_image'])){  ?>
                        <img style="height: 50px;" class="mr-3" src="<?php echo $this->Url->build('/uploads/writerProfile/'.$assignment_offer['user']['profile_image']); ?>">
                    <?php }else{ ?>
                        <img class="mr-3" src="<?php echo $this->Url->build('/images/oval.png'); ?>">
                    <?php } ?>
                    <div class="media-body">
                        <div class="writer-name"><?php echo $assignment_offer['user']['first_name']; ?> <span><img
                                    src="<?php echo $this->Url->build('/images/star-img.svg'); ?>"></span><span><small>( 0 )</small></span></div>
                        <div class="skills"><b>Documents:</b> 

                            <?php 
                            if(!empty($assignment_offer['user']['writer_profile']['writer_documents'])){
                            foreach ($assignment_offer['user']['writer_profile']['writer_documents'] as $key => $document) {
                                    $documentArray[] = " ".$document->document_type." ";
                                } ?></span>
                                <?php echo implode(",",$documentArray);  } ?>

                        </div>
                        <div class="offers">Offer Price : € <?php echo $assignment_offer['price']+ ( $assignment->admin_commission*$assignment_offer['price']/100  ); ?></div>
                    </div>
                </div>
            </div>
            <?php if($assignment['status'] == 1){ ?>
            <div class="col-sm-3 text-center stamp-section">
                <div class="mb-1">
                    <img class="stamp" src="<?php echo $this->Url->build('/images/stamp.png'); ?>">
                </div>
                <button data-id="<?php echo $assignment_offer['id']; ?>" class="btn dark-blue-btn acceptOfferButton">Accept
                    Offer</button>
            </div>
            <?php }else{ 
                if($assignment_offer['status'] == 0){
                    echo "<span class='alert alert-danger'>Offer Not Accepted</span>";                    
                }else{
                    echo "<span class='alert alert-success'>Offer Accepted</span>";
                }
            } ?>

        </div>
        <?php } }else{ echo "<center>No offers found yet</center>"; } ?>
    </div>
</div>


<div class="modal fade" id="acceptOfferModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Accept Offer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <div class="col-12">
                    <p class="modal-text-paragraph">Are you want to send offer to the writer ?</p>

                </div>
            </div>

            <div class="modal-footer">
                <?= $this->Form->create('acceptoffer',['class' => 'form-horizontal' , 'url' => '/student/assignments/acceptOffer'  , 'id' => 'acceptoffer' , 'novalidate' => true ]) ?>
                <input type="hidden" class="offerId" name="offer_id">
                <button type="submit" class="btn btn-primary px-3">Yes</button>
                 <?= $this->Form->end() ?>
                <span type="button" class="btn btn-secondary px-3" data-dismiss="modal">NO</span>

            </div>

        </div>
    </div>
</div>



<script type="text/javascript">
    $(document).on('click','.acceptOfferButton',function(){
        var id = $(this).attr('data-id');
        $('.offerId').val(id);
        $('#acceptOfferModal').modal('show');
    });

</script>
