<?= $this->Form->create($assignment,['class' => 'form-horizontal' , 'id' => 'editAssignement' , 'novalidate' => true , 'type' => 'file'  ]) ?>
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 p-2 p-sm-0 mb-4 mt-4">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <h4>Assignments</h4>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <?php echo $this->Flash->render(); ?>
</div>
<div class="section-content  mt-1 mr-4 ml-4 mb-4 add-assignments">
    <div class="form-group col-12 col-sm-6">
        <label for="exampleInputEmail1">Assignment Title</label>
        <?php
            echo $this->Form->control('title',['class' => 'form-control'   , 'placeholder' => 'Assignment Title', 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
        ?>
    </div>
    <div class="form-group col-12 col-sm-6">
        <label for="exampleInputEmail1">Type of Document</label>
        <?php
            echo $this->Form->control('document_type',['class' => 'form-control'   , 'options' =>  [1=>'Thesis',2 => 'Bachelors thesis' , 3 => 'Doctoral dissertation' , 4 => 'Statistical analysis' , 5  => 'Corrections' , 6  => 'Semester assigment' , 7 => 'Postgraduate assignment' , 8 => 'Presentation' , 9 => 'Scientific Article' , 10 => 'Translation' , 11 => 'Citation Resources'] , 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
        ?>
    </div>
    <div class="form-group col-12 col-sm-6">
        <label for="exampleInputEmail1">Thematic Unity</label>
        <?php
            echo $this->Form->control('thematic_unity',['class' => 'form-control'   , 'options' =>  [1=>'Finance and Economics',2 => 'Law' , 3 => 'Business and Management' , 4 => 'Computing and ICT' , 5  => 'Contruction and Engineering' , 6  => 'Education' , 7 => 'Health and Nursing' , 8 => 'Marketing' , 9 => 'Mathematics and Statistics' , 10 => 'Medical Science' , 11 => 'Politics and International Relations' , 12 => 'Science' , 13 => 'Accounting' , 14 => 'Social Sciences' , 15 => 'Shipping and Maritime' , 16 => 'Other'] , 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
        ?>
    </div>
    <div class="form-group col-12 col-sm-6">
        <label for="exampleInputEmail1">Language</label>
        <?php
            echo $this->Form->control('language',['class' => 'form-control'   , 'options' =>  [1=>'English',2 => 'Greek' ] , 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
        ?>
    </div>
    <div class="form-group col-12 col-sm-7">
        <label for="exampleInputEmail1">Brirf Description</label>
        <?php
            echo $this->Form->control('description',['class' => 'form-control'   , 'type' => 'textarea', 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
        ?>
    </div>
    <div class="form-group col-12 col-sm-6 ">
        <label class="mb-3" for="exampleInputEmail1">How many pages or words ?</label>
        <div class="mb-3">
            <label class="custom-radio">Pages
                <input type="radio" <?php if($assignment->assignment_type_by == 1){echo "Checked";} ?> value="1" name="assignment_type_by">
                <span class="checkmark"></span>
            </label>
            <label class="custom-radio">Words
                <input type="radio" <?php if($assignment->assignment_type_by == 2){echo "Checked";} ?> value="2" name="assignment_type_by">
                <span class="checkmark"></span>
            </label>
        </div>
        <div class="pages-section">
            <div class="d-flex">
                <?php
                    echo $this->Form->control('assignment_type_value',['class' => 'form-control'   , 'placeholder' => '1 Page or 500 words ', 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
                ?>
                <!-- <div><button class="btn btn-plus  ml-2 mr-2">-</button></div>
                <div><button class="btn btn-plus">+</button></div>
                <div class="col-4 mt-2 words-text">400 Words</div> -->
            </div>
            <div class=" mb-4"><small><strong>(400 words is equivalent to 1 page)</strong></small>
            </div>
        </div>
    </div>

    <!-- <div class="form-group col-12 col-sm-6 ">
        <label for="exampleInputEmail1">How many words</label>
        <div class="d-flex">
            <input type="text" class="form-control" placeholder="700 words">
            <div><button class="btn btn-plus  ml-2 mr-2">-</button></div>
            <div><button class="btn btn-plus">+</button></div>
            <div class="col-4 mt-2 words-text">2 Page</div>
        </div>
    </div>
    <div class="col-12 mb-4"><small><strong>(400 words is equivalent to 1 page)</strong></small> </div> -->
    <div class="form-group col-12 col-sm-6">
        <label for="exampleInputEmail1">Reference URL <small>(you can paste here referencing
                URL)</small></label>
        <?php
            echo $this->Form->control('reference_url',['class' => 'form-control'   , 'placeholder' => 'Enter Reference URL', 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
        ?>
    </div>
    <div class="form-group col-12 col-sm-6">
        <label for="exampleInputEmail1">Attachment file</label>
        <div class="upload-btn-wrapper">
            <button class="btn btn-blue">Add file</button>
            <?php
                echo $this->Form->control('attachment_file',['class' => 'form-control'   , 'type' => 'file', 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
            ?>
            <input type="hidden" name="attachment_file_empty" value="<?php echo $assignment->attachment_file; ?>">
            <?php if(!empty($assignment->attachment_file)){ ?>
                <div class="row  download-file">
                    <div class="col-sm-12">
                        <a target="_blank" href="<?php echo $this->Url->build('/uploads/student-assignment/'.$assignment->attachment_file) ?>">1. <?php echo $assignment->attachment_file; ?> <img src="<?php echo $this->Url->build('/images/file-download.png'); ?>"></a>
                    </div>
                </div>
            <?php } ?>
        </div>

        <small id="emailHelp" class="form-text text-muted">You can upload multiple files with a total limit
            of 15MB </small>
    </div>
    <div class="form-group col-12 col-sm-3 date-to-delivered">
        <img src="<?php echo $this->Url->build('/images/small-calendar.svg'); ?>">
        <label for="exampleInputEmail1">Date to be Delivered </label>
        <?php
            echo $this->Form->control('delivery_date',['class' => 'form-control datepicker' , 'type' => 'text'  , 'placeholder' => 'dd/mm/yyyy', 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  , 'value' => date('Y-m-d',strtotime($assignment->delivery_date)) ]);
        ?>
    </div>
    <div class="col-sm-12 mt-4">
        <button class="btn browse-assignment">
            Save Assignment</button>
    </div>
</div>
 <?= $this->Form->end() ?>