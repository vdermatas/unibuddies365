<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 p-2 p-sm-0 mb-4 mt-4">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <h4>Ongoing Assignments</h4>
                        </div>
                        <a href="<?php echo $this->Url->build('/student/assignments/add'); ?>" class="btn browse-assignment">
                            Add Assignment</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <?php echo $this->Flash->render(); ?>
</div>
<div class="section-content mt-1 mr-4 ml-4 mb-4">
    <div class="col-sm-12 px-4 sort-by">
        <form class="form-inline">
            <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Sort by: </label>
            <select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">
                <option selected>Recent Post</option>
                <option value="1">Recent Offer</option>
            </select>
        </form>
    </div>
    <div class="table-responsive mobile-res-tb">
        <table class="table  header-text-left">
            <thead>
                <tr>
                    <th scope="col">Lesson status</th>
                    <th scope="col">Lesson title</th>
                    <th scope="col">Offers</th>
                    <th scope="col">Delivered Notes</th>
                    <th scope="col">Delivery Date</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php if(!empty($assignments)){
                foreach ($assignments as $key => $assignment) {  ?>

                <?php 
                $offerId= '';
                $offerSubmited = 0; 
                foreach ($assignment['assignment_offers'] as $key => $assignment_offer) {
                            
                            $offerId = $assignment_offer['id'];
                            if($assignment_offer['status'] == 2){


                                $offerSubmited = 1;
                                $writerSubmitted = "<span style='color:green;'>Writer Submitted</span>";

                            }

                    }  ?>
                <tr>
                    <?php if($assignment->status == 1){ ?>
                    <td scope="row">
                        <span class="badge badge-warning">Not Assigned Yet</span>
                    </td>
                    <?php }elseif($assignment->status == 2){ ?>
                    <td scope="row">
                        <span class="badge badge-info">Assigned</span>
                        <div class="mt-2 text-danger">In Progress</div>

                        

                        <?php if(isset($writerSubmitted)){ echo $writerSubmitted; } ?>

                    </td>
                    <?php }elseif($assignment->status == 3){ ?>
                    <td scope="row">
                        <span class="badge badge-info">Assigned</span>
                        <div class="mt-2 text-success"><img style="width:15px" src="<?php echo $this->Url->build('/images/check-mark.png'); ?>">
                            Completed </div>
                       <!--  <div class="mt-2"><a href="#" data-toggle="modal" data-target="#ViewFeedback">Give
                                Feedback</a></div> -->
                    </td>
                    <?php } ?>
                    <td><?php echo $assignment->title; ?></td>
                    <td>
                        <a href="<?php echo $this->Url->build('/student/assignments/view/'.$assignment->id); ?>">
                            <?php if(count($assignment['assignment_offers']) > 0){ echo count($assignment['assignment_offers']); }else{ echo 'NA';} ?>
                        </a>                            
                    </td>
                    <td class="delivery-schedule-td">
                        <span> 
                            <!-- <a data-toggle="modal" class="add-delivery-date" data-target="#viewDelivery"
                                href="#"> -->
                        <?php if(!empty($offerId)){ ?>
                            <a class="add-delivery-date deliveryDetailModal" data-offer-id="<?php echo $offerId; ?>"  href="#"> 

                            <img src="<?php echo $this->Url->build('/images/information.png'); ?>"> View</a>
                        <?php } ?>
                        </span>
                    </td>
                    <td><?php echo date('d M, Y',strtotime($assignment->delivery_date)); ?></td>
                    <td>
                        <div class="dropdown">
                            <button class=" more-btn dropdown-toggles" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                <img src="<?php echo $this->Url->build('/images/more-horiz.svg'); ?>">
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="<?php echo $this->Url->build('/student/assignments/view/'.$assignment->id); ?>">View</a>
                                <a class="dropdown-item" href="<?php echo $this->Url->build('/student/assignments/edit/'.$assignment->id); ?>">Edit</a>
                                <?php if($offerSubmited == 1){ ?>
                                    <a class="dropdown-item submitAssignment" data-assignment_id = "<?php echo $assignment['id']; ?>" data-offer_id = "<?php echo $offerId; ?>"
                                        href=" #">Mark as Completed
                                    </a>
                                    <a class="dropdown-item sendChangesMessage" data-assignment_id = "<?php echo $assignment['id']; ?>" data-offer_id = "<?php echo $offerId; ?>"
                                        href=" #">Send Message to writer
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php } ?> 
                <?php }else{ echo "<tr><td colspan='4'><center>No records found</center></td></tr>"; } ?>
            </tbody>
        </table>

    </div>                    
    <br><div class="float-right">
        <div class="col-md-6 text-right">
            <ul class="pagination">
            <?php
                echo $this->Paginator->prev('<i class="fa fa-chevron-left"></i>',
                array('escape' => false), null, array());
                echo $this->Paginator->numbers(array('separator' => ''));
                echo $this->Paginator->next('<i class="fa fa-chevron-right"></i>',
                array('escape' => false), null, array());
            ?>
            </ul>
        </div>
    </div>
    <div class="float-right">
         <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
     </div><br><br>
</div>




<!--modal for feedback to writer-->
<div class="modal fade" id="ViewFeedback" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Feedback to Writer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body view-delivery">
                <div class="col-12">

                    <div class="form-group">
                        <label for="">Submit Your Review</label>
                        <textarea class="form-control" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="">Rating</label>
                        <div class="rateyo"></div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-primary">Submit</button>
            </div>

        </div>
    </div>
</div>
<!-- Modal view delivery -->
<div class="modal fade" id="viewDelivery" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">View Deliveries</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body view-delivery">
                <!-- <div class="col-12">
                    <h6 class="mb-4 pb-2 border-bottom">Deliverable 1</h6>

                    <div class="row mb-3">
                        <div class="col-sm-4"><label>Description</label></div>
                        <div class="col-sm-8">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry.</p>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-sm-4"><label>Date of Delivery</label> </div>
                        <div class="col-sm-8">12/7/2019 </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-sm-4"><label>Upload File</label> </div>
                        <div class="col-sm-8"> <a class="download-file" href="#">claudia-woods.pgf <img
                                    src="<?php echo $this->Url->build('/images/download.png'); ?>"> </a>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-sm-4"><label>Notes to be delivered</label> </div>
                        <div class="col-sm-8">
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry.
                            </p>
                        </div>
                    </div>
                </div> -->

                <div class="deliveryScheduleData"></div>

            </div>

        </div>
    </div>
</div>



<script type="text/javascript">


    $(document).on('click','.deliveryDetailModal',function(){


        var offer_id = $(this).attr('data-offer-id');

        $('.deliveryScheduleData').html('');

        $.ajax({
              type: "POST",
              url: "getDeliverySchedules",
              dataType: "json",
              data: {offer_id:offer_id},
             
              success:function(data)
              { 
                var html = '';

                // console.log(data);
                var i = 1;
                if(data.data.length != 0){

                    $.each(data.data, function(keys,vals) {

                        
                        var someDate = new Date(vals.delivery_date);
                        var numberOfDaysToAdd = 0;
                        var del_date_convertion = new Date(someDate.setDate(someDate.getDate() + numberOfDaysToAdd));  

                        var del_date =  
                              ((del_date_convertion.getMonth() > 8) ? (del_date_convertion.getMonth() + 1) : ('0' + (del_date_convertion.getMonth() + 1))) 
                              + '/' + 
                             ((del_date_convertion.getDate() > 9) ? del_date_convertion.getDate() : ('0' + del_date_convertion.getDate())) 
                             + '/' +  
                              del_date_convertion.getFullYear()
                              ;

                        html+= '<div class="col-12"><h6 class="mb-4 pb-2 border-bottom">Deliverable '+i+'</h6><div class="row mb-3"><div class="col-sm-4"><label>Description</label></div><div class="col-sm-8"><p>'+vals.description+'</p></div></div><div class="row mb-3"><div class="col-sm-4"><label>Date of Delivery</label> </div><div class="col-sm-8">'+del_date+'</div></div><div class="row mb-3"><div class="col-sm-4"><label>Upload File</label> </div><div class="col-sm-8">';

                        if(vals.file != ''){
                            html+= '<a class="download-file" target="_blank" href="<?php echo $this->Url->build('/uploads/student-assignment/');?>'+vals.file+'">'+vals.file+'<img src="<?php echo $this->Url->build('/images/download.png'); ?>"> </a>';
                        }else{
                            html+= '<a class="download-file" href="#">No files attached </a>';
                        }

                        html+='</div></div><div class="row mb-3"><div class="col-sm-4"><label>Notes to be delivered</label> </div><div class="col-sm-8"><p>'+vals.delivery_notes+'</p></div></div></div>'

                        i++;
                    });

                }else{
                   html+= '<div class="col-12"><div class="row mb-12"><div class="col-sm-12"><label><center>No Records Found</center></label></div></div></div>';
                }

                $('.deliveryScheduleData').html(html);

                $('#viewDelivery').modal('show');
              },
              error: function (){ }
        });

        


    });

     $(document).on('click','.submitAssignment',function(){

        var assignment_id = $(this).attr('data-assignment_id');
        var offer_id = $(this).attr('data-offer_id');

        $('.assignmentId').val(assignment_id);
        $('.offerId').val(offer_id);

        $('#submitAssignment').modal('show');



    });

      $(document).on('click','.sendChangesMessage',function(){

        var assignment_id = $(this).attr('data-assignment_id');
        var offer_id = $(this).attr('data-offer_id');

        $('.assignmentId').val(assignment_id);
        $('.offerId').val(offer_id);

        $('#sendChangesMessage').modal('show');



    });
</script>


<div class="modal fade" id="submitAssignment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Accept Offer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <div class="col-12">
                    <p class="modal-text-paragraph">Are you want to mark this assignment as complete ?</p>

                </div>
            </div>

            <div class="modal-footer">
                <?= $this->Form->create('submitAssignment',['class' => 'form-horizontal' , 'url' => '/student/assignments/submitAssignment'  , 'id' => 'submitAssignment' , 'novalidate' => true ]) ?>
                <input type="hidden" class="assignmentId" name="assignment_id">
                <input type="hidden" class="offerId" name="offer_id">
                <button type="submit" class="btn btn-primary px-3">Yes</button>
                 <?= $this->Form->end() ?>
                <span type="button" class="btn btn-secondary px-3" data-dismiss="modal">NO</span>

            </div>

        </div>
    </div>
</div>


<div class="modal fade" id="sendChangesMessage" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <?= $this->Form->create('sendChangesMessage',['class' => 'form-horizontal' , 'url' => '/student/assignments/sendChangesMessage'  , 'id' => 'sendChangesMessage' , 'novalidate' => true ]) ?>
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Send message to writer regarding the submitted documents</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <div class="col-12">
                    <!-- <input type="textarea" name="message" class="form-control"> -->
                    <textarea rows="6" cols="50" name="message" required></textarea>
                </div>
            </div>

            <div class="modal-footer">
                <input type="hidden" class="assignmentId" name="assignment_id">
                <input type="hidden" class="offerId" name="offer_id">
                <button type="submit" class="btn btn-primary px-3">Submit</button> 

            </div>

        </div>
        <?= $this->Form->end() ?>
    </div>
</div>