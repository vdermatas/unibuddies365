<?= $this->Form->create($user,['class' => 'form-horizontal' , 'id' => 'studentProfile' , 'novalidate' => true , 'type' => 'file'  ]) ?>
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 p-2 p-sm-0 mb-0 mb-sm-4 mt-0 mt-sm-4">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <h4>Edit Profile</h4>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="row px-0 px-sm-2 mx-1 edit-profile">    
    <div class="col-sm-3 text-center">
        <div class="shadow p-4 edit-pic-profile">
            <div class="img-setting-profile">
                <?php if(!empty($user->profile_image)){  ?>
                    <img src="<?php echo $this->Url->build('/uploads/studentProfile/'.$user->profile_image); ?>">
                    <input type="hidden" name="profile_image_empty" value="<?php echo $user->profile_image;?>">
                <?php }else{ ?>
                    <img src="<?php echo $this->Url->build('/images/user-male.png'); ?>">
                <?php } ?>
                
                <div class="edit-pic" style="display: none;">
                    <input type="file" name="profile_image" style="display: none;" class="fileInput">
                    <a href="#" id="openButton"><img src="<?php echo $this->Url->build('/images/mode-edit.png'); ?>"></a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-9 ">
        <?php echo $this->Flash->render(); ?>
        <div class="shadow px-4 py-4 pb-5">
            <h5>Basic Information</h5>
            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>First Name</label>
                        <?php
                            echo $this->Form->control('first_name',['class' => 'form-control'   , 'placeholder' => 'First Name', 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
                        ?>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Last Name</label>
                        <?php
                            echo $this->Form->control('last_name',['class' => 'form-control'   , 'placeholder' => 'Last Name', 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Phone Number</label>
                        <?php
                            echo $this->Form->control('mobile',['class' => 'form-control'   , 'placeholder' => 'Contact Number', 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Email Address</label>
                        <?php
                            echo $this->Form->control('email',['class' => 'form-control'   , 'placeholder' => 'Email Address', 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
                        ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>City</label>
                        <?php
                            echo $this->Form->control('city', [ 'class' => 'form-control' , 'label' => false , 'options' => ['Massachusetts' => 'Massachusetts'] ,
                                'templates' => [
                                            'inputContainer' => '{{content}}',
                                            'inputDiv' => ''
                                        ] 
                                ]); 
                        ?>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>State</label>
                        <?php
                            echo $this->Form->control('city', [ 'class' => 'form-control' , 'label' => false , 'options' => ['Massachusetts' => 'Massachusetts'] ,
                                'templates' => [
                                            'inputContainer' => '{{content}}',
                                            'inputDiv' => ''
                                        ] 
                                ]); 
                        ?>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Zipcode</label>
                        <?php
                            echo $this->Form->control('zipcode',['class' => 'form-control'   , 'placeholder' => 'Zipcode', 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ]  ]);
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Address</label>
                        <?php
                            echo $this->Form->control('address',['class' => 'form-control'   , 'placeholder' => 'Address', 'label' => false , 'templates' => [  'inputContainer' => '{{content}}' ] ,'type' => 'textarea' ]);
                        ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <button class="btn browse-assignment">
                        Save Changes</button>
                </div>
            </div>
        </div>
    </div>
</div>
 <?= $this->Form->end() ?>
 <script type="text/javascript">
     $(document).ready(function(){
        $("#openButton").click(function(){

          $(".fileInput").trigger('click');
        });
     });
 </script>
