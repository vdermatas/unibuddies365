 <section class="login-bg d-flex justify-content-center align-items-center login-section">
        <div class="container">
            <div class="col-sm-12 col-xl-8 m-auto">

                <div class="row login-page">
                <div class="col-sm-4 login-left-bg  ">
                    <div class="welcome-back">Welcome Back</div>
                    <div class="to-keep-connected">To keep connected with us please login with personal Information by email address and password.</div>
                    
                </div>
                <div class="col-sm-8 pl-0">
                    <div class="signup-wraper">
                    <h3>Sign In</h3>
                    <div class="text-center">
                    <ul class="list-unstyled">
                        <li class="list-inline-item">

                            <?php 
                                echo $this->Html->image('../images/fb.png', [
                                'alt' => '']);
                            ?>
                            
                        </li>
                        <li class="list-inline-item">
                            <?php 
                                echo $this->Html->image('../images/google.png', [
                                'alt' => '']);
                            ?>
                            <!-- <img src="images/google.png"> -->
                        </li>
                    </ul>
                </div>
                    <div class="Or-you-can-use-your">Or</div>
                    
               <?php echo $this->Flash->render(); ?>
                    <div class="form-group">
                        <label>Email address</label>
                        <input type="email" class="form-control" placeholder="keithcarlson@example.com ">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="text" class="form-control" placeholder=" ">
                    </div>

                    <div class="form-group text-right forgot-password">
                        <a href="#">Forgot Password?</a>
                    </div>
                   

                    <div class="form-group mb-4">
                        <button class="btn btn-dark-blue">Sign In</button>
                    </div>

                    <div class="form-group text-center">
                        <p class="already-have-an-account">Don’t have an account? <a href="<?php echo $this->Url->build(['controller'=> 'users', 'action' => 'register']);?>"> Sign Up</a></p>
                    </div>
                </div>
                </div>
                </div>
           
            </div>
        </div>
    </section>