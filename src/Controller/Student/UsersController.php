<?php
namespace App\Controller\Student;

use App\Controller\Student\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Mailer\MailerAwareTrait;
use Cake\Mailer\Email;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

     public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['studentRegister']);
    }

     protected function _sendEmailMessage($to = null, $email_body = null, $subject = null, $bcc = null, $cc = null) {

        $email = new Email('default');

        $email->setEmailFormat('both');

        $email->setFrom('admin@unibuddies.com');

        $email->setSender('admin@unibuddies.com');

        $email->setTo($to);

        $email->setSubject($subject);

        if ($email->send($email_body)) {

            return true;

        }

        return false;

    }
    
    public function studentRegister()
    { 
        $users = $this->Users->newEntity();

        $this->loadModel('EmailTemplates');    
         $this->loadModel('Settings');
        $settings = $this->Settings->get(1);

       
        if($this->request->is('post')) {

            $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
            $remember_token = substr(str_shuffle(str_repeat($pool, 64)), 0,64);
            $user = $this->Users->patchEntity($users, $this->request->getData());

            $user['user_type_id'] = 2;
            $user['status'] = 0;
            $user['remember_token'] = $remember_token;
            $user['username'] = $this->request->getData()['email'];


            if(!empty($user->getErrors())){
                foreach ($user->getErrors() as $key => $value) {
                    foreach ($value as $key => $values) {
                       // $error.= $values."<br>";
                        $this->Flash->error(__($values));
                    }
                }
            }else{
                if ($result = $this->Users->save($user)) {

                    $emailTemplate = $this->EmailTemplates->get(2);
                    $emailTemplateAdmin = $this->EmailTemplates->get(5);


                    $emailTemplate['body'] = str_replace(
                        array('#NAME'), array(
                              $user->first_name." ".$user->last_name
                        ), $emailTemplate['body']
                      ); 


                    $emailTemplate['body'] = str_replace(
                        array('#DETAILS'), array(
                              "<br> <br><b>User Name</b> : ".$user->username." <br> <b>Password</b> : ".$this->request->getData()['password']
                        ), $emailTemplate['body']
                      ); 


                    $emailTemplateAdmin['body'] = str_replace(
                        array('#DETAILS'), array(
                              "You have new user registration<br> <br><b>Writer Name</b> : ".$user->username." <br> "
                        ), $emailTemplateAdmin['body']
                      ); 

                    $this->_sendEmailMessage($this->request->getData()['email'], $emailTemplate['body'], $emailTemplate['subject']);
                    $this->_sendEmailMessage($settings->notification_email, $emailTemplateAdmin['body'], $emailTemplateAdmin['subject']);   

                    // Retrieve user from DB
                    $authUser = $this->Users->get($result->id)->toArray();

                    // Log user in using Auth
                    $this->Auth->setUser($authUser);
                    $this->Flash->success(__('User registered successfully.'));

                    return $this->redirect($this->Auth->redirectUrl());

                    
                }else{
                    $this->Flash->error(__('Error occured while registering user. Please try again.'));

                    return $this->redirect($this->referer());
                }
            }
        }
        return $this->redirect($this->referer());
        $this->set(compact('users'));
       $this->set('_serialize', ['users']);
    }


    public function login()
    {
        if ($this->request->is('post')) {

            $user = $this->Auth->identify();

            if($user) {
                if($user['user_type_id'] != 2 ){
                    $this->Flash->error(__('Students are allowed to login '));
                    return $this->redirect($this->referer());
                }else{
                    $this->Auth->setUser($user);
                    $this->Flash->success(__('Logged in successfully'));
                    return $this->redirect($this->Auth->redirectUrl());
                }
            }else{
                $this->Flash->error(__('Invalid User Name and Password'));
                return $this->redirect($this->referer());
            }
        }

        return $this->redirect('/users/login');
    }


    public function changePassword()
    {
        $this->viewBuilder()->setLayout('student/dashboard');
        $user =$this->Users->get($this->Auth->user('id'));
        if (!empty($this->request->getData())) {
              // pr($this->request->getData()['old_password']); die;
            $user = $this->Users->patchEntity($user, [
                    'old_password'  => $this->request->getData()['old_password'],
                    'password'      => $this->request->getData()['password'],
                    'confirm_password'     => $this->request->getData()['confirm_password']
                ],
                ['validate' => 'password']
            );
            $error = '';
            if(!empty($user->getErrors())){
                foreach ($user->getErrors() as $key => $value) {
                    foreach ($value as $key => $values) {
                       // $error.= $values."<br>";
                        $this->Flash->error(__($values));
                    }
                }
            }else{
                if ($this->Users->save($user)) {
                    $this->Flash->success('Your password has been changed successfully');
                    $this->redirect(['action' => 'changePassword']);
                } else {
                    $this->Flash->error('Old Password Does Not Match');
                }
            }
        }
        $this->set('user',$user);
    }

    public function dashboard()
    {
        $this->viewBuilder()->setLayout('student/dashboard');
        
    }

     public function logout()
    {
         $this->Flash->success('You have successfully loged out'); 
         $this->Auth->logout();
         return $this->redirect('/users/login');
     }

     public function myProfile()
     {
        $this->viewBuilder()->setLayout('student/dashboard');

        $user = $this->Users->get($this->Auth->User('id'));
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $Pimage = $user->profile_image;
            $user = $this->Users->patchEntity($user, $this->request->getData());

            if(!empty($user->getErrors())){
                foreach ($user->getErrors() as $key => $value) {
                    foreach ($value as $key => $values) {
                       // $error.= $values."<br>";
                        $this->Flash->error(__($values));
                    }
                }
            }else{
                 
               

                if( !empty($this->request->getData()['profile_image']['name']) ){

                    if(move_uploaded_file($this->request->getData()['profile_image']['tmp_name'], WWW_ROOT . 'uploads/studentProfile' . DS . 'profile_'.$this->request->getData()['mobile']."_".$this->request->getData()['profile_image']['name']))
                    {
                        $user->profile_image = 'profile_'.$this->request->getData()['mobile']."_".$this->request->getData()['profile_image']['name'];
                    }

                }else{
                     $user->profile_image = $Pimage;
                }

                if ($this->Users->save($user)) {

                    $this->Auth->setUser($user);

                    $this->Flash->success(__('Profile has been saved successfully'));               

                }else{

                    $this->Flash->error(__('Something went wrong , Please try again later .'));
                }
            }

            return $this->redirect($this->referer());
        }

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);

         
     }
}
