<?php
namespace App\Controller\Student;

use App\Controller\Student\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Mailer\MailerAwareTrait;
use Cake\Mailer\Email;

/**
 * Assignments Controller
 *
 *
 * @method \App\Model\Entity\Assignment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AssignmentsController extends AppController
{

     public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->viewBuilder()->setLayout('student/dashboard');

        if($this->Auth->User('status') == 0){

            $this->Flash->error(__('Your account has not approved or you will be blocked by Admin , please contact administration'));

            return $this->redirect($this->Auth->redirectUrl());

        }
    }

    protected function _sendEmailMessage($to = null, $email_body = null, $subject = null, $bcc = null, $cc = null)
    {
        $email = new Email('default');
        $email->setEmailFormat('both');
        $email->setFrom('admin@unibuddies.com');
        $email->setSender('admin@unibuddies.com');
        $email->setTo($to);
        $email->setSubject($subject);
        if ($email->send($email_body)) {
            return true;
        }
        return false;
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $assignments = $this->paginate($this->Assignments);

        $this->set(compact('assignments'));
    }

     /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function ongoingAssignments()
    {


        $this->paginate = [
            'conditions' => [
                'user_id' => $this->Auth->User('id') ,
                'status IN' => ['1','2']
            ], 
            'contain' => ['AssignmentOffers' => ['AssignmentDeliverySchedules' => [ 'conditions' => [ 'AssignmentDeliverySchedules.delivery_date <=' => date('Y-m-d') ] ] ] ]
        ];

       
        
        $assignments = $this->paginate($this->Assignments);
         // pr($assignments); die;
        $this->set(compact('assignments'));
    }


     /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function pastAssignments()
    {


        $this->paginate = [
            'conditions' => [
                'user_id' => $this->Auth->User('id') ,
                'status IN' => ['3','4']
            ], 
            'contain' => ['AssignmentOffers' => [ 'Users' , 'conditions' => [
                'AssignmentOffers.status !=' => 0                
            ] , 'AssignmentDeliverySchedules' ] , 'RatingByWriter' , 'RatingByStudent' ]
        ];

       
        
        $assignments = $this->paginate($this->Assignments);
         // pr($assignments); die;
        $this->set(compact('assignments'));
    }

    /**
     * View method
     *
     * @param string|null $id Assignment id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->loadModel('Settings');
        
        // $assignment = $this->Assignments->get($id, [
        //     'contain' => ['AssignmentOffers' => ['Users' => ['WriterProfiles' => ['WriterSkills','WriterDocuments']]]]
        // ]);

        $assignment = $this->Assignments->get($id, [
            'contain' => [
                'AssignmentOffers' => [
                    'Users' => ['WriterProfiles' => ['WriterSkills','WriterDocuments']] ,   'AssignmentDeliverySchedules' 
                ]
            ]
        ]);

        $setting = $this->Settings->get(1);

        $this->set('assignment', $assignment);
        $this->set('setting', $setting);

        // pr($assignment); die;
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $assignment = $this->Assignments->newEntity();

        $this->loadModel('Settings');
        $this->loadModel('EmailTemplates'); 
        $emailTemplateAdmin = $this->EmailTemplates->get(5);

        $setting = $this->Settings->get(1);
        
        if ($this->request->is('post')) {

            $assignment = $this->Assignments->patchEntity($assignment, $this->request->getData());
           
            if(!empty($assignment->getErrors())){
                foreach ($assignment->getErrors() as $key => $value) {
                    foreach ($value as $key => $values) {
                        $this->Flash->error(__($values));
                    }
                }
            }else{

                if(move_uploaded_file($this->request->getData()['attachment_file']['tmp_name'], WWW_ROOT . 'uploads/student-assignment' . DS . 'attachment_'.$this->Auth->User('id')."-".date('Y-m-d-H-i-s')."_".$this->request->getData()['attachment_file']['name']))
                {
                   
                    $assignment->attachment_file = 'attachment_'.$this->Auth->User('id')."-".date('Y-m-d-H-i-s')."_".$this->request->getData()['attachment_file']['name'];
                }

                $assignment->user_id = $this->Auth->User('id');
                $assignment->admin_commission = $setting->commission;

                if ($this->Assignments->save($assignment)) {

                    $emailTemplateAdmin['body'] = str_replace(
                        array('#DETAILS'), array(
                              "We have new assignment on our system <br><br> Assignment Title : ".$assignment->title
                        ), $emailTemplateAdmin['body']
                      ); 

                    $this->_sendEmailMessage($setting->notification_email, $emailTemplateAdmin['body'], $emailTemplateAdmin['subject']);  
                    $this->Flash->success(__('The assignment has been saved.'));

                    return $this->redirect(['action' => 'ongoingAssignments']);
                }else{
                    $this->Flash->error(__('The assignment could not be saved. Please, try again.'));
                }
            }
        }
        $this->set(compact('assignment'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Assignment id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $assignment = $this->Assignments->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $assignment = $this->Assignments->patchEntity($assignment, $this->request->getData());
           
            if(!empty($assignment->getErrors())){
                foreach ($assignment->getErrors() as $key => $value) {
                    foreach ($value as $key => $values) {
                        $this->Flash->error(__($values));
                    }
                }
            }else{


                if( !empty($this->request->getData()['attachment_file']['name']) ){

                    if(move_uploaded_file($this->request->getData()['attachment_file']['tmp_name'], WWW_ROOT . 'uploads/student-assignment' . DS . 'attachment_'.$this->Auth->User('id')."-".date('Y-m-d-H-i-s')."_".$this->request->getData()['attachment_file']['name']))
                    {
                       echo "string";
                        $assignment->attachment_file = 'attachment_'.$this->Auth->User('id')."-".date('Y-m-d-H-i-s')."_".$this->request->getData()['attachment_file']['name'];
                    }

                }else{

                    $assignment->attachment_file = $this->request->getData()['attachment_file_empty'];

                }   

                if ($this->Assignments->save($assignment)) {
                    $this->Flash->success(__('The assignment has been saved.'));

                    return $this->redirect(['action' => 'ongoingAssignments']);
                }else{
                    $this->Flash->error(__('The assignment could not be saved. Please, try again.'));
                }
            }
        }
        $this->set(compact('assignment'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Assignment id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $assignment = $this->Assignments->get($id);
        if ($this->Assignments->delete($assignment)) {
            $this->Flash->success(__('The assignment has been deleted.'));
        } else {
            $this->Flash->error(__('The assignment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }


    public function acceptOffer($id = null)
    {
        $this->loadModel('AssignmentOffers');
        $this->loadModel('EmailTemplates'); 
        $this->loadModel('Settings'); 
        $emailTemplateAdmin = $this->EmailTemplates->get(5);
        $emailTemplate = $this->EmailTemplates->get(8);
        $setting = $this->Settings->get(1);


        $assignmentOffer = $this->AssignmentOffers->get($this->request->getData()['offer_id'],[
                'contain' => ['Users']
        ]);

        // pr($assignmentOffer['user']['email']); die;
        $assignmentOffer->status = 1;       
        $this->AssignmentOffers->save($assignmentOffer);


        $assignment = $this->Assignments->get($assignmentOffer['assignment_id']);
        $assignment->status = 2;
        $this->Assignments->save($assignment);


        $emailTemplateAdmin['body'] = str_replace(
            array('#DETAILS'), array(
                  "Offer has been accepted for this assignment : ".$assignment->title
            ), $emailTemplateAdmin['body']
          ); 

        $emailTemplate['body'] = str_replace(
            array('#DETAILS'), array(
                  "Your offer has been accepted by student for this assignment : ".$assignment->title
            ), $emailTemplate['body']
          ); 

        $this->_sendEmailMessage($setting->notification_email, $emailTemplateAdmin['body'], $emailTemplateAdmin['subject']);
        $this->_sendEmailMessage($assignmentOffer['user']['email'], $emailTemplate['body'], $emailTemplate['subject']);

        $this->Flash->success(__('Offer has been accepted'));

        return $this->redirect($this->referer());
       
    }

     public function getDeliverySchedules()
    {
        $this->loadModel('AssignmentDeliverySchedules');
        $this->loadModel('AssignmentOffers');

        $offerCheck = $this->AssignmentOffers->get($this->request->getData()['offer_id']);


        if($offerCheck['status'] == 1){
            $conditions = ['assignment_offer_id' => $this->request->getData()['offer_id'] , 'delivery_date <=' => date('Y-m-d') ];
        }else{
            $conditions = ['assignment_offer_id' => $this->request->getData()['offer_id']  ];
        }

        $schedules = $this->AssignmentDeliverySchedules->find('all',[
                'conditions' => $conditions
        ])->toArray();

        // pr($schedules); die;

        echo json_encode(['data' => $schedules]);
        exit;
    }


    public function submitAssignment()
    {
        // $this->loadModel('Assignments');
        // pr($this->request->getData()['offer_id']); die;

        $offer = $this->Assignments->get($this->request->getData()['assignment_id']);

        $offer->status = 3 ;

        $this->Assignments->save($offer);

        $this->Flash->success(__('The assignment has been submitted to student'));

        return $this->redirect($this->referer());


    }


    public function sendChangesMessage()
    {
        $this->loadModel('AssignmentOffers');
        $this->loadModel('Users');
        $this->loadModel('Settings');
        $this->loadModel('EmailTemplates'); 
        $emailTemplateAdmin = $this->EmailTemplates->get(5);
        $setting = $this->Settings->get(1);        


        $getUserId = $this->AssignmentOffers->get($this->request->getData()['offer_id'],[
            'contain' => ['Users' , 'Assignments']
        ]);

        $emailTemplateAdmin['body'] = str_replace(
            array('#DETAILS'), array(
                  "Student sent some corrections to writer through email for this assignment : <b>".$getUserId['assignment']['title']."</b><br>Message : ".$this->request->getData()['message']
            ), $emailTemplateAdmin['body']
          ); 
        // pr($getUserId); die;
        // $userEmail = $this->Users->get($getUserId['user_id']);
        // pr($userEmail->email); die;
        $this->loadModel('EmailTemplates');                
        $temp = $this->EmailTemplates->get(4);  
        $temp['body'] = str_replace(array('#NAME', '#Message'), array(
                                        'Writer',
                                        $this->request->getData()['message']
                                        ), $temp['body']
                                      );       

         $this->_sendEmailMessage($getUserId['user']['email'], $temp['body'], $temp['subject']);

        $this->_sendEmailMessage($setting->notification_email, $emailTemplateAdmin['body'], $emailTemplateAdmin['subject']);

         $this->Flash->success(__('Message submitted to writer successfully'));

        return $this->redirect($this->referer());
    }

    public function submitRatings()
    {
        $this->loadModel('RatingByStudent');
        $ratingByStudent = $this->RatingByStudent->newEntity();
        if ($this->request->is('post')) {
            $ratingByStudent = $this->RatingByStudent->patchEntity($ratingByStudent, $this->request->getData());
            if ($this->RatingByStudent->save($ratingByStudent)) {
                $this->Flash->success(__('The rating has been submitted successfully'));                
            }else{
                $this->Flash->error(__('Please fill all the fields and try again.'));
            }
            return $this->redirect($this->referer());
        }
    }

    public function submittedRatings()
    {
        $this->paginate = [
            'conditions' => [
                'user_id' => $this->Auth->User('id') 
            ], 
            'contain' => ['AssignmentOffers' => [ 'Users' , 'conditions' => [
                'AssignmentOffers.status !=' => 0                
            ] , 'AssignmentDeliverySchedules' ]  , 'RatingByStudent' ]
        ];

       
        
        $ratings = $this->paginate($this->Assignments);
         // pr($ratings); die;
        $this->set(compact('ratings'));
    }

    public function ourRatings()
    {
        $this->paginate = [
            'conditions' => [
                'user_id' => $this->Auth->User('id') 
            ], 
            'contain' => ['AssignmentOffers' => [ 'Users' , 'conditions' => [
                'AssignmentOffers.status !=' => 0                
            ] , 'AssignmentDeliverySchedules' ]  , 'RatingByWriter' ]
        ];

       
        
        $ratings = $this->paginate($this->Assignments);
         // pr($ratings); die;
        $this->set(compact('ratings'));
    }
}
