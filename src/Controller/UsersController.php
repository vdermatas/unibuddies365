<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

use Cake\Mailer\MailerAwareTrait;

use Cake\Mailer\Email;

/**
 * Tours Controller
 *
 * @property \App\Model\Table\ToursTable $Tours
 *
 * @method \App\Model\Entity\Tour[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
	
	public function initialize()
    {
        parent::initialize();
      	$this->Auth->Allow(['login','logout', 'register','forgotPassword','resetPassword']);
    }

    protected function _sendEmailMessage($to = null, $email_body = null, $subject = null, $bcc = null, $cc = null)
    {
        $email = new Email('default');
        $email->setEmailFormat('both');
        $email->setFrom('admin@unibuddies.com');
        $email->setSender('admin@unibuddies.com');
        $email->setTo($to);
        $email->setSubject($subject);
        if ($email->send($email_body)) {
            return true;
        }
        return false;
    }

    /**
     * Login method
     *
     * @return \Cake\Http\Response|void
     */
    public function login()
    {
        // echo Configure::read("APP_BASE_URL");die;
        $this->viewBuilder()->setLayout('frontend/login_layout');
    }

    /**
     * Register method
     *
     * @return \Cake\Http\Response|void
     */
    public function register()
    {
       $this->viewBuilder()->setLayout('frontend/login_layout');
      

    }



    /*This method will return profile of users*/
    public function myProfile()
    {
        $this->viewBuilder()->setLayout('frontend_layout');
        $user = $this->Users->get($this->Auth->User('id'));
        if ($this->request->is(['patch', 'post', 'put'])) {

            
            //Check if image has been uploaded
            if(!empty($this->request->getData()['profile_image']['name']))
            {
                $file = $this->request->getData()['profile_image']; //put the  data into a var for easy use
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
                
                if(in_array($ext, $arr_ext))
                {                    
                    //do the actual uploading of the file. First arg is the tmp name, second arg is
                    //where we are putting it
                    if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'files/uploads' . DS . 'profile_image_'.$file['name']))
                    {
                        //prepare the filename for database entry
                        $this->request->getData()['profile_image'] = 'profile_image_'.$file['name'];
                    }
                }
            }
            
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Your Profile has been saved.'));
            }else{
                $this->Flash->error(__('Your Profile could not be saved. Please, try again.')); 
            }
        }

        $this->set('user',$user);
        $this->set('_serialize', ['user']);
    }

    /*Changing the users Password*/
    public function changePassword()
    {
        $this->viewBuilder()->setLayout('frontend_layout');
        $user =$this->Users->get($this->Auth->user('id'));
        if (!empty($this->request->getData())) {
              // pr($this->request->getData()['old_password']); die;
            $user = $this->Users->patchEntity($user, [
                    'old_password'  => $this->request->getData()['old_password'],
                    'password'      => $this->request->getData()['password1'],
                    'password1'     => $this->request->getData()['password1'],
                    'password2'     => $this->request->getData()['password2']
                ],
                ['validate' => 'password']
            );
                
            /*$flashMsg = '';            
            foreach($user->getErrors() as $k => $v) {
                pr(array_values($v));   
            }*/

            if ($this->Users->save($user)) {
                $this->Flash->success('Your password has been changed successfully');
                $this->redirect(['action' => 'changePassword']);
            } else {
                $this->Flash->error('Old Password Does Not Match');
            }
        }
        $this->set('user',$user);
    }

    /*Forgot Password*/
    public function forgotPassword()
    {
         $this->viewBuilder()->setLayout('frontend/login_layout');
         // echo "test"; die;
         if ($this->request->is(['patch', 'post', 'put'])) {


          $user = $this->Users->find('all')->where(['email' => $this->request->getData()['email']])->first();
          // pr($user); die;
          if(empty($user)){
                 $this->Flash->error('User Not Found , Please Register');
                 $this->redirect(['action' => 'register']);
          }else{
             $reset_token_link = '<a href="http://projects.thedigifrog.com/unibuddies/app/users/reset-password/'.$user->remember_token.'">Click Here</a>';

            //Send email      
            $this->loadModel('EmailTemplates');                
            $temp = $this->EmailTemplates->get(1);  
            $temp['body'] = str_replace(array('#NAME', '#FORGET_PASSWORD_LINK'), array(
                                                $user->first_name." ".$user->last_name,
                                                $reset_token_link
                                                ), $temp['body']
                                              );       

             $this->_sendEmailMessage($user->email, $temp['body'], $temp['subject']);
             $this->Flash->success('Please check your email to get reset password link');
             $this->redirect($this->referer());
          }
          
        }
    }

    public function resetPassword($id = null)
    {
        $this->viewBuilder()->setLayout('frontend/login_layout');
        if ($this->request->is(['patch', 'post', 'put'])) {
            if($id == $this->request->getData()['remember_token']){
                $user = $this->Users->find('all')->where(['remember_token' => $this->request->getData()['remember_token']])->first();
                if(empty($user)){
                    $this->Flash->error('Something went wrong, Please try after sometime');
                }else{

                    if($this->request->getData()['password'] == $this->request->getData()['confirm_password']){
                        $user =$this->Users->get($user->id);
                        $user = $this->Users->patchEntity($user,$this->request->getData());
                        if ($this->Users->save($user)) {
                            $this->Flash->success('Your password has been changed successfully');
                            $this->redirect(['action' => 'login']);
                        } else {
                            $this->Flash->error('Something went wrong, Please try after sometime');
                        }
                    }else{
                        $this->Flash->error('Password and Confirm password not same');
                    }
                }
                
            }else{
                $this->Flash->error('UnAuthorised Hit');
            }
        }
        $this->set('id',$id);
        $this->set('_serialize', ['id']);
    }

}
