<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Mailer\MailerAwareTrait;
use Cake\Routing\Router;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class CmsController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->viewBuilder()->setLayout('frontend/cms_layout');
	    $this->Auth->Allow();
    }


    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function index()
    {
        $this->loadModel('homepageBannerSections');
        $this->loadModel('Users');
        $this->loadModel('Assignments');
        $this->loadModel('HomepageSections');
       
        $title = "Home Page";

        $bannerSection = $this->homepageBannerSections->get(1);
        $whychooseUs = $this->HomepageSections->get(1);
        $service = $this->HomepageSections->get(2);
        $about = $this->HomepageSections->get(3);

        $student = $this->Users->find('all',[
            'conditions'  => ['user_type_id' => 2]
        ])->toArray();

        $writer = $this->Users->find('all',[
            'conditions'  => ['user_type_id' => 3]
        ])->toArray();


        $assignments = $this->Assignments->find('all',[
            'conditions' => ['status IN' => [3,4]]
        ])->toArray();

        $this->set(compact('title','bannerSection','student','writer','assignments','whychooseUs','service','about'));
    }

    public function view($id = null)
    {
       $this->loadModel('CmsPages');
       
       $pages = $this->CmsPages->find('all',[
        'conditions' => ['slug' => $id]
       ])->first();


       $title =  $pages->title_en;
       $this->set(compact('pages','title'));
       // pr($pages); die;
    }
}
