<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Controller\Component;
use Cake\Http\Client;

require WWW_ROOT  . '../vendor/autoload.php';

/**
 * Pages Controller
 *
 * @property \App\Model\Table\PagesTable $Pages
 *
 * @method \App\Model\Entity\Page[] paginate($object = null, array $settings = [])
 */
class PagesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['aboutUs','delivery','contactUs','cmspage','index','becomeSupplier','search']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->viewBuilder()->setLayout('home_layout');
        $this->loadModel('Products');
        $this->loadModel('HomepageSliderImage');
        $this->loadModel('VideoDetail');
        
        $products = $this->Products->find('all', array('limit' => 4,'order' => 'Products.created DESC'))->contain(['Galleries' => ['images']])->toArray();
        $topproducts = $this->Products->find('all', array('limit' => 4,'order' => 'Products.created DESC'))->contain(['Galleries' => ['images']])->where(['is_featured' => 1])->toArray();
        $sliderImages = $this->HomepageSliderImage->find('all')->toArray();

        $videos = $this->VideoDetail->find('all')->contain(['Videos'])->where(['video_id'=>1])->toArray();
        // pr($sliderImages); die;

        $this->set('topproducts', $topproducts);
        $this->set('_serialize', ['topproducts']);
        $this->set('sliderImages', $sliderImages);
        $this->set('_serialize', ['sliderImages']); 
        $this->set('videos', $videos);
        $this->set('_serialize', ['videos']);       
        
        $this->set('products', $products);
        $this->set('_serialize', ['products']);
    }
    
    public function home()
    {
    
    }

    public function contactUs()
    {
        $this->viewBuilder()->setLayout('frontend_layout'); 
        $this->loadModel('Enquiries');
        $customerQuery = $this->Enquiries->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $response = json_decode($this->apiCall());
            if ($response->success == 1 && $response->score >= 0.5) {
                   $customerQuery = $this->Enquiries->patchEntity($customerQuery, $this->request->getData());
                  
                    if ($this->Enquiries->save($customerQuery)) {
                        $this->Flash->success(__('Query Submitted Successfully , we will reach you soon.'));
                        return $this->redirect(['action' => 'contactUs']);
                    }else{
                        $this->Flash->error(__('Something went wrong please try after sometime.'));
                    }
            }else{
                $this->Flash->error(__('Captcha validation error'));
            }               
           
            
            
        }
    }

    public function apiCall()
    {
        $client = new Client($this->_config['httpClientOptions']);
       return $client->post('https://www.google.com/recaptcha/api/siteverify', [
            'secret' =>'6Lco5owUAAAAAAdM98Oij5U-byStTI1kCzJ2bGtf',
            'response' => $this->request->getData()['g-recaptcha-response'],
            'remoteip' => $_SERVER['REMOTE_ADDR']
        ])->getBody();
    }
    
    /**
     * View method
     *
     * @param string|null $id Page id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $page = $this->Pages->get($id, [
            'contain' => ['Categories', 'Sites']
        ]);

        $this->set('page', $page);
        $this->set('_serialize', ['page']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $page = $this->Pages->newEntity();
        if ($this->request->is('post')) {
            $page = $this->Pages->patchEntity($page, $this->request->getData());
            if ($this->Pages->save($page)) {
                $this->Flash->success(__('The page has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The page could not be saved. Please, try again.'));
        }
        $categories = $this->Pages->Categories->find('list', ['limit' => 200]);
        $sites = $this->Pages->Sites->find('list', ['limit' => 200]);
        $this->set(compact('page', 'categories', 'sites'));
        $this->set('_serialize', ['page']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Page id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $page = $this->Pages->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $page = $this->Pages->patchEntity($page, $this->request->getData());
            if ($this->Pages->save($page)) {
                $this->Flash->success(__('The page has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The page could not be saved. Please, try again.'));
        }
        $categories = $this->Pages->Categories->find('list', ['limit' => 200]);
        $sites = $this->Pages->Sites->find('list', ['limit' => 200]);
        $this->set(compact('page', 'categories', 'sites'));
        $this->set('_serialize', ['page']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Page id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $page = $this->Pages->get($id);
        if ($this->Pages->delete($page)) {
            $this->Flash->success(__('The page has been deleted.'));
        } else {
            $this->Flash->error(__('The page could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function cmspage($id){
        $this->viewBuilder()->setLayout('frontend_layout');

        $this->loadModel('CmsPages');
        $this->loadModel('VideoDetail');
// echo $id; die;
        $content = $this->CmsPages->find('all')->where(['slug'=>$id])->first();
// pr($content); die;
        if(!empty($content)){
            $videoId = $content->video_id;
            if($videoId > 0){
                $videos = $this->VideoDetail->find('all')->contain(['Videos'])->where(['video_id'=>$videoId])->toArray();
            }else{
                $videos = [];
            }  
        }else{
            $videos = [];
        }
        
        // pr($videos); die;
        $this->set('videos', $videos);
        $this->set('content', $content);
        $this->set('meta_description', $content['meta_description_en']);
        $this->set('meta_keyword', $content['meta_keyword_en']);
        $this->set('_serialize', ['content']);

    }
    
    public function becomeSupplier(){
        $this->viewBuilder()->setLayout('frontend_layout');
        $this->loadModel('SupplierEnquiries');
        $customerQuery = $this->SupplierEnquiries->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $customerQuery = $this->SupplierEnquiries->patchEntity($customerQuery, $this->request->getData());
            // pr($customerQuery); die;
            if ($this->SupplierEnquiries->save($customerQuery)) {
                $this->Flash->success(__('Query Submitted Successfully , we will reach you soon.'));
                return $this->redirect(['action' => 'becomeSupplier']);
            }else{
                $this->Flash->error(__('Something went wrong please try after sometime.'));
            }
            
        }

    }

    public function search($id = null)
    {
       // echo $search = $this->request->getQuery()('search'); 
        $this->viewBuilder()->setLayout('frontend_layout');
        $this->loadModel('Products');
        
        $cartDetail= $this->request->session()->read('Cart');
        $productCategories = TableRegistry::get('ProductCategories');


        $productCategories = $productCategories->find('all')->contain(['products'])->toArray();



        $paginateArray = [
            'contain' => ['ProductCategories','Galleries' => ['images']],
            'order' => ['Products.created' => 'DESC']
        ];

        if ($this->request->is('get')) {

            $conditions = [];

            if (!empty($this->request->getQuery()['search'])) {
                $conditions[] = array('Products.name_en LIKE' => '%'.$this->request->getQuery()['search'].'%');
                $conditions[] = array('Products.meta_description_en LIKE' => '%'.$this->request->getQuery()['search'].'%');
                $conditions[] = array('Products.meta_content_en LIKE' => '%'.$this->request->getQuery()['search'].'%');
                $conditions[] = array('Products.meta_keyword_en LIKE' => '%'.$this->request->getQuery()['search'].'%');
                $conditions[] = array('Products.slug LIKE' => '%'.$this->request->getQuery()['search'].'%');
                $conditions[] = array('Products.price LIKE' => '%'.$this->request->getQuery()['search'].'%');
                $conditions[] = array('Products.description_en LIKE' => '%'.$this->request->getQuery()['search'].'%');
                $paginateArray['conditions'] = ['OR' => $conditions];
            }
        }
        
        $this->paginate = $paginateArray;
        $product = $this->paginate($this->Products);



        $this->set('product',$product);
        $this->set('_serialize', ['product']);


        // echo $productCount; die;
        $this->set('productCategories',$productCategories);
        $this->set('_serialize', ['productCategories']);
        $this->set('cartDetail',$cartDetail);
        $this->set('_serialize', ['cartDetail']);
     }
    
    
}
