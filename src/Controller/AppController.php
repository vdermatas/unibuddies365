<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\I18n\I18n;
use Cake\Http\Session;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false
        ]);
        $this->loadComponent('Flash');

        $this->loadComponent('Cart');
        $this->loadComponent('Auth', [
            'authorize' => ['Controller'],
            'authenticate' => [
            'Form' => [
                'fields' => ['username' => 'email', 'password' => 'password'],
                ]
            ],
            'authError' => 'Please Login Here',
            'loginRedirect' => [
                'controller' => 'Products',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'storage' => [
                'className' => 'Session',
                'key' => 'Auth.Customer',               
            ],
        ]);


        /*
         * Enable the following components for recommended CakePHP security settings.
         * see http://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */
    public function beforeFilter(Event $event)
    {
        // $this->Auth->allow('login');
        // $this->request->session()->destroy();
        $get_whole_cart = $this->Cart->get_whole_cart();
        // pr($get_whole_cart); die;
        if(!empty($get_whole_cart)){
            if(!empty($get_whole_cart['Products'])){
                $get_count_cart = count($get_whole_cart['Products']);
            }else{
                $get_count_cart = 0;
            }
            
        }else{
            
        }
        $this->loadModel('CmsPages');
        $this->loadModel('HomepageSections');
        $cmsPages = $this->CmsPages->find('all')->toArray();
        $couponText = $this->HomepageSections->find('all')->where(['id' => 3])->first();

        $this->set(compact('get_count_cart','cmsPages','couponText'));
        $this->set('_serialize', ['get_count_cart']);
        $this->set('_serialize', ['cmsPages']);
        $this->set('_serialize', ['couponText']);

        $cart_count = $this->Cart->count_items();
        $this->set(compact('cart_count'));
        $this->set('_serialize', ['cart_count']);

        $this->loadModel('HomepageSections');
        $homePageSections = $this->HomepageSections->find('all')->toArray();
        $this->set('homePageSections', $homePageSections);
        $this->set('_serialize', ['homePageSections']);

        $this->loadModel('FrontendMenus');
        $frontendMenus = $this->FrontendMenus->find('all')->toArray();
        $this->set('frontendMenus', $frontendMenus);
        $this->set('_serialize', ['frontendMenus']);


        $this->loadModel('socialMedias');
        $socialmedialinks = $this->socialMedias->get(1);
        $this->set('socialmedialinks', $socialmedialinks);
        $this->set('_serialize', ['socialmedialinks']);


        $this->loadModel('BackgroundImages');
        $bg_images = $this->BackgroundImages->find('all')->toArray();
        $this->set('bg_images', $bg_images);
        $this->set('_serialize', ['bg_images']);


        $session = New Session();
        //code to switch to between languages
        if (null !== $this->request->getQuery('lang') || $session->check('Config.language')) {
            if (null !== $this->request->getQuery('lang')) {
                $sessionLangName = ($this->request->getQuery('lang') == 'en_EN') ? 'en' : 'gre';
                $session->write('Config.language', $sessionLangName);
            }

            I18n::setLocale(Configure::read('Languages.' . $session->read('Config.language')));
        } else {
            $session->write('Config.language', 'en');
        }


    // I18n::setLocale('en_GRE');

        

    }

    

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */
    public function beforeRender(Event $event)
    {

		if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }

    }

    public function isAuthorized($user) {         
       return true;            
    }
}
