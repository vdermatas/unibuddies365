<?php
namespace App\Controller\Component;

use Cake\Controller\Component;

class CartComponent extends Component
{
    public function initialize(array $config) 
    {
        parent::initialize($config);
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function get_whole_cart()
    {   
        $cartItems = $this->request->getSession()->read('Cart');
        if(!empty($cartItems)) {
            return $cartItems;
        }
    }
    

	/**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function exist_tour_incart($id)
    {   
        //$this->request->session()->destroy();
        $cartItems = $this->request->session()->read('Cart');
        if ($id){
            if(!empty($cartItems)) {
				if(isset($cartItems['Tours'])) {
					if (array_key_exists($id, $cartItems['Tours'])) {
						return $cartItems['Tours'][$id];
					}
				}
            }
		}
    }
	
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add_tour($data)
    {   
		//$this->request->session()->destroy();
        $cartItems = $this->request->session()->read('Cart.Tours');
        if(empty($cartItems)) {
           $arrStore = array($data['tour_id'] => $data);
           $this->request->session()->write('Cart.Tours', $arrStore);
        }else{
            if(isset($cartItems)) {
                if(!empty($cartItems)) {
                    if (array_key_exists($data['tour_id'], $cartItems)) {

                        unset($cartItems[$data['tour_id']]);
                        
                        $cartItems[$data['tour_id']]['adults'] = $data['adults'];
                        
                        $cartItems[$data['tour_id']]['childs'] = $data['childs'];

                        $cartItems[$data['tour_id']]['booking_date'] = $data['booking_date'];

                        $cartItems[$data['tour_id']]['pick_up_location'] = $data['pick_up_location'];
        				
                        $this->request->session()->write('Cart.Tours', $cartItems);

                    } else {

                        $cartItems[$data['tour_id']] = $data;
                        $this->request->session()->write('Cart.Tours', $cartItems);

                    }
                }
            }
        }
    }

     /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add_product($data)
    {   
		
        $cartItems = $this->request->session()->read('Cart.Products');
        // $this->request->session()->destroy();
        // pr($this->request->session()->read('Cart.Products')); die;
        // pr( $cartItems); die;

        if(empty($cartItems)) {
           $arrStore = array($data['product_id'] => $data);
           $this->request->session()->write('Cart.Products', $arrStore);
        }else{
            if (array_key_exists($data['product_id'], $cartItems)) {
                
                unset($cartItems[$data['product_id']]);
                $cartItems[$data['product_id']] = $data;
                
                $this->request->session()->write('Cart.Products', $cartItems);
            } else { 
                $cartItems[$data['product_id']] = $data;
                
                $this->request->session()->write('Cart.Products', $cartItems);
            }
        }
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function count_items()
    {   
        // $this->request->session()->destroy();
        $cartItems = $this->request->getSession()->read('Cart');
        // pr(count($cartItems['Products'])); die;
		if(!empty($cartItems['Products'])) {
            return count($cartItems['Products']);
        }else{
            return 0;
        }
        // return $cartItems[];
    }
	
	 /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
   public function remove_item($data)
    {  
        $cartItems = $this->request->session()->read('Cart');
        if(!empty($cartItems)) {
            unset($cartItems['Products'][$data['product_id']]);
            $this->request->session()->write('Cart', $cartItems);
        }
        return true;
    }

}
