<?php
namespace App\Controller\Component;
use Cake\Controller\Component;
use Cake\Mailer\Email;

class EmailerComponent extends Component
{
    public function initialize(array $config) 
    {
        parent::initialize($config);
    }

   
    public function send_email($data, $email_id)
    { 

            $email = new Email('default');
            $email->from(['info@bestshuttlecancun.com' => 'BestShuttleCancun'])
                ->emailFormat('both')
                ->to($email_id)
                ->subject($data['subject']) 
                ->send($data['body']);
    }

    public function generateTrTdtour($data) {

        $html = '';

        foreach($data as $key => $value) {

             $html.='<tr>
                <td style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt; padding:8px; border:1px solid #000;">'.$value['billing_detail']['first_name'].' '.$value['billing_detail']['last_name'].'</td>
                <td style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt; padding:8px; border:1px solid #000;">'.$value['tour']['name_en'].'</td>
                <td style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt; padding:8px; border:1px solid #000;">'.$value['adults'].'</td>
                <td style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt; padding:8px; border:1px solid #000;">'.$value['childs'].'</td>
                <td style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt; padding:8px; border:1px solid #000;">'.date("F j, Y g:i a",strtotime($value['tour_booking_date'])).'</td>
                <td style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt; padding:8px; border:1px solid #000;">'.$value['pick_up_location'].'</td>

                <td style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt; padding:8px; border:1px solid #000;">'.date("F j, Y g:i a",strtotime($value['created'])).'</td>

            </tr>';
        }
        return $html;
    }
   
    public function tour_detail_email_html($tour) {
        $htmlTours = $this->generateTrTdtour($tour);
        $templatePart = ' <table style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;width:100%;margin-top:10px;">
                                <tr>
                                  <th>Name</th>
                                  <th>Tour</th>
                                  <th>Adult</th>
                                  <th>Child</th>
                                  <th>Booking date</th>
                                  <th>Pick up place</th>
                                  <th>Booked at</th>                                  
                                </tr>'.$htmlTours.'</tbody></table>';
        return $templatePart;       
    }
    
    public function generateTrTdShuttle($data) {

        $html = '';

        foreach($data as $key => $value) {

            $trip = $value['is_round_trip'] == 1?'Round':'Single';

            $pickup = $value['is_airport_pickup'] == 1?'Airport '.$value['pick_up_location']:$value['pick_up_location'];

            $html.='<tr align="center">
                <td style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt; padding:8px; border:1px solid #000;">'.$value['billing_detail']['first_name'].' '.$value['billing_detail']['last_name'].'</td>

                <td style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt; padding:8px; border:1px solid #000;">'.$trip.'</td>

                <td style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt; padding:8px; border:1px solid #000;">'.$pickup.'</td>

                <td style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt; padding:8px; border:1px solid #000;">'.$value['drop_of_location']['location_en'].'</td>


                <td style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt; padding:8px; border:1px solid #000;">'.date("g:i a",strtotime($value['arrival'])).'</td>

                <td style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt; padding:8px; border:1px solid #000;">'.date("F j, Y",strtotime($value['arrival'])).'</td>

                <td style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt; padding:8px; border:1px solid #000;">'.str_replace('_', '-', $value['travellers']).'</td>
               
            </tr>';

        }

        return $html;
    }
   
    public function shuttle_detail_email_html($tour) {
        $htmlTours = $this->generateTrTdShuttle($tour);
        $templatePart = ' <table style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;width:100%;margin-top:10px;">
                                <tr>
                                  <th style="padding:8px; border:1px solid #000;">Name</th>
                                  <th style="padding:8px; border:1px solid #000;">Type</th>
                                  <th style="padding:8px; border:1px solid #000;">Pick up place</th>
                                  <th style="padding:8px; border:1px solid #000;">Destination</th>
                                  <th style="padding:8px; border:1px solid #000;">Pick up time</th>                    
                                  <th style="padding:8px; border:1px solid #000;">Date</th>                    
                                  <th style="padding:8px; border:1px solid #000;">Pax</th>                                   
                                </tr>'.$htmlTours.'</tbody></table>';
        return $templatePart;       
        
    }
}

