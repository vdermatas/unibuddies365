<?php
namespace App\Controller\Writer;

use App\Controller\Writer\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Mailer\MailerAwareTrait;
use Cake\Mailer\Email;

/**
 * Assignments Controller
 *
 *
 * @method \App\Model\Entity\Assignment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AssignmentsController extends AppController
{

     public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->viewBuilder()->setLayout('writer/dashboard');

        if($this->Auth->User('status') == 0){

             $this->Flash->error(__('Your account has not approved or you will be blocked by Admin , please contact administration'));

            return $this->redirect($this->Auth->redirectUrl());

        }
    }


    protected function _sendEmailMessage($to = null, $email_body = null, $subject = null, $bcc = null, $cc = null)
    {
        $email = new Email('default');
        $email->setEmailFormat('both');
        $email->setFrom('admin@unibuddies.com');
        $email->setSender('admin@unibuddies.com');
        $email->setTo($to);
        $email->setSubject($subject);
        if ($email->send($email_body)) {
            return true;
        }
        return false;
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function browseAssignments()
    {
        $this->loadModel('Assignments');

        $conditions[] = ['status' => 1];

        if ($this->request->is('post')) {

            $conditions = [
                    'document_type' => $this->request->getData()['document_type'],
                    'thematic_unity' => $this->request->getData()['thematic_unity'],
                    'language' => $this->request->getData()['language'],
                    'title LIKE' => '%'.$this->request->getData()['title'].'%',
                    'status' => 1
                ];

            

        }


        $this->paginate = [
            'conditions' => $conditions , 
            'contain' => 'AssignmentOffers'
        ];
        
        $assignments = $this->paginate($this->Assignments);
        
        $this->set(compact('assignments'));

        
        
    }

     /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function ongoingAssignments()
    {

        $this->loadModel('AssignmentDeliverySchedules');
        $this->loadModel('AssignmentOffers');
        $this->loadModel('EmailTemplates'); 
        $this->loadModel('Settings'); 
        $emailTemplateAdmin = $this->EmailTemplates->get(5);
        $setting = $this->Settings->get(1);

        $this->paginate = [
            'conditions' => ['AssignmentOffers.status !=' => 0 , 'AssignmentOffers.user_id' => $this->Auth->User('id') ],
            'contain' => ['Assignments' => ['Users' ,  'conditions' => ['Assignments.status' => 2] ] , 'AssignmentDeliverySchedules']
        ];
        
        $assignments = $this->paginate($this->AssignmentOffers);

        if ($this->request->is('post')) {

            $assignmentDelivery = $this->AssignmentDeliverySchedules->newEntity();

            $assignmentDelivery = $this->AssignmentDeliverySchedules->patchEntity($assignmentDelivery, $this->request->getData());

            $assignment = $this->AssignmentOffers->find('all',[
                'conditions' => ['AssignmentOffers.id' => $this->request->getData()['assignment_offer_id']],
                'contain' => ['Assignments']
            ])->first();


            // pr($assignment); die;


            if(!empty($assignmentDelivery->getErrors())){
                foreach ($assignmentDelivery->getErrors() as $key => $value) {
                    foreach ($value as $key => $values) {
                        $this->Flash->error(__($values));
                    }
                }
            }else{

                if(move_uploaded_file($this->request->getData()['file']['tmp_name'], WWW_ROOT . 'uploads/student-assignment' . DS . 'attachment_'.$this->Auth->User('id')."-".date('Y-m-d-H-i-s')."_".$this->request->getData()['file']['name']))
                {
                   
                    $assignmentDelivery->file = 'attachment_'.$this->Auth->User('id')."-".date('Y-m-d-H-i-s')."_".$this->request->getData()['file']['name'];
                }

                if ($this->AssignmentDeliverySchedules->save($assignmentDelivery)) {

                    $emailTemplateAdmin['body'] = str_replace(
                        array('#DETAILS'), array(
                              "Delivery schedule has been added by writer for this assignment : ".$assignment['assignment']['title']
                        ), $emailTemplateAdmin['body']
                      ); 

                    $this->_sendEmailMessage($setting->notification_email, $emailTemplateAdmin['body'], $emailTemplateAdmin['subject']);


                    $this->Flash->success(__('The assignment delivery schedule has been saved.'));

                    return $this->redirect($this->referer());

                }else{  
                 
                    $this->Flash->error(__('The assignment delivery schedule could not be saved. Please, try again.'));

                }
            }
        }


        $this->set(compact('assignments'));

        // pr($assignments); die;
    }
     /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function pastAssignments()
    {

        $this->loadModel('AssignmentDeliverySchedules');
        $this->loadModel('AssignmentOffers');

        $this->paginate = [
            'conditions' => ['AssignmentOffers.status IN' => [2,3] , 'AssignmentOffers.user_id' => $this->Auth->User('id') ],
            'contain' => ['Assignments' => ['Users' ,  'conditions' => ['Assignments.status IN' => [3,4]]  , 'RatingByWriter' , 'RatingByStudent' ] , 'AssignmentDeliverySchedules']
        ];
        
        $assignments = $this->paginate($this->AssignmentOffers);
// pr($assignments); die;
         $this->set(compact('assignments'));
    }

    /**
     * View method
     *
     * @param string|null $id Assignment id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->loadModel('AssignmentOffers');
        $this->loadModel('EmailTemplates');

        $assignment = $this->Assignments->get($id, [
            'contain' => [
                'AssignmentOffers' => [
                    'Users' , 'AssignmentDeliverySchedules' , 'conditions' => [

                        'AssignmentOffers.status IN' => [2,3]

                    ]
                ] , 'Users'
            ]
        ]);

        // pr($assignment); die;

        $assignmentOfferData = $this->AssignmentOffers->find('all',[
            'conditions' => [
                'user_id' => $this->Auth->User('id'),
                'assignment_id' => $id
            ]
        ])->first();

        if ($this->request->is(['patch', 'post', 'put'])) {

            if(!empty($assignmentOfferData)){

                $assignmentOffer = $this->AssignmentOffers->get($assignmentOfferData['id'], [
                    'contain' => []
                ]);

                $assignmentOffer = $this->AssignmentOffers->patchEntity($assignmentOffer, $this->request->getData());

                if ($this->AssignmentOffers->save($assignmentOffer)) {

                    $this->Flash->success(__('The assignment offer has been saved.'));

                }else{  
                 
                    $this->Flash->error(__('The assignment offer could not be saved. Please, try again.'));

                }

            }else{

                $assignmentOffer = $this->AssignmentOffers->newEntity();

                $assignmentOffer = $this->AssignmentOffers->patchEntity($assignmentOffer, $this->request->getData());

                $assignmentOffer->user_id = $this->Auth->User('id');

                $assignmentOffer->assignment_id = $id;
                // pr($assignmentOffer); die;

                if ($this->AssignmentOffers->save($assignmentOffer)) {

                 $emailTemplate = $this->EmailTemplates->get(7);
                 
                 $emailTemplate['body'] = str_replace(
                                array('#DETAILS'), array(
                                      "One of our writer has been submitted their offer to your assignment , Assignment Title : ".$assignment['title']
                                ), $emailTemplate['body']
                              ); 

                $this->_sendEmailMessage($assignment['user']['email'], $emailTemplate['body'], $emailTemplate['subject']);


                    $this->Flash->success(__('The assignment offer has been saved.'));

                }else{
                    
                    $this->Flash->error(__('The assignment offer could not be saved. Please, try again.'));

                }

            }

            return $this->redirect($this->referer());

        }

        $this->set('assignment', $assignment);
        $this->set('assignmentOfferData', $assignmentOfferData);

        // pr($assignment); die;

    }


    public function getDeliverySchedules()
    {
        $this->loadModel('AssignmentDeliverySchedules');

        $schedules = $this->AssignmentDeliverySchedules->find('all',[
                'conditions' => ['assignment_offer_id' => $this->request->getData()['offer_id'] ]
        ])->toArray();

        // pr($schedules); die;

        echo json_encode(['data' => $schedules]);
        exit;
    }

    public function submitAssignment()
    {
        $this->loadModel('AssignmentOffers');        
        $this->loadModel('EmailTemplates'); 
        $this->loadModel('Settings'); 
        $emailTemplateAdmin = $this->EmailTemplates->get(5);
        $setting = $this->Settings->get(1);
                   

        // pr($this->request->getData()['offer_id']); die;

        $offer = $this->AssignmentOffers->get($this->request->getData()['offer_id'],[
            'contain' => ['Assignments']
        ]);

        // pr($offer); die;

        $offer->status = 2 ;

        $this->AssignmentOffers->save($offer);

         $emailTemplateAdmin['body'] = str_replace(
                        array('#DETAILS'), array(
                              "Assignment has been completed from writers end and submitted to student , Assignment Title : ".$offer['assignment']['title']
                        ), $emailTemplateAdmin['body']
                      ); 

        $this->_sendEmailMessage($setting->notification_email, $emailTemplateAdmin['body'], $emailTemplateAdmin['subject']);


        $this->Flash->success(__('The assignment has been submitted to student'));

        return $this->redirect($this->referer());


    }

    public function submitRatings()
    {
        $this->loadModel('RatingByWriter');
        $ratingByStudent = $this->RatingByWriter->newEntity();
        if ($this->request->is('post')) {
            $ratingByStudent = $this->RatingByWriter->patchEntity($ratingByStudent, $this->request->getData());
            if ($this->RatingByWriter->save($ratingByStudent)) {
                $this->Flash->success(__('The rating has been submitted successfully'));                
            }else{
                $this->Flash->error(__('Please fill all the fields and try again.'));
            }
            return $this->redirect($this->referer());
        }
    }

    public function submittedRatings()
    {
        $this->loadModel('AssignmentOffers');

        $this->paginate = [
            'conditions' => [
                'AssignmentOffers.status IN' => [2, 3] ,
                'AssignmentOffers.user_id' => $this->Auth->User('id')
            ], 
            'contain' => ['Assignments' => [ 'Users' , 'RatingByWriter'  ] ] 
        ];

       
        
        $ratings = $this->paginate($this->AssignmentOffers);
         // pr($ratings); die;
        $this->set(compact('ratings'));
    }

    public function ourRatings()
    {
        $this->loadModel('AssignmentOffers');

        $this->paginate = [
            'conditions' => [
                'AssignmentOffers.status IN' => [2, 3] ,
                'AssignmentOffers.user_id' => $this->Auth->User('id')
            ], 
            'contain' => ['Assignments' => [ 'Users' , 'RatingByStudent'  ] ] 
        ];

       
        
        $ratings = $this->paginate($this->AssignmentOffers);
         // pr($ratings); die;
        $this->set(compact('ratings'));
    }



}
