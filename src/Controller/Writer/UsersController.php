<?php
namespace App\Controller\Writer;

use App\Controller\Writer\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Mailer\MailerAwareTrait;
use Cake\Mailer\Email;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

     public function beforeFilter(Event $event)
    {

        parent::beforeFilter($event);
        $this->Auth->allow(['writerRegister','dashboard']);

        // echo $this->Auth->User('status'); die;
    }

    protected function _sendEmailMessage($to = null, $email_body = null, $subject = null, $bcc = null, $cc = null) {

        $email = new Email('default');

        $email->setEmailFormat('both');

        $email->setFrom('admin@unibuddies.com');

        $email->setSender('admin@unibuddies.com');

        $email->setTo($to);

        $email->setSubject($subject);

        if ($email->send($email_body)) {

            return true;

        }

        return false;

    }
    
    public function writerRegister()
    { 
        $this->loadModel('writerProfiles');
        $this->loadModel('EmailTemplates');
        $this->loadModel('Settings');
        $settings = $this->Settings->get(1);

        $users = $this->Users->newEntity();     
        $profiles = $this->writerProfiles->newEntity();  
       
        if($this->request->is('post')) {

            // pr($settings->notification_email); die;


            $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
            $remember_token = substr(str_shuffle(str_repeat($pool, 64)), 0,64);
            $user = $this->Users->patchEntity($users, $this->request->getData());
            $writerProfile = $this->writerProfiles->patchEntity($profiles, $this->request->getData());

            $user['user_type_id'] = 3;
            $user['status'] = 0;
            $user['remember_token'] = $remember_token;
            $user['username'] = $this->request->getData()['email'];


            if(!empty($user->getErrors()) || !empty($writerProfile->getErrors())){
                foreach ($user->getErrors() as $key => $value) {
                    foreach ($value as $key => $values) {
                       // $error.= $values."<br>";
                        $this->Flash->error(__($values));
                    }
                }

                foreach ($writerProfile->getErrors() as $keys => $Pvalue) {
                    foreach ($Pvalue as $keys => $Pvalues) {
                       // $error.= $values."<br>";
                        $this->Flash->error(__($Pvalues));
                    }
                }
            }else{
                if ($result = $this->Users->save($user)) {

                    $emailTemplate = $this->EmailTemplates->get(3);
                    $emailTemplateAdmin = $this->EmailTemplates->get(5);


                    $emailTemplate['body'] = str_replace(
                        array('#NAME'), array(
                              $user->first_name." ".$user->last_name
                        ), $emailTemplate['body']
                      ); 


                    $emailTemplate['body'] = str_replace(
                        array('#DETAILS'), array(
                              "<br> <br><b>User Name</b> : ".$user->username." <br> <b>Password</b> : ".$this->request->getData()['password']
                        ), $emailTemplate['body']
                      );


                      $emailTemplateAdmin['body'] = str_replace(
                        array('#DETAILS'), array(
                              "You have new user registration <br> <br><b>Writer Name</b> : ".$user->username." <br> "
                        ), $emailTemplateAdmin['body']
                      );  

                    $this->_sendEmailMessage($this->request->getData()['email'], $emailTemplate['body'], $emailTemplate['subject']); 
                    $this->_sendEmailMessage($settings->notification_email, $emailTemplateAdmin['body'], $emailTemplateAdmin['subject']); 


                    if(move_uploaded_file($this->request->getData()['resume']['tmp_name'], WWW_ROOT . 'uploads/resumes' . DS . 'resume_'.$this->request->getData()['mobile']."_".$this->request->getData()['resume']['name']))
                        {
                            $writerProfileData = $this->writerProfiles->newEntity();
                            $writerProfileData->resume = 'resume_'.$this->request->getData()['mobile']."_".$this->request->getData()['resume']['name'];
                            $writerProfileData->user_id = $result['id'];
                            $writerProfileData->experience = $this->request->getData()['experience'];
                            $writerProfileData->about_me = $this->request->getData()['about_me'];
                            $this->writerProfiles->save($writerProfileData);

                        }


                    // Retrieve user from DB
                    $authUser = $this->Users->get($result->id)->toArray();

                    // Log user in using Auth
                    $this->Auth->setUser($authUser);
                    $this->Flash->success(__('User registered successfully.'));

                    return $this->redirect($this->Auth->redirectUrl());

                    
                }else{
                    $this->Flash->error(__('Error occured while registering user. Please try again.'));

                    return $this->redirect($this->referer());
                }
            }
        }
        return $this->redirect($this->referer());
        $this->set(compact('users'));
       $this->set('_serialize', ['users']);
    }


    public function login()
    {
        if ($this->request->is('post')) {

            $user = $this->Auth->identify();

            if($user) {
                if($user['user_type_id'] != 3 ){
                    $this->Flash->error(__('Writers are allowed to login '));
                    return $this->redirect($this->referer());
                }else{
                    $this->Auth->setUser($user);
                    $this->Flash->success(__('Logged in successfully'));
                    return $this->redirect($this->Auth->redirectUrl());
                }
            }else{
                $this->Flash->error(__('Invalid User Name and Password'));
                return $this->redirect($this->referer());
            }
        }
    }


    public function changePassword()
    {
        $this->viewBuilder()->setLayout('writer/dashboard');
        $user =$this->Users->get($this->Auth->user('id'));
        if (!empty($this->request->getData())) {
              // pr($this->request->getData()['old_password']); die;
            $user = $this->Users->patchEntity($user, [
                    'old_password'  => $this->request->getData()['old_password'],
                    'password'      => $this->request->getData()['password'],
                    'confirm_password'     => $this->request->getData()['confirm_password']
                ],
                ['validate' => 'password']
            );
            $error = '';
            if(!empty($user->getErrors())){
                foreach ($user->getErrors() as $key => $value) {
                    foreach ($value as $key => $values) {
                       // $error.= $values."<br>";
                        $this->Flash->error(__($values));
                    }
                }
            }else{
                if ($this->Users->save($user)) {
                    $this->Flash->success('Your password has been changed successfully');
                    $this->redirect(['action' => 'changePassword']);
                } else {
                    $this->Flash->error('Old Password Does Not Match');
                }
            }
        }
        $this->set('user',$user);
    }

    public function dashboard()
    {
        $this->viewBuilder()->setLayout('writer/dashboard');

        $user = $this->Users->get($this->Auth->User('id'),[
            'contain' => [ 'WriterProfiles' => ['WriterSkills','WriterDocuments','WriterThematics'] ]
        ]);

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);

        // pr($user); die;
        
    }

     public function logout()
    {
          $this->Flash->success('You have successfully loged out'); 
         $this->Auth->logout();
         return $this->redirect('/users/login');
     }


     public function myProfile()
     {
        $this->viewBuilder()->setLayout('writer/dashboard');

        $this->loadModel('WriterProfiles');
        $this->loadModel('WriterSkills');
        $this->loadModel('WriterDocuments');
        $this->loadModel('WriterThematics');

        $user = $this->Users->get($this->Auth->User('id'),[
            'contain' => [ 'WriterProfiles' => ['WriterSkills','WriterDocuments','WriterThematics'] ]
        ]);
// pr($user); die;
        $writerProfile = $this->WriterProfiles->get($user['writer_profile']['id']);

        if ($this->request->is(['patch', 'post', 'put'])) {
 

            $Pimage = $user->profile_image;
            $resume = $user->writer_profile['resume'];
            $certificate = $user->writer_profile['certificate'];

            $user = $this->Users->patchEntity($user, $this->request->getData() , [
                 // 'associated' => ['WriterProfiles'] , 'validate' => true 
            ] );

            $writerProfile = $this->WriterProfiles->patchEntity($writerProfile, $this->request->getData());
       
            if(!empty($user->getErrors()) || !empty($writerProfile->getErrors())  ){
                foreach ($user->getErrors() as $key => $value) {
                    foreach ($value as $key => $values) {
                       // $error.= $values."<br>";
                        $this->Flash->error(__($values));
                    }
                }
                foreach ($writerProfile->getErrors() as $keys => $Pvalue) {
                    foreach ($Pvalue as $keys => $Pvalues) {
                       // $error.= $values."<br>";
                        $this->Flash->error(__($Pvalues));
                    }
                }
            }else{

                if( !empty($this->request->getData()['profile_image']['name']) ){

                    if(move_uploaded_file($this->request->getData()['profile_image']['tmp_name'], WWW_ROOT . 'uploads/writerProfile' . DS . 'profile_'.$this->request->getData()['mobile']."_".$this->request->getData()['profile_image']['name']))
                    {
                        $user->profile_image = 'profile_'.$this->request->getData()['mobile']."_".$this->request->getData()['profile_image']['name'];
                    }

                }else{
                     $user->profile_image = $Pimage;
                }

                if( !empty($this->request->getData()['resume']['name']) ){

                    if(move_uploaded_file($this->request->getData()['resume']['tmp_name'], WWW_ROOT . 'uploads/resumes' . DS . 'resume_'.$this->request->getData()['mobile']."_".$this->request->getData()['resume']['name']))
                    {
                        $writerProfile->resume = 'resume_'.$this->request->getData()['mobile']."_".$this->request->getData()['resume']['name'];
                    }

                }else{
                     $writerProfile->resume = $resume;
                }

                if( !empty($this->request->getData()['certificate']['name']) ){

                    if(move_uploaded_file($this->request->getData()['certificate']['tmp_name'], WWW_ROOT . 'uploads/resumes' . DS . 'certificate_'.$this->request->getData()['mobile']."_".$this->request->getData()['certificate']['name']))
                    {
                        $writerProfile->certificate = 'certificate_'.$this->request->getData()['mobile']."_".$this->request->getData()['certificate']['name'];
                    }

                }else{
                     $writerProfile->certificate = $certificate;
                }

                
                if ($this->Users->save($user)) {

                    $this->WriterProfiles->save($writerProfile);

                    $this->WriterSkills->deleteAll(['writer_profile_id' => $user['writer_profile']['id']]);

                    $this->WriterDocuments->deleteAll(['writer_profile_id' => $user['writer_profile']['id']]);

                    $this->WriterThematics->deleteAll(['writer_profile_id' => $user['writer_profile']['id']]);


                    if(!empty($this->request->getData()['languages'])){

                        foreach ($this->request->getData()['languages'] as $keyCat => $language) {

                            $writerSkillData = $this->WriterSkills->newEntity();
                            $writerSkillData->languages = $language;
                            $writerSkillData->writer_profile_id = $user['writer_profile']['id'];
                            $this->WriterSkills->save($writerSkillData);
                       }

                    }

                    if(!empty($this->request->getData()['document_type'])){

                        foreach ($this->request->getData()['document_type'] as $keyCat => $document) {

                            $writerDocumentData = $this->WriterDocuments->newEntity();
                            $writerDocumentData->document_type = $document;
                            $writerDocumentData->writer_profile_id = $user['writer_profile']['id'];
                            $this->WriterDocuments->save($writerDocumentData);
                       }

                    }


                    if(!empty($this->request->getData()['thematic_type'])){

                        foreach ($this->request->getData()['thematic_type'] as $keyCat => $thematic) {

                            $writerThematicData = $this->WriterThematics->newEntity();
                            $writerThematicData->thematic_type = $thematic;
                            $writerThematicData->writer_profile_id = $user['writer_profile']['id'];
                            $this->WriterThematics->save($writerThematicData);
                       }

                    }

                    $this->Auth->setUser($user);

                    $this->Flash->success(__('Profile has been saved successfully'));               

                }else{

                    $this->Flash->error(__('Something went wrong , Please try again later .'));
                }
            }

            return $this->redirect($this->referer());
        }

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);

         
     }
}
