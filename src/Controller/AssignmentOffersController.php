<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AssignmentOffers Controller
 *
 * @property \App\Model\Table\AssignmentOffersTable $AssignmentOffers
 *
 * @method \App\Model\Entity\AssignmentOffer[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AssignmentOffersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Assignments', 'Users']
        ];
        $assignmentOffers = $this->paginate($this->AssignmentOffers);

        $this->set(compact('assignmentOffers'));
    }

    /**
     * View method
     *
     * @param string|null $id Assignment Offer id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $assignmentOffer = $this->AssignmentOffers->get($id, [
            'contain' => ['Assignments', 'Users']
        ]);

        $this->set('assignmentOffer', $assignmentOffer);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $assignmentOffer = $this->AssignmentOffers->newEntity();
        if ($this->request->is('post')) {
            $assignmentOffer = $this->AssignmentOffers->patchEntity($assignmentOffer, $this->request->getData());
            if ($this->AssignmentOffers->save($assignmentOffer)) {
                $this->Flash->success(__('The assignment offer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The assignment offer could not be saved. Please, try again.'));
        }
        $assignments = $this->AssignmentOffers->Assignments->find('list', ['limit' => 200]);
        $users = $this->AssignmentOffers->Users->find('list', ['limit' => 200]);
        $this->set(compact('assignmentOffer', 'assignments', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Assignment Offer id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $assignmentOffer = $this->AssignmentOffers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $assignmentOffer = $this->AssignmentOffers->patchEntity($assignmentOffer, $this->request->getData());
            if ($this->AssignmentOffers->save($assignmentOffer)) {
                $this->Flash->success(__('The assignment offer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The assignment offer could not be saved. Please, try again.'));
        }
        $assignments = $this->AssignmentOffers->Assignments->find('list', ['limit' => 200]);
        $users = $this->AssignmentOffers->Users->find('list', ['limit' => 200]);
        $this->set(compact('assignmentOffer', 'assignments', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Assignment Offer id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $assignmentOffer = $this->AssignmentOffers->get($id);
        if ($this->AssignmentOffers->delete($assignmentOffer)) {
            $this->Flash->success(__('The assignment offer has been deleted.'));
        } else {
            $this->Flash->error(__('The assignment offer could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
