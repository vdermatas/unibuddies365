<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * HomepageBannerSections Controller
 *
 * @property \App\Model\Table\HomepageBannerSectionsTable $HomepageBannerSections
 *
 * @method \App\Model\Entity\HomepageBannerSection[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HomepageBannerSectionsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $homepageBannerSections = $this->HomepageBannerSections->get(1);


        if ($this->request->is(['patch', 'post', 'put'])) {
            $bgImage = $homepageBannerSections->background_image;

            $homepageBannerSections = $this->HomepageBannerSections->patchEntity($homepageBannerSections, $this->request->getData());

            if(!empty($homepageBannerSections->getErrors())){
                foreach ($homepageBannerSections->getErrors() as $key => $value) {
                    foreach ($value as $key => $values) {
                        $this->Flash->error(__($values));
                    }
                }
            }else{               

                // $cmsPage['background_image'] = $cmsPage->background_image;

                if(!empty($this->request->getData()['background_image']['name']))
                {
                    $file = $this->request->getData()['background_image']; 
                    if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'images' . DS . 'background'.$file['name']))
                    {
                        //prepare the filename for database entry
                        $homepageBannerSections['background_image'] = 'images/background'.$file['name'];
                    }
                }else{
                    $homepageBannerSections['background_image'] = $bgImage;
                }
                
                if ($this->HomepageBannerSections->save($homepageBannerSections)) {
                    $this->Flash->success(__('The homepage banner section has been saved.'));

                    return $this->redirect(['action' => 'index']);
                }else{
                    $this->Flash->error(__('The homepage banner section could not be saved. Please, try again.'));
                }
            }
        }


        $this->set(compact('homepageBannerSections'));
    }

}
