<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * NavigationMenus Controller
 *
 * @property \App\Model\Table\NavigationMenusTable $NavigationMenus
 *
 * @method \App\Model\Entity\NavigationMenu[] paginate($object = null, array $settings = [])
 */
class NavigationMenusController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ParentNavigationMenus']
        ];
        $navigationMenus = $this->paginate($this->NavigationMenus);

        $this->set(compact('navigationMenus'));
        $this->set('_serialize', ['navigationMenus']);
    }

    /**
     * View method
     *
     * @param string|null $id Navigation Menu id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $navigationMenu = $this->NavigationMenus->get($id, [
            'contain' => ['ParentNavigationMenus', 'ChildNavigationMenus']
        ]);

        $this->set('navigationMenu', $navigationMenu);
        $this->set('_serialize', ['navigationMenu']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $navigationMenu = $this->NavigationMenus->newEntity();
        if ($this->request->is('post')) {
            $navigationMenu = $this->NavigationMenus->patchEntity($navigationMenu, $this->request->getData());
            if ($this->NavigationMenus->save($navigationMenu)) {
                $this->Flash->success(__('The navigation menu has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The navigation menu could not be saved. Please, try again.'));
        }
        $parentNavigationMenus = $this->NavigationMenus->ParentNavigationMenus->find('list', ['limit' => 200]);
        $this->set(compact('navigationMenu', 'parentNavigationMenus'));
        $this->set('_serialize', ['navigationMenu']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Navigation Menu id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $navigationMenu = $this->NavigationMenus->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $navigationMenu = $this->NavigationMenus->patchEntity($navigationMenu, $this->request->getData());
            if ($this->NavigationMenus->save($navigationMenu)) {
                $this->Flash->success(__('The navigation menu has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The navigation menu could not be saved. Please, try again.'));
        }
        $parentNavigationMenus = $this->NavigationMenus->ParentNavigationMenus->find('list', ['limit' => 200]);
        $this->set(compact('navigationMenu', 'parentNavigationMenus'));
        $this->set('_serialize', ['navigationMenu']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Navigation Menu id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $navigationMenu = $this->NavigationMenus->get($id);
        if ($this->NavigationMenus->delete($navigationMenu)) {
            $this->Flash->success(__('The navigation menu has been deleted.'));
        } else {
            $this->Flash->error(__('The navigation menu could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
