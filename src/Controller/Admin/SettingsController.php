<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Settings Controller
 *
 * @property \App\Model\Table\SettingsTable $Settings
 *
 * @method \App\Model\Entity\Setting[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SettingsController extends AppController
{
    

    /**
     * View method
     *
     * @param string|null $id Setting id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function index($id = null)
    {
        $setting = $this->Settings->get(1);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $setting = $this->Settings->patchEntity($setting, $this->request->getData());
            if ($this->Settings->save($setting)) {
                $this->Flash->success(__('The settings has been saved.'));
            }else{
                $this->Flash->error(__('The settings could not be saved. Please, try again.'));
            }
        }

        $this->set('setting', $setting);
    }

}
