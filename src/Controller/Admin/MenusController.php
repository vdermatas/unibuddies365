<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

use Cake\ORM\TableRegistry;

/**
 * Menus Controller
 *
 * @property \App\Model\Table\MenusTable $Menus
 *
 * @method \App\Model\Entity\Menu[] paginate($object = null, array $settings = [])
 */
class MenusController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'ParentMenus']
        ];
        $menus = $this->paginate($this->Menus);

        $this->set(compact('menus'));
        $this->set('_serialize', ['menus']);
    }

    /**
     * View method
     *
     * @param string|null $id Menu id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $menu = $this->Menus->get($id, [
            'contain' => ['Users', 'ParentMenus', 'ChildMenus']
        ]);

        $this->set('menu', $menu);
        $this->set('_serialize', ['menu']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $menu = $this->Menus->newEntity();
        if ($this->request->is('post')) {
			
			$this->request->getData()['user_id'] = $this->Auth->user('id');
            $menu = $this->Menus->patchEntity($menu, $this->request->getData());
            if ($this->Menus->save($menu)) {
                $this->Flash->success(__('The menu has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The menu could not be saved. Please, try again.'));
        }
		
		$CmsPagesTable = TableRegistry::get('CmsPages');
		$CmsPages = $CmsPagesTable->find('list');
		
		
		$this->set(compact('CmsPages'));
		
		
        $users = $this->Menus->Users->find('list', ['limit' => 200]);
        $parentMenus = $this->Menus->ParentMenus->find('list', ['limit' => 200]);
        $this->set(compact('menu', 'users', 'parentMenus'));
        $this->set('_serialize', ['menu']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Menu id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $menu = $this->Menus->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $menu = $this->Menus->patchEntity($menu, $this->request->getData());
            if ($this->Menus->save($menu)) {
                $this->Flash->success(__('The menu has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The menu could not be saved. Please, try again.'));
        }
        $users = $this->Menus->Users->find('list', ['limit' => 200]);
        $parentMenus = $this->Menus->ParentMenus->find('list', ['limit' => 200]);
        $this->set(compact('menu', 'users', 'parentMenus'));
        $this->set('_serialize', ['menu']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Menu id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $menu = $this->Menus->get($id);
        if ($this->Menus->delete($menu)) {
            $this->Flash->success(__('The menu has been deleted.'));
        } else {
            $this->Flash->error(__('The menu could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function createMenu()
    {
        $CmsPagesTable = TableRegistry::get('CmsPages');
        $CmsPages = $CmsPagesTable->find('all')->toArray();
        $this->loadModel('FrontendMenus');
        $menus = $this->FrontendMenus->get(1);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $menu = $this->FrontendMenus->patchEntity($menus, $this->request->getData());
            if ($this->FrontendMenus->save($menu)) {
                echo json_encode(['message' => 'success' , 'status' => 1]);
                die;
            }else{
                echo json_encode(['message' => 'error' , 'status' => 0]);
                die;
            }
        }
        $this->set(compact('CmsPages','menus'));
        $this->set('_serialize', ['CmsPages']);
        $this->set('_serialize', ['menus']);
    }
}
