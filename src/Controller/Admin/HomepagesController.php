<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;


use Cake\Core\Configure;

/**
 * Coupons Controller
 *
 * @property \App\Model\Table\CouponsTable $Coupons
 *
 * @method \App\Model\Entity\Coupon[] paginate($object = null, array $settings = [])
 */
class HomepagesController extends AppController
{
    public function slider()
    {
        $this->loadModel('HomepageSliderImage');
        $uploadData = '';
        if ($this->request->is('post')) {
            if(!empty($this->request->getData()['file']['name'])){
                $fileName = $this->request->getData()['file']['name'];
                $uploadPath = 'files/slider/';
                $uploadFile = $uploadPath.$fileName;
                $file = $this->request->getData()['file'];
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
                
                if(in_array($ext, $arr_ext))
                {
                    if(move_uploaded_file($this->request->getData()['file']['tmp_name'],$uploadFile)){
                        $uploadData = $this->HomepageSliderImage->newEntity();
                        $uploadData->url = $uploadFile;
                        $uploadData->text = $this->request->getData()['text'];
                        $uploadData->title = $this->request->getData()['title'];
                        $uploadData->created = date("Y-m-d H:i:s");
                        $uploadData->modified = date("Y-m-d H:i:s");
                        if ($this->HomepageSliderImage->save($uploadData)) {
                            $this->Flash->success(__('File has been uploaded and inserted successfully.'));
                        }else{
                            $this->Flash->error(__('Unable to upload file, please try again.'));
                        }
                    }else{
                        $this->Flash->error(__('Unable to upload file, please try again.'));
                    }
                }else{
                    $this->Flash->error(__('Upload only Images.'));
                }
            }else{
                $this->Flash->error(__('Please choose a file to upload.'));
            }
            
        }
        $this->set('uploadData', $uploadData);
        
        $files = $this->HomepageSliderImage->find('all', ['order' => ['HomepageSliderImage.created' => 'DESC']]);
        $filesRowNum = $files->count();
        $this->set('files',$files);
        $this->set('filesRowNum',$filesRowNum);
    }
    /*delete a slider*/
    public function deleteSlider()
    {
        $this->loadModel('HomepageSliderImage');
        $slider = $this->HomepageSliderImage->find('all')->toArray();
        if(count($slider) <= 1){            
            echo json_encode(array('status' => 0 , 'message' => 'Cant able to delete image , Need atleast one image for Slider'));
        }else{
            $deleteimage = $this->HomepageSliderImage->get($this->request->getData()['id']);
            if ($this->HomepageSliderImage->delete($deleteimage)) {
                echo json_encode(array('status' => 1 , 'message' => 'Image Deleted successfully'));
            } else {
                echo json_encode(array('status' => 0 , 'message' => 'Something went wrong , please try again later'));
            }
        }
        return $this->response;
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->loadModel('HomepageSections');
        $homepageSections = $this->paginate($this->HomepageSections->find('all')->where(['id !=' => 6]));

        $this->set(compact('homepageSections'));
        $this->set('_serialize', ['homepageSections']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Homepage Section id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->loadModel('HomepageSections');
        $homepageSection = $this->HomepageSections->get($id, [
            'contain' => []
        ]);
        // echo "test";
        if ($this->request->is(['patch', 'post', 'put'])) {

            //Check if image has been uploaded
            if(!empty($this->request->getData()['image']['name']))
            {
                $file = $this->request->getData()['image']; //put the  data into a var for easy use
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
                
                if(in_array($ext, $arr_ext))
                {
                    
                    //do the actual uploading of the file. First arg is the tmp name, second arg is
                    //where we are putting it
                    if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'files/homepage' . DS . 'image'.$file['name']))
                    {
                        //prepare the filename for database entry
                        $this->request->getData()['image'] = 'files/homepage/image'.$file['name'];
                    }
                }
            }else{
                $this->request->getData()['image'] = $homepageSection->image;
            }
            if(!empty($this->request->getData()['map'])){
                $this->request->getData()['image'] = $this->request->getData()['map'];
            }
            $homepageSection = $this->HomepageSections->patchEntity($homepageSection, $this->request->getData());
            if ($this->HomepageSections->save($homepageSection)) {
                $this->Flash->success(__('The homepage section has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The homepage section could not be saved. Please, try again.'));
        }
        $this->set(compact('homepageSection'));
        $this->set('_serialize', ['homepageSection']);
    }


    /*Section which is below the slider*/
    public function sliderSubSection()
    {
        $this->loadModel('HomepageSliderSubSections');
        $uploadData = '';
        if ($this->request->is('post')) {
            if(!empty($this->request->getData()['file']['name'])){
                $fileName = $this->request->getData()['file']['name'];
                $uploadPath = 'files/slider/';
                $uploadFile = $uploadPath.$fileName;
                $file = $this->request->getData()['file'];
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
                
                if(in_array($ext, $arr_ext))
                {
                    if(move_uploaded_file($this->request->getData()['file']['tmp_name'],$uploadFile)){
                        $uploadData = $this->HomepageSliderSubSections->newEntity();
                        $uploadData->url = $uploadFile;
                        $uploadData->title = $this->request->getData()['title'];
                        $uploadData->created = date("Y-m-d H:i:s");
                        $uploadData->modified = date("Y-m-d H:i:s");
                        if ($this->HomepageSliderSubSections->save($uploadData)) {
                            $this->Flash->success(__('File has been uploaded and inserted successfully.'));
                        }else{
                            $this->Flash->error(__('Unable to upload file, please try again.'));
                        }
                    }else{
                        $this->Flash->error(__('Unable to upload file, please try again.'));
                    }
                }else{
                    $this->Flash->error(__('Upload only Images.'));
                }
            }else{
                $this->Flash->error(__('Please choose a file to upload.'));
            }
            
        }
        $this->set('uploadData', $uploadData);
        
        $files = $this->HomepageSliderSubSections->find('all', ['order' => ['HomepageSliderSubSections.created' => 'DESC']]);
        $filesRowNum = $files->count();
        $this->set('files',$files);
        $this->set('filesRowNum',$filesRowNum);
    }
    /*delete a slider sub section*/
    public function deleteSliderSubSection()
    {
        // echo "hai"; die;
        $this->loadModel('HomepageSliderSubSections');
        $slider = $this->HomepageSliderSubSections->find('all')->toArray();
        if(count($slider) <= 4){            
            echo json_encode(array('status' => 0 , 'message' => 'Cant able to delete image , Need atleast four image for slider section'));
        }else{
            $deleteimage = $this->HomepageSliderSubSections->get($this->request->getData()['id']);
            if ($this->HomepageSliderSubSections->delete($deleteimage)) {
                echo json_encode(array('status' => 1 , 'message' => 'Image Deleted successfully'));
            } else {
                echo json_encode(array('status' => 0 , 'message' => 'Something went wrong , please try again later'));
            }
        }
        return $this->response;
    }

    public function editSlider($id = null)
    {
        $this->loadModel('HomepageSliderImage');
        $homepageslider = $this->HomepageSliderImage->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            //Check if image has been uploaded
            if(!empty($this->request->getData()['image']['name']))
            {
                $file = $this->request->getData()['image']; //put the  data into a var for easy use
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
                $fileName = $this->request->getData()['image']['name'];
                $uploadPath = 'files/slider/';
                $uploadFile = $uploadPath.$fileName;
                if(in_array($ext, $arr_ext))
                {
                    
                    //do the actual uploading of the file. First arg is the tmp name, second arg is
                    //where we are putting it
                    if(move_uploaded_file($this->request->getData()['image']['tmp_name'],$uploadFile)){
                        //prepare the filename for database entry
                        $homepageslider['url'] = 'files/slider/'.$file['name'];
                    }
                }
            }
            
            $homepageslider = $this->HomepageSliderImage->patchEntity($homepageslider, $this->request->getData());
            // pr($homepageslider); die;
            if ($this->HomepageSliderImage->save($homepageslider)) {
                $this->Flash->success(__('The homepage section has been saved.'));

                return $this->redirect(['action' => 'slider']);
            }
            $this->Flash->error(__('The homepage section could not be saved. Please, try again.'));
        }
        $this->set(compact('homepageslider'));
        $this->set('_serialize', ['homepageslider']);
    }

    public function pageBanner()
    {
        $this->loadModel('HomepageSections');
        $homepageSection = $this->HomepageSections->get(6, [
            'contain' => []
        ]);
        // echo "test";
        if ($this->request->is(['patch', 'post', 'put'])) {

            //Check if image has been uploaded
            if(!empty($this->request->getData()['image']['name']))
            {
                $file = $this->request->getData()['image']; //put the  data into a var for easy use
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
                
                if(in_array($ext, $arr_ext))
                {
                    
                    //do the actual uploading of the file. First arg is the tmp name, second arg is
                    //where we are putting it
                    if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'files/homepage' . DS . 'image'.$file['name']))
                    {
                        //prepare the filename for database entry
                        $this->request->getData()['image'] = 'files/homepage/image'.$file['name'];
                    }
                }
            }
            $homepageSection = $this->HomepageSections->patchEntity($homepageSection, $this->request->getData());
            if ($this->HomepageSections->save($homepageSection)) {
                $this->Flash->success(__('Banner Image has been saved.'));

                return $this->redirect(['action' => 'pageBanner']);
            }
            $this->Flash->error(__('The homepage section could not be saved. Please, try again.'));
        }
        $this->set(compact('homepageSection'));
        $this->set('_serialize', ['homepageSection']);
    }

    public function changeStatus()
    {
        $this->loadModel('HomepageSections');
        $section = $this->HomepageSections->get($this->request->getData()['id']);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $section = $this->HomepageSections->patchEntity($section, $this->request->getData());
            if ($this->HomepageSections->save($section)) {
                echo json_encode( array('code' => 1));
            }else{
                echo json_encode( array('code' => 0));
            }
        }
        exit;
    }

    public function socialMedias()
    {
        $this->loadModel('socialMedias');
        $socialMedia = $this->socialMedias->get(1);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $socialMedia = $this->socialMedias->patchEntity($socialMedia, $this->request->getData());
            if ($this->socialMedias->save($socialMedia)) {
                $this->Flash->success(__('Social Media values has been saved.'));

                return $this->redirect(['action' => 'socialMedias']);
            }else{
                $this->Flash->error(__('Social Media values could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('socialMedia'));
        $this->set('_serialize', ['socialMedia']);
        // pr($socialMedia); die;
    }
}