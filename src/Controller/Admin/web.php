<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', function () {
	 return view('auth.login');
	});

Auth::routes();

Route::get('forgot_password', 'HomeController@forgot_password');
Route::get('logout', 'HomeController@logout');
Route::post('reset_password', 'HomeController@reset_password');
Route::post('update_password_forgot', 'HomeController@update_password_forgot');
Route::resource('users','UsersController');
Route::get('pdfview',array('as'=>'pdfview','uses'=>'HomeController@pdfview'));



Route::get('import_reorder_products', 'ReorderController@import_reorder_products');
Route::get('reorder_products_reports', 'ReorderController@reorder_products_reports');
Route::get('reorder_products', 'ReorderController@reorder_products');
Route::get('change_password', 'UsersController@change_password');
Route::post('update_password', 'UsersController@update_password');
Route::post('quick_email', 'UsersController@quick_email');
Route::resource('email_templates', 'EmailtemplateController');
Route::get('reset/password/{id?}', [
    
    'uses' => 'Auth\ForgotPasswordController@password_reset',
]);
Route::post('submit_report', 'ReorderController@submit_report');
Route::post('submit_report_change', 'ReorderController@submit_report_change');
Route::post('reset_reorder_reports', 'ReorderController@reset_reorder_reports');
Route::get('import_backorder_products', 'ReorderController@import_backorder_products');
Route::get('backorder_products', 'ReorderController@backorder_products');
Route::get('backorder_product/{id?}', [
    
    'uses' => 'ReorderController@backorder_product',
]);

// Route::any('trello_get', 'TrelloController@trello_get');
Route::resource('trello-task', 'TrelloController');
Route::get('boards/{id?}', [
    
    'uses' => 'TrelloController@boards',
]);

Route::get('/home', 'HomeController@index');








