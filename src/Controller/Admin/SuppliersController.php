<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;


use Cake\Mailer\MailerAwareTrait;

use Cake\Mailer\Email;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class SuppliersController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        //$this->Auth->allow(['add', 'logout']);
    }

    protected function _sendEmailMessage($to = null, $email_body = null, $subject = null, $bcc = null, $cc = null) {
        $email = new Email('default');
        $email->setEmailFormat('both');
        $email->setFrom('admin@cheolis.com');
        $email->setSender('admin@cheolis.com');
        $email->setTo($to);
        $email->setSubject($subject);
        if ($email->send($email_body)) {
            return true;
        }
        return false;
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->loadModel('Users');
        $paginateArray = [
              'conditions' => ['Users.user_type_id' => 2],
            'contain' => ['UserTypes'],
            'order' => ['Users.created' => 'DESC']
        ];

        if ($this->request->is('get')) {

            $conditions = [];

            if (!empty($this->request->getQuery()['search'])) {
                $conditions[] = array('Users.username LIKE' => '%'.$this->request->getQuery()['search'].'%');
                $conditions[] = array('Users.first_name LIKE' => '%'.$this->request->getQuery()['search'].'%');
                $conditions[] = array('Users.last_name LIKE' => '%'.$this->request->getQuery()['search'].'%');
                $conditions[] = array('Users.email LIKE' => '%'.$this->request->getQuery()['search'].'%');
                $conditions[] = array('Users.mobile LIKE' => '%'.$this->request->getQuery()['search'].'%');
                $paginateArray['conditions'] = ['OR' => $conditions];
            }
        }
        
        $this->paginate = $paginateArray;
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->loadModel('Users');
        $user = $this->Users->get($id, [
            'contain' => ['UserTypes', 'Articles', 'Categories', 'CmsPages', 'Images', 'ManageHomePage', 'Menus']
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel('Users');
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            // echo $this->request->getData()['password']; die;
            $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
            $remember_token = substr(str_shuffle(str_repeat($pool, 64)), 0,64);
			$this->request->getData()['Users']['user_type_id'] = 2;
            $this->request->getData()['Users']['remember_token'] = $remember_token;
            $user = $this->Users->patchEntity($user, $this->request->getData());
            
			if ($this->Users->save($user)) {

                $this->loadModel('EmailTemplates');                
                $temp = $this->EmailTemplates->get(3);  
                $details = "username : ".$this->request->getData()['username']." password : ".$this->request->getData()['password'];
                $temp['body'] = str_replace(array('#NAME', '#DETAILS'), array(
                                                    $this->request->getData()['first_name']." ".$this->request->getData()['last_name'] , $details
                                                    ), $temp['body']
                                                  );       

                 $this->_sendEmailMessage($this->request->getData()['email'], $temp['body'], $temp['subject']);
                $this->Flash->success(__('The supplier has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The supplier could not be saved. Please, try again.'));
        }
        $userTypes = $this->Users->UserTypes->find('list', ['limit' => 200]);
        $this->set(compact('user', 'userTypes'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->loadModel('Users');
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $userTypes = $this->Users->UserTypes->find('list', ['limit' => 200]);
        $this->set(compact('user', 'userTypes'));
        $this->set('_serialize', ['user']);
    }
    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->loadModel('Users');
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
	
	/*--------------Order --------------------*/
	/**
     * AddOrder method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function addOrder()
    {
        
    }
	
	
	
}
