<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

use Cake\Mailer\MailerAwareTrait;
use Cake\Mailer\Email;
/**
 * Assignments Controller
 *
 * @property \App\Model\Table\AssignmentsTable $Assignments
 *
 * @method \App\Model\Entity\Assignment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AssignmentsController extends AppController
{

    protected function _sendEmailMessage($to = null, $email_body = null, $subject = null, $bcc = null, $cc = null)
    {
        $email = new Email('default');
        $email->setEmailFormat('both');
        $email->setFrom('admin@unibuddies.com');
        $email->setSender('admin@unibuddies.com');
        $email->setTo($to);
        $email->setSubject($subject);
        if ($email->send($email_body)) {
            return true;
        }
        return false;
    }
    /**
     * assigned method
     *
     * @return \Cake\Http\Response|null
     */
    public function assigned()
    {
        $this->paginate = [
            'contain' => ['Users','AssignmentOffers' => [  'Users' , 'conditions' => [ 'AssignmentOffers.status' => 1 ] , 'AssignmentDeliverySchedules'  ] ],
            'conditions' => ['Assignments.status' => 2]
        ];
        $assignments = $this->paginate($this->Assignments);
        // pr($assignments); die;
        $this->set(compact('assignments'));
    }

    /**
     * completed method
     *
     * @return \Cake\Http\Response|null
     */
    public function completed()
    {
        $this->paginate = [
            'contain' => ['Users','AssignmentOffers' => [  'Users'  ] ],
            'conditions' => ['Assignments.status IN' => [3,4]  ]
        ];
        $assignments = $this->paginate($this->Assignments);

        $this->set(compact('assignments'));
    }

    /**
     * unassigned method
     *
     * @return \Cake\Http\Response|null
     */
    public function unassigned()
    {
        $this->paginate = [
            'contain' => ['Users','AssignmentOffers' => [  'Users'  ] ],
            'conditions' => ['Assignments.status' => 1]
        ];
        $assignments = $this->paginate($this->Assignments);

        $this->set(compact('assignments'));

        // pr($assignments); die;
    }

    public function markComplete($id = null)
    {
        $this->loadModel('EmailTemplates');

        $assignment = $assignment = $this->Assignments->get($id,[
            'contain' => ['Users']
         ]);
        $assignment->status = 3;


        $emailTemplate = $this->EmailTemplates->get(7);

         $this->Assignments->save($assignment);

         $emailTemplate['body'] = str_replace(
            array('#DETAILS'), array(
                  "Your assignment has been marked as completed by admin and go to your dashboard to check ,  Assignment : ".$assignment['title']
            ), $emailTemplate['body']
          ); 

        $this->_sendEmailMessage($assignment['user']['email'], $emailTemplate['body'], $emailTemplate['subject']);


         $this->Flash->success(__('This Assignment Marked as completed'));

        return $this->redirect($this->referer());

        // pr($assignments); die;
    }

    /**
     * unassigned method
     *
     * @return \Cake\Http\Response|null
     */
    public function view($id = null)
    {
       // $assignment = $this->Assignments->get($id, [
       //      'contain' => [
       //          'AssignmentOffers' => [
       //              'Users' 
       //          ]
       //      ]
       //  ]);

       $assignment = $this->Assignments->get($id, [
            'contain' => [
                'AssignmentOffers' => [
                    'Users' , 'AssignmentDeliverySchedules' , 'conditions' => [

                        // 'AssignmentOffers.status IN' => [2,3]

                    ]
                ] , 'Users' , 'RatingByWriter' , 'RatingByStudent'
            ]
        ]);
// pr($assignment); die;
        $this->set('assignment', $assignment);
    }

    public function edit($id = null )
    {
        $assignment = $this->Assignments->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {

             // $assignment = $this->Assignments->patchEntity($assignment, $this->request->getData());

             $assignment->admin_commission = $this->request->getData()['admin_commission'];

             // pr($this->request->getData()); die;

             if ($this->Assignments->save($assignment)) {

                $this->Flash->success(__('The assignment commission has been saved.'));

                return $this->redirect(['action' => 'unassigned']);

            }else{

                $this->Flash->error(__('The assignment commission could not be saved. Please, try again.'));

            }

        }

        $this->set(compact('assignment'));
    }

    public function deliverySchedules($id = null)
    {
        $this->loadModel('AssignmentDeliverySchedules');

        $schedules = $this->AssignmentDeliverySchedules->find('all',[
                'conditions' => ['assignment_offer_id' => $id]
        ])->toArray();

        $this->set('schedules', $schedules);

        // pr($schedules); die;
    }

}
