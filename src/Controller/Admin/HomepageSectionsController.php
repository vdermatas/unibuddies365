<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * HomepageSections Controller
 *
 * @property \App\Model\Table\HomepageSectionsTable $HomepageSections
 *
 * @method \App\Model\Entity\HomepageSection[] paginate($object = null, array $settings = [])
 */
class HomepageSectionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */

    public function index()
    {

    }


    public function whyChooseUs()
    {
        $homepageSections = $this->HomepageSections->get(1);


        if ($this->request->is(['patch', 'post', 'put'])) {

            $bgImage = $homepageSections->background_image;
            $section_1_image = $homepageSections->section_1_image;
            $section_2_image = $homepageSections->section_2_image;
            $section_3_image = $homepageSections->section_3_image;

            $homepageSections = $this->HomepageSections->patchEntity($homepageSections, $this->request->getData());

            if(!empty($homepageSections->getErrors())){
                foreach ($homepageSections->getErrors() as $key => $value) {
                    foreach ($value as $key => $values) {
                        $this->Flash->error(__($values));
                    }
                }
            }else{               

                // $cmsPage['background_image'] = $cmsPage->background_image;

                if(!empty($this->request->getData()['background_image']['name']))
                {
                    $file = $this->request->getData()['background_image']; 
                    if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'images' . DS . 'background'.$file['name']))
                    {
                        //prepare the filename for database entry
                        $homepageSections['background_image'] = 'images/background'.$file['name'];
                    }
                }else{
                    $homepageSections['background_image'] = $bgImage;
                }

                if(!empty($this->request->getData()['section_1_image']['name']))
                {
                    $file = $this->request->getData()['section_1_image']; 
                    if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'images' . DS . 'section_1_image'.$file['name']))
                    {
                        //prepare the filename for database entry
                        $homepageSections['section_1_image'] = 'images/section_1_image'.$file['name'];
                    }
                }else{
                    $homepageSections['section_1_image'] = $section_1_image;
                }

                if(!empty($this->request->getData()['section_2_image']['name']))
                {
                    $file = $this->request->getData()['section_2_image']; 
                    if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'images' . DS . 'section_2_image'.$file['name']))
                    {
                        //prepare the filename for database entry
                        $homepageSections['section_2_image'] = 'images/section_2_image'.$file['name'];
                    }
                }else{
                    $homepageSections['section_2_image'] = $section_2_image;
                }

                if(!empty($this->request->getData()['section_3_image']['name']))
                {
                    $file = $this->request->getData()['section_3_image']; 
                    if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'images' . DS . 'section_3_image'.$file['name']))
                    {
                        //prepare the filename for database entry
                        $homepageSections['section_3_image'] = 'images/section_3_image'.$file['name'];
                    }
                }else{
                    $homepageSections['section_3_image'] = $section_3_image;
                }
                
                if ($this->HomepageSections->save($homepageSections)) {
                    $this->Flash->success(__('The homepage Section has been saved.'));
                }else{
                    $this->Flash->error(__('The homepage Section could not be saved. Please, try again.'));
                }
            }
        }

        $this->set(compact('homepageSections'));
        $this->set('_serialize', ['homepageSections']);
    }


     /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function services()
    {
        $homepageSections = $this->HomepageSections->get(2);


        if ($this->request->is(['patch', 'post', 'put'])) {

            $bgImage = $homepageSections->background_image;
            $section_1_image = $homepageSections->section_1_image;
            $section_2_image = $homepageSections->section_2_image;
            $section_3_image = $homepageSections->section_3_image;
            $section_4_image = $homepageSections->section_4_image;

            $homepageSections = $this->HomepageSections->patchEntity($homepageSections, $this->request->getData());

            if(!empty($homepageSections->getErrors())){
                foreach ($homepageSections->getErrors() as $key => $value) {
                    foreach ($value as $key => $values) {
                        $this->Flash->error(__($values));
                    }
                }
            }else{               

                // $cmsPage['background_image'] = $cmsPage->background_image;

                if(!empty($this->request->getData()['background_image']['name']))
                {
                    $file = $this->request->getData()['background_image']; 
                    if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'images' . DS . 'background'.$file['name']))
                    {
                        //prepare the filename for database entry
                        $homepageSections['background_image'] = 'images/background'.$file['name'];
                    }
                }else{
                    $homepageSections['background_image'] = $bgImage;
                }

                if(!empty($this->request->getData()['section_1_image']['name']))
                {
                    $file = $this->request->getData()['section_1_image']; 
                    if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'images' . DS . 'section_1_image'.$file['name']))
                    {
                        //prepare the filename for database entry
                        $homepageSections['section_1_image'] = 'images/section_1_image'.$file['name'];
                    }
                }else{
                    $homepageSections['section_1_image'] = $section_1_image;
                }

                if(!empty($this->request->getData()['section_2_image']['name']))
                {
                    $file = $this->request->getData()['section_2_image']; 
                    if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'images' . DS . 'section_2_image'.$file['name']))
                    {
                        //prepare the filename for database entry
                        $homepageSections['section_2_image'] = 'images/section_2_image'.$file['name'];
                    }
                }else{
                    $homepageSections['section_2_image'] = $section_2_image;
                }

                if(!empty($this->request->getData()['section_3_image']['name']))
                {
                    $file = $this->request->getData()['section_3_image']; 
                    if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'images' . DS . 'section_3_image'.$file['name']))
                    {
                        //prepare the filename for database entry
                        $homepageSections['section_3_image'] = 'images/section_3_image'.$file['name'];
                    }
                }else{
                    $homepageSections['section_3_image'] = $section_3_image;
                }


                if(!empty($this->request->getData()['section_4_image']['name']))
                {
                    $file = $this->request->getData()['section_4_image']; 
                    if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'images' . DS . 'section_4_image'.$file['name']))
                    {
                        //prepare the filename for database entry
                        $homepageSections['section_4_image'] = 'images/section_4_image'.$file['name'];
                    }
                }else{
                    $homepageSections['section_4_image'] = $section_4_image;
                }
                
                if ($this->HomepageSections->save($homepageSections)) {
                    $this->Flash->success(__('The homepage Section has been saved.'));
                }else{
                    $this->Flash->error(__('The homepage Section could not be saved. Please, try again.'));
                }
            }
        }

        $this->set(compact('homepageSections'));
        $this->set('_serialize', ['homepageSections']);
    }


    public function about()
    {
        $homepageSections = $this->HomepageSections->get(3);


        if ($this->request->is(['patch', 'post', 'put'])) {

            $bgImage = $homepageSections->background_image;

            $homepageSections = $this->HomepageSections->patchEntity($homepageSections, $this->request->getData());

            if(!empty($homepageSections->getErrors())){
                foreach ($homepageSections->getErrors() as $key => $value) {
                    foreach ($value as $key => $values) {
                        $this->Flash->error(__($values));
                    }
                }
            }else{               

                // $cmsPage['background_image'] = $cmsPage->background_image;

                if(!empty($this->request->getData()['background_image']['name']))
                {
                    $file = $this->request->getData()['background_image']; 
                    if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'images' . DS . 'background'.$file['name']))
                    {
                        //prepare the filename for database entry
                        $homepageSections['background_image'] = 'images/background'.$file['name'];
                    }
                }else{
                    $homepageSections['background_image'] = $bgImage;
                }

                
                if ($this->HomepageSections->save($homepageSections)) {
                    $this->Flash->success(__('The homepage Section has been saved.'));
                }else{
                    $this->Flash->error(__('The homepage Section could not be saved. Please, try again.'));
                }
            }
        }

        $this->set(compact('homepageSections'));
        $this->set('_serialize', ['homepageSections']);
    }


   

}
