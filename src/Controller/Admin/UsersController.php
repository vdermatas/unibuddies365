<?php

namespace App\Controller\Admin;


use App\Controller\Admin\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Mailer\MailerAwareTrait;
use Cake\Mailer\Email;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */

class UsersController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['add', 'logout', 'changePassword']);
    }

    protected function _sendEmailMessage($to = null, $email_body = null, $subject = null, $bcc = null, $cc = null) {

        $email = new Email('default');

        $email->setEmailFormat('both');

        $email->setFrom('admin@unibuddies.com');

        $email->setSender('admin@unibuddies.com');

        $email->setTo($to);

        $email->setSubject($subject);

        if ($email->send($email_body)) {

            return true;

        }

        return false;

    }





    public function dashboard()

    {



        if($this->request->is('ajax')){

            $ProductsTable = TableRegistry::get('Products'); 

            $Products = $ProductsTable->find('all')

                                            ->where(['Products.is_deleted' => 0]);

            $this->loadModel('FrontendOrders');

            $totalOrders = $this->FrontendOrders->find('all')->where(['is_deleted' => 0]);            

            $this->loadModel('Enquiries');

            $enquires = $this->Enquiries->find('all');

            $this->loadModel('FrontendInvoices');

            $totalPrice = $this->FrontendInvoices->find()->toArray();

            $jantotal = 0;$febtotal = 0;$martotal = 0;$aprtotal = 0;$maytotal = 0;$juntotal = 0;

            $jultotal = 0;$augtotal = 0;$septotal = 0;$octtotal = 0;$novtotal = 0;$dectotal = 0;

            $total_price = 0;

            // $date = date("F", strtotime('2016-11-17 00:00:00')); //May

            // die;

            foreach ($totalPrice as $key => $value) {

                if(date("F", strtotime($value->created)) == 'January' && date("Y", strtotime($value->created)) == date('Y')){

                    $jantotal += $value->total;

                }elseif(date("F", strtotime($value->created)) == 'February' && date("Y", strtotime($value->created)) == date('Y')){

                    $febtotal += $value->total;

                }elseif(date("F", strtotime($value->created)) == 'March' && date("Y", strtotime($value->created)) == date('Y')){

                    $martotal += $value->total;

                }elseif(date("F", strtotime($value->created)) == 'April' && date("Y", strtotime($value->created)) == date('Y')){

                    $aprtotal += $value->total;

                }elseif(date("F", strtotime($value->created)) == 'May' && date("Y", strtotime($value->created)) == date('Y')){

                    $maytotal += $value->total;

                }elseif(date("F", strtotime($value->created)) == 'June' && date("Y", strtotime($value->created)) == date('Y')){

                    $juntotal += $value->total;

                }elseif(date("F", strtotime($value->created)) == 'July' && date("Y", strtotime($value->created)) == date('Y')){

                    $jultotal += $value->total;

                }elseif(date("F", strtotime($value->created)) == 'August' && date("Y", strtotime($value->created)) == date('Y')){

                    $augtotal += $value->total;

                }elseif(date("F", strtotime($value->created)) == 'September' && date("Y", strtotime($value->created)) == date('Y')){

                    $septotal += $value->total;

                }elseif(date("F", strtotime($value->created)) == 'October' && date("Y", strtotime($value->created)) == date('Y')){

                    $octtotal += $value->total;

                }elseif(date("F", strtotime($value->created)) == 'November' && date("Y", strtotime($value->created)) == date('Y')){

                    $novtotal += $value->total;

                }elseif(date("F", strtotime($value->created)) == 'December' && date("Y", strtotime($value->created))== date('Y')){

                    $dectotal += $value->total;

                }else{}     

                $total_price += $value->total;       

            }

            echo json_encode(['Products' => $Products->count() , 'TotalOrders' => $totalOrders->count() , 'Enquiries' => $enquires->count() , 'TotalPrice' => $total_price , 'jan' => $jantotal , 'feb' => $febtotal , 'mar' => $martotal , 'apr' => $aprtotal ,'may' => $maytotal , 'jun' => $juntotal ,'jul' => $jultotal ,'aug' => $augtotal ,'sep' => $septotal ,'oct' => $octtotal ,'nov' => $novtotal ,'dec' => $dectotal ]);

            exit;

        }



    }

    

    public function profile()

    {

        //pr($this->Auth->user());

        $user = $this->Users->get($this->Auth->user('id'));

        if ($this->request->is(['patch', 'post', 'put'])) {

           
            $data  = $this->request->getData();
            

            //Check if image has been uploaded

            if(!empty($this->request->getData()['upload_image']['name']))

            {

                $file = $this->request->getData()['upload_image']; //put the  data into a var for easy use

                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension

                $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions

                if(in_array($ext, $arr_ext))
                {

                    //do the actual uploading of the file. First arg is the tmp name, second arg is

                    //where we are putting it

                    if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'files/uploads' . DS . 'profile_image_'.$file['name']))

                    {

                        //prepare the filename for database entry

                        $data['profile_image'] = 'profile_image_'.$file['name'];

                    }

                }

            }

            

            $user = $this->Users->patchEntity($user, $data);


            if ($this->Users->save($user)) {

                $this->Auth->setUser($user);

                $this->Flash->success(__('The user profile has been saved.'));



                return $this->redirect(['action' => 'profile']);

            }

            $this->Flash->error(__('The user profile could not be saved. Please, try again.'));

        }

        $this->set(compact('user'));

    }

    

    public function login()

    {

         // Set the layout.

        $this->viewBuilder()->setLayout('login');

        if ($this->request->is('post')) {

            $this->request->getSession()->destroy(); ///=-------------

            $user = $this->Auth->identify();

            if ($user) {

                $this->Auth->setUser($user);

                return $this->redirect($this->Auth->redirectUrl());

            }

            $this->Flash->error(__('Invalid username or password, try again'));

        }

    }



    public function logout()

    {

        $this->Flash->success('You are now logged out.');

        return $this->redirect($this->Auth->logout());

    }





    public function changePassword()

    {

        $user =$this->Users->get($this->Auth->user('id'));

        if (!empty($this->request->getData())) {



            $user = $this->Users->patchEntity($user, [

                    'old_password'  => $this->request->getData()['old_password'],

                    'password'      => $this->request->getData()['password1'],

                    'password1'     => $this->request->getData()['password1'],

                    'password2'     => $this->request->getData()['password2']

                ],

                ['validate' => 'password']

            );

                

            /*$flashMsg = '';            

            foreach($user->getErrors() as $k => $v) {

                pr(array_values($v));   

            }*/



            if ($this->Users->save($user)) {

                $this->Flash->success('The password is successfully changed');

                $this->redirect(['action' => 'changePassword']);

            } else {

                $this->Flash->error('There was an error during the save!');

            }

        }

        $this->set('user',$user);

    }

    

    public function profileImage($id = null)

    {

        $this->autoRender = false;

       // example options

        $options = array(

            'max_file_size' => 2048000,

            //'max_number_of_files' => 10,

            'access_control_allow_methods' => array(

                'POST'

            ),

            'access_control_allow_origin' => Router::fullBaseUrl(),

            'accept_file_types' => '/\.(jpe?g|png)$/i',

            'upload_dir' => WWW_ROOT . 'files' . DS . 'uploads' . DS,

            'upload_url' => '/files/uploads/',

            'print_response' => false

        );



        $result = $this->JqueryFileUpload->upload($options);

        

        if($result) {           

            $UserTable = TableRegistry::get('Users');

            $User = $UserTable->newEntity();

            

            $User->profile_image = $result['files'][0]->url;

            $User->user_id = $this->Auth->user('id');



            if ($UserTable->save($User)) {              

                echo json_encode($result);

                exit;

            }

        }

    }







    /**

     * Index method

     *

     * @return \Cake\Http\Response|void

     */

    public function index()

    {

        $this->paginate = [

            'contain' => ['UserTypes'],
            'conditions' => ['user_type_id != ' =>  1]


        ];

        $users = $this->paginate($this->Users);



        $this->set(compact('users'));

        $this->set('_serialize', ['users']);

    }



    /**

     * View method

     *

     * @param string|null $id User id.

     * @return \Cake\Http\Response|void

     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.

     */

    /*public function view($id = null)

    {

        $user = $this->Users->get($id, [

            'contain' => ['UserTypes', 'Articles', 'Categories', 'CmsPages', 'Images', 'ManageHomePage', 'Menus']

        ]);



        $this->set('user', $user);

        $this->set('_serialize', ['user']);

    }



    /**

     * Add method

     *

     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.

     */

     

    public function add()

    {

        $user = $this->Users->newEntity();

        if ($this->request->is('post')) {

            $user = $this->Users->patchEntity($user, $this->request->getData());

            if ($this->Users->save($user)) {

                $this->Flash->success(__('The user has been saved.'));



                return $this->redirect(['action' => 'index']);

            }

            $this->Flash->error(__('The user could not be saved. Please, try again.'));

        }

        $userTypes = $this->Users->UserTypes->find('list', ['limit' => 200]);

        $this->set(compact('user', 'userTypes'));

        $this->set('_serialize', ['user']);

    }

    



    /**

     * Edit method

     *

     * @param string|null $id User id.

     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.

     * @throws \Cake\Network\Exception\NotFoundException When record not found.

     */

    public function edit($id = null)

    {

        $user = $this->Users->get($id, [

            'contain' => []

        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $user = $this->Users->patchEntity($user, $this->request->getData());

            if ($this->Users->save($user)) {

                $this->Flash->success(__('The user has been saved.'));



                return $this->redirect(['action' => 'index']);

            }

            $this->Flash->error(__('The user could not be saved. Please, try again.'));

        }

        $userTypes = $this->Users->UserTypes->find('list', ['limit' => 200]);

        $this->set(compact('user', 'userTypes'));

        $this->set('_serialize', ['user']);

    }



    /**

     * Delete method

     *

     * @param string|null $id User id.

     * @return \Cake\Http\Response|null Redirects to index.

     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.

     */

    /*public function delete($id = null)

    {

        $this->request->allowMethod(['post', 'delete']);

        $user = $this->Users->get($id);

        if ($this->Users->delete($user)) {

            $this->Flash->success(__('The user has been deleted.'));

        } else {

            $this->Flash->error(__('The user could not be deleted. Please, try again.'));

        }



        return $this->redirect(['action' => 'index']);

    }*/



    /*Send email*/

    public function sendEmail()

    {

        $this->loadModel('QuickEmails');

          $quickEmail = $this->QuickEmails->newEntity();

        if($this->request->is('post')) {

            $quickEmail = $this->QuickEmails->patchEntity($quickEmail, $this->request->getData());            

            if ($this->QuickEmails->save($quickEmail)) {

                  $this->_sendEmailMessage($this->request->getData()['emailto'], $this->request->getData()['message'], $this->request->getData()['subject']);               

                 $this->Flash->success(__('Email Sent Successfully'));

                 return $this->redirect(['action' => 'dashboard']);

            }else{

                $this->Flash->error(__('Something went wrong'));

                 return $this->redirect(['action' => 'dashboard']);

            }

        }

       

    }

    /**

     * Writer method

     *

     * @return \Cake\Http\Response|void

     */

    public function writers()

    {

        $this->paginate = [

            'contain' => ['WriterProfiles'],
            'conditions' => ['user_type_id' =>  3]


        ];

        $users = $this->paginate($this->Users);



        $this->set(compact('users'));

        $this->set('_serialize', ['users']);

        // pr($users); die;

    }

     /**

     * Student method

     *

     * @return \Cake\Http\Response|void

     */

    public function students()

    {

        $this->paginate = [

            'conditions' => ['user_type_id' =>  2]


        ];

        $users = $this->paginate($this->Users);


        $this->set(compact('users'));

        $this->set('_serialize', ['users']);

    }

    /**

     * Edit Student method

     *

     * @param string|null $id User id.

     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.

     * @throws \Cake\Network\Exception\NotFoundException When record not found.

     */

    public function editStudent($id = null)

    {

        $user = $this->Users->get($id, [

            'contain' => ['Assignments']

        ]);
// pr($user); die;
        if ($this->request->is(['patch', 'post', 'put'])) {

            $user = $this->Users->patchEntity($user, $this->request->getData());

            if ($this->Users->save($user)) {

                $this->Flash->success(__('The student has been saved.'));



                return $this->redirect(['action' => 'students']);

            }

            $this->Flash->error(__('The student could not be saved. Please, try again.'));

        }

        $userTypes = $this->Users->UserTypes->find('list', ['limit' => 200]);

        $this->set(compact('user', 'userTypes'));

        $this->set('_serialize', ['user']);

    }


    /**

     * Edit Writer method

     *

     * @param string|null $id User id.

     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.

     * @throws \Cake\Network\Exception\NotFoundException When record not found.

     */

    public function editWriter($id = null)

    {

        $user = $this->Users->get($id, [

            'contain' => [ 'WriterProfiles' => ['WriterSkills','WriterDocuments','WriterThematics'] ]

        ]);
// pr($user); die;
        if ($this->request->is(['patch', 'post', 'put'])) {

            $user = $this->Users->patchEntity($user, $this->request->getData());

            if ($this->Users->save($user)) {

                $this->Flash->success(__('The writer has been saved.'));



                return $this->redirect(['action' => 'writers']);

            }

            $this->Flash->error(__('The writer could not be saved. Please, try again.'));

        }

        $userTypes = $this->Users->UserTypes->find('list', ['limit' => 200]);

        $this->set(compact('user', 'userTypes'));

        $this->set('_serialize', ['user']);

    }

    public function deactivate($id = null)
    {
        $user = $this->Users->get($id);
        $user->status = 0;
        $this->Users->Save($user);
        $this->Flash->success(__('User has been deactivated.'));
        return $this->redirect($this->referer());
    }

    public function activate($id = null)
    {
        $this->loadModel('EmailTemplates');
        $emailTemplate = $this->EmailTemplates->get(6);
        $user = $this->Users->get($id);
        $user->status = 1;
        $this->Users->Save($user);
        $this->_sendEmailMessage($user->email, $emailTemplate['body'], $emailTemplate['subject']); 
        $this->Flash->success(__('User has been activated.'));
        return $this->redirect($this->referer());
    }



}