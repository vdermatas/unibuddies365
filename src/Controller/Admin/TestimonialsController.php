<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
/**
 * Testimonials Controller
 *
 * @property \App\Model\Table\TestimonialsTable $Testimonials
 *
 * @method \App\Model\Entity\Testimonial[] paginate($object = null, array $settings = [])
 */
class TestimonialsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $paginateArray = [ 'order' => ['Testimonials.created' => 'DESC']];

        $this->paginate = $paginateArray;
        $testimonials = $this->paginate($this->Testimonials);

        $this->set(compact('testimonials'));
        $this->set('_serialize', ['testimonials']);
    }

    /**
     * View method
     *
     * @param string|null $id Testimonial id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $testimonial = $this->Testimonials->get($id, [
            'contain' => []
        ]);

        $this->set('testimonial', $testimonial);
        $this->set('_serialize', ['testimonial']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $testimonial = $this->Testimonials->newEntity();
        if ($this->request->is('post')) {
            
            //Check if image has been uploaded
            if(!empty($this->request->getData()['image']['name']))
            {
                $file = $this->request->getData()['image']; //put the  data into a var for easy use
               
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
                
                if(in_array($ext, $arr_ext))
                {                    
                    //do the actual uploading of the file. First arg is the tmp name, second arg is
                    //where we are putting it
                    if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'files/uploads' . DS . 'testimonial_'.$file['name']))
                    {
                        //prepare the filename for database entry
                        $this->request->getData()['image'] = 'files/uploads' . DS . 'testimonial_'.$file['name'];
                    }
                }
            }

            $testimonial = $this->Testimonials->patchEntity($testimonial, $this->request->getData());
            if ($this->Testimonials->save($testimonial)) {
                $this->Flash->success(__('The testimonial has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The testimonial could not be saved. Please, try again.'));
        }
        $this->set(compact('testimonial'));
        $this->set('_serialize', ['testimonial']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Testimonial id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $testimonial = $this->Testimonials->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {

            //Check if image has been uploaded
            if(!empty($this->request->getData()['image']['tmp_name']))
            {

                $file = $this->request->getData()['image']; //put the  data into a var for easy use
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
                
                if(in_array($ext, $arr_ext))
                {
                    //do the actual uploading of the file. First arg is the tmp name, second arg is
                    //where we are putting it
                    if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'files/uploads' . DS . 'testimonial_'.$file['name']))
                    {
                       
                        //prepare the filename for database entry
                        $this->request->getData()['image'] = 'files/uploads' . DS . 'testimonial_'.$file['name'];
                    }
                }
            }else{
                $this->request->getData()['image'] = $testimonial->image;
            }

            $testimonial = $this->Testimonials->patchEntity($testimonial, $this->request->getData());
            if ($this->Testimonials->save($testimonial)) {
                $this->Flash->success(__('The testimonial has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The testimonial could not be saved. Please, try again.'));
        }
        $this->set(compact('testimonial'));
        $this->set('_serialize', ['testimonial']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Testimonial id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $testimonial = $this->Testimonials->get($id);
        if ($this->Testimonials->delete($testimonial)) {
            $this->Flash->success(__('The testimonial has been deleted.'));
        } else {
            $this->Flash->error(__('The testimonial could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
