<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * ManageHomePage Controller
 *
 * @property \App\Model\Table\ManageHomePageTable $ManageHomePage
 *
 * @method \App\Model\Entity\ManageHomePage[] paginate($object = null, array $settings = [])
 */
class ManageHomePageController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'order' => ['ManageHomePage.created' => 'DESC']
        ];
        $manageHomePage = $this->paginate($this->ManageHomePage);

        $this->set(compact('manageHomePage'));
        $this->set('_serialize', ['manageHomePage']);
    }

    /**
     * View method
     *
     * @param string|null $id Manage Home Page id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $manageHomePage = $this->ManageHomePage->get($id, [
            'contain' => []
        ]);

        $this->set('manageHomePage', $manageHomePage);
        $this->set('_serialize', ['manageHomePage']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $manageHomePage = $this->ManageHomePage->get('6', [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $manageHomePage = $this->ManageHomePage->patchEntity($manageHomePage, $this->request->getData());
            if ($this->ManageHomePage->save($manageHomePage)) {
                $this->Flash->success(__('The manage home page has been saved.'));

                return $this->redirect(['action' => 'add']);
            }
            $this->Flash->error(__('The manage home page could not be saved. Please, try again.'));
        }
        $this->set(compact('manageHomePage'));
        $this->set('_serialize', ['manageHomePage']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Manage Home Page id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $manageHomePage = $this->ManageHomePage->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $manageHomePage = $this->ManageHomePage->patchEntity($manageHomePage, $this->request->getData());
            if ($this->ManageHomePage->save($manageHomePage)) {
                $this->Flash->success(__('The manage home page has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The manage home page could not be saved. Please, try again.'));
        }
        $this->set(compact('manageHomePage'));
        $this->set('_serialize', ['manageHomePage']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Manage Home Page id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $manageHomePage = $this->ManageHomePage->get($id);
        if ($this->ManageHomePage->delete($manageHomePage)) {
            $this->Flash->success(__('The manage home page has been deleted.'));
        } else {
            $this->Flash->error(__('The manage home page could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
