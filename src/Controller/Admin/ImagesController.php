<?php
namespace App\Controller\Admin;
use Cake\Core\App;
use App\Controller\Admin\AppController;
use Cake\Routing\Router;

/**
 * Images Controller
 *
 * @property \App\Model\Table\ImagesTable $Images
 *
 * @method \App\Model\Entity\Image[] paginate($object = null, array $settings = [])
 */
class ImagesController extends AppController
{
	
	public $components = ['CakephpBlueimpUpload.Uploader'];
	public $helpers    = ['CakephpBlueimpUpload.BlueimpUpload'];

	public function initialize()
	{
		parent::initialize();
	}

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Galleries']
        ];
        $images = $this->paginate($this->Images);

        $this->set(compact('images'));
        $this->set('_serialize', ['images']);
    }

    /**
     * View method
     *
     * @param string|null $id Image id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $image = $this->Images->get($id, [
            'contain' => ['Users', 'Galleries']
        ]);

        $this->set('image', $image);
        $this->set('_serialize', ['image']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
		
		// example options
		$options = array(
			'max_file_size' => 2048000,
			'max_number_of_files' => 10,
			'access_control_allow_methods' => array(
				'POST'
			),
			'access_control_allow_origin' => Router::fullBaseUrl(),
			'accept_file_types' => '/\.(jpe?g|png)$/i',
			'upload_dir' => WWW_ROOT . 'files' . DS . 'uploads' . DS,
			'upload_url' => '/files/uploads/',
			'print_response' => false
		);

		$result = $this->JqueryFileUpload->upload($options);
		
    }

    /**
     * Edit method
     *
     * @param string|null $id Image id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $image = $this->Images->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $image = $this->Images->patchEntity($image, $this->request->getData());
            if ($this->Images->save($image)) {
                $this->Flash->success(__('The image has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The image could not be saved. Please, try again.'));
        }
        $users = $this->Images->Users->find('list', ['limit' => 200]);
        $galleries = $this->Images->Galleries->find('list', ['limit' => 200]);
        $this->set(compact('image', 'users', 'galleries'));
        $this->set('_serialize', ['image']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Image id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $image = $this->Images->get($id);
        if ($this->Images->delete($image)) {
            $this->Flash->success(__('The image has been deleted.'));
        } else {
            $this->Flash->error(__('The image could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
	
	
	public function upload_picture($id = null)
	{
		$upload = $this->Uploader->upload($upload_folder, ['accepted_mimetypes' => ['image/jpeg', 'image/tiff', 'image/png']]);

		if($upload !== false)
		{
			if($upload->complete)
			{
			   /*
				* The upload is over. 
				* Do what you want with the $upload entity
				*/
			}
		}
	}

}
