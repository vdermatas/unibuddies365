<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * CmsPages Controller
 *
 * @property \App\Model\Table\CmsPagesTable $CmsPages
 *
 * @method \App\Model\Entity\CmsPage[] paginate($object = null, array $settings = [])
 */
class CmsPagesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $paginateArray = [
             // 'contain' => ['Users'],
            'order' => ['CmsPages.created' => 'DESC']
        ];

        if ($this->request->is('get')) {

            $conditions = [];

            if (!empty($this->request->getQuery()['search'])) {
                $conditions[] = array('CmsPages.title_en LIKE' => '%'.$this->request->getQuery()['search'].'%');
                $conditions[] = array('CmsPages.meta_description_en LIKE' => '%'.$this->request->getQuery()['search'].'%');
                $conditions[] = array('CmsPages.meta_content_en LIKE' => '%'.$this->request->getQuery()['search'].'%');
                $conditions[] = array('CmsPages.meta_keyword_en LIKE' => '%'.$this->request->getQuery()['search'].'%');
                $conditions[] = array('CmsPages.slug LIKE' => '%'.$this->request->getQuery()['search'].'%');
                $paginateArray['conditions'] = ['OR' => $conditions];
            }
        }
        
        $this->paginate = $paginateArray;
        $cmsPages = $this->paginate($this->CmsPages);

        $this->set(compact('cmsPages'));
        $this->set('_serialize', ['cmsPages']);
    }

    /**
     * View method
     *
     * @param string|null $id Cms Page id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cmsPage = $this->CmsPages->get($id, [
            // 'contain' => ['Users']
        ]);

        $this->set('cmsPage', $cmsPage);
        $this->set('_serialize', ['cmsPage']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $cmsPage = $this->CmsPages->newEntity();
        $this->loadModel('Videos');
        if ($this->request->is('post')) {
			
			$this->request->getData()['user_id'] = $this->Auth->user('id');
		
            $cmsPage = $this->CmsPages->patchEntity($cmsPage, $this->request->getData());
            // if ($this->CmsPages->save($cmsPage)) {
            //     $this->Flash->success(__('The cms page has been saved.'));

            //     return $this->redirect(['action' => 'index']);
            // }
            // $this->Flash->error(__('The cms page could not be saved. Please, try again.'));


            if(!empty($cmsPage->getErrors())){
                foreach ($cmsPage->getErrors() as $key => $value) {
                    foreach ($value as $key => $values) {
                        $this->Flash->error(__($values));
                    }
                }
            }else{               

                // $cmsPage['background_image'] = $cmsPage->background_image;

                if(!empty($this->request->getData()['bg_image']['name']))
                {
                    $file = $this->request->getData()['bg_image']; 
                    if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'uploads/images' . DS . 'background'.$file['name']))
                    {
                        //prepare the filename for database entry
                        $cmsPage['bg_image'] = 'background'.$file['name'];
                    }
                }else{
                    $cmsPage['bg_image'] = 'about-us.jpg';
                }

                if ($this->CmsPages->save($cmsPage)) {
                    $this->Flash->success(__('The cms page has been saved.'));

                    return $this->redirect(['action' => 'index']);
                }else{
                    $this->Flash->error(__('The cms page could not be saved. Please, try again.'));
                }
            }
        }
        // $users = $this->CmsPages->Users->find('list', ['limit' => 200]);
        $this->set(compact('cmsPage'));
        $this->set('_serialize', ['cmsPage']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cms Page id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cmsPage = $this->CmsPages->get($id, [
            'contain' => []
        ]);
        $this->loadModel('Videos');
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bgImage = $cmsPage->bg_image;

            $cmsPage = $this->CmsPages->patchEntity($cmsPage, $this->request->getData());

            if(!empty($cmsPage->getErrors())){
                foreach ($cmsPage->getErrors() as $key => $value) {
                    foreach ($value as $key => $values) {
                        $this->Flash->error(__($values));
                    }
                }
            }else{               

                // $cmsPage['background_image'] = $cmsPage->background_image;

                if(!empty($this->request->getData()['bg_image']['name']))
                {
                    $file = $this->request->getData()['bg_image']; 
                    if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'uploads/images' . DS . 'background'.$file['name']))
                    {
                        //prepare the filename for database entry
                        $cmsPage['bg_image'] = 'background'.$file['name'];
                    }
                }else{
                    $cmsPage['bg_image'] = $bgImage;
                }

                if ($this->CmsPages->save($cmsPage)) {
                    $this->Flash->success(__('The cms page has been saved.'));

                    return $this->redirect(['action' => 'index']);
                }else{
                    $this->Flash->error(__('The cms page could not be saved. Please, try again.'));
                }
            }
        }
        $this->set(compact('cmsPage'));
        $this->set('_serialize', ['cmsPage']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cms Page id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        // echo $id ; die;
        $cmsPage = $this->CmsPages->get($id);
        if ($this->CmsPages->delete($cmsPage)) {
            $this->Flash->success(__('The cms page has been deleted.'));
        } else {
            $this->Flash->error(__('The cms page could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    
    public function backgroundImages()
    {
        $this->loadModel('BackgroundImages');
        $images = $this->BackgroundImages->find('all')->toArray();
        $this->set(compact('images'));
    }


    public function imageEdit($id = null)
    {
        $this->loadModel('BackgroundImages');
        $image = $this->BackgroundImages->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
           
            
            //Check if image has been uploaded
            if(!empty($this->request->getData()['image']['name']))
            {
                $file = $this->request->getData()['image']; //put the  data into a var for easy use
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
                
                if(in_array($ext, $arr_ext))
                {
                    if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/sectionimages' . DS . $file['name']))
                    {
                        $imageFile =  $file['name'];
                    }
                }else{
                    $this->Flash->error(__('Please Upload image files only'));
                }
            }else{
                $imageFile = $image->image;
            }

            $image = $this->BackgroundImages->patchEntity($image, $this->request->getData());
            $image->image = $imageFile;
            if ($this->BackgroundImages->save($image)) {
                $this->Flash->success(__('Section updated successfully'));
                return $this->redirect(['action' => 'backgroundImages']);
            }else{
                $this->Flash->error(__('Something went wrong please try again'));
            }
        }
        $this->set(compact('image'));
    }


    public function upload()
    {
        // pr($this->request->getData()); die;

      move_uploaded_file( $this->request->getData()['file']['tmp_name'], WWW_ROOT . 'uploads/images' . DS . 'image_'.date('Y-m-d-H-i-s').'_'.$this->request->getData()['file']['name']  );

       echo json_encode(array('location' =>  '/uploads/images' . DS . 'image_'.date('Y-m-d-H-i-s').'_'.$this->request->getData()['file']['name']));
      
        die;

    }

    
}
