<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Enquiries Controller
 *
 * @property \App\Model\Table\EnquiriesTable $Enquiries
 *
 * @method \App\Model\Entity\Enquiry[] paginate($object = null, array $settings = [])
 */
class EnquiriesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $paginateArray = [
              'order' => ['Enquiries.created' => 'DESC']
        ];

        if ($this->request->is('get')) {

            $conditions = [];

            if (!empty($this->request->getQuery()['search'])) {
                $conditions[] = array('Enquiries.name LIKE' => '%'.$this->request->getQuery()['search'].'%');
                $conditions[] = array('Enquiries.email LIKE' => '%'.$this->request->getQuery()['search'].'%');
                $conditions[] = array('Enquiries.phone LIKE' => '%'.$this->request->getQuery()['search'].'%');
                $conditions[] = array('Enquiries.message LIKE' => '%'.$this->request->getQuery()['search'].'%');
                $paginateArray['conditions'] = ['OR' => $conditions];
            }
        }
        
        $this->paginate = $paginateArray;
        $enquiries = $this->paginate($this->Enquiries);

        $this->set(compact('enquiries'));
        $this->set('_serialize', ['enquiries']);
    }

    /**
     * View method
     *
     * @param string|null $id Enquiry id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $enquiry = $this->Enquiries->get($id, [
            'contain' => []
        ]);

        $this->set('enquiry', $enquiry);
        $this->set('_serialize', ['enquiry']);
    }

    /**
     * View method
     *
     * @param string|null $id Enquiry id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function supplierView($id = null)
    {
        // echo $id; die;
        $this->loadModel('SupplierEnquiries');
        $enquiry = $this->SupplierEnquiries->get($id, [
            'contain' => []
        ]);

        $this->set('enquiry', $enquiry);
        $this->set('_serialize', ['enquiry']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $enquiry = $this->Enquiries->newEntity();
        if ($this->request->is('post')) {
            $enquiry = $this->Enquiries->patchEntity($enquiry, $this->request->getData());
            if ($this->Enquiries->save($enquiry)) {
                $this->Flash->success(__('The enquiry has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The enquiry could not be saved. Please, try again.'));
        }
        $this->set(compact('enquiry'));
        $this->set('_serialize', ['enquiry']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Enquiry id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $enquiry = $this->Enquiries->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $enquiry = $this->Enquiries->patchEntity($enquiry, $this->request->getData());
            if ($this->Enquiries->save($enquiry)) {
                $this->Flash->success(__('The enquiry has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The enquiry could not be saved. Please, try again.'));
        }
        $this->set(compact('enquiry'));
        $this->set('_serialize', ['enquiry']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Enquiry id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $enquiry = $this->Enquiries->get($id);
        if ($this->Enquiries->delete($enquiry)) {
            $this->Flash->success(__('The enquiry has been deleted.'));
        } else {
            $this->Flash->error(__('The enquiry could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function supplierEnquires()
    {
        // echo "test"; die;
        $this->loadModel('SupplierEnquiries');
        $paginateArray = [
              'order' => ['SupplierEnquiries.created' => 'DESC']
        ];

        if ($this->request->is('get')) {

            $conditions = [];

            if (!empty($this->request->getQuery()['search'])) {
                $conditions[] = array('SupplierEnquiries.name LIKE' => '%'.$this->request->getQuery()['search'].'%');
                $conditions[] = array('SupplierEnquiries.email LIKE' => '%'.$this->request->getQuery()['search'].'%');
                $conditions[] = array('SupplierEnquiries.phone LIKE' => '%'.$this->request->getQuery()['search'].'%');
                $conditions[] = array('SupplierEnquiries.message LIKE' => '%'.$this->request->getQuery()['search'].'%');
                $paginateArray['conditions'] = ['OR' => $conditions];
            }
        }
        
        $this->paginate = $paginateArray;
        $enquiries = $this->paginate($this->SupplierEnquiries);

        $this->set(compact('enquiries'));
        $this->set('_serialize', ['enquiries']);
    }
}
