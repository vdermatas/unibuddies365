<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AssignmentDeliverySchedules Model
 *
 * @property \App\Model\Table\AssignmentOffersTable&\Cake\ORM\Association\BelongsTo $AssignmentOffers
 *
 * @method \App\Model\Entity\AssignmentDeliverySchedule get($primaryKey, $options = [])
 * @method \App\Model\Entity\AssignmentDeliverySchedule newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AssignmentDeliverySchedule[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AssignmentDeliverySchedule|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AssignmentDeliverySchedule saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AssignmentDeliverySchedule patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AssignmentDeliverySchedule[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AssignmentDeliverySchedule findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AssignmentDeliverySchedulesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('assignment_delivery_schedules');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('AssignmentOffers', [
            'foreignKey' => 'assignment_offer_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmptyString('description');

        $validator
            ->date('delivery_date')
            ->requirePresence('delivery_date', 'create')
            ->notEmptyDate('delivery_date');

        $validator
            ->allowEmpty('file')
            ->add('file', [
                'validExtension' => [
                    'rule' => ['extension',['docx','doc','pdf','rtf']], 
                    'message' => __('Please upload file formats properly eg: pdf , docx , rtf')
                ]
            ]);

        $validator
            ->scalar('delivery_notes')
            ->requirePresence('delivery_notes', 'create')
            ->notEmptyString('delivery_notes');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['assignment_offer_id'], 'AssignmentOffers'));

        return $rules;
    }
}
