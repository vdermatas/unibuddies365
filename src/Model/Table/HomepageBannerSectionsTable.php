<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * HomepageBannerSections Model
 *
 * @method \App\Model\Entity\HomepageBannerSection get($primaryKey, $options = [])
 * @method \App\Model\Entity\HomepageBannerSection newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\HomepageBannerSection[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\HomepageBannerSection|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\HomepageBannerSection saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\HomepageBannerSection patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\HomepageBannerSection[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\HomepageBannerSection findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class HomepageBannerSectionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('homepage_banner_sections');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->requirePresence('title', 'create')
            ->notEmptyString('title');

        // $validator
        //     ->scalar('background_image')
        //     ->maxLength('background_image', 255)
        //     ->requirePresence('background_image', 'create')
        //     ->notEmptyFile('background_image');

        $validator
            ->allowEmpty('background_image')
            ->add('background_image', [
                'validExtension' => [
                    'rule' => ['extension',['gif', 'jpeg', 'png', 'jpg']], 
                    'message' => __('Please upload image formats eg: jpg , png ')
                ]
            ]);

        $validator
            ->scalar('content_en')
            ->requirePresence('content_en', 'create')
            ->notEmptyString('content_en');

        $validator
            ->scalar('writer_button')
            ->maxLength('writer_button', 100)
            ->requirePresence('writer_button', 'create')
            ->notEmptyString('writer_button');

        $validator
            ->scalar('student_button')
            ->maxLength('student_button', 100)
            ->requirePresence('student_button', 'create')
            ->notEmptyString('student_button');

        return $validator;
    }
}
