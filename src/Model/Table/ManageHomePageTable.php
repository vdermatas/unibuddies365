<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ManageHomePage Model
 *
 * @method \App\Model\Entity\ManageHomePage get($primaryKey, $options = [])
 * @method \App\Model\Entity\ManageHomePage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ManageHomePage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ManageHomePage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ManageHomePage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ManageHomePage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ManageHomePage findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ManageHomePageTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('manage_home_page');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('best_prices_best_holidays_en', 'create')
            ->notEmpty('best_prices_best_holidays_en');

        $validator
            ->requirePresence('best_prices_best_holidays_spa', 'create')
            ->notEmpty('best_prices_best_holidays_spa');

        $validator
            ->requirePresence('shared_shuttle_en', 'create')
            ->notEmpty('shared_shuttle_en');

        $validator
            ->requirePresence('shared_shuttle_spa', 'create')
            ->notEmpty('shared_shuttle_spa');

        $validator
            ->requirePresence('private_shuttle_en', 'create')
            ->notEmpty('private_shuttle_en');

        $validator
            ->requirePresence('private_shuttle_spa', 'create')
            ->notEmpty('private_shuttle_spa');

        $validator
            ->requirePresence('best_and_cheapest_shuttles_in_cancun_en', 'create')
            ->notEmpty('best_and_cheapest_shuttles_in_cancun_en');

        $validator
            ->requirePresence('best_and_cheapest_shuttles_in_cancun_spa', 'create')
            ->notEmpty('best_and_cheapest_shuttles_in_cancun_spa');

        $validator
            ->requirePresence('whats_app_en', 'create')
            ->notEmpty('whats_app_en');

        $validator
            ->requirePresence('whats_app_spa', 'create')
            ->notEmpty('whats_app_spa');

        return $validator;
    }
}
