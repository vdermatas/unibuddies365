<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WriterProfiles Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\WriterProfile get($primaryKey, $options = [])
 * @method \App\Model\Entity\WriterProfile newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\WriterProfile[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\WriterProfile|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\WriterProfile saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\WriterProfile patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\WriterProfile[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\WriterProfile findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WriterProfilesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('writer_profiles');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('WriterSkills', [
            'foreignKey' => 'writer_profile_id'
        ]);

        $this->hasMany('WriterDocuments', [
            'foreignKey' => 'writer_profile_id'
        ]);

        $this->hasMany('WriterThematics', [
            'foreignKey' => 'writer_profile_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('experience')
            ->requirePresence('experience', 'create')
            ->notEmptyString('experience');

        $validator
            ->scalar('about_me')
            ->requirePresence('about_me', 'create')
            ->notEmptyString('about_me');

        $validator
            ->allowEmpty('resume')
            ->add('resume', [
                'validExtension' => [
                    'rule' => ['extension',['docx','doc','pdf']], 
                    'message' => __('Please upload file formats properly for resume eg: pdf , docx ')
                ]
            ]);

        $validator
            ->allowEmpty('certificate')
            ->add('certificate', [
                'validExtension' => [
                    'rule' => ['extension',['docx','doc','pdf']], 
                    'message' => __('Please upload file formats properly for certificate eg: pdf , docx ')
                ]
            ]);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
