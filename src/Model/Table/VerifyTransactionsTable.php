<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * VerifyTransactions Model
 *
 * @property \App\Model\Table\TxnsTable|\Cake\ORM\Association\BelongsTo $Txns
 *
 * @method \App\Model\Entity\VerifyTransaction get($primaryKey, $options = [])
 * @method \App\Model\Entity\VerifyTransaction newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\VerifyTransaction[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\VerifyTransaction|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\VerifyTransaction patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\VerifyTransaction[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\VerifyTransaction findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class VerifyTransactionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('verify_transactions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('payment_date');

        $validator
            ->allowEmpty('payment_status');

        $validator
            ->allowEmpty('payment_type');

        $validator
            ->allowEmpty('mc_gross');

        $validator
            ->allowEmpty('mc_gross_1');

        $validator
            ->allowEmpty('txn_type');

        $validator
            ->allowEmpty('payer_email');

        $validator
            ->allowEmpty('json_data');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->existsIn(['txn_id'], 'Txns'));

        return $rules;
    }
}
