<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Assignments Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Assignment get($primaryKey, $options = [])
 * @method \App\Model\Entity\Assignment newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Assignment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Assignment|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Assignment saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Assignment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Assignment[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Assignment findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AssignmentsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('assignments');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('AssignmentOffers', [
            'foreignKey' => 'assignment_id',
            'joinType' => 'INNER'
        ]);

        $this->hasOne('RatingByStudent', [
            'foreignKey' => 'assignment_id'
        ]);

        $this->hasOne('RatingByWriter', [
            'foreignKey' => 'assignment_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('title')
            ->maxLength('title', 2551)
            ->requirePresence('title', 'create')
            ->notEmptyString('title');

        $validator
            ->integer('document_type')
            ->requirePresence('document_type', 'create')
            ->notEmptyString('document_type');

        $validator
            ->integer('thematic_unity')
            ->requirePresence('thematic_unity', 'create')
            ->notEmptyString('thematic_unity');

        $validator
            ->integer('language')
            ->requirePresence('language', 'create')
            ->notEmptyString('language');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmptyString('description');

        $validator
            ->integer('assignment_type_value')
            ->requirePresence('assignment_type_value', 'create')
            ->notEmptyString('assignment_type_value');

        $validator
            ->scalar('reference_url')
            ->maxLength('reference_url', 255)
            ->allowEmptyString('reference_url');

        $validator
            ->allowEmpty('attachment_file')
            ->add('attachment_file', [
                'validExtension' => [
                    'rule' => ['extension',['docx','doc','pdf','rtf']], 
                    'message' => __('Please upload file formats properly eg: pdf , docx , rtf')
                ]
            ]);

        $validator
            ->date('delivery_date')
            ->requirePresence('delivery_date', 'create')
            ->notEmptyDate('delivery_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
