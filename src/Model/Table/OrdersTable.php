<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tours Model
 *
 * @property \App\Model\Table\GalleriesTable|\Cake\ORM\Association\BelongsTo $Galleries
 *
 * @method \App\Model\Entity\Tour get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tour newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tour[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tour|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tour patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tour[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tour findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrdersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('orders');
        //$this->setDisplayField('name_en');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

       /* $this->belongsTo('Galleries', [
            'foreignKey' => 'gallery_id',
            'joinType' => 'INNER'
        ]);*/
       $this->hasMany('OrderProducts', [
            'foreignKey' => 'order_id'
        ]);	
		
		// $this->addBehavior('Muffin/Slug.Slug', [
		// 	// Optionally define your custom options here (see Configuration)
  //           'onUpdate' => true 
		// ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    /*public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name_en', 'create')
            ->notEmpty('name_en');

        $validator
            ->requirePresence('name_spa', 'create')
            ->notEmpty('name_spa');

        $validator
            ->requirePresence('meta_description_en', 'create')
            ->notEmpty('meta_description_en');

        $validator
            ->requirePresence('meta_description_spa', 'create')
            ->notEmpty('meta_description_spa');

        $validator
            ->requirePresence('meta_content_en', 'create')
            ->notEmpty('meta_content_en');

        $validator
            ->requirePresence('meta_content_spa', 'create')
            ->notEmpty('meta_content_spa');

        $validator
            ->requirePresence('meta_keyword_en', 'create')
            ->notEmpty('meta_keyword_en');

        $validator
            ->requirePresence('meta_keyword_spa', 'create')
            ->notEmpty('meta_keyword_spa');

        $validator
            ->decimal('rating')
            ->allowEmpty('rating');

        $validator
            ->decimal('price_from')
            ->requirePresence('price_from', 'create')
            ->notEmpty('price_from'); 
            //->range('price_from',['1', null] ,'Negative value in price not allowed');

        $validator
            ->decimal('price_to')
            ->requirePresence('price_to', 'create')
            ->notEmpty('price_to');
            //->nonNegativeInteger('price_to', 'Negative value in price not allowed');

        $validator
            ->requirePresence('description_en', 'create')
            ->notEmpty('description_en');

        $validator
            ->requirePresence('description_spa', 'create')
            ->notEmpty('description_spa');

        $validator
            ->decimal('adult_price')
            ->requirePresence('adult_price', 'create')
            ->notEmpty('adult_price');
            //->nonNegativeInteger('adult_price', 'Negative value in price not allowed');
            

        $validator
            ->decimal('child_price')
            ->requirePresence('child_price', 'create')
            ->notEmpty('child_price');
            //->nonNegativeInteger('child_price', 'Negative value in price not allowed');

        $validator
            ->decimal('promo_adult_price')
            ->requirePresence('promo_adult_price', 'create')
            ->notEmpty('promo_adult_price');
            //->nonNegativeInteger('promo_adult_price', 'Negative value in price not allowed');

        $validator
            ->decimal('promo_child_price')
            ->requirePresence('promo_child_price', 'create')
            ->notEmpty('promo_child_price');
            //->nonNegativeInteger('promo_child_price', 'Negative value in price not allowed');

      

        return $validator;
    }*/

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
   /* public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['gallery_id'], 'Galleries'));

        return $rules;
    }*/
}
