<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Testimonials Model
 *
 * @method \App\Model\Entity\Testimonial get($primaryKey, $options = [])
 * @method \App\Model\Entity\Testimonial newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Testimonial[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Testimonial|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Testimonial patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Testimonial[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Testimonial findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TestimonialsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('testimonials');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('author_name_en', 'create')
            ->notEmpty('author_name_en');

        $validator
            ->requirePresence('author_name_spa', 'create')
            ->notEmpty('author_name_spa');

        $validator
            ->requirePresence('company_name_en', 'create')
            ->notEmpty('company_name_en');

         $validator
            ->requirePresence('company_name_spa', 'create')
            ->notEmpty('company_name_spa');

        $validator
            ->requirePresence('position_en', 'create')
            ->notEmpty('position_en');

        $validator
            ->requirePresence('position_spa', 'create')
            ->notEmpty('position_spa');

        $validator
            ->requirePresence('feeback_en', 'create')
            ->notEmpty('feeback_en');
       
        $validator
            ->requirePresence('feeback_spa', 'create')
            ->notEmpty('feeback_spa');

        return $validator;
    }
}
