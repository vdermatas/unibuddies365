<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * NavigationMenus Model
 *
 * @property \App\Model\Table\NavigationMenusTable|\Cake\ORM\Association\BelongsTo $ParentNavigationMenus
 * @property \App\Model\Table\NavigationMenusTable|\Cake\ORM\Association\HasMany $ChildNavigationMenus
 *
 * @method \App\Model\Entity\NavigationMenu get($primaryKey, $options = [])
 * @method \App\Model\Entity\NavigationMenu newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\NavigationMenu[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\NavigationMenu|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\NavigationMenu patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\NavigationMenu[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\NavigationMenu findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Cake\ORM\Behavior\TreeBehavior
 */
class NavigationMenusTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('navigation_menus');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tree');

        $this->belongsTo('ParentNavigationMenus', [
            'className' => 'NavigationMenus',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('ChildNavigationMenus', [
            'className' => 'NavigationMenus',
            'foreignKey' => 'parent_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['parent_id'], 'ParentNavigationMenus'));

        return $rules;
    }
}
