<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WriterDocuments Model
 *
 * @property \App\Model\Table\WriterProfilesTable&\Cake\ORM\Association\BelongsTo $WriterProfiles
 *
 * @method \App\Model\Entity\WriterDocument get($primaryKey, $options = [])
 * @method \App\Model\Entity\WriterDocument newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\WriterDocument[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\WriterDocument|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\WriterDocument saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\WriterDocument patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\WriterDocument[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\WriterDocument findOrCreate($search, callable $callback = null, $options = [])
 */
class WriterDocumentsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('writer_documents');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('WriterProfiles', [
            'foreignKey' => 'writer_profile_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('document_type')
            ->maxLength('document_type', 255)
            ->requirePresence('document_type', 'create')
            ->notEmptyString('document_type');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['writer_profile_id'], 'WriterProfiles'));

        return $rules;
    }
}
