<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SocialMedias Model
 *
 * @method \App\Model\Entity\SocialMedia get($primaryKey, $options = [])
 * @method \App\Model\Entity\SocialMedia newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SocialMedia[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SocialMedia|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SocialMedia patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SocialMedia[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SocialMedia findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SocialMediasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('social_medias');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('facebook');

        $validator
            ->allowEmpty('twitter');

        $validator
            ->allowEmpty('pinterest');

        $validator
            ->allowEmpty('google');

        $validator
            ->allowEmpty('linkedin');

        return $validator;
    }
}
