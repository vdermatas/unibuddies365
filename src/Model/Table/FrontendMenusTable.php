<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FrontendMenus Model
 *
 * @method \App\Model\Entity\FrontendMenu get($primaryKey, $options = [])
 * @method \App\Model\Entity\FrontendMenu newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\FrontendMenu[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FrontendMenu|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FrontendMenu patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FrontendMenu[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\FrontendMenu findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class FrontendMenusTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('frontend_menus');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('data', 'create')
            ->notEmpty('data');

        return $validator;
    }
}
