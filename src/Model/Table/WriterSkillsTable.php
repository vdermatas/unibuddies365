<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WriterSkills Model
 *
 * @property \App\Model\Table\WritersTable&\Cake\ORM\Association\BelongsTo $Writers
 *
 * @method \App\Model\Entity\WriterSkill get($primaryKey, $options = [])
 * @method \App\Model\Entity\WriterSkill newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\WriterSkill[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\WriterSkill|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\WriterSkill saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\WriterSkill patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\WriterSkill[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\WriterSkill findOrCreate($search, callable $callback = null, $options = [])
 */
class WriterSkillsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('writer_skills');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Writers', [
            'foreignKey' => 'writer_profile_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('languages')
            ->maxLength('languages', 255)
            ->requirePresence('languages', 'create')
            ->notEmptyString('languages');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->existsIn(['writer_id'], 'Writers'));

        return $rules;
    }
}
