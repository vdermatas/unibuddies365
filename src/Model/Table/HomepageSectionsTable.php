<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * HomepageSections Model
 *
 * @method \App\Model\Entity\HomepageSection get($primaryKey, $options = [])
 * @method \App\Model\Entity\HomepageSection newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\HomepageSection[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\HomepageSection|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\HomepageSection saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\HomepageSection patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\HomepageSection[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\HomepageSection findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class HomepageSectionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('homepage_sections');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->requirePresence('title', 'create')
            ->notEmptyString('title');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->allowEmpty('background_image')
            ->add('background_image', [
                'validExtension' => [
                    'rule' => ['extension',['gif', 'jpeg', 'png', 'jpg']], 
                    'message' => __('Please upload image formats eg: jpg , png ')
                ]
            ]);


        $validator
            ->scalar('section_1_title')
            ->maxLength('section_1_title', 255)
            ->allowEmptyString('section_1_title');


        $validator
            ->allowEmpty('section_1_image')
            ->add('section_1_image', [
                'validExtension' => [
                    'rule' => ['extension',['gif', 'jpeg', 'png', 'jpg']], 
                    'message' => __('Please upload image formats eg: jpg , png ')
                ]
            ]);


        $validator
            ->scalar('section_1_content')
            ->allowEmptyString('section_1_content');

        $validator
            ->scalar('section_2_title')
            ->maxLength('section_2_title', 255)
            ->allowEmptyString('section_2_title');

        $validator
            ->allowEmpty('section_2_image')
            ->add('section_2_image', [
                'validExtension' => [
                    'rule' => ['extension',['gif', 'jpeg', 'png', 'jpg']], 
                    'message' => __('Please upload image formats eg: jpg , png ')
                ]
            ]);

        $validator
            ->scalar('section_2_content')
            ->allowEmptyString('section_2_content');

        $validator
            ->scalar('section_3_title')
            ->maxLength('section_3_title', 255)
            ->allowEmptyString('section_3_title');

        $validator
            ->allowEmpty('section_3_image')
            ->add('section_3_image', [
                'validExtension' => [
                    'rule' => ['extension',['gif', 'jpeg', 'png', 'jpg']], 
                    'message' => __('Please upload image formats eg: jpg , png ')
                ]
            ]);

        $validator
            ->scalar('section_3_content')
            ->allowEmptyString('section_3_content');

        $validator
            ->scalar('section_4_title')
            ->maxLength('section_4_title', 255)
            ->allowEmptyString('section_4_title');

        $validator
            ->allowEmpty('section_4_image')
            ->add('section_4_image', [
                'validExtension' => [
                    'rule' => ['extension',['gif', 'jpeg', 'png', 'jpg']], 
                    'message' => __('Please upload image formats eg: jpg , png ')
                ]
            ]);

        $validator
            ->scalar('section_4_content')
            ->allowEmptyString('section_4_content');

        return $validator;
    }
}
