<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WriterDocument Entity
 *
 * @property int $id
 * @property int $writer_profile_id
 * @property string $document_type
 *
 * @property \App\Model\Entity\WriterProfile $writer_profile
 */
class WriterDocument extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'writer_profile_id' => true,
        'document_type' => true,
        'writer_profile' => true
    ];
}
