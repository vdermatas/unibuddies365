<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DropOfLocation Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $location_en
 * @property string $location_spa
 * @property string $img
 * @property string $description_en
 * @property string $description_spa
 * @property string $short_description_en
 * @property string $short_description_spa
 * @property float $adult_price_spa
 * @property float $child_price_en
 * @property float $child_price_spa
 * @property float $adult_round_price_en
 * @property float $adult_round_price_spa
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 */
class DropOfLocation extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
