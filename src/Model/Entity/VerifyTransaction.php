<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * VerifyTransaction Entity
 *
 * @property int $id
 * @property string $txn_id
 * @property string $payment_date
 * @property string $payment_status
 * @property string $payment_type
 * @property string $mc_gross
 * @property string $mc_gross_1
 * @property string $txn_type
 * @property string $payer_email
 * @property string $json_data
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Txn $txn
 */
class VerifyTransaction extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
