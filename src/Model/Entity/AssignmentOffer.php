<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AssignmentOffer Entity
 *
 * @property int $id
 * @property int $assignment_id
 * @property int $user_id
 * @property int $price
 * @property string $notes
 * @property int $status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Assignment $assignment
 * @property \App\Model\Entity\User $user
 */
class AssignmentOffer extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'assignment_id' => true,
        'user_id' => true,
        'price' => true,
        'notes' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'assignment' => true,
        'user' => true
    ];
}
