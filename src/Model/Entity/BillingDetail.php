<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BillingDetail Entity
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $door_no
 * @property string $street_name
 * @property string $landmark
 * @property string $address_type
 * @property string $city
 * @property string $zip
 * @property string $phone
 * @property string $email
 * @property int $user_id
 * @property int $frontend_order_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\State $state
 * @property \App\Model\Entity\Country $country
 * @property \App\Model\Entity\Transection[] $transections
 * @property \App\Model\Entity\Booking[] $bookings
 */
class BillingDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
