<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WriterProfile Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $resume
 * @property int $experience
 * @property string $about_me
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 */
class WriterProfile extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'resume' => true,
        'experience' => true,
        'about_me' => true,
        'bank_code' => true,
        'iban' => true,
        'studied_institute' => true,
        'course' => true,
        'course_from' => true,
        'course_to' => true,
        'certificate' => true,
        'created' => true,
        'modified' => true,
        'user' => true
    ];
}
