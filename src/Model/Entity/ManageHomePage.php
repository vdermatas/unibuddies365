<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ManageHomePage Entity
 *
 * @property int $id
 * @property string $best_prices_best_holidays_en
 * @property string $best_prices_best_holidays_spa
 * @property string $shared_shuttle_en
 * @property string $shared_shuttle_spa
 * @property string $private_shuttle_en
 * @property string $private_shuttle_spa
 * @property string $best_and_cheapest_shuttles_in_cancun_en
 * @property string $best_and_cheapest_shuttles_in_cancun_spa
 * @property string $whats_app_en
 * @property string $whats_app_spa
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class ManageHomePage extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
