<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * HomepageBannerSection Entity
 *
 * @property int $id
 * @property string $title
 * @property string $background_image
 * @property string $content
 * @property string $writer_button
 * @property string $student_button
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class HomepageBannerSection extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'background_image' => true,
        'content_en' => true,
        'content_gre' => true,
        'writer_button' => true,
        'student_button' => true,
        'created' => true,
        'modified' => true
    ];
}
