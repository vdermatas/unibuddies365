<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WriterSkill Entity
 *
 * @property int $id
 * @property int $writer_id
 * @property string $languages
 *
 * @property \App\Model\Entity\Writer $writer
 */
class WriterSkill extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'writer_profile_id' => true,
        'languages' => true
        // 'writer_profile' => true
    ];
}
