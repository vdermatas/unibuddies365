<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Tour Entity
 *
 * @property int $id
 * @property string $name_en
 * @property string $name_spa
 * @property string $meta_description_en
 * @property string $meta_description_spa
 * @property string $meta_content_en
 * @property string $meta_content_spa
 * @property string $meta_keyword_en
 * @property string $meta_keyword_spa
 * @property string $slug
 * @property float $rating
 * @property float $price_from
 * @property float $price_to
 * @property string $description_en
 * @property string $description_spa
 * @property float $adult_price
 * @property float $child_price
 * @property float $promo_adult_price
 * @property float $promo_child_price
 * @property float $round_trip_child
 * @property float $round_trip_adult
 * @property int $gallery_id
 * @property int $is_deleted
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Gallery $gallery
 */
class Tour extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
