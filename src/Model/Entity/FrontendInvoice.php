<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FrontendInvoice Entity
 *
 * @property int $id
 * @property string $invoice_number
 * @property float $sub_total
 * @property float $vat
 * @property float $delivery_charge
 * @property float $total
 * @property int $frontend_order_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\FrontendOrder $frontend_order
 */
class FrontendInvoice extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
