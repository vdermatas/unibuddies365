<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Booking Entity
 *
 * @property int $id
 * @property int $booking_type
 * @property int $adults
 * @property int $childs
 * @property int $tour_id
 * @property int $drop_of_location_id
 * @property int $is_airport
 * @property string $pick_up_location
 * @property int $id_round_trip
 * @property int $arrival
 * @property int $departure
 * @property int $tour_booking_date
 * @property int $total_price
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Tour $tour
 * @property \App\Model\Entity\DropOfLocation $drop_of_location
 */
class Booking extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
