<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * HomepageSection Entity
 *
 * @property int $id
 * @property string $title
 * @property string|null $description
 * @property string|null $background_image
 * @property string|null $section_1_title
 * @property string|null $section_1_image
 * @property string|null $section_1_content
 * @property string|null $section_2_title
 * @property string|null $section_2_image
 * @property string|null $section_2_content
 * @property string|null $section_3_title
 * @property string|null $section_3_image
 * @property string|null $section_3_content
 * @property string|null $section_4_title
 * @property string|null $section_4_image
 * @property string|null $section_4_content
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class HomepageSection extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'description' => true,
        'background_image' => true,
        'section_1_title' => true,
        'section_1_image' => true,
        'section_1_content' => true,
        'section_2_title' => true,
        'section_2_image' => true,
        'section_2_content' => true,
        'section_3_title' => true,
        'section_3_image' => true,
        'section_3_content' => true,
        'section_4_title' => true,
        'section_4_image' => true,
        'section_4_content' => true,
        'created' => true,
        'modified' => true
    ];
}
