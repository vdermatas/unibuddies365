<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Widget Entity
 *
 * @property int $id
 * @property string $site_maps
 * @property string $quick_links
 * @property string $social_icons
 * @property string $footer_content
 * @property string $contact_address
 * @property string $contact_telephone
 * @property string $contact_email
 * @property string $created
 * @property string $modified
 */
class Widget extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
