<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Invoice Entity
 *
 * @property int $id
 * @property string $invoice_number
 * @property int $invoice_to
 * @property \Cake\I18n\FrozenDate $date_generated
 * @property string $customer_message
 * @property float $subtotal
 * @property float $vat
 * @property float $payments
 * @property float $total
 * @property float $delivery_charge
 * @property float $extra_charge
 * @property string $extra_charge_reason
 * @property int $is_vat_included
 * @property int $is_invoice_paid
 * @property int $invoice_payment_method
 * @property int $is_deleted
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\InvoiceProduct[] $invoice_products
 */
class Invoice extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
