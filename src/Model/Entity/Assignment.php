<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Assignment Entity
 *
 * @property int $id
 * @property string $title
 * @property int $document_type
 * @property int $thematic_unity
 * @property int $language
 * @property string $description
 * @property int $assignment_type_by
 * @property int $assignment_type_value
 * @property string|null $reference_url
 * @property string|null $attachment_file
 * @property \Cake\I18n\FrozenDate $delivery_date
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 */
class Assignment extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'document_type' => true,
        'thematic_unity' => true,
        'language' => true,
        'description' => true,
        'assignment_type_by' => true,
        'assignment_type_value' => true,
        'reference_url' => true,
        'attachment_file' => true,
        'delivery_date' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'user' => true
    ];
}
