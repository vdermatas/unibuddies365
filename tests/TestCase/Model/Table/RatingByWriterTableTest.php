<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RatingByWriterTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RatingByWriterTable Test Case
 */
class RatingByWriterTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RatingByWriterTable
     */
    public $RatingByWriter;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.RatingByWriter',
        'app.Assignments'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RatingByWriter') ? [] : ['className' => RatingByWriterTable::class];
        $this->RatingByWriter = TableRegistry::getTableLocator()->get('RatingByWriter', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RatingByWriter);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
