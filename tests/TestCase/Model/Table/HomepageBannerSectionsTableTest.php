<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HomepageBannerSectionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HomepageBannerSectionsTable Test Case
 */
class HomepageBannerSectionsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\HomepageBannerSectionsTable
     */
    public $HomepageBannerSections;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.HomepageBannerSections'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('HomepageBannerSections') ? [] : ['className' => HomepageBannerSectionsTable::class];
        $this->HomepageBannerSections = TableRegistry::getTableLocator()->get('HomepageBannerSections', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->HomepageBannerSections);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
