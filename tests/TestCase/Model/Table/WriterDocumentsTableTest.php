<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WriterDocumentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WriterDocumentsTable Test Case
 */
class WriterDocumentsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\WriterDocumentsTable
     */
    public $WriterDocuments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.WriterDocuments',
        'app.WriterProfiles'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('WriterDocuments') ? [] : ['className' => WriterDocumentsTable::class];
        $this->WriterDocuments = TableRegistry::getTableLocator()->get('WriterDocuments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WriterDocuments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
