<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RatingByStudentTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RatingByStudentTable Test Case
 */
class RatingByStudentTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RatingByStudentTable
     */
    public $RatingByStudent;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.RatingByStudent',
        'app.Assignments'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RatingByStudent') ? [] : ['className' => RatingByStudentTable::class];
        $this->RatingByStudent = TableRegistry::getTableLocator()->get('RatingByStudent', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RatingByStudent);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
