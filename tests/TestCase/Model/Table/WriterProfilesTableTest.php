<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WriterProfilesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WriterProfilesTable Test Case
 */
class WriterProfilesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\WriterProfilesTable
     */
    public $WriterProfiles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.WriterProfiles',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('WriterProfiles') ? [] : ['className' => WriterProfilesTable::class];
        $this->WriterProfiles = TableRegistry::getTableLocator()->get('WriterProfiles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WriterProfiles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
