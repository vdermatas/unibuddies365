<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AssignmentDeliverySchedulesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AssignmentDeliverySchedulesTable Test Case
 */
class AssignmentDeliverySchedulesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AssignmentDeliverySchedulesTable
     */
    public $AssignmentDeliverySchedules;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.AssignmentDeliverySchedules',
        'app.AssignmentOffers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AssignmentDeliverySchedules') ? [] : ['className' => AssignmentDeliverySchedulesTable::class];
        $this->AssignmentDeliverySchedules = TableRegistry::getTableLocator()->get('AssignmentDeliverySchedules', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AssignmentDeliverySchedules);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
