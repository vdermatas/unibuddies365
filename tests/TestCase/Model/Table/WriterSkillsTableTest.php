<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WriterSkillsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WriterSkillsTable Test Case
 */
class WriterSkillsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\WriterSkillsTable
     */
    public $WriterSkills;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.WriterSkills',
        'app.Writers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('WriterSkills') ? [] : ['className' => WriterSkillsTable::class];
        $this->WriterSkills = TableRegistry::getTableLocator()->get('WriterSkills', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WriterSkills);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
