<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WriterThematicsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WriterThematicsTable Test Case
 */
class WriterThematicsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\WriterThematicsTable
     */
    public $WriterThematics;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.WriterThematics',
        'app.WriterProfiles'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('WriterThematics') ? [] : ['className' => WriterThematicsTable::class];
        $this->WriterThematics = TableRegistry::getTableLocator()->get('WriterThematics', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WriterThematics);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
