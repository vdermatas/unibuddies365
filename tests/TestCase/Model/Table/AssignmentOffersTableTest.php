<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AssignmentOffersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AssignmentOffersTable Test Case
 */
class AssignmentOffersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AssignmentOffersTable
     */
    public $AssignmentOffers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.AssignmentOffers',
        'app.Assignments',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AssignmentOffers') ? [] : ['className' => AssignmentOffersTable::class];
        $this->AssignmentOffers = TableRegistry::getTableLocator()->get('AssignmentOffers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AssignmentOffers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
