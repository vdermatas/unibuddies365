<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HomepageSectionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HomepageSectionsTable Test Case
 */
class HomepageSectionsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\HomepageSectionsTable
     */
    public $HomepageSections;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.HomepageSections'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('HomepageSections') ? [] : ['className' => HomepageSectionsTable::class];
        $this->HomepageSections = TableRegistry::getTableLocator()->get('HomepageSections', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->HomepageSections);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
