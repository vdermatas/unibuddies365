<?php
include('config.php');
// Open File
$file = 'mapping.txt';

$post = $_POST;
// has user input
if($post && count($post) && isset($post['inputName'])) {
	$newLine = $_SERVER['SSL_CLIENT_S_DN'] .','. $post['inputName'];
	$newLine = substr($newLine, +3);
	$file = fopen($file, 'a');
	fwrite($file, $newLine . "\n");
	fclose($file);
	header("Location: http://".$_SERVER['HTTP_HOST']);
	exit;
}
// no-input - show form
else { ?>
<h1>myIP Registration Form</h1>
Welcome to the myIP<sup>SM</sup> Registration Page.  Here we collect information about the owner of each myIP<sup>SM</sup> for future reference.  
<p>
<b>PLEASE NOTE:</b> This information will be used in the event your myIP<sup>SM</sup> is compromised and needs to be revoked. For your information, the answers you provide are stored in a '1 Way Hash', which means we cannot see your answers.  They can only be used to confirm the answers you provide in the future when revoking your myIP<sup>SM</sup> Credential.<p><b>PLEASE PRINT:</b> Please print this page after you fill in the form and keep it in a secure space.  

<table border="1" style="width:100%">
<tr>
<td width="50%">
<h2>myIP Information</h2>
Information from your myIP<sup>SM</sup> Credential:<p>
<?php echo 'Name / Pseudonym: <b>               ' . ($pseudo ? $pseudo : 'phpdevelopernk') . '</b><br> '; ?>
<?php echo 'Organization:<b>      ' . ($pseudo ? $pseudo : $rest = 'phpdevelopernk') . '</b><br>'; ?>
<?php echo 'Organizational Unit#:<b>        ' . ($pseudo ? $pseudo : 'phpdevelopernk') . '</b><br>'; ?>
<?php echo 'Email Address is:<b>  ' . ($pseudo ? $pseudo : $rest = 'phpdevelopernk') . '<br></b>'; ?>
<?php echo 'Unique Identifier:<b> ' . ($pseudo ? $pseudo : $rest = 'phpdevelopernk') . '<br></b>'; ?>
<?php echo 'Serial#:<b> ' . ($pseudo ? $pseudo : $rest = 'phpdevelopernk') . '<br></b>'; ?>
<?php echo 'Issuer:<b>  ' . ($pseudo ? $pseudo : $rest = 'phpdevelopernk') . '<br><br></b>'; ?>
</td>
<td width="50%">
<h2>Shared Secrets</h2>

In the event your myIP<sup>SM</sup> credential is destroyed or compromised, it should be revoked. This can be done via a Shared Secret. 
<p>
By answering the questions below, we can confirm your identity in the event you need to revoke your myIP<sup>SM</sup> credential. 
<?php 
        $ques_flag = array();
        $sql = "SELECT * FROM `question`";
        $result = mysqli_query($conn,$sql);
       $option = '';
        if (mysqli_num_rows($result) > 0) {
         while($row = mysqli_fetch_assoc($result)) {
               $option .= '<option value="'.$row["id"].'">'.$row["question"].'</option>';
                
            }
        }
?>
<form action="form.php" method="post">
        <p>
<?php echo 'Random 15 Character Passphrase (No Spaces): <label for="test2" ></label><input class="password" type="text" size="20" name="phassphrase">' ;?><br><br>
1st Question: <select name="ques[1][]" 1stquestion="1stquestion">

<?= $option; ?>
</select>        
<br>
<label for="inputemail">1st Secret Answer (25 Chars):</label>
        <input type="text" size="25" name="ques[1][]"> <br>
2nd Question: <select name="ques[2][]" 2ndquestion="2ndquestion">
<?= $option; ?>
</select><br>
<label for="inputemail">2nd Secret Answer (25 Chars):</label>
        <input type="text" size="25" name="ques[2][]"> <br>
3rd Question: <select name="ques[3][]" 3rdquestion="3rdquestion">
<?= $option; ?>
</select><br>
<label for="inputemail">3rd Secret Answer (25 Chars):</label>
        <input type="text" size="25" name="ques[3][]"> <br>
4th Question: <select name="ques[4][]" 4thquestion="4thquestion">
<?= $option; ?>
</select><br>
<label for="inputemail">4th Secret Answer (25 Chars):</label>
        <input type="text" size="25" name="ques[4][]"> <br>
5th Question: <select name="ques[5][]" 5thquestion="5thquestion">
<?= $option; ?>
</select><br>
<label for="inputemail">5th Secret Answer (25 Chars):</label>
        <input type="text" size="25" name="ques[5][]"> <br>
<br>
        <input type="submit" name="submit" class="submit_form" value="Submit">
        <!-- <input type="Reset" value="Reset"> -->
</form>

</td></tr></table>

<?php } ?>

