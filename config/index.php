<?php
include('config.php');
session_start();
$_SESSION = array();
session_destroy();
include_once 'functions.php';
$pseudo = findMatches();
?>

<!DOCTYPE html>
  <html lang="en-US">
    <head>
      <meta http-equiv="pragma" content="no-cache" />
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
      <link rel="profile" href="http://gmpg.org/xfn/11" />
      <link rel="pingback" href="http://50.87.195.207/pseudonym/xmlrpc.php" />
      <title>OMGTV &#8211; Pseudo-Nym</title>
      <link rel='dns-prefetch' href='//s.w.org' />
      <link rel="alternate" type="application/rss+xml" title="Pseudo-Nym &raquo; Feed" href="http://50.87.195.207/pseudonym/feed/" />
      <link rel="alternate" type="application/rss+xml" title="Pseudo-Nym &raquo; Comments Feed" href="http://50.87.195.207/pseudonym/comments/feed/" />
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='animate-css'  href='http://50.87.195.207/pseudonym/wp-content/themes/oneline-lite/css/animate.css?ver=1.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='http://50.87.195.207/pseudonym/wp-content/themes/oneline-lite/css/font-awesome/css/font-awesome.css?ver=1.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='bx-slider-css'  href='http://50.87.195.207/pseudonym/wp-content/themes/oneline-lite/css/bxslider.css?ver=1.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='oneline-lite-style-css'  href='http://50.87.195.207/pseudonym/wp-content/themes/one-page-club/style.css?ver=4.9' type='text/css' media='all' />
<style id='oneline-lite-style-inline-css' type='text/css'>
.demo-image{ background-color:#; }
</style>
<link rel='stylesheet' id='onepageclub-parent-style-css'  href='http://50.87.195.207/pseudonym/wp-content/themes/oneline-lite/style.css?ver=4.9' type='text/css' media='all' />
<link rel='stylesheet' id='one-page-club-style-css'  href='http://50.87.195.207/pseudonym/wp-content/themes/one-page-club/onepageclub.css?ver=4.9' type='text/css' media='all' />
<script type='text/javascript' src='http://50.87.195.207/pseudonym/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='http://50.87.195.207/pseudonym/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='http://50.87.195.207/pseudonym/wp-content/themes/oneline-lite/js/classie.js?ver=4.9'></script>
<link rel='https://api.w.org/' href='http://50.87.195.207/pseudonym/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://50.87.195.207/pseudonym/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://50.87.195.207/pseudonym/wp-includes/wlwmanifest.xml" />
<meta name="generator" content="WordPress 4.9" />
<link rel="canonical" href="http://50.87.195.207/pseudonym/omgtv/" />
<link rel='shortlink' href='http://50.87.195.207/pseudonym/?p=49' />
<link rel="alternate" type="application/json+oembed" href="http://50.87.195.207/pseudonym/wp-json/oembed/1.0/embed?url=http%3A%2F%2F50.87.195.207%2Fpseudonym%2Fomgtv%2F" />
<link rel="alternate" type="text/xml+oembed" href="http://50.87.195.207/pseudonym/wp-json/oembed/1.0/embed?url=http%3A%2F%2F50.87.195.207%2Fpseudonym%2Fomgtv%2F&#038;format=xml" />
    </head>
    <body class="page-template page-template-blank-page page-template-blank-page-php page page-id-49 index"  >
      <div class="overlayloader1">
        <div class="loader1">&nbsp;</div>
      </div>
<!-- script to split menu -->

<!-- script to split menu -->
<div id="header" class="header      " >
        <div class="container clearfix">
          <div id="logo">
                        <h1 class="site-title"><a href="http://50.87.195.207/pseudonym/" rel="home">Pseudo-Nym</a></h1>
                    </div>
          <div id="main-menu-wrapper">
            <a href="#" id="pull" class="toggle-mobile-menu"></a>
            <nav class="navigation clearfix mobile-menu-wrapper">
              <ul class="menu" id="menu"><li class="page_item page-item-11"><a href="http://50.87.195.207/pseudonym/a-homepage-section/">A homepage section</a></li>
<li class="page_item page-item-8"><a href="http://50.87.195.207/pseudonym/about/">About</a></li>
<li class="page_item page-item-10"><a href="http://50.87.195.207/pseudonym/blog/">Blog</a></li>
<li class="page_item page-item-9"><a href="http://50.87.195.207/pseudonym/contact/">Contact</a></li>
<li class="page_item page-item-42"><a href="http://50.87.195.207/pseudonym/demo/">Demo</a></li>
<li class="page_item page-item-7"><a href="http://50.87.195.207/pseudonym/home/">Home</a></li>
<li class="page_item page-item-57"><a href="http://50.87.195.207/pseudonym/omg-tv/">OMG-TV</a></li>
<li class="page_item page-item-49 current_page_item"><a href="http://50.87.195.207/pseudonym/omgtv/">OMGTV</a></li>
<li class="page_item page-item-2"><a href="http://50.87.195.207/pseudonym/sample-page/">Sample Page</a></li>
<li class="page_item page-item-32"><a href="http://50.87.195.207/pseudonym/">Welcome</a></li>
</ul>            </nav>
          </div>
        </div>
</div>
      <div class="clearfix"></div><style>
#header,.footer-wrapper,.foot-copyright{
	display:none;
}


</style>
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
<br>

<?php
	if($pseudo === false) {
	include 'form_master2nd.php'; }
?>


<hr>
<p>
<div id="blank-page" class="clearfix">
<div class="content">
				<p><hr>
		</div>
</div>
<div class="clear"></div>
<div class="footer-wrapper"><!-- Footer wrapper start -->
</div>
<div class="foot-copyright">
<div class="svg-top-container">
        <svg xmlns="http://www.w3.org/2000/svg" width="0" version="1.1" viewBox="0 0 100 100" preserveAspectRatio="none">
        <path d="M0 100 L100 100 L100 2 L0 100 Z" stroke-width="0"></path>
      </svg>
</div><div class="footer-content">

<span class="text-footer">
<a href="http://50.87.195.207/pseudonym/" rel="home">Pseudo-Nym</a>
<a href="https://themehunk.com/">Powered by ThemeHunk</a>
</span>
</div>
</div>
<script>
  $(document).on('click', '.submit_form', function(e){
    e.preventDefault();
    if($(".password").val() == ''){
      return false;
      $(".password").css('border', '1px solid red');
      $('Please enter password').insertAfter(".password")
    }else{
      $('.ques_form').submit();
    }
  });
</script>
</body>
</html>
<!--Generated by Endurance Page Cache-->
