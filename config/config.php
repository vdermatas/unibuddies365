<?php

return [
    'Calculations' => [
        'INVOICE_VAT_PERCENTAGE' => 12 ,
        'CURRENCY' => 'USD'
    ],
    'ApplicationName' => [
    	'Name' => 'Unibuddies365'
    ],
];