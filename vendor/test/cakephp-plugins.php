<?php
$baseDir = dirname(dirname(__FILE__));
return [
    'plugins' => [
        'AdminLTE' => $baseDir . '/vendor/maiconpinto/cakephp-adminlte-theme/',
        'Alaxos' => $baseDir . '/vendor/alaxos/cakephp3-libs/',
        'Bake' => $baseDir . '/vendor/cakephp/bake/',
        'Burzum/FileStorage' => $baseDir . '/vendor/burzum/file-storage/',
        'CakephpBlueimpUpload' => $baseDir . '/vendor/alaxos/cakephp3-blueimp-upload/',
        'CakephpJqueryFileUpload' => $baseDir . '/vendor/hashmode/cakephp-jquery-file-upload/',
        'CakephpTinymceElfinder' => $baseDir . '/vendor/hashmode/cakephp-tinymce-elfinder/',
        'ContactManager' => $baseDir . '/plugins/ContactManager/',
        'DebugKit' => $baseDir . '/vendor/cakephp/debug_kit/',
        'Migrations' => $baseDir . '/vendor/cakephp/migrations/',
        'Muffin/Slug' => $baseDir . '/vendor/muffin/slug/',
        'Muffin/Trash' => $baseDir . '/vendor/muffin/trash/',
        'WyriHaximus/TwigView' => $baseDir . '/vendor/wyrihaximus/twig-view/'
    ]
];