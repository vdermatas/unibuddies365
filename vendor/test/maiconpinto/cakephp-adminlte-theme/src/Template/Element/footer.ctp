<?php
$file = $theme['folder'] . DS . 'src' . DS . 'Template' . DS . 'Element' . DS . 'footer.ctp';

if (file_exists($file)) {
    ob_start();
    include_once $file;
    echo ob_get_clean();
} else {
?>
<footer class="main-footer">
   <div class="pull-right hidden-xs">
        
    </div>
    <strong>Copyright &copy; 2017-2018 <a href="#">Cheolis Food</a>.</strong> All rights
    reserved.
</footer>
<?php } ?>

 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="box box-info">
            <div class="box-header">
              <i class="fa fa-envelope"></i>
              <h3 class="box-title">Quick Email</h3>              
            </div>
            <div class="box-body">
            	<?php if($this->request->session()->read('Auth.Admin.user_type_id') == 1) { ?>
              <form action="<?php echo $this->Url->build('/admin/users/sendEmail'); ?>" method="post" id='quick_email'>
               <?php }else{?>
              <form action="<?php echo $this->Url->build('/supplier/users/sendEmail'); ?>" method="post" id='quick_email'>
         		 <?php } ?>
                <div class="form-group">
                  <input type="text" class="form-control" name="subject"  required="true"  placeholder="Subject">
                </div>
                <div>
                  <textarea  name="message" class="textarea" placeholder="Message"  required="true"  style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                </div>
                <div class="box-footer clearfix">
                <button type="submit" class="pull-right btn btn-default" id="sendEmail">Send
                  <i class="fa fa-arrow-circle-right"></i></button>
              </div>
              </form>
            </div>
            
          </div>
        </div>
      </div>
      
    </div>
  </div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#quick_email').validate({
        rules:{
            subject:"required",
            message:"required"
        },
        messages:{
            subject:"Please enter subject",
            message:"Please enter message"
        }
    });
  });
</script>