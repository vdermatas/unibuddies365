<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo isset($theme['title']) ? $theme['title'] : 'AdminLTE 2'; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <?php echo $this->Html->css('AdminLTE./bootstrap/css/bootstrap'); ?>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        
    <!-- Theme style -->
    <?php echo $this->Html->css('AdminLTE.AdminLTE.min'); ?>
    <?php echo $this->Html->css('AdminLTE.bootstrap-iconpicker.min'); ?>
<!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <?php echo $this->Html->css('AdminLTE.skins/skin-blue'); ?>

    <!-- jQuery 2.1.4 -->
    <?php echo $this->Html->script('AdminLTE./plugins/jQuery/jQuery-2.1.4.min'); ?>
    <?php echo $this->Html->script('AdminLTE./plugins/bs-iconpicker/js/iconset/iconset-fontawesome-4.7.0.min.js'); ?>
    <?php echo $this->Html->script('AdminLTE./plugins/bs-iconpicker/js/bootstrap-iconpicker.js'); ?>

    <?php echo $this->Html->script('AdminLTE./plugins/menu-editor/jquery-menu-editor.min.js'); ?>
    <?php echo $this->Html->script('jquery.validate.min'); ?>
    <?php echo $this->Html->script('jquery.float'); ?>
    <?php echo $this->Html->script('jquery.floatCategories'); ?>
    <?php echo $this->Html->script('underscore.min.js'); ?>
    <?php echo $this->Html->script('moment.js'); ?>
    <?php echo $this->Html->script('jquery.dataTables.min.js'); ?>

    <?php echo $this->Html->css('frontend/bootstrap-datetimepicker'); ?>
    <?php echo $this->Html->script('frontend/bootstrap-datetimepicker.min'); ?>

    
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script> -->
    <?php echo $this->fetch('css'); ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="#" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini">C-F</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><?php echo $this->Html->image('frontend/logo.png',['alt' => 'logo', 'class' => 'img-responsive img-header']); ?></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <?php echo $this->element('nav-top') ?>
        </header>

        <!-- Left side column. contains the sidebar -->
        <?php echo $this->element('aside-main-sidebar'); ?>

        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            <?php echo $this->Flash->render(); ?>
            <?php echo $this->Flash->render('auth'); ?>
            <?php echo $this->fetch('content'); ?>

        </div>
        <!-- /.content-wrapper -->

        <?php echo $this->element('footer'); ?>

        <!-- Control Sidebar -->
        <?php echo $this->element('aside-control-sidebar'); ?>

        <!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- Bootstrap 3.3.5 -->
<?php echo $this->Html->script('AdminLTE./bootstrap/js/bootstrap'); ?>
<!-- SlimScroll -->
<?php echo $this->Html->script('AdminLTE./plugins/slimScroll/jquery.slimscroll.min'); ?>
<!-- FastClick -->
<?php echo $this->Html->script('AdminLTE./plugins/fastclick/fastclick'); ?>
<!-- AdminLTE App -->
<?php echo $this->Html->script('AdminLTE.AdminLTE.min'); ?>
<!-- AdminLTE for demo purposes -->

<?php echo $this->Html->script('jquery.validate'); ?>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<?php echo $this->fetch('script'); ?>
<?php echo $this->fetch('scriptBotton'); ?>

<script type="text/javascript">
    $(document).ready(function(){
        $(".navbar .menu").slimscroll({
            height: "200px",
            alwaysVisible: false,
            size: "3px"
        }).css("width", "100%");

        var a = $('a[href="<?php echo $this->request->webroot . $this->request->url ?>"]');
        if (!a.parent().hasClass('treeview')) {
            a.parent().addClass('active').parents('.treeview').addClass('active');
        }

        $('#example').DataTable();
    });
</script>

<?php
    $arrControllers = array("Testimonials", "CmsPages",'Articles', 'Products' ,'Tours','ManageHomePage','Widgets','EmailTemplates','Homepages'); 
    $arrActions = array("add", 'edit','slider','editSlider');
    
    if(in_array($this->request->controller,$arrControllers )) {
        if(in_array($this->request->action,$arrActions )) { ?>

        <script>           
            $(document).ready(function() {
                // tinymce init
                tinymce.init({
                    file_browser_callback : elFinderBrowser,
                    selector: 'textarea',
                    height: 500,
                    theme: 'modern',
                    relative_urls : false,
                    remove_script_host : false,
                    convert_urls : true,
                    plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
                    ],
                    toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                    toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
                    image_advtab: true,
                    templates: [
                    { title: 'Test template 1', content: 'Test 1' },
                    { title: 'Test template 2', content: 'Test 2' }
                    ],
                    content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                    '//www.tinymce.com/css/codepen.min.css'
                    ]
                });
            });
        </script>
<?php } } ?>

<?php echo $this->JqueryFileUpload->loadCss(); ?>
<?php echo $this->JqueryFileUpload->loadScripts(); ?>

<script>
/*jslint unparam: true */
/*global window, $ */
$(function () {
	'use strict';
	// Change this to the location of your server-side upload handler:
	var url = 'galleries/img_upload/';
	$('#fileupload').fileupload({
		url: url,
		dataType: 'json',
		success: function (data) {
// console.log(data);
            var imgHtml = '<div class="col-xs-6 col-md-3"><a href="#" class="thumbnail"><span class="delete_img pull-right" style="color:red" data-img-id="'+data.id+'">x</span><img class="" src="../../food'+data.url+'"  data-img-id="'+data.id+'" alt="Loading Image..."></a></div>';
			$('.image-success').html('Image Updated Successfully');
			$('#imgHtml').append(imgHtml);
            
			
		},
		progressall: function (e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$('#progress .progress-bar').css(
			'width',
			progress + '%'
			);
		}
	}).bind('fileuploadsubmit', function (e, data) {
        data.formData = {
            'id': $("#gpid").attr("data-gpid")
        };
    }).prop('disabled', !$.support.fileInput)
		.parent().addClass($.support.fileInput ? undefined : 'disabled');
	});
</script>

<script>
	(function($){
		$(".view_images").on("click",function() {
			var gid = $(this).attr("data-gid");
			$("#gpid").attr("data-gpid",gid);
			
			$("body").addClass("modal-open");
			$("body").append('<div class="modal-backdrop fade in"></div>');
			
			$.ajax({
				url: 'galleries/getImages/'+gid,
				dataType: 'json',
				success: function( result ) {
					$("#show_image_upload #progress .progress-bar").css("width","0px");
					$('#imgHtml').html('');
					$("body").removeClass("modal-open");
					$("body").find('.modal-backdrop').remove();
					if(result.gallery.images.length != 0) {
						$.each(result.gallery.images, function (index, file) {
							var defaultTxt = 'Set as default';

                            if(result.gallery.default_image_id == file.id) {
                                defaultTxt = 'Default';
                            }

							var imgHtml = '<div class="col-xs-6 col-md-3"><a href="#" class="thumbnail"> <span class="delete_img pull-right" style="color:red" data-img-id="'+file.id+'">x</span><img class="" src="../../food'+file.url+'"  alt="Loading Image..."><center><span class="setAsDefault" data-img-id="'+file.id+'" data-gallery-id="'+result.gallery.id+'">'+defaultTxt+'</span></center></a></div>';
							
							$('#imgHtml').append(imgHtml);

						});
					}
					$("#show_image_upload").modal("show");
				},
				error: function( req, status, err ) {
					
				}
			});
		});
		
		$(document).on( "click", ".delete_img",function() {
			var $this = $(this);
			var img_id = $(this).attr("data-img-id");
            $('.image-success').html('');
			var result = confirm("Are you sure you want to delete this image?");
			if (result) {
				$.ajax({
					url: 'galleries/delete_image/'+img_id,
					dataType: 'json',
					success: function( result ) {
						$this.closest('div.col-md-3').remove();
					},
					error: function( req, status, err ) {
						
					}
				});
			}
		});
        // alert('hi');

        $(document).on( "click", ".setAsDefault",function() {
            var $this = $(this);
            var img_id = $(this).attr("data-img-id");
            var gallery_id = $(this).attr("data-gallery-id");
          
            $.ajax({
                url: 'galleries/set_as_default/'+gallery_id+'/'+img_id,
                dataType: 'json',
                success: function( result ) {
                    $(document).find(".setAsDefault").html("Set as default");
                    $this.html("Default");
                },
                error: function( req, status, err ) {
                    
                }
            });
           
        });

		
	})(jQuery)
</script>

<style type="text/css">
.img-header{
    width: 90px;
    margin-left: 50px;
}
@media screen and (max-width: 799px) {
    .img-header{
        width: 95px;
        margin-left: 90px;
    }
}
</style>
</body>
</html>
