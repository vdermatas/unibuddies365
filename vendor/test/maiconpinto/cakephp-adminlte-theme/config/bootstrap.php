<?php
use Cake\Core\Configure;
use Cake\Core\Exception\MissingPluginException;
use Cake\Core\Plugin;

Configure::write('Theme', [
    'title' => 'Cheolis Food',
    'logo' => [
        'mini' => '<b>Cheolis Food</b>',
        'large' => '<b>Cheolis Food</b>'
    ],
    'login' => [
        'show_remember' => true,
        'show_register' => true,
        'show_social' => true
    ],
    'folder' => ROOT
]);
