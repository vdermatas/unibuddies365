<?php
$file = $theme['folder'] . DS . 'src' . DS . 'Template' . DS . 'Element' . DS . 'aside' . DS . 'sidebar-menu.ctp';

if (file_exists($file)) {
    ob_start();
    include_once $file;
    echo ob_get_clean();
} else {


?>
<ul class="sidebar-menu">
    <li class="header"></li>
   	
	<li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-dashboard')) .  __('<span>Dashboard</span>') ,array('controller' => 'users', 'action' => 'dashboard'), array('escape' => false)); ?></li>

	<?php  if($this->request->session()->read('Auth.Admin.user_type_id') == 1) { ?>
	<li class="treeview">
        <a href="#">
            <i class="fa fa-file-o "></i> <span>CMS</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
           <li>
                <a href="#"><i class="fa fa-circle-o"></i> Categories <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li>
						<?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('All Categories') ,array('controller' => 'categories', 'action' => 'index'), array('escape' => false)); ?>
					</li>
					<li>
						<?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('Add Category') ,array('controller' => 'Categories', 'action' => 'add'), array('escape' => false)); ?>
					</li>
                    
                </ul>
            </li>
			
			<li>
                <a href="#"><i class="fa fa-circle-o"></i> Pages <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
				
                    <li>
						<?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('All Pages') ,array('controller' => 'cms_pages', 'action' => 'index'), array('escape' => false)); ?>
					</li>
					
					<li>
						<?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('Add Page') ,array('controller' => 'cms_pages', 'action' => 'add'), array('escape' => false)); ?>
					</li>
                    
                </ul>
            </li>
			
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Articles <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li>
						<?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('All Articles') ,array('controller' => 'articles', 'action' => 'index'), array('escape' => false)); ?>
					</li>
					<li>
						<?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('Add Article') ,array('controller' => 'articles', 'action' => 'add'), array('escape' => false)); ?>
					</li>                    
                </ul>
            </li>
			<li>
                <a href="#"><i class="fa fa-circle-o"></i> HomePage Management <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li>
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('Slider Settings') ,array('controller' => 'homepages', 'action' => 'slider'), array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('Section Settings') ,array('controller' => 'homepages', 'action' => 'index'), array('escape' => false)); ?>
                    </li>  
                    <li>
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('Slider Sub Section ') ,array('controller' => 'homepages', 'action' => 'slider-sub-section'), array('escape' => false)); ?>
                    </li>                   
                </ul>
            </li>
			<!--<li>
                <a href="#"><i class="fa fa-circle-o"></i> Menus <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li>
						<?php //echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('All Menus') ,array('controller' => 'menus', 'action' => 'index'), array('escape' => false)); ?>
					</li>
					<li>
						<?php //echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('Add Menus') ,array('controller' => 'menus', 'action' => 'add'), array('escape' => false)); ?>
					</li>
                    
                </ul>
            </li>-->
			
        </ul>
    </li>

     <li class="treeview">
        <a href="#">
            <i class="fa fa-picture-o"></i> <span>Galleries</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .   __('All Galleries'),
                array('controller' => 'galleries', 'action' => 'index'), array('escape' => false)); ?></li>
            <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .   __('Add Gallery'),
                array('controller' => 'galleries', 'action' => 'add'), array('escape' => false)); ?></li>
        </ul>
    </li>	
	
    <li class="treeview">
        <a href="#">
            <i class="fa fa-plane"></i> <span>Product Management</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
           <li>
                <a href="#"><i class="fa fa-circle-o"></i> Products <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li>
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('All Products') ,array('controller' => 'products', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('Add Product') ,array('controller' => 'products', 'action' => 'add'), array('escape' => false)); ?>
                    </li>
                    
                </ul>
            </li>
            
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Product Categories <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                
                    <li>
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('All Categories') ,array('controller' => 'product_categories', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                    
                    <li>
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('Add Category') ,array('controller' => 'product_categories', 'action' => 'add'), array('escape' => false)); ?>
                    </li>
                    
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Product Units <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                
                    <li>
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('All Units') ,array('controller' => 'product_units', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                    
                    <li>
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('Add Unit') ,array('controller' => 'product_units', 'action' => 'add'), array('escape' => false)); ?>
                    </li>
                    
                </ul>
            </li>
            
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-user"></i> <span>Supplier Management</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
           <li>
                <a href="#"><i class="fa fa-circle-o"></i> Suppliers <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li>
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('All Suppliers') ,array('controller' => 'suppliers', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('Add Supplier') ,array('controller' => 'suppliers', 'action' => 'add'), array('escape' => false)); ?>
                    </li>
                    
                </ul>
            </li>
            
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Orders <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                
                    <li>
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('All Orders') ,array('controller' => 'orders', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                    
                    <li>
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('Add Order') ,array('controller' => 'orders', 'action' => 'add'), array('escape' => false)); ?>
                    </li>
                    
                </ul>
            </li>
            
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-cog"></i> <span>Settings</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
			<li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) . " Change Password ",
                array('controller' => 'users', 'action' => 'change_password'), array('escape' => false)); ?></li>
           
        </ul>
    </li>
    <li>
        <a href="#"><i class="fa fa-phone"></i> <span>Enquiries</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
        
            <li>
                <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('All Enquiries') ,array('controller' => 'enquiries', 'action' => 'index'), array('escape' => false)); ?>
            </li>
            
            
        </ul>
    </li>
    <li>
        <a href="#"><i class="fa fa-phone"></i> <span>Supplier Enquiries</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
        
            <li>
                <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('All Enquiries') ,array('controller' => 'enquiries', 'action' => 'supplierEnquires'), array('escape' => false)); ?>
            </li>
            
            
        </ul>
    </li>
    <li>
        <a href="#"><i class="fa fa-bolt "></i> <span>Widgets</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
        
            <li>
                <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('Widgets') ,array('controller' => 'widgets', 'action' => 'add'), array('escape' => false)); ?>
            </li>
            
        </ul>
    </li> 
   
   <li>
        <a href="#"><i class="fa fa-envelope"></i> <span>Email Templates</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
        
            <li>
                <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('All Email Templates') ,array('controller' => 'email_templates', 'action' => 'index'), array('escape' => false)); ?>
            </li>
            <li>
                <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('Add Email Templates') ,array('controller' => 'email_templates', 'action' => 'add'), array('escape' => false)); ?>
            </li>
            
            
        </ul>
    </li>
    <li>
        <a href="#"><i class="fa fa-gift"></i> <span>Coupons Management</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
        
            <li>
                <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('All Coupons') ,array('controller' => 'coupons', 'action' => 'index'), array('escape' => false)); ?>
            </li>
            <li>
                <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('Add Coupon') ,array('controller' => 'coupons', 'action' => 'add'), array('escape' => false)); ?>
            </li>
            
            
        </ul>
    </li>
    
    
    <!--  <li>
        <a href="#"><i class="fa fa-quote-left"></i> <span>Testimonials</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
        
            <li>
                <?php //echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('All Testimonials') ,array('controller' => 'testimonials', 'action' => 'index'), array('escape' => false)); ?>
            </li>
	    
	     <li>
                <?php// echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('Add Testimonial') ,array('controller' => 'testimonials', 'action' => 'add'), array('escape' => false)); ?>
            </li>
            
            
        </ul>
    </li> -->
    
     <li>
        <a href="#"><i class="fa fa-circle"></i> <span>Reports</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
        
            <li>
                <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('All Transactions') ,array('controller' => 'reports', 'action' => 'transactions'), array('escape' => false)); ?>
            </li>
	    
	     <li>
                <?php //echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .  __('All Bookings') ,array('controller' => 'reports', 'action' => 'bookings'), array('escape' => false)); ?>
            </li>
            
            
        </ul>
    </li>
    <li class="treeview">
            <a href="#">
                <i class="fa fa-shopping-cart"></i> <span>Front Orders</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .   __('All Orders'),
                    array('controller' => 'orders', 'action' => 'frontOrders'), array('escape' => false)); ?>
                </li>
                
            </ul>
    </li> 
    
    
    
    <?php }else { ?>
			<li class="treeview">
            <a href="#">
                <i class="fa fa-shopping-cart"></i> <span>Orders</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .   __('All Orders'),
                    array('controller' => 'orders', 'action' => 'index'), array('escape' => false)); ?></li>
                <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-circle-o')) .   __('All Invoice'),
                    array('controller' => 'invoices', 'action' => 'index'), array('escape' => false)); ?></li>
                
            </ul>
    </li> 
    <?php }

    /*
    <li class="treeview">
        <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Layout Options</span>
            <span class="label label-primary pull-right">4</span>
        </a>
        <ul class="treeview-menu">
            <li><a href="<?php echo $this->Url->build('/pages/layout/top-nav'); ?>"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
            <li><a href="<?php echo $this->Url->build('/pages/layout/boxed'); ?>"><i class="fa fa-circle-o"></i> Boxed</a></li>
            <li><a href="<?php echo $this->Url->build('/pages/layout/fixed'); ?>"><i class="fa fa-circle-o"></i> Fixed</a></li>
            <li><a href="<?php echo $this->Url->build('/pages/layout/collapsed-sidebar'); ?>"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
        </ul>
    </li>
    <li>
        <a href="<?php echo $this->Url->build('/pages/widgets'); ?>">
            <i class="fa fa-th"></i> <span>Widgets</span>
            <small class="label pull-right bg-green">Hot</small>
        </a>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Charts</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="<?php echo $this->Url->build('/pages/charts/chartjs'); ?>"><i class="fa fa-circle-o"></i> ChartJS</a></li>
            <li><a href="<?php echo $this->Url->build('/pages/charts/morris'); ?>"><i class="fa fa-circle-o"></i> Morris</a></li>
            <li><a href="<?php echo $this->Url->build('/pages/charts/flot'); ?>"><i class="fa fa-circle-o"></i> Flot</a></li>
            <li><a href="<?php echo $this->Url->build('/pages/charts/inline'); ?>"><i class="fa fa-circle-o"></i> Inline charts</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-laptop"></i>
            <span>UI Elements</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="<?php echo $this->Url->build('/pages/ui/general'); ?>"><i class="fa fa-circle-o"></i> General</a></li>
            <li><a href="<?php echo $this->Url->build('/pages/ui/icons'); ?>"><i class="fa fa-circle-o"></i> Icons</a></li>
            <li><a href="<?php echo $this->Url->build('/pages/ui/buttons'); ?>"><i class="fa fa-circle-o"></i> Buttons</a></li>
            <li><a href="<?php echo $this->Url->build('/pages/ui/sliders'); ?>"><i class="fa fa-circle-o"></i> Sliders</a></li>
            <li><a href="<?php echo $this->Url->build('/pages/ui/timeline'); ?>"><i class="fa fa-circle-o"></i> Timeline</a></li>
            <li><a href="<?php echo $this->Url->build('/pages/ui/modals'); ?>"><i class="fa fa-circle-o"></i> Modals</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-edit"></i> <span>Forms</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="<?php echo $this->Url->build('/pages/forms/general'); ?>"><i class="fa fa-circle-o"></i> General Elements</a></li>
            <li><a href="<?php echo $this->Url->build('/pages/forms/advanced'); ?>"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
            <li><a href="<?php echo $this->Url->build('/pages/forms/editors'); ?>"><i class="fa fa-circle-o"></i> Editors</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-table"></i> <span>Tables</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="<?php echo $this->Url->build('/pages/tables/simple'); ?>"><i class="fa fa-circle-o"></i> Simple tables</a></li>
            <li><a href="<?php echo $this->Url->build('/pages/tables/data'); ?>"><i class="fa fa-circle-o"></i> Data tables</a></li>
        </ul>
    </li>
    <li>
        <a href="<?php echo $this->Url->build('/pages/calendar'); ?>">
            <i class="fa fa-calendar"></i> <span>Calendar</span>
            <small class="label pull-right bg-red">3</small>
        </a>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-envelope"></i> <span>Mailbox</span>
            <small class="label pull-right bg-yellow">12</small>
        </a>
        <ul class="treeview-menu">
            <li><a href="<?php echo $this->Url->build('/pages/mailbox/mailbox'); ?>">Inbox <span class="label label-primary pull-right">13</span></a></li>
            <li><a href="<?php echo $this->Url->build('/pages/mailbox/compose'); ?>">Compose</a></li>
            <li><a href="<?php echo $this->Url->build('/pages/mailbox/read-mail'); ?>">Read</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-folder"></i> <span>Examples</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="<?php echo $this->Url->build('/pages/starter'); ?>"><i class="fa fa-circle-o"></i> Starter</a></li>
            <li><a href="<?php echo $this->Url->build('/pages/examples/invoice'); ?>"><i class="fa fa-circle-o"></i> Invoice</a></li>
            <li><a href="<?php echo $this->Url->build('/pages/examples/profile'); ?>"><i class="fa fa-circle-o"></i> Profile</a></li>
            <li><a href="<?php echo $this->Url->build('/pages/examples/login'); ?>"><i class="fa fa-circle-o"></i> Login</a></li>
            <li><a href="<?php echo $this->Url->build('/pages/examples/register'); ?>"><i class="fa fa-circle-o"></i> Register</a></li>
            <li><a href="<?php echo $this->Url->build('/pages/examples/lockscreen'); ?>"><i class="fa fa-circle-o"></i> Lockscreen</a></li>
            <li><a href="<?php echo $this->Url->build('/pages/examples/404'); ?>"><i class="fa fa-circle-o"></i> 404 Error</a></li>
            <li><a href="<?php echo $this->Url->build('/pages/examples/500'); ?>"><i class="fa fa-circle-o"></i> 500 Error</a></li>
            <li><a href="<?php echo $this->Url->build('/pages/examples/blank'); ?>"><i class="fa fa-circle-o"></i> Blank Page</a></li>
            <li><a href="<?php echo $this->Url->build('/pages/examples/pace'); ?>"><i class="fa fa-circle-o"></i> Pace Page</a></li>
        </ul>
    </li>
    

    <li class="treeview">
        <a href="#">
            <i class="fa fa-share"></i> <span>Multilevel</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Level One <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                    <li>
                        <a href="#"><i class="fa fa-circle-o"></i> Level Two <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                            <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
        </ul>
    </li>
    <li><a href="<?php echo $this->Url->build('/pages/documentation'); ?>"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
    <li class="header">LABELS</li>
    <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
    <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
    <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
    <li><a href="<?php echo $this->Url->build('/pages/debug'); ?>"><i class="fa fa-bug"></i> Debug</a></li>
    */
    ?>


</ul>

<?php } ?>
