<?php
$file = $theme['folder'] . DS . 'src' . DS . 'Template' . DS . 'Element' . DS . 'aside' . DS . 'user-panel.ctp';

if (file_exists($file)) {
    ob_start();
    include_once $file;
    echo ob_get_clean();
} else {
	/*
?>
<div class="user-panel">
    <div class="pull-left image">
        <?php 
			if($this->request->session()->read('Auth.Admin.user_type_id') == 2) {
				if($this->request->session()->read('Auth.Admin.profile_image') == '') {
					echo $this->Html->image('avatar.png', array('class' => 'img-circle', 'alt' => 'User Image'));
				}else {
					echo $this->Html->image('/files/uploads/'.$this->request->session()->read('Auth.Admin.profile_image'), array('class' => 'img-circle', 'alt' => 'User Image'));
				}
			}
			
			if($this->request->session()->read('Auth.Transporter.user_type_id') == 2) {
				if($this->request->session()->read('Auth.Transporter.profile_image') == '') {
					echo $this->Html->image('avatar.png', array('class' => 'img-circle', 'alt' => 'User Image'));
				}else {
					echo $this->Html->image('/files/uploads/'.$this->request->session()->read('Auth.Transporter.profile_image'), array('class' => 'img-circle', 'alt' => 'User Image'));
				}
			}
		 ?>
    </div>
	
    <div class="pull-left info">
        <p>
			<?php 
				if($this->request->session()->read('Auth.Admin.user_type_id') == 1) {
					echo $this->request->session()->read('Auth.Admin.first_name'). ' '.$this->request->session()->read('Auth.Admin.last_name'); 
				}				
				if($this->request->session()->read('Auth.Transporter.user_type_id') == 1) {
					echo $this->request->session()->read('Auth.Transporter.first_name'). ' '.$this->request->session()->read('Auth.Transporter.last_name'); 
				}				
			?>
		</p>
        <a href="#">
		<?php
			if($this->request->session()->read('Auth.Transporter.user_type_id') == 2) {
				echo $this->request->session()->read('Auth.Transporter.email');
			}
			if($this->request->session()->read('Auth.Admin.user_type_id') == 1) {
				echo $this->request->session()->read('Auth.Admin.email');
			}
		?>
		</a>
    </div>
	
</div>
<?php */ } ?>