<?php
$file = $theme['folder'] . DS . 'src' . DS . 'Template' . DS . 'Element' . DS . 'nav-top.ctp';

if (file_exists($file)) {
    ob_start();
    include_once $file;
    echo ob_get_clean();
} else {
?> 

<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </a>
    
    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <?php if($this->request->getSession()->read('Auth.Admin.user_type_id') != 1) { ?>
            <li>
                 <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="margin-top:5px;">Support</button>
            </li>
            <!-- Messages: style can be found in dropdown.less-->
           <?php } /* <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-envelope-o"></i>
                    <span class="label label-success">4</span>
                </a>
                <ul class="dropdown-menu">
                    <li class="header">You have 4 messages</li>
                    <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                            <li><!-- start message -->
                                <a href="#">
                                    <div class="pull-left">
                                        <?php echo $this->Html->image('user2-160x160.jpg', array('class' => 'img-circle', 'alt' => 'User Image')); ?>
                                    </div>
                                    <h4>
                                        Support Team
                                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                    </h4>
                                    <p>Why not buy a new awesome theme?</p>
                                </a>
                            </li>
                            <!-- end message -->
                        </ul>
                    </li>
                    <li class="footer"><a href="#">See All Messages</a></li>
                </ul>
            </li>
			
			
            <!-- Notifications: style can be found in dropdown.less -->
			<li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-bell-o"></i>
                    <span class="label label-warning">10</span>
                </a>
                <ul class="dropdown-menu">
                    <li class="header">You have 10 notifications</li>
                    <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                            <li>
                                <a href="#">
                                    <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="footer"><a href="#">View all</a></li>
                </ul>
            </li>
            <!-- Tasks: style can be found in dropdown.less -->
            <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-flag-o"></i>
                    <span class="label label-danger">9</span>
                </a>
                <ul class="dropdown-menu">
                    <li class="header">You have 9 tasks</li>
                    <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                            <li><!-- Task item -->
                                <a href="#">
                                    <h3>
                                        Design some buttons
                                        <small class="pull-right">20%</small>
                                    </h3>
                                    <div class="progress xs">
                                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                            <span class="sr-only">20% Complete</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <!-- end task item -->
                        </ul>
                    </li>
                    <li class="footer">
                        <a href="#">View all tasks</a>
                    </li>
                </ul>
            </li>
			*/?>

            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <?php 
						if($this->request->getSession()->read('Auth.Admin.user_type_id') == 1) {
							if($this->request->getSession()->read('Auth.Admin.profile_image') == '') {
								echo $this->Html->image('avatar.png', array('class' => 'user-image', 'alt' => 'User Image'));
							}else {
								echo $this->Html->image('/files/uploads/'.$this->request->getSession()->read('Auth.Admin.profile_image'), array('class' => 'user-image', 'alt' => 'User Image'));
							}
						}
						
						if($this->request->getSession()->read('Auth.Supplier.user_type_id') == 2){
							if($this->request->getSession()->read('Auth.Supplier.profile_image') == '') {
								echo $this->Html->image('avatar.png', array('class' => 'user-image', 'alt' => 'User Image')); 
							}else {
								echo $this->Html->image('/files/uploads/'.$this->request->getSession()->read('Auth.Supplier.profile_image'), array('class' => 'user-image', 'alt' => 'User Image'));
							}
						}
					?>
					
                    <span class="hidden-xs">
						<?php 
							if($this->request->getSession()->read('Auth.Admin.user_type_id') == 1){
								echo $this->request->getSession()->read('Auth.Admin.first_name'). ' '.$this->request->getSession()->read('Auth.Admin.last_name'); 
							}
							
							if($this->request->getSession()->read('Auth.Supplier.user_type_id') == 2){
								echo $this->request->getSession()->read('Auth.Supplier.first_name'). ' '.$this->request->getSession()->read('Auth.Supplier.last_name'); 
							}
						?>
					</span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                        <?php 
							if($this->request->getSession()->read('Auth.Admin.user_type_id') == 1) {
								if($this->request->getSession()->read('Auth.Admin.profile_image') == '') {
									echo $this->Html->image('avatar.png', array('class' => 'img-circle', 'alt' => 'User Image'));
								}else {
									echo $this->Html->image('/files/uploads/'.$this->request->getSession()->read('Auth.Admin.profile_image'), array('class' => 'img-circle', 'alt' => 'User Image'));
								}
							}
							
							if($this->request->getSession()->read('Auth.Supplier.user_type_id') == 2) {
								if($this->request->getSession()->read('Auth.Supplier.profile_image') == '') {
									echo $this->Html->image('avatar.png', array('class' => 'img-circle', 'alt' => 'User Image'));
								}else {
									echo $this->Html->image('/files/uploads/'.$this->request->getSession()->read('Auth.Supplier.profile_image'), array('class' => 'img-circle', 'alt' => 'User Image'));
								}
							}
							
						?>
                        <p>
                            <?php 
								if($this->request->getSession()->read('Auth.Admin.user_type_id') == 1){
									echo $this->request->getSession()->read('Auth.Admin.first_name'). ' '.$this->request->getSession()->read('Auth.Admin.last_name'); 
								}
								if($this->request->getSession()->read('Auth.Supplier.user_type_id') == 2){
									echo $this->request->getSession()->read('Auth.Supplier.first_name'). ' '.$this->request->getSession()->read('Auth.Supplier.last_name'); 
								}
								?>
                           
                            <small> 
								<?php
									if($this->request->getSession()->read('Auth.Admin.user_type_id') == 1){
										echo $this->request->getSession()->read('Auth.Admin.email'); 
									}
									if($this->request->getSession()->read('Auth.Supplier.user_type_id') == 2){
										echo $this->request->getSession()->read('Auth.Supplier.email'); 
									}
								?>
							</small>
                        </p>
                    </li>
                    <!-- Menu Body -->
                    <!-- <li class="user-body">
                        <div class="row">
                            <div class="col-xs-4 text-center">
                                <a href="#">Followers</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#">Sales</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#">Friends</a>
                            </div>
                        </div>
                        <!-- /.row -->
                   <!-- </li>-->
                    <!-- Menu Footer-->
                    
                    <li class="user-footer">
                        <div class="pull-left">
                            
							<?php 
							//if($this->request->getSession()->read('Auth.Admin.user_type_id') == 1){
								echo $this->Html->link(__('Profile') ,array('controller' => 'users', 'action' => 'profile'), array('escape' => false,'class' => 'btn btn-default btn-flat'));
							//}
							//if($this->request->getSession()->read('Auth.Transporter.user_type_id') == 2){
								//echo $this->Html->image('/files/uploads/'.$this->request->getSession()->read('Auth.Transporter.profile_image'), array('class' => 'img-circle', 'alt' => 'User Image'));
							//}
							?>
							
                        </div>
                        <div class="pull-right">
                           <?php echo $this->Html->link(__('Log Out'), ['controller' => 'Users', 'action' => 'logout'],['class' => 'btn btn-default btn-flat']);  ?>
                        </div>
                    </li>
                </ul>
            </li>
           
        </ul>
    </div>
</nav>
 
<?php } ?> 

