<?php
use Cake\Core\Configure;
use Cake\Core\Exception\MissingPluginException;
use Cake\Core\Plugin;

Configure::write('Theme', [
    'title' => 'UniBuddies365',
    'logo' => [
        'mini' => '<b>UniBuddies365</b>',
        'large' => '<b>UniBuddies365</b>'
    ],
    'login' => [
        'show_remember' => true,
        'show_register' => true,
        'show_social' => true
    ],
    'folder' => ROOT
]);
